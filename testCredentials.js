// Copyright 2017 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/**
 * Demonstrates how to authenticate to Google Cloud Platform APIs using the
 * Google Cloud Client Libraries.
 */

 'use strict';

 // Imports the Google Cloud client library.
 const { Storage } = require('@google-cloud/storage');
 const admin = require('firebase-admin');
 const fetch = require('node-fetch');
 const { google } = require('googleapis');
 
 
 const cloudresourcemanager = google.cloudresourcemanager('v1');
 // Create Google Cloud project
 // https://cloud.google.com/resource-manager/reference/rest/v1/projects/create
 async function createGCProject(props) {
 
   // BEFORE RUNNING:
   // ---------------
   // 1. If not already done, enable the Cloud Resource Manager API
   //    and check the quota for your project at
   //    https://console.developers.google.com/apis/api/cloudresourcemanager
   // 2. This sample uses Application Default Credentials for authentication.
   //    If not already done, install the gcloud CLI from
   //    https://cloud.google.com/sdk and run
   //    `gcloud beta auth application-default login`.
   //    For more information, see
   //    https://developers.google.com/identity/protocols/application-default-credentials
   // 3. Install the Node.js client library by running
   //    `npm install googleapis --save`
 
 
   const authClient = await authorize();
   
   const request = {
     resource: props,
     auth: authClient
   };
 
 
   try {
     const response = (await cloudresourcemanager.projects.create(request)).data;
     return response;
   } catch (err) {
     console.error(err);
   }
 }
 
 async function authorize() {
   const auth = new google.auth.GoogleAuth({
     scopes: ['https://www.googleapis.com/auth/cloud-platform']
   });
   return await auth.getClient();
 }
 
 
 // Instantiates a client. If you don't specify credentials when constructing
 // the client, the client library will look for credentials in the
 // environment.
 const storage = new Storage();
 // Makes an authenticated API request.
 async function checkCredentials() {
   try {
     const results = await storage.getBuckets();
     return true;    // If no errors, credentials are valid
     
   } catch (err) {
     console.error(':CREDENTIALS ERROR:', err);
   }
 }
 
 
 async function getAccessToken() {
   try {
     const token = await admin.credential.applicationDefault().getAccessToken()
     return token
   } catch (err) {
     console.error('ACCESS TOKEN ERROR:', err);
   }
 }
 
 
 /**
  * A Project will only be listed if:
  *
  * The caller has sufficient Google IAM permissions to call projects.addFirebase.
  * The Project is not already a FirebaseProject.
  * The Project is not in an Organization which has policies that prevent Firebase resources from being added.
  */
 async function listProjects() {
   const accessToken = await getAccessToken();
   const uri = 'https://firebase.googleapis.com/v1beta1/availableProjects';
   const options = {
     method: 'GET',
     headers: {
       'Authorization': 'Bearer ' + accessToken.access_token
     },
   };
 
   try {
     const rawResponse = await fetch(uri, options);
     const resp = await rawResponse.json();
     const projects = resp['projectInfo'];
 
     if (!projects || !projects.hasOwnProperty("projectInfo")) {
       return 0;
     }
 
     console.log('Project total: ' + projects.length);
     console.log('');
     for (let i in projects) {
       const project = projects[i];
       console.log('Project ' + i);
       console.log('ID: ' + project['project']);
       console.log('Display Name: ' + project['displayName']);
       console.log('');
     }
 
     return projects.length;
   } catch (err) {
     console.error(err);
   }
 }
 
 
 async function run() {
   console.clear();
 
   // =======================STEP 1 - Create Google Cloud project
   // https://cloud.google.com/resource-manager/reference/rest/v1/projects#Project
   const props = {   // TODO: Add desired properties to the request body.
     "parent": "",
     "projectId": "my-gcproject-123",
     "name": "My API Generated Project 1",
     "labels": {   // keep them lowercase
       "client":"internal",
       "environment": "dev"
     }
   }
   // const gcProject = await createGCProject(props);  // Create Google Cloud project
   // console.log(JSON.stringify(gcProject, null, 2));
   // console.log('STEP 1: Google Cloud project:', true);
 
   // return;
 
   // =======================STEP 2 - Validate Credentials
   let credentials = await checkCredentials();    // Check credentials
   console.log('STEP 2: Credentials:', credentials);
 
 
   // =======================STEP 3 - Get Access Token
   let token = await getAccessToken(); // Get access token
   // console.log("STEP 3: Access Token:", token.access_token.length > 0);
 
 
   // =======================STEP 4 - Get Available Projects
   let projects = await listProjects();
   // console.log("STEP 4: Projects:", projects);
 
   return;
 }
 
 run();