const path = require('path');
const express = require('express');
//const host = (process.env.NODE_ENV.toUpperCase().substr(0,3) == 'PRO')? '0.0.0.0' : 'localhost';
const port = (process.env.PORT || 8080);

var host="";
switch (process.env.NODE_ENV)
{
    case "qa" :
        host= "0.0.0.0"
        break;
    case  "prod_RD" :
        host= "0.0.0.0"
        break;
    case "prod" :
        host= "0.0.0.0"
        break;
    default:
        host= "0.0.0.0"
        break;
}

const app = express();
var compression = require('compression')
app.use(compression())
const publicPath = path.join(__dirname, 'dist');

console.log('publicPath >>',publicPath)
console.log('Index file >>', path.join(publicPath + '/index.html'))

var options = {
    index: "index.html"
};


app.use(express.static(publicPath, options));
//app.use(express.bodyParser({limit: '50mb'}));

/**
 * Express.js serves the same index.html for every incoming request.
 * Angular.js on the client should load respective view based on the route.
 */
app.get('*', function(req, res) {
    res.sendFile(path.join(publicPath + '/index.html'));
});


var server = app.listen(port, host, function(){
    var host = server.address().address;
    var port = server.address().port;

    console.log('FoodStore UI server is listening at http://%s:%s', host, port);
});