import { NgModule } from "@angular/core";
import { HTTPTrafficFilterPipe } from "./httpTrafficFilter.pipe";

@NgModule({
  declarations: [HTTPTrafficFilterPipe],
  imports: [],
  exports: [HTTPTrafficFilterPipe]
})
export class HTTPTrafficFilterPipeModule {

    static forRoot() {
        return {
            ngModule: HTTPTrafficFilterPipeModule,
            providers: [],
        };
     }
}