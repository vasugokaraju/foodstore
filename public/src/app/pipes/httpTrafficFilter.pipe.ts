import { Pipe, PipeTransform } from '@angular/core';
import { HTTPTrafficRecord, HTTP_REQUEST_FILTER, HTTP_REQUEST_STATUS } from '@app/helpers/app.interfaces';

/** Pipe to indicate whether the given http traffic record matches the filter type.  
 * Developed for http traffic grid in App Monitor  */
@Pipe({
  name: 'httpTrafficFilterPipe',
  pure: false
})
export class HTTPTrafficFilterPipe implements PipeTransform {

  constructor() { }

  /** Determines whether a record matches the filter type */
  transform(value: HTTPTrafficRecord, filter: number): boolean {

    /**
     * The 'filter' contains a mix up of HTTP_REQUEST_STATUS (0-2) 
     * and two added states like notification-shown (3), notification-not-shown (4) options
     * 
     * The 'value.status' represents HTTPTrafficRecord.status
     */
    if (filter < 3) {
      return (value.status === filter)
    }
    else if (filter === HTTP_REQUEST_FILTER.HIDDEN) {
      return (value.status === HTTP_REQUEST_STATUS.RECEIVED && !value.notify)
    }
    else if (filter === HTTP_REQUEST_FILTER.NOTIFICATION) {
      return (value.status === HTTP_REQUEST_STATUS.RECEIVED && value.notify)
    }
  }
}