import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WelcomeComponent } from './pages/welcome.component';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { ReportsComponent } from './components/reports/reports.component';
import { AuthComponent } from './components/auth/auth.component';
import { AuthGuard } from './services/authGuard.service';


export const routes: Routes = [
    { path: '', component: WelcomeComponent},
    { path: 'welcome', component: WelcomeComponent},
    {path: 'home', redirectTo: '/home/dashboard', pathMatch: 'full'},
    {
        path: 'home',
        component: HomeComponent,
        canActivate: [AuthGuard],
        // resolve: {homeData: HomeResolver},
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./components/home/dashboard/dashboard.module').then(m => m.DashboardModule)
            }
        ]
    },
    {path: 'auth', redirectTo: '/auth/login', pathMatch: 'full'},
    {path: 'login', redirectTo: '/auth/login', pathMatch: 'full'},
    {path: 'changePassword', redirectTo: '/auth/changePassword', pathMatch: 'full'},
    {path: 'forgotPassword', redirectTo: '/auth/forgotPassword', pathMatch: 'full'},
    {path: 'logout', redirectTo: '/auth/logout', pathMatch: 'full'},
    {path: 'register', redirectTo: '/auth/register', pathMatch: 'full'},
    {path: 'verifyRegisteredEmail', redirectTo: '/auth/verifyRegisteredEmail', pathMatch: 'full'},
    {
        path: 'auth',
        component: AuthComponent,
        children: [
            {
                path: 'login',
                loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule)
            },
            {
                path: 'changePassword',
                loadChildren: () => import('./components/auth/changePassword/changePassword.module').then(m => m.ChangePasswordModule)
            },
            {
                path: 'forgotPassword',
                loadChildren: () => import('./components/auth/forgotPassword/forgotPassword.module').then(m => m.ForgotPasswordModule)
            },
            {
                path: 'logout',
                loadChildren: () => import('./components/auth/logout/logout.module').then(m => m.LogoutModule)
            },
            {
                path: 'register',
                loadChildren: () => import('./components/auth/register/register.module').then(m => m.RegisterModule)
            },
            {
                path: 'verifyRegisteredEmail',
                loadChildren: () => import('./components/auth/verifyRegisteredEmail/verifyRegisteredEmail.module').then(m => m.VerifyRegisteredEmailModule)
            },
            {
                path: 'verifyEmailOTP',
                loadChildren: () => import('./components/auth/verifyEmailOTP/verifyEmailOTP.module').then(m => m.VerifyEmailOTPModule)
            }
        ]
    },    
    {path: 'admin', redirectTo: '/admin/users', pathMatch: 'full'},
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        // resolve: {adminData: AdminResolver},
        children: [
            {
                path: 'appmonitor',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/appMonitor/appMonitor.module').then(m => m.AppMonitorModule)
            },
            {
                path: 'users',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/users/users.module').then(m => m.UsersModule)
            },
            {
                path: 'products',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/products/products.module').then(m => m.ProductsModule)
            },
            {
                path: 'carts',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/carts/carts.module').then(m => m.CartsModule)
            },
            {
                path: 'meeting',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/meeting/meeting.module').then(m => m.MeetingModule)
            },
            {
                path: 'appcods',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/appCods/appCods.module').then(m => m.AppCodsModule)
            },
            {
                path: 'appconf',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/admin/appConf/appConf.module').then(m => m.AppConfModule)
            },

        ]
    },
    {path: 'reports', redirectTo: '/reports/summaryReport', pathMatch: 'full'},
    {
        path: 'reports',
        component: ReportsComponent,
        canActivate: [AuthGuard],
        // resolve: {reportData: ReportsResolver},
        children: [
            {
                path: 'summaryReport',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/reports/summaryReport/summaryReport.module').then(m => m.SummaryReportModule)
            },
            {
                path: 'financeReport',
                canActivate: [AuthGuard],
                loadChildren: () => import('./components/reports/financeReport/financeReport.module').then(m => m.FinanceReportModule)
            },
        ]
    },    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }