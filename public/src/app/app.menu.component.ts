import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AppComponent } from './app.component';
import { AppDataService } from './services/appData.service';
import { NotifyService } from './services/notify.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    items: MenuItem[];

    constructor(
        public app: AppComponent,
        public appData: AppDataService,
        private notify: NotifyService
        ) { }

    ngOnInit() {

        // Load main menu on page reload, if available
        if (this.appData.loggedIn) {
            try {
                const mainMenu = JSON.parse(localStorage.getItem("mainMenu"));
                this.appData.mainMenu.next(mainMenu);
            } catch (error) { }
        }

        // Subscribe for menu data
        this.appData.mainMenu.subscribe((data: MenuItem[]) => {
            this.items = data;
        })
        
        // Get user name from localStorage to show on sreen
        let user = JSON.parse( localStorage.getItem('user') );
        this.appData.userName = user?.name;
        this.appData.email = user?.email;
    }

    onMenuClick(e:any){
        this.notify.clear();
     }

}