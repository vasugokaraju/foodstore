import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/services/authGuard.service';
import { AppMonitorComponent }   from '../appMonitor/appMonitor.component';

const routes: Routes = [
    { path: '', component: AppMonitorComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: []
})
export class AppMonitorRoutingModule {

}