import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTPTrafficFilterPipeModule } from '@app/pipes/httpTrafficFilter.pipe.module';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextModule } from 'primeng/inputtext';
import { ListboxModule } from 'primeng/listbox';
import { MenuModule } from 'primeng/menu';
import { MultiSelectModule } from 'primeng/multiselect';
import { ProgressBarModule } from 'primeng/progressbar';
import { TableModule } from 'primeng/table';
import { TooltipModule } from 'primeng/tooltip';
import { AppMonitorComponent } from './appMonitor.component';
import { AppMonitorRoutingModule } from './appMonitor.routing';

@NgModule({
    imports: [
        CommonModule,
        AppMonitorRoutingModule,
        TableModule,
        CommonModule,
        ButtonModule,
        TooltipModule,
        DropdownModule,
        ProgressBarModule,
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        MultiSelectModule,
        HTTPTrafficFilterPipeModule,
        CalendarModule,
        ListboxModule,
        MenuModule
    ],
    declarations: [AppMonitorComponent],
    providers: [],
    bootstrap: [AppMonitorComponent],
    exports: []
})
export class AppMonitorModule { }