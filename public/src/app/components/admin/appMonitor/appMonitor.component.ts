import { Component, NgZone, ViewChild, Pipe, PipeTransform, OnInit } from '@angular/core';
import { AuthService } from '@app/services/auth.service';
import { HTTPService } from '@app/services/http.service';
import { SessionService } from '@app/services/session.service';
import { HTTPRequestStates, HTTPTrafficRecord, HTTP_REQUEST_FILTER, HTTP_REQUEST_STATUS } from '@app/helpers/app.interfaces';
import { HTTPTrafficFilterPipe } from '@app/pipes/httpTrafficFilter.pipe';
import { environment } from '@environments/environment';
import { Table } from 'primeng/table';
import { BehaviorSubject, from, fromEvent, interval, Observable, of, Subject } from 'rxjs';
import { filter, map, reduce, skipWhile, take, takeWhile } from 'rxjs/operators';
import { AppDataService } from '@app/services/appData.service';
import { FilterUtils } from 'primeng/utils'
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import { SelectItem } from 'primeng/api';

/**
 * This component helps to see various critical parameters in live
 */
@Component({
    templateUrl: './appMonitor.component.html'
})
export class AppMonitorComponent implements OnInit {
    /** Reference to traffic grid */
    @ViewChild("tblHTTPTraffic") tblHTTPTraffic: Table;

    
    /** To hold regular request delay selection */
    regularReqDelay: ResponseDelay = { seconds: '1' };


    /** Delay dropdown data */
    delayOptions: ResponseDelay[] = [{ seconds: '0' }, { seconds: '1' }, { seconds: '2' }, { seconds: '3' }, { seconds: '4' }, { seconds: '5' }];

    /** HTTP traffic states to populate grid filter list. */
    httpRequestStates: HTTPRequestStates[] = [
        { 'label': 'Pending', 'status': HTTP_REQUEST_FILTER.PENDING },
        { 'label': 'Received', 'status': HTTP_REQUEST_FILTER.RECEIVED },
        { 'label': 'Cancelled', 'status': HTTP_REQUEST_FILTER.CANCELLED },
        { 'label': 'Hidden', 'status': HTTP_REQUEST_FILTER.HIDDEN },
        { 'label': 'Notification', 'status': HTTP_REQUEST_FILTER.NOTIFICATION }
    ]

    /** User selected states for grid filter */
    selectedHTTPRequestStates: HTTPRequestStates[];

    txtURLFilter: string = '';
    filterFromDate: Date = new Date();
    filterToDate: Date = new Date();
    trafficTableCols: any[];

    // get activeFiltersCount(){
    //     return of(true).subscribe(data => {
    //        return (this.tblHTTPTraffic)? Object.keys(this.tblHTTPTraffic.filters).length : 0;
    //     })
    // }

    activeFiltersCount: BehaviorSubject<number> = new BehaviorSubject<number>(0)

    /** Local variable to hold inactive duration.  This variable is used in .html file. */
    inactiveDuration: number;

    /** Helps to change the progressbar color */
    remainingSessionTime: number;

    /** Elapsed % of inactivity threshold. Helps to run the session progressbar */
    elapsedSessionPercent: number;

    /** A value that helps to position the threshold icon on the progressbar */
    sessionWarningIconPosition: number = Math.round(((this.appData.inactivityThreshold - this.appData.countdownThreshold) / this.appData.inactivityThreshold) * 100)

    /** Helps to change the progressbar color */
    remainingTokenTime: number;

    /** Elapsed % of token lifespan. Helps to run the token progressbar */
    elapsedTokenPercent: number;          // Remaining % of time before token expires

    /** Elapsed time of the token. Feeds the middle label of the progressbar */
    tokenElapsedTime: number;

    /** A value that helps to position the threshold icon on the progressbar */
    tokenWarningIconPosition: number = Math.round(((this.auth.tokenLifespan - this.appData.tokenRenewThreshold) / this.auth.tokenLifespan) * 100)

    /** A flag to show hide Traffic Monitor help section */
    trafficShowMore: boolean = false;

    /** Provides export file name */
    fileName: BehaviorSubject<string> = new BehaviorSubject<string>("")
    
    /** List of download options to feed list control */
    downloadOptions: SelectItem[] = [
        { label: exportOptions.CSV, value: { format: exportOptions.CSV, icon: "pi-file-o" } },
        { label: exportOptions.EXCEL, value: { format: exportOptions.EXCEL, icon: "pi-file-excel" } },
        { label: exportOptions.PDF, value: { format: exportOptions.PDF, icon: "pi-file-pdf" } },
    ]

    /** Control switch for download options list */
    _showDownloadOptions:boolean = false;



    /** A variable to generate count value for test requests. */
    private counter: number = 0;


    /** Constructor help */
    constructor(
        public appData: AppDataService,
        public session: SessionService,
        private ngZone: NgZone,
        public auth: AuthService,
        private http: HTTPService
    ) { }


    /**
     * Initialize the observables to receive their emits
     */
    ngOnInit() {

        /** 
         * This is to hide download format options list when clicked on body. 
         * 
         * This workaround is chosen because OverlayPanel and Menu seems to have problem 
         * with positioning with controls in the table footer.
         */
        fromEvent(document,"click").subscribe(data => {
            if(this._showDownloadOptions) this._showDownloadOptions = !this._showDownloadOptions;
        })

        // Define table columns
        this.trafficTableCols = [
            { field: "index", header: "index" },
            { field: "requestId", header: "Request ID" },
            { field: "url", header: "URL" },
            { field: "status", header: "Status" },
            { field: "notify", header: "Notification" },
            { field: "method", header: "HTTP Method" },
            { field: "sentAt", header: "Sent At" },
            { field: "receivedAt", header: "Received At" }
        ]

        /**
         * 
         * @param value - Value of the column
         * @param filters User selected filter options
         */
        FilterUtils['requestStatus'] = (value: any, filters: HTTPRequestStates[]) => {
            let isMatch: boolean = false;

            if (filters === undefined || filters === null) return true;
            if (value === undefined || value === null) return false;
            let statusPipe = new HTTPTrafficFilterPipe();   // Pipe to determine whether the given record matches the status filter

            // Loop through the table datasource to find the matching records for Status column filter
            from(this.appData.httpTrafficData)
                .pipe(
                    // Get the record from table datasource based on requestId
                    filter((rec: HTTPTrafficRecord) => rec.requestId === value),
                    // Send the record and filter type through the pipe to get match status
                    filter((rec: HTTPTrafficRecord) => {
                        let found: boolean = false;
                        from(filters).pipe(filter(fltr => statusPipe.transform(rec, fltr.status)))
                            .subscribe(data => { found = true; })

                        return found;
                    })
                )
                .subscribe(data => { isMatch = true; });

            // Unless the above observable returns true, by default false.
            return isMatch;
        }


        /**
         * Custom filter that facilitates comma separated keys search.
         * @param value - request url
         * @param filters - comma separated search keys
         */
        FilterUtils['requestURL'] = (value: string, filters: any) => {

            if (filters === undefined || filters === null) return true;
            if (value === undefined || value === null) return false;

            console.log('requestURL filter: ', value, filters);

            let isMatch: boolean = false;
            let url = this.trimURL(value);

            from(<string[]>filters.split(',')).pipe(filter(key => url.indexOf(key) !== -1)).subscribe(rec => isMatch = true)

            return isMatch;
        }

        /**
         * Custom filter that facilitates date range filtering.
         * @param value - number (date)
         * @param filters - [fromDate, toDate]
         */
        FilterUtils['dateRangeFilter'] = (value: number, filters: Date[]) => {
            if (filters === undefined || filters === null) return true;
            if (value === undefined || value === null) return false;

            filters[0].setSeconds(0);   // Set the start time seconds to 0 to make it easy for the user.  Otherwise user has to select seconds also.
            filters[1].setSeconds(59);  // Set the start time seconds to 59 to make it easy for the user.  Otherwise user has to select seconds also.

            return filters[0].getTime() <= value && filters[1].getTime() >= value;
        }


        // A timer that provides user inactivity duration in seconds
        this.session.inactivityDuration$.subscribe(elapsed => {
            // Need to run ngZone as the parent observable runs outside angular
            this.ngZone.run(() => {
                this.inactiveDuration = elapsed;
                this.elapsedSessionPercent = Math.round((elapsed / this.appData.inactivityThreshold) * 100);
                this.remainingSessionTime = this.appData.inactivityThreshold - elapsed
            });
        });

        // A timer that provides token elapsed time in seconds
        this.auth.tokenElapsedTime$
            .pipe(filter(elapsed => elapsed >= 0))
            .subscribe(counter => {
                this.tokenElapsedTime = counter
                this.remainingTokenTime = (this.auth.tokenLifespan - this.tokenElapsedTime);

                this.elapsedTokenPercent = Math.round((counter / this.auth.tokenLifespan) * 100);
            })

        // An observable to receive http traffic data to populate the Traffic Monitor grid
        this.appData.httpTraffic$
            .pipe(skipWhile(data => data.requestId === ''))
            .subscribe((data: HTTPTrafficRecord) => {
                // Check whether record exists
                let recIndex = this.appData.httpTrafficData.findIndex(record => record.requestId === data.requestId);

                if (recIndex !== -1) this.appData.httpTrafficData[recIndex] = Object.assign(this.appData.httpTrafficData[recIndex], data);   // Replace fields
                else this.appData.httpTrafficData.unshift(data);    // Add record on the top of the array

                if (this.appData.httpTrafficData.length > this.appData.httpTrafficMaxRows) this.appData.httpTrafficData.pop();


                // This will rebuild the page numbers as data is added.
                if (this.tblHTTPTraffic) {

                    // Setting totalRecords is not helping in case of sort
                    // this.tblHTTPTraffic.totalRecords = this.httpTraffic.length;

                    // Records that are added after sort are not being rendered
                    // Forum suggested method is to rebuild the datasource
                    // https://github.com/primefaces/primeng/issues/298
                    this.appData.httpTrafficData = [...this.appData.httpTrafficData];       // To rebuild the grid

                    // Find a better way to apply filters on updated data.
                    if (this.tblHTTPTraffic.hasFilter()) {
                        // this.tblHTTPTraffic.value = this.httpTraffic;

                        let filters = this.tblHTTPTraffic.filters;
                        Object.keys(filters).forEach(filterField => {
                            this.tblHTTPTraffic.filter(filters[filterField].value, filterField, filters[filterField].matchMode)
                        });

                    }
                }

            })


    }

    /** Helper function to convert seconds to HH MM SS */
    seconds2HHMMSS(seconds: number) {
        if (isNaN(seconds)) return seconds;

        let date = new Date(0);
        date.setSeconds(seconds);
        return date.toISOString().substr(11, 8);
    }


    /** Sends a regular request for testing */
    sendRegularRequest() {
        this.counter++;
        const endpoint = `${environment.api.host}${environment.api.test}?delay=${this.regularReqDelay.seconds}&count=${this.counter}`;
        this.http.post(endpoint, { delay: 1, count: this.counter }).subscribe(data => data);
    }


    /** A method to trim url for better presentation */
    trimURL(url: string) {
        return url? url.split('/api')[1] : "";
    }


    getActiveFilterCount() {
        return Object.keys(this.tblHTTPTraffic.filters).length
        //  this.tblHTTPTraffic.sortMode
    }


    clearFilter() {
        console.log('clearing filters');
        
        this.selectedHTTPRequestStates = [];    // Set Status filter field to default
        this.txtURLFilter = '';                 // Set URL filter field to default
        this.tblHTTPTraffic.clear();            // Clear filters of the table

        this.activeFiltersCount.next(this.getActiveFilterCount())
    }

    clearSort() {
        this.tblHTTPTraffic.sortField = "index";
        this.tblHTTPTraffic.sortOrder = -1;

        // Records that are added after sort are not being rendered
        // Forum suggested method is to rebuild the datasource
        // https://github.com/primefaces/primeng/issues/298
        this.appData.httpTrafficData = [...this.appData.httpTrafficData];       // To rebuild the grid
    }

    clearGrid(){
        this.clearFilter();
        this.clearSort();
        this.appData.httpTrafficData=[];
    }

    get activeSortField() {
        let str = `${this.tblHTTPTraffic.sortField} ${(this.tblHTTPTraffic.sortOrder === 1)? " asc":" desc"}`
        return str;
    }
    
    
    /** Shows the download list popup */
    showDownloadOptions(e:MouseEvent) {
        this.fileName.next(`HTTPTrafficData_${new Date().getTime()}`);
        this._showDownloadOptions = !this._showDownloadOptions
        e.preventDefault();
        e.stopPropagation();
    }

    /** Calls respective download function based download selection */
    downloadData(e: any) {
        this._showDownloadOptions = !this._showDownloadOptions;

        switch (e.value.format) {
            case exportOptions.CSV:
                this.tblHTTPTraffic.exportCSV();
                break;
            case exportOptions.EXCEL:
                this.exportExcel();
                break;
            case exportOptions.PDF:
                this.exportPdf();
                break;
            default:
                break;
        }
    }


    /** Exports table data as pdf file  */
    exportPdf() {
        let exportColumns: any[]

        const doc = new jsPDF()

        exportColumns = [...this.trafficTableCols.map(data => { return { title: data.header, dataKey: data.field } })];

        autoTable(doc, {
            columns: this.trafficTableCols.map(data => { return { title: data.header, dataKey: data.field } }),
            body: [...this.appData.httpTrafficData.map((data: HTTPTrafficRecord): any => data)],
            theme: "striped"
        });

        doc.save(`${this.tblHTTPTraffic.exportFilename}.pdf`)
    }

    /** Exports table data as excel file  */
    exportExcel() {
        import("xlsx").then(xlsx => {
            let data = this.tblHTTPTraffic.value.map( (d:HTTPTrafficRecord) => {
                // convert epoch value to ISO date before exporting data
                return Object.assign(d,{sentAt:new Date(d.sentAt).toISOString(), receivedAt: new Date(d.sentAt).toISOString()});
            })
            const worksheet = xlsx.utils.json_to_sheet(data);
            const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
            const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
            this.saveAsExcelFile(excelBuffer, "products");
        });
    }

    saveAsExcelFile(buffer: any, fileName: string): void {
        import("file-saver").then(FileSaver => {
            let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
            let EXCEL_EXTENSION = '.xlsx';
            const data: Blob = new Blob([buffer], {
                type: EXCEL_TYPE
            });
            FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        });
    }


    /** Formats data before exporting as csv */
    formatExportData(e: any): string {


        switch (e.field) {
            case "sentAt":
            case "receivedAt":
                return new Date(e.data).toISOString()
            default:
                return e.data;
        }
    }

    stringify(json: any) {
        return JSON.stringify(json, null, 2)
    }


}

/** Interface for response delay dropdown data */
interface ResponseDelay {
    seconds: string;
}

enum exportOptions {
    CSV = "CSV",
    EXCEL = "EXCEL",
    PDF = "PDF"
}