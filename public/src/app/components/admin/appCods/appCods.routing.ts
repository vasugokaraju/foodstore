import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppCodsComponent }   from '../../admin/appCods/appCods.component';
import { AppCodsResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: AppCodsComponent, resolve: {appCodsData:AppCodsResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [AppCodsResolver]
})
export class AppCodsRoutingModule {

}