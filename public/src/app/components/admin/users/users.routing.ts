import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersComponent }   from '../../admin/users/users.component';
import { UsersResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: UsersComponent, resolve: {usersData:UsersResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [UsersResolver]
})
export class UsersRoutingModule {

}