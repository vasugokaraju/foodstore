import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdmintoolbarModule } from '../admintoolbar/admintoolbar.module';
import { BulkUploadModule } from '@app/components/common/bulkUpload/bulkUpload.module';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { UsersRoutingModule } from './users.routing';
import { UsersComponent } from './users.component';


@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule,
        TableModule,
        PanelModule,
        ButtonModule,
        AdmintoolbarModule,
        BulkUploadModule,
        InputTextModule,
        FormsModule,
        ReactiveFormsModule,
        CalendarModule,
        InputNumberModule,
        InputSwitchModule,
        AutoCompleteModule,
        DropdownModule,
        SplitButtonModule
    ],
    declarations: [UsersComponent],
    providers: [],
    bootstrap: [UsersComponent]
})
export class UsersModule { }