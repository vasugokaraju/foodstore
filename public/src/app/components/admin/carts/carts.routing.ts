import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CartsComponent }   from '../../admin/carts/carts.component';
import { CartsResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: CartsComponent, resolve: {cartsData:CartsResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [CartsResolver]
})
export class CartsRoutingModule {

}