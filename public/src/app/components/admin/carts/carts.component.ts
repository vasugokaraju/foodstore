import { Component, ElementRef, ViewChild, OnInit, AfterViewInit, ViewChildren, QueryList, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Table } from 'primeng/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdmintoolbarComponent, ADMIN_TOOLBAR_COMMANDS } from '@app/components/admin/admintoolbar/admintoolbar.component';
import { ValidationService } from '@app/services/validation.service'
import { NotifyService } from 'src/app/services/notify.service';
import { Helper } from '@app/helpers';
import { saveAs } from 'file-saver';
import { CartsService } from '@app/services/carts.service'
import { UsersService } from '@app/services/users.service'
import { ProductsService } from '@app/services/products.service'
import { ICarts } from '@app/interfaces/ICarts';
import { EXPORT_OPTIONS, ROLES } from '@app/interfaces/IAppInterfaces';
import { distinctUntilChanged, map, take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Subject } from 'rxjs/internal/Subject';
import { Subscription } from 'rxjs/internal/Subscription';
import { IWhereQuery } from '@app/helpers/app.interfaces';
import { environment } from '@environments/environment';
import { AppDataService } from '@app/services/appData.service';


@Component({
    styleUrls: ['./carts.component.css'],
    templateUrl: './carts.component.html',
    encapsulation: ViewEncapsulation.None
})
export class CartsComponent implements OnInit, AfterViewInit {
     cartsData:any;
    _selectedMenuId:string = ADMIN_TOOLBAR_COMMANDS.LIST;  // Holds the id of the selected menu
    submitLabel:string = "Submit";

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    submitted:boolean = false;

    formGroup: FormGroup;       // form group control

    loading: boolean = true;
    @ViewChild('tblCarts') tblCarts: Table;
    @ViewChildren('filterElements') filterElements: QueryList<ElementRef>;

    initializationValue:ICarts = {
  "id": "string",
  "customerId": "",
  "customerName": "",
  "productId": "",
  "productName": ""
};
    selectedRecord:ICarts = this.helper.deepClone<any>(this.initializationValue);

    // To hold data for list view
    cartsListViewData:BehaviorSubject<ICarts[]> = new BehaviorSubject<ICarts[]>([this.selectedRecord]);
    // To hold data for fromGroup (add/edit/delete) view.
    cartsEditViewData:BehaviorSubject<ICarts> = new BehaviorSubject<ICarts>(this.selectedRecord);

    public keyUp = new Subject<KeyboardEvent>();
    
    private subscription: Subscription;

    private isFirstLazyLoad:boolean = true;

    totalRows:number = 0;
    rowsPerPage:number = 5;
    filter: any = {filters:{}, sortField:undefined, sortOrder:undefined};

    _filterVisible:boolean = true;    // This variable is used by the clearFilter() to re-render the components after resetting thieir selected values
    _activeFiltersCount:number= 0;
    _activeSortCount:number= 0;
    
    _queryInProgress:boolean = false;
    _isNewSort:boolean = false;
    _readOnly: boolean = false;

    fileNamePrefix:string = "Carts";

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        public adminToolbar: AdmintoolbarComponent,
        private notify: NotifyService,
        private helper: Helper,
        private cartsService: CartsService,
        public appDataService: AppDataService,
        private usersService: UsersService,
        private productsService: ProductsService,
    ) {
        this.subscription = this.keyUp.pipe(
            distinctUntilChanged(),
        ).subscribe(($event: any) => this.filterData($event));
    }

    ngOnInit() {
        // Preload data
        this.route.data
            .pipe(
                take(1),
                map((record: any) => {
                    let formatted: any = {"listViewData":[]};

                    if (record.cartsData) {     // Transform the incoming object to fit the page needs
                        if (record.cartsData.cartsData) {
                            formatted.listViewData = record.cartsData.cartsData;
                        }

                    }
                    return formatted;
                })
            )
            .subscribe((resp: any) => {
                this.cartsListViewData.next(resp.listViewData);
                this.totalRows = this.cartsListViewData.value.length;
            });


        // Initialize the form and its controls
        this.formGroup = this.formBuilder.group({

            customerName: [
				{ value: this.cartsEditViewData.value.customerName, disabled: false },
			],
                
            productName: [
				{ value: this.cartsEditViewData.value.productName, disabled: false },
			],
                

        })
    }

    ngAfterViewInit(): void {
        // Add validation tooltip layer to fromgroup.
        ValidationService.bindFormControls(this.formGroup);
    }


    clearFilter(){

        // Clear all text input fields
        this.filterElements.forEach(item => {
            this.helper.resetElementValue(item)
        })

        setTimeout(() => this._filterVisible = false);
        setTimeout(() => this._filterVisible = true);

        this.filter.filters={};
        this.filterData(this.filter);
        // this.activeFiltersCount.next(this.getActiveFilterCount())
    }



    onAdminMenuClick(e:any){

        if(!e.target) return;

        // If target ID is not available, it could be one of the filter buttons.  Look for the parent button id
        const id = (e.target.id === "") ? e.target.closest("button").id : e.target.id;

        if (!id || id === "") return;

        switch(id){
            case ADMIN_TOOLBAR_COMMANDS.CLEAR_FILTER:
                this.clearFilter();
                return;
        }

        this._selectedMenuId = e.target.id;

        switch(this._selectedMenuId){

            case ADMIN_TOOLBAR_COMMANDS.NEW:
                this.submitLabel = "Create";
                this.selectedRecord = this.helper.deepClone<any>(this.initializationValue);
                this.cartsEditViewData.next(this.selectedRecord);

                // Reset form's state, otherwise the form performs validation
                // thus highliting all mandatory fields when creating new record second time in row.
                this.formGroup.reset(this.selectedRecord);

                (!this.showActionButton('create'))? this.formGroup.disable() : this.formGroup.enable();

                break;
        }
                
        setTimeout(() => {
            ValidationService.bindFormControls(this.formGroup);
        }, 1000);
    }

    async onSubmit() {
        
        // If the form is not valid, do not submit.
        if (this.formGroup.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.",false,null,10000);
            return;
        }

        if (this._selectedMenuId == ADMIN_TOOLBAR_COMMANDS.NEW) {
            let data:ICarts = this.helper.formToJson(this.formGroup);  // Build data out of formGroup
            data = this.helper.dateToString(data, []); // Convert date object to ISO date string.

            // code-name to data record converstion for the Users autocomplete field
            this.helper.codeName2RecordMapping(data.customerName, data, "customerId", "customerName");
            // code-name to data record converstion for the Products autocomplete field
            this.helper.codeName2RecordMapping(data.productName, data, "productId", "productName");

            try {
                const response:any = await this.cartsService.create(data);

                data.id = response.data.id; // Add new record id
                this.cartsListViewData.value.unshift(data); // Place new record at the top of the list

                this.close();  // Close the New window.
            } catch (error) {
                this.submitted = false;
                /** Error messages are handled in HTTPInterceptor service (http.interceptor.ts)*/
                // this.notify.showError(error.title, error.message, false, null, 10000);
            }

        }
        else if (this._selectedMenuId == ADMIN_TOOLBAR_COMMANDS.UPDATE) {
            let data:ICarts = this.helper.formToJson(this.formGroup);  // Build data out of formGroup
            data = this.helper.dateToString(data, []); // Convert date object to ISO date string.

            // code-name to data record converstion for the Users autocomplete field
            this.helper.codeName2RecordMapping(data.customerName, data, "customerId", "customerName");
            // code-name to data record converstion for the Products autocomplete field
            this.helper.codeName2RecordMapping(data.productName, data, "productId", "productName");

            // this.cartsEditViewData.value contains original record id value which is needed to update record.
            data = Object.assign({}, this.cartsEditViewData.value, data);

            try {
                await this.cartsService.update(data);

                // Get the record index from this.cartsListViewData array to update it.
                let recIndex = this.cartsListViewData.value.findIndex(item => item.id === this.cartsEditViewData.value.id);
                // Update changes in this.cartsListViewData
                this.cartsListViewData.value[recIndex] = data;

                this.close();  // Close the Edit window.

                // This is needed to clear the form and make it ready for next edit
                this.formGroup.reset( this.helper.deepClone(this.initializationValue) );

            } catch (error) {
                this.submitted = false;
                /** Error messages are handled in HTTPInterceptor service (http.interceptor.ts)*/
                // this.notify.showError(error.title, error.message, false, null, 10000);
            }

        }
        else if (this._selectedMenuId == ADMIN_TOOLBAR_COMMANDS.DELETE) {
            let data = this.cartsEditViewData.value;

            try {
                const response: any = await this.cartsService.delete(data);

                if(response.status === 'success'){
                    
                    const filteredData  = this.cartsListViewData.value.filter( item => {
                        return item.id != data.id
                    });

                    this.cartsListViewData.next(filteredData);

                    this.close();  // Close the Delete window.

                    // This is needed to clear the form and make it ready for next delete
                    this.formGroup.reset( this.helper.deepClone(this.initializationValue) );
                }

            } catch (error) {
                this.submitted = false;
                /** Error messages are handled in HTTPInterceptor service (http.interceptor.ts)*/
                // this.notify.showError(error.title, error.message, false, null, 10000);
            }

        }

    }


    // TODO:  Remove index as it is not useful when data is sorted.  Pass entire record from HTML and assign that to this.selectedRecord
    async editRecord(record: ICarts){
        this._selectedMenuId = ADMIN_TOOLBAR_COMMANDS.UPDATE;
        this.submitLabel = "Update";

        this._readOnly = !this.showEdit(record);

        // Get complete record of the carts
        let cartsRecord = await this.cartsService.readOne(record.id);
        this.cartsEditViewData.next(cartsRecord[0]);

        // disable form if update action is false
        (!this.showActionButton('update'))? this.formGroup.disable() : this.formGroup.enable();

        setTimeout(() => {
            ValidationService.bindFormControls(this.formGroup);
        }, 0);
    }


    async deleteRecord(record: ICarts){
        this._selectedMenuId = ADMIN_TOOLBAR_COMMANDS.DELETE;
        this.submitLabel = "Delete";

        // Get complete record of the carts
        let cartsRecord = await this.cartsService.readOne(record.id);
        this.cartsEditViewData.next(cartsRecord[0]);

        setTimeout(() => {
            ValidationService.bindFormControls(this.formGroup);
        }, 0);
    }

    async downloadData(e: MouseEvent, fileType: string){
        
        // Set download filename
        const fileName = `Carts_${new Date().getTime()}`;

        switch (fileType.toUpperCase()) {
            case EXPORT_OPTIONS.JSON:

                const content = new Blob([JSON.stringify(this.cartsListViewData.value, null, 2)], { type: 'text/csv;charset=utf-8;' })
                saveAs(content,fileName + ".json")
                break;
            case EXPORT_OPTIONS.EXCEL:
                //this.exportExcel();
                break;
            case EXPORT_OPTIONS.PDF:
                //this.exportPdf();
                break;
            default:
                break;
        }

/*
        try {
            const response: any = await this.cartsService.download({dataFilter:{}, fields:"customerId,customerName,productId,productName", fileType:fileType});
            if(response.status === 'success'){
                const file = new File([response.data], `cartsData_${new Date().toISOString()}.csv`, {type: "text/plain;charset=utf-8"});
                saveAs(file);
            }
        } catch (error) {
            this.submitted = false;
            //Error messages are handled in HTTPInterceptor service (http.interceptor.ts)

            this.notify.showError(error.title, error.message, false, null, 10000);
        }
*/

    }

    close(){
        this._selectedMenuId = ADMIN_TOOLBAR_COMMANDS.LIST
    }
    
    onListRowSelect(e:any){
    }

    onListRowUnselect(e:any){
    }


    // When pagination changes
    onPageChange(e:any){
        this.rowsPerPage = e.rows;
    }

    // This custom sort gets triggered when sort is changed.
    onSortChange(value: any) {
        // The intention of this function function is to trigger when user clicks on the column heading to sort data.
        // But when sort is active on any column and data set us changed/updated, 
        // it is triggering this function automatically causing infinite loop of retrieval and sort
        // To avoid sunc situation, using a flag to break the loop
        if(this._queryInProgress) return;

        this.filter.sortField = value.field;
        this.filter.sortOrder = value.order;
        this._isNewSort = true;

        this.filterData(this.filter);
    }

    // Check for more data
    more() {
        this.filterData(this.filter, 1);
    }

// TODO:  In "Meeting Admin" page, when filtered on clStts field with 0 as value, error occurs in client
    // This custom function gets triggered when user presses enter key in filter input controls.
    onFilterChange(value: any, field: string, operator: string) {
        // Rebuild filters
        this.filter.filters = [];
        this.filterElements.forEach(item => {
            if(item.nativeElement.value){
                const field = item.nativeElement.getAttribute("data-field-name");
                const operator = item.nativeElement.getAttribute("data-where-operator");
                const value = item.nativeElement.value;
                this.filter.filters[field]= {value:value, matchMode:operator};
            }
        })

        this.filterData(this.filter);
    }


    async filterData(filter: any, direction:number=0) {

        // Remove the collection name from field path as it is not needed
        let orderByField: any = filter.sortField ? filter.sortField.match("^([^.]*).(.*)")[2] : filter.sortField

        // Make sure that there is at least one filter.  This is enforced to limit database hits.
        if( Object.keys(filter.filters).length == 0){
            this.notify.showError("No Filters Found", "Use at least one filter to retrieve more records.");
            return;
        }

        let where = this.helper.filter2Where(filter.filters);

        // If no filters are available, retrieve data as it was when page is loaded.
        if (where.length == 0) {
            // Do something here to retrieve the default data set
        }


        // Make it as sartsWith() query.
        where = this.helper.firebaseStartsWith(where);
        

        const whereQuery: IWhereQuery = {
            where: where,
            orderBy: orderByField ? [[orderByField, (filter.sortOrder === 1) ? "asc" : "desc"]] : ['customerId','asc'],
            limit: environment.maxRowsToPull,
            paginationRec: (!this._isNewSort && this.cartsListViewData.value.length > 0)? this.cartsListViewData.value[this.cartsListViewData.value.length-1] : {},
            direction: direction
        }


        // If where criteria contains range operators (>=, <=), make sure that field is positioned top in order by array
        // If there is range operator in where query, it would be at the top of the query thus check the first element
        if(whereQuery.where[0][1] == ">="){
            // Check if the field that is being inserted does not exist already
            const enforcedSortField = whereQuery.where[0][0];
            if(!whereQuery.orderBy[0].includes(enforcedSortField)) whereQuery.orderBy.unshift([whereQuery.where[0][0], "asc"]);
        }


        // Get the filter count to show on clear filter button
        this._activeFiltersCount = Object.keys(filter.filters).length
        this._queryInProgress = true;
        this._isNewSort = false;

        // Send find request to server
        try{

            // console.log(whereQuery, JSON.stringify(whereQuery,null,2));

            const response = await this.cartsService.readMany({whereQuery:whereQuery});

            if(direction !== 1) this.cartsListViewData.next(response.data);  // Replace existing data as it could be new filter
            else this.cartsListViewData.next([...this.cartsListViewData.value, ...response.data]);   // Append data as it is more... data query
            this.totalRows = this.cartsListViewData.value.length;

            // This flag is needed to control the infinite loop of data retrieval when Sort is active
            setTimeout(() => this._queryInProgress = false);
        }
        catch(error){
            this.notify.showError(error.title, error.message ,false,null,10000);
        }

    }


    showEdit(rec:any){

        // Implement data entitlement logic here
        // If not admin, disable edit
        return (parseFloat(this.appDataService.userProfile.role.code) > ROLES.SUPERADMIN)? false:true;
    }


    showDelete(rec:any){
            // Implement logic as needed
            // If not admin, disable delete
            return (parseFloat(this.appDataService.userProfile.role.code) > ROLES.SUPERADMIN)? false:true;
    }

    // Determines whether an action button to be shown
    showActionButton(action:string){
        // Show all for superadmin
        if(this.appDataService.userRole.code == ROLES.SUPERADMIN) return true;

        // Check whether this action button can be shown
        const show = this.helper.showActionButton(
            this.route.snapshot['_routerState'].url, 
            this.appDataService.userRole.name.toLowerCase(), 
            action.toLowerCase());

        return show;
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }    


    /**
    * Get Carts data in the form of code-name for the purpose of Autocomplete controls on UI.
    */
    usersIdDisplayNameData:Array<any> = [];
    async getUsersIdDisplayName(e:any) {

        const where: any = { 
            where: [
                ["displayName", ">=", e.query],
                ["displayName", "<", this.helper.incrementString(e.query)]], 
            orderBy: ["displayName", "asc"], 
            limit: 5 
        };

        const response = await this.usersService.getUsersIdDisplayName({autoFilterQuery:where});
        this.usersIdDisplayNameData = response.data;
    }
    /**
    * Get Carts data in the form of code-name for the purpose of Autocomplete controls on UI.
    */
    productsIdNameData:Array<any> = [];
    async getProductsIdName(e:any) {

        const where: any = { 
            where: [
                ["name", ">=", e.query],
                ["name", "<", this.helper.incrementString(e.query)]], 
            orderBy: ["name", "asc"], 
            limit: 5 
        };

        const response = await this.productsService.getProductsIdName({autoFilterQuery:where});
        this.productsIdNameData = response.data;
    }
    async onUploadSubmitClick(data:Array<any>){
        try {
            const uploadStatus = await this.cartsService.upload({bulkData:data});
            return;
        } catch (error) {
            return Promise.reject(error);
        }
    }

}