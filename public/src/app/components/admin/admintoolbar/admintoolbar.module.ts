import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdmintoolbarComponent } from './admintoolbar.component';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';
import { MenubarModule } from 'primeng/menubar';


@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        FileUploadModule,
        MenubarModule
    ],
    declarations: [AdmintoolbarComponent],
    providers: [],
    exports:[CommonModule, AdmintoolbarComponent],    // Export the component so that it can be used as child
    bootstrap: [AdmintoolbarComponent]
})
export class AdmintoolbarModule { }