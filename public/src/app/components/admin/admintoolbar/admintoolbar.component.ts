import { Component, EventEmitter, Injectable, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helper } from '@app/helpers';
import { AppDataService } from '@app/services/appData.service';
import { MenuItem } from 'primeng/api';
import { Menubar } from 'primeng/menubar';

@Injectable({ providedIn: "root" }) // allows this child component injectable in parent constructor

@Component({
  selector: 'app-admintoolbar',
  templateUrl: './admintoolbar.component.html',
  styleUrls: ['./admintoolbar.component.css']
})
export class AdmintoolbarComponent implements OnInit {

  @ViewChildren('adminToolbar') adminToolbar: Menubar;

  @Output() onAdminMenuClick:EventEmitter<any> = new EventEmitter();  // Sends communication to Parent component
  

  constructor(
    private helper: Helper,
    private appDataService: AppDataService,
    private route: ActivatedRoute
  ) { }

  items: MenuItem[] = [
    {
      id:"read",
      label: 'List',
      icon: 'pi pi-fw pi-table',
      styleClass:'adminToolbarMenuSelected'
    },
    {
      id:"create",
      label: 'New',
      icon: 'pi pi-fw pi-plus',
      styleClass:'adminToolbarMenuUnselected'
    },
    {
      id:"upload",
      label: 'Upload',
      icon: 'pi pi-upload',
      styleClass:'adminToolbarMenuUnselected'
    },
    {
      id:"download",
      label: 'Download',
      icon: 'pi pi-download',
      styleClass:'adminToolbarMenuUnselected'
    }
  ];
  @Input() selectedMenuId:string = ADMIN_TOOLBAR_COMMANDS.LIST;  // Holds the id of the selected menu
  @Input() activeFiltersCount:number = 0;  // Holds count of active filters
  @Input() activeSortCount:number = 0;  // Holds count of active filters

  isNewRecord:boolean = true;

  /** Makes ADMIN_TOOLBAR_COMMANDS enum available in html */
  get AdminToolbarCommands(): typeof ADMIN_TOOLBAR_COMMANDS{
    return ADMIN_TOOLBAR_COMMANDS;
  }

  ngOnInit(): void {
    
  }

  ngOnChanges(): void{
    if(this.selectedMenuId === ADMIN_TOOLBAR_COMMANDS.UPDATE || this.selectedMenuId === ADMIN_TOOLBAR_COMMANDS.LIST) {
      this.underlineSelectedMenu(this.selectedMenuId);
      
      // Filter out the admin menu items based on the user role permissions
      const _menuItems = this.items.filter( menuItem => {
        return this.helper.showActionButton( this.route.snapshot['_routerState'].url, this.appDataService.userRole.name, menuItem.id )
      });

      // This is to force the menu component re-render as a workaround for underline change not reflecting
      if(this.items) this.items = [..._menuItems];
    }
  }

  onAdminMenuClicked(e: any) {

    if (!e.target) return;
    let id = "";

    try {
      // If target ID is not available, it could be one of the filter buttons.  Look for the parent button id
      id = (e.target.id === "") ? e.target.closest("button").id : e.target.id;
    } catch (error) {
      return;
    }

    if (!id || id === "") return;

    if (id !== ADMIN_TOOLBAR_COMMANDS.CLEAR_SORT && id !== ADMIN_TOOLBAR_COMMANDS.CLEAR_FILTER) {
      this.selectedMenuId = id;
      this.underlineSelectedMenu(this.selectedMenuId);
    }

    this.onAdminMenuClick.emit(e);
  }

  underlineSelectedMenu(menuId:string){
    if(!this.items) return;
    
    this.items.forEach(item => {
      item.styleClass = 'adminToolbarMenuUnselected'
      if(item.id == menuId) {
        item.styleClass = 'adminToolbarMenuSelected';
      }
    });

  }

}

export enum ADMIN_TOOLBAR_COMMANDS {
  LIST = 'read',
  NEW = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
  UPLOAD = 'upload',
  DOWNLOAD = 'download',
  CLEAR_FILTER = 'clearFilter',
  CLEAR_SORT = 'clearSort'
}