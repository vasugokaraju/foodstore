import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsComponent }   from '../../admin/products/products.component';
import { ProductsResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: ProductsComponent, resolve: {productsData:ProductsResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [ProductsResolver]
})
export class ProductsRoutingModule {

}