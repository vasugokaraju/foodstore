import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdmintoolbarModule } from '../admintoolbar/admintoolbar.module';
import { BulkUploadModule } from '@app/components/common/bulkUpload/bulkUpload.module';
import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { MeetingRoutingModule } from './meeting.routing';
import { MeetingComponent } from './meeting.component';


@NgModule({
    imports: [
        CommonModule,
        MeetingRoutingModule,
        TableModule,
        PanelModule,
        ButtonModule,
        AdmintoolbarModule,
        BulkUploadModule,
        InputTextModule,
        FormsModule,
        ReactiveFormsModule,
        CalendarModule,
        InputNumberModule,
        InputSwitchModule,
        AutoCompleteModule,
        DropdownModule,
        SplitButtonModule
    ],
    declarations: [MeetingComponent],
    providers: [],
    bootstrap: [MeetingComponent]
})
export class MeetingModule { }