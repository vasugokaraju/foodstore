import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MeetingComponent }   from '../../admin/meeting/meeting.component';
import { MeetingResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: MeetingComponent, resolve: {meetingData:MeetingResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [MeetingResolver]
})
export class MeetingRoutingModule {

}