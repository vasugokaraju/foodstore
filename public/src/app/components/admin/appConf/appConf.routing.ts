import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppConfComponent }   from '../../admin/appConf/appConf.component';
import { AppConfResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: AppConfComponent, resolve: {appConfData:AppConfResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [AppConfResolver]
})
export class AppConfRoutingModule {

}