import { Component, OnInit, Input, ViewEncapsulation, AfterViewInit, AfterViewChecked} from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';
import { AppDataService } from '../../services/appData.service'; // Use this service to share data among components.

@Component({
    selector: 'my-admin',
    styleUrls: [ './admin.component.css' ],
    templateUrl: './admin.component.html'
})

export class AdminComponent implements OnInit, AfterViewInit, AfterViewChecked{
    items: MenuItem[];
    
    constructor(
        private appData: AppDataService
        ){};

    ngOnInit(): void {

        // Subscribe for menu data
        this.appData.mainMenu.subscribe((data: MenuItem[]) => {
            this.items = data.filter( menuItem => menuItem.id === "ADMINISTRATION" )
        })
    }

    ngAfterViewInit() {

    }

    ngAfterViewChecked(){
    }

}