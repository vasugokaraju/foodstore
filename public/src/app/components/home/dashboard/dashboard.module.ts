import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { ButtonModule } from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';

@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        ButtonModule,
        FormsModule,
        DropdownModule,
        CardModule
    ],
    declarations: [DashboardComponent],
    providers: [],
    bootstrap: [DashboardComponent]
})
export class DashboardModule { }