import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from '../../home/dashboard/dashboard.component';
import { DashboardResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: DashboardComponent, resolve: {dashData:DashboardResolver}},
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [DashboardResolver]
})
export class DashboardRoutingModule {

}