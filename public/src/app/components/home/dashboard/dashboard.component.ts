import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/services/auth.service';
import { AppDataService } from '../../../services/appData.service'

interface Book {
    name: string;
    author: string;
}

@Component({
    selector: 'dashboard',
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent {
    dashData: any;
    selectedBook: Book;


    constructor(
        private appDataService: AppDataService,
        private route: ActivatedRoute,
        private auth: AuthService
    ) {

    }

    ngOnInit() {
        // Preload data
        this.route.data.subscribe((resp: any) => {
            this.dashData = resp.dashData;
        });
    }

    
    counter:number=1;

    test(){
        this.auth.test(this.counter++).subscribe( data => data);
    }
}