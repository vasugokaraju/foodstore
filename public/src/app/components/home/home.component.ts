import { Component, OnInit, Input, ViewEncapsulation, AfterViewInit, AfterViewChecked} from '@angular/core';
import { AppDataService } from '../../services/appData.service'; // Use this service to share data among components.

@Component({
    selector: 'my-home',
    styleUrls: [ './home.component.css' ],
    templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit, AfterViewInit, AfterViewChecked{

    constructor(
        private appData: AppDataService
        ){};

    ngOnInit(): void {

    }

    ngAfterViewInit() {

    }

    ngAfterViewChecked(){
    }

}