import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '@app/services/auth.service';
import firebase from "firebase/app";
import "firebase/auth";


@Component({
    selector: 'app-federated-login',
    templateUrl: './federatedLogin.component.html'
})
export class FederatedLoginComponent implements OnInit {

    @Input() firebaseConfig: any = {}

    constructor(
        private auth:AuthService
    ) {}

    ngOnInit() {}

    
    // Sign-in with google
    signInWithGoogle(){

        if (!firebase.apps.length) {
            firebase.initializeApp(this.firebaseConfig);
        } else {
            firebase.app(); // if already initialized, use that one
        }

        let provider = new firebase.auth.GoogleAuthProvider();
        
        firebase.auth().signInWithPopup(provider)
            .then( async (result) => {

                let credential:firebase.auth.OAuthCredential = result.credential;

                // This gives you a Google Access Token. You can use it to access the Google API.
                let token = credential.accessToken;

                // The signed-in user info.
                let user = result.user;

                const userInfo = {
                    displayName: user.displayName,
                    email: user.email,
                    emailVerified: user.emailVerified,
                    phoneNumber: user.phoneNumber,
                    photoURL: user.photoURL,
                    id: user.uid,
                    token: await user.getIdToken(true),
                    refreshToken: user.refreshToken,
                    providerData: user.providerData
                }

                const response = await this.auth.federatedLogin({credential:credential, user:userInfo});

                this.auth.onUserLogin(response);
                
                return;
            })
    }


    // Sign-in with facebook
    signInWithFacebook(){

        if (!firebase.apps.length) {
            firebase.initializeApp(this.firebaseConfig);
        } else {
            firebase.app(); // if already initialized, use that one
        }

        let provider = new firebase.auth.FacebookAuthProvider();
        
        firebase.auth().signInWithPopup(provider)
            .then( async (result) => {

                let credential:firebase.auth.OAuthCredential = result.credential;

                // This gives you a Google Access Token. You can use it to access the Google API.
                let token = credential.accessToken;

                // The signed-in user info.
                let user = result.user;

                const userInfo = {
                    displayName: user.displayName,
                    email: user.email,
                    emailVerified: user.emailVerified,
                    phoneNumber: user.phoneNumber,
                    photoURL: user.photoURL,
                    id: user.uid,
                    token: await user.getIdToken(true),
                    refreshToken: user.refreshToken,
                    providerData: user.providerData
                }

                const response = await this.auth.federatedLogin({credential:credential, user:userInfo});

                this.auth.onUserLogin(response);
                
                return;
            })
    }
}