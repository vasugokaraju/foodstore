import { Component, OnInit, Input, ViewEncapsulation, AfterViewInit, AfterViewChecked} from '@angular/core';
import { AppDataService } from '../../services/appData.service'; // Use this service to share data among components.

@Component({
    selector: 'my-auth',
    styleUrls: [ './auth.component.css' ],
    templateUrl: './auth.component.html'
})

export class AuthComponent implements OnInit, AfterViewInit, AfterViewChecked{

    constructor(
        private appData: AppDataService
        ){};

    ngOnInit(): void {

    }

    ngAfterViewInit() {

    }

    ngAfterViewChecked(){
    }

}