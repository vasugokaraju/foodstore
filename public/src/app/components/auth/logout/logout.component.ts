import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AuthService } from '@app/services/auth.service';


@Component({
    templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit, AfterViewInit {

    constructor(
        private auth: AuthService,
    ) {

        this.auth.logout();
        localStorage.clear();
    }

    ngOnInit() {

    }

    public ngAfterViewInit(): void {

    }

}