import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoutComponent } from './logout.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { RouterModule } from '@angular/router';
import { LogoutRoutingModule } from './logout.routing';

@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        FormsModule, 
        ReactiveFormsModule,
        CardModule,
        RouterModule,
        LogoutRoutingModule
    ],
    declarations: [LogoutComponent],
    providers: [],
    bootstrap: [LogoutComponent]
})
export class LogoutModule { }