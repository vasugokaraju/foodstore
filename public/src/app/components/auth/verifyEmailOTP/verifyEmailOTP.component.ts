import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'src/app/services/notify.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/services/auth.service';
import { AppDataService } from '@app/services/appData.service';


@Component({
    templateUrl: './verifyEmailOTP.component.html'
})
export class VerifyEmailOTPComponent implements OnInit {

    statusMessage:string = "Verifying OTP...";
    showEmailTOPLoginLink:boolean = false;

    constructor(
        private route: ActivatedRoute,
        private notify: NotifyService,
        private authService: AuthService,
        private appData: AppDataService,
    ) { }

    ngOnInit() {
        this.verifyEmailOTP()
    }

    async verifyEmailOTP(){
        
        try {
            const params = Object.assign({}, this.route.snapshot.queryParams, {email: this.appData.userEmail })
            
            switch(params.mode.toLowerCase()){
                case "signin":
                    const userProfile = await this.authService.verifyEmailOTPLink(params);
                    this.authService.onUserLogin(userProfile);
                    break;
            }
            
        } catch (error) {

            if(error.code && error.code == "auth/invalid-action-code"){
                this.showEmailTOPLoginLink=true;
            }

            this.statusMessage = error.message
        }
    }
}