import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VerifyEmailOTPComponent }   from './verifyEmailOTP.component';

const routes: Routes = [
    { path: '', component: VerifyEmailOTPComponent},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: []
})
export class VerifyEmailOTPRoutingModule {

}