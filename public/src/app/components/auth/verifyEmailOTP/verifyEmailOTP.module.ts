import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyEmailOTPRoutingModule } from './verifyEmailOTP.routing';
import { VerifyEmailOTPComponent } from './verifyEmailOTP.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';

@NgModule({
    imports: [
        CommonModule,
        VerifyEmailOTPRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
    ],
    declarations: [VerifyEmailOTPComponent],
    providers: [],
    bootstrap: [VerifyEmailOTPComponent]
})
export class VerifyEmailOTPModule { }