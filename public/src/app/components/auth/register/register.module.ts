import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from './register.routing';
import { RegisterComponent } from './register.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { InputSwitchModule } from 'primeng/inputswitch';

import { CalendarModule } from 'primeng/calendar';
import { InputNumberModule } from 'primeng/inputnumber';

import { ConfirmRegistrationModule } from '@app/components/common/confirm-registration/confirm-registration.module';

@NgModule({
    imports: [
        CommonModule,
        RegisterRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
        ConfirmRegistrationModule,
		InputTextModule,
		InputSwitchModule,
		CalendarModule,
		InputNumberModule
    ],
    declarations: [RegisterComponent],
    providers: [],
    bootstrap: [RegisterComponent]
})
export class RegisterModule { }