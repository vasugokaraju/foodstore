import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { map, take } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { ValidationService } from '@app/services/validation.service';
import { fromEvent } from 'rxjs';
import { NotifyService } from 'src/app/services/notify.service';
import { IUsers } from '@app/interfaces/IUsers';
import { AppDataService } from '@app/services/appData.service';

@Component({
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {
    registerForm: FormGroup;       // form group control

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    submitted:boolean = false;

    showConfirmationInfo:boolean = false;
    
    emailSubject:string = "Registration Confirmation"

    initializationValue:IUsers = {
 "displayName": "Ronaldo",
 "email": "no-reply@managudifoundation.in",
 "password": "ifUmJ35Gy3sqjf2",
 "phoneNumber": "+14010042761",
 "emailVerified": false,
 "disabled": false,
 "photoURL": "http://myphotosdfsf.com",
 "role": {
  "code": 2.2,
  "name": "SUPERVISOR"
 },
 "dob": new Date(),
 "ssn": 873088905,
 "address": {
  "city": {
   "colony": "Conroyville",
   "foundedon": new Date()
  },
  "country": "France"
 },
 "id": ""
};
    selectedRecord:IUsers = this.initializationValue;

    // To hold data for list view
    usersListViewData:BehaviorSubject<IUsers[]> = new BehaviorSubject<IUsers[]>([this.initializationValue]);
    // To hold data for fromGroup (add/edit/delete) view.
    usersEditViewData:BehaviorSubject<IUsers> = new BehaviorSubject<IUsers>(this.initializationValue);


    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

    constructor(
        private authService: AuthService,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private appData: AppDataService
    ) {}

    ngOnInit() {
        // Initialize the form and its controls
        this.registerForm = this.formBuilder.group({
            displayName: [
				{ value: this.usersEditViewData.value.displayName, disabled: false },
				[
					Validators.minLength(3),
					Validators.maxLength(50),
				]
			],
                
            email: [
				{ value: this.usersEditViewData.value.email, disabled: false },
				[
					Validators.minLength(3),
					Validators.maxLength(50),
				]
			],
                
            password: [
				{ value: this.usersEditViewData.value.password, disabled: false },
				[
					Validators.minLength(3),
					Validators.maxLength(50),
				]
			],
                
            phoneNumber: [
				{ value: this.usersEditViewData.value.phoneNumber, disabled: false },
				[
					Validators.minLength(10),
					Validators.maxLength(12),
				]
			],
                
            emailVerified: [
				{ value: this.usersEditViewData.value.emailVerified, disabled: false },
			],
                
            disabled: [
				{ value: this.usersEditViewData.value.disabled, disabled: false },
			],
                
            photoURL: [
				{ value: this.usersEditViewData.value.photoURL, disabled: false },
				[
					Validators.maxLength(100),
				]
			],
                
            role: [
				{ value: this.usersEditViewData.value.role, disabled: false }
			],
            dob: [
				{ value: this.usersEditViewData.value.dob, disabled: false },
			],
                
            ssn: [
				{ value: this.usersEditViewData.value.ssn, disabled: false },
			],
                
            address: this.formBuilder.group({city: this.formBuilder.group({colony: [
				{ value: this.usersEditViewData.value.address.city.colony, disabled: false },
			],
                foundedon: [
				{ value: this.usersEditViewData.value.address.city.foundedon, disabled: false },
			],
                }),country: [
				{ value: this.usersEditViewData.value.address.country, disabled: false },
				[
					Validators.minLength(2),
					Validators.maxLength(100),
				]
			],
                }),

        })        
    }

    public ngAfterViewInit(): void {
        // Add validation tooltip layer to fromgroup.
        ValidationService.bindFormControls(this.registerForm);
    }

    onSubmit() {
        
        // If the form is not valid, do not submit.
        if (this.registerForm.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.",false,null,10000);
            return;
        }

        this.appData.userEmail = this.f.email.value;

        this.authService.registerUser(this.selectedRecord)
            .pipe(take(1))
            .subscribe(
                data => {
                    // Show registration verification instructions
                    this.showConfirmationInfo=true;
                },
                error => {
                    this.submitted = false;
                    /** Error messages are handled in HTTPInterceptor service (http.interceptor.ts)*/
                }
            )
    }

}