import { Component, OnInit } from '@angular/core';
import { NotifyService } from 'src/app/services/notify.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '@app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Helper } from '@app/helpers';
import { AppDataService } from '@app/services/appData.service';


@Component({
    templateUrl: './verifyRegisteredEmail.component.html'
})
export class VerifyRegisteredEmailComponent implements OnInit {

    passwordForm: FormGroup;       // form group control
    submitted: boolean = false;

    statusMessage: string = "";
    isEmailConfirmed: number = -1;   // 1=Email verified
    passwordMinLen: number = 8;
    passwordMaxLen: number = 100;
    
    userId: string = "";
    secret:string;  // Secret is provided by verifyEmailConfirmation().  This is expected by api to create first time password

    constructor(
        private route: ActivatedRoute,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private authService: AuthService,
        private helper: Helper,
        private appData: AppDataService
    ) { }

    ngOnInit() {
        this.verifyEmailConfirmation();

        this.userId = this.appData.userEmail;

        // Initialize the form and its controls
        this.passwordForm = this.formBuilder.group({

            userId: [
                { value: this.userId, disabled: false },
                [
                    Validators.required
                ]
            ],

            password: [
                { value: "Aa$4dddd", disabled: false },
                [
                    Validators.minLength(this.passwordMinLen),
                    Validators.maxLength(this.passwordMaxLen)
                ]
            ],

            confirmPassword: [
                { value: "Aa$4dddd", disabled: false },
                [
                    Validators.minLength(this.passwordMinLen),
                    Validators.maxLength(this.passwordMaxLen),
                ]

            ],


        })
    }


    async verifyEmailConfirmation() {

        try {
            const token = this.route.snapshot.queryParams["token"];

            const confirmationStatus = await this.authService.verifyEmailConfirmation(token);

            this.userId = confirmationStatus.data.email;
            this.secret = confirmationStatus.data.secret;

            this.passwordForm.patchValue({userId:this.userId}); // Set value to form field

            if (confirmationStatus.data && confirmationStatus.data.status && confirmationStatus.data.status == true) {
                this.statusMessage = confirmationStatus.message;
                this.isEmailConfirmed = 1;
            }

        } catch (error) {

            switch (error.code) {
                case "E1035":   // This email has already been verified.  Please login.
                    this.statusMessage = error.message
                    this.isEmailConfirmed = 1;
                    break;

                case "auth/user-not-found":
                    this.statusMessage = error.message  // Please register.
                    this.isEmailConfirmed = 2;
                    break;

                default:
            }

        }
    }

    async onSubmit() {

        // If the form is not valid, do not submit.
        if (this.passwordForm.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.", false, null, 10000);
            return;
        }

        try {

            // On successful creation of new password, user login is performed with new credentials and user profile is returned.
            const userProfile = await this.authService.createPassword(
                {
                    secret : this.secret,
                    email:this.passwordForm.get('userId').value,
                    newPassword:this.passwordForm.get('password').value
            })

            // Save user profile on client.
            this.authService.onUserLogin(userProfile);

        } catch (error) {
            
        }

    }

    // Make sure the password has special characters
    checkPasswordStrength(password: string) {

        return this.helper.checkPasswordStrength(password, this.passwordMinLen, this.passwordMaxLen)

    }

}