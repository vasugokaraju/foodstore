import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VerifyRegisteredEmailRoutingModule } from './verifyRegisteredEmail.routing';
import { VerifyRegisteredEmailComponent } from './verifyRegisteredEmail.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
    imports: [
        CommonModule,
        VerifyRegisteredEmailRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
        InputTextModule
    ],
    declarations: [VerifyRegisteredEmailComponent],
    providers: [],
    bootstrap: [VerifyRegisteredEmailComponent]
})
export class VerifyRegisteredEmailModule { }