import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { VerifyRegisteredEmailComponent }   from './verifyRegisteredEmail.component';

const routes: Routes = [
    { path: '', component: VerifyRegisteredEmailComponent},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: []
})
export class VerifyRegisteredEmailRoutingModule {

}