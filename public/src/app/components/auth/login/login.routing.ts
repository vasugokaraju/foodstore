import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginResolver } from '@app/services/common.resolve.service';

import { LoginComponent }   from '../../auth/login/login.component';

const routes: Routes = [
    { path: '', component: LoginComponent, resolve: {preloadData:LoginResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [LoginResolver]
})
export class LoginRoutingModule {

}