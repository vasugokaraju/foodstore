import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login.routing';
import { LoginComponent } from './login.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { FederatedLoginComponent } from '../federatedLogin/federatedLogin.component';
import { PasswordLoginComponent } from '../passwordLogin/passwordLogin.component';
import { TabViewModule } from 'primeng/tabview';
import { OTPLoginComponent } from '../otpLogin/otpLogin.component';
import { DropdownModule } from 'primeng/dropdown';
import { InputMaskModule } from 'primeng/inputmask';

@NgModule({
    imports: [
        CommonModule,
        LoginRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
        InputTextModule,
        TabViewModule,
        InputMaskModule,
        DropdownModule
    ],
    declarations: [LoginComponent, PasswordLoginComponent, OTPLoginComponent, FederatedLoginComponent],
    providers: [],
    bootstrap: [LoginComponent]
})
export class LoginModule { }