import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationService } from '../../../services/validation.service'
import { NotifyService } from 'src/app/services/notify.service';
import { AppDataService } from '@app/services/appData.service';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, AfterViewInit {
    loginForm: FormGroup;       // form group control
    submitted: boolean = false; // To enable/disable submit button

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    firebaseAppConfig: any = {};

    showLogin:boolean = true;

    activeTab: number = 0;   // 0 = With Password tab

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    constructor(
        private route: ActivatedRoute,
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private appData: AppDataService
    ) { }

    ngOnInit() {

        // Preload data
        this.route.data
            .pipe(take(1))
            .subscribe((resp: any) => {
                this.firebaseAppConfig = resp.preloadData.data;
            });

    
        // Clear session timeout message 
        this.notify.clear();

        // Initialize the form and its controls
        this.loginForm = this.formBuilder.group({
            loginId: [
                { value: "no-reply@managudifoundation.in", disabled: false },
                [
                    Validators.required,
                    Validators.minLength(3),
                    // ValidationService.emailValidator,        // Custom validator
                ]
            ],
            password: [
                { value: "ifUmJ35Gy3sqjf2", disabled: false },
                [Validators.required, Validators.minLength(3)]
            ],
        });
    }
    

    public ngAfterViewInit(): void {
        // Add validation tooltip layer to fromgroup.
        ValidationService.bindFormControls(this.loginForm);
    }


    async onSubmit() {

        // If the form is not valid, do not submit.
        if (this.loginForm.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.", false, null, 10000);
            return;
        }

        this.submitted = true;
        this.appData.userEmail = this.f.loginId.value;

        try {
            const response = await this.auth.login(this.f.loginId.value, this.f.password.value);
            
            // Store user info for later use
            this.auth.onUserLogin(response);
            this.showLogin = true;
        } catch (error) {

            switch(error.code){
                case "auth/user-not-found":
                case "auth/wrong-password":
                    this.showLogin = true;
                    break;
                default:
                    this.showLogin = false;
            }
            
            this.submitted = false;
            /** Error messages are handled in HTTPInterceptor service (http.interceptor.ts)*/
        }

    }

    // Send verification email on demand.
    async sendEmailVerification(){
            try {
                // Send verification email
                await this.auth.sendEmailVerification({email:this.f.loginId.value});
            } catch (error) {
                this.notify.showError("Email Exception","Failed to send email due to technical reasons.")
            }
    }

}