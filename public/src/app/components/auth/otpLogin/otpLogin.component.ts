import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { NotifyService } from 'src/app/services/notify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppDataService } from '@app/services/appData.service';
import { Helper } from '@app/helpers/helper'

import { WindowService } from '@app/services/window.service'

import firebase from "firebase/app";
import "firebase/auth";

export class PhoneNumber {
    country: string;
    area: string;
    prefix: string;
    line: string;

    // format phone numbers as E.164
    get e164() {
        const num = this.country + this.area + this.prefix + this.line
        return `+${num}`
    }

}

@Component({
    selector: 'app-otp-login',
    templateUrl: './otpLogin.component.html'
})
export class OTPLoginComponent implements OnInit, AfterViewInit {

    @Input() firebaseConfig: any = {}

    phoneForm: FormGroup;       // form group control
    emailForm: FormGroup;       // form group control

    activeTab: number = 0;   // 0 = Phone OTP Login tab

    submitted: boolean = false; // To enable/disable submit button

    statusMessage: string = "";

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    // convenience getter for easy access to form fields
    get f() { return this.phoneForm.controls; }


    windowRef: any;
    // phoneNumber = new PhoneNumber();
    verificationCode: string;
    confirmationResult: firebase.auth.ConfirmationResult;
    showOTPPhone: boolean = true;

    otpEmail: string;    // Holds otp email address

    countryCodes: Array<{ code: string, name: string, mask:string }> = [
        { code: "91", name: "Bharat - 91", mask:"99999-99999" },
        { code: "1", name: "USA - 1" , mask:"999-999-9999"}
    ];

    selectedCountryCode: any = this.countryCodes[0];

    constructor(
        private route: ActivatedRoute,
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private appData: AppDataService,
        private win: WindowService,
        private helper: Helper
    ) { }


    ngOnInit() {

        this.initializeFirebaseApp();
        
        // Set default tab
        this.activeTab = (this.route.snapshot.queryParams.t == 1) ? 1 : 0;

        this.otpEmail = this.appData.userEmail;

        // Initialize the form and its controls
        this.phoneForm = this.formBuilder.group({
            countryCode: [
                { value: this.countryCodes[0], disabled: false },
                [
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(5),
                ]
            ],
            phoneNumber: [
                { value: "", disabled: false },
                [
                    Validators.minLength(4),
                    Validators.required                    
                ]
            ],
            OTP: [
                { value: "282922", disabled: false }
            ]
        });

        this.emailForm = this.formBuilder.group({
            email: [
                { value: this.otpEmail, disabled: false },
                [
                    Validators.email,
                    Validators.required
                ]
            ]
        });

        this.windowRef = this.win.windowRef;
    }


    public ngAfterViewInit(): void {

        // STEP 1: Initialize recaptcha and render the image
        this.windowRef.recaptchaVerifier = <firebase.auth.RecaptchaVerifier>new firebase.auth.RecaptchaVerifier("recaptcha-container", {
            "size": "invisible",
            "callback": async (response) => { // STEP 2: Triggers when user passes reCaptcha test
                // console.log('reCAPTCHA resolved');

                // STEP 3: Call firebase to initiate SMS to send OTP number
                // This is useful if recaptcha is visible
                // this.signInWithPhoneNumber();
            },
            'expired-callback': () => {
                console.log('reCAPTCHA expired. Try again.');

                // Response expired. Ask user to solve reCAPTCHA again.
            }
        });

        // This will render the recaptcha image if size is NOT "invisible"
        // this.windowRef.recaptchaVerifier.render();

        // Add validation tooltip layer to fromgroup.
        // ValidationService.bindFormControls(this.phoneForm);
    }


    initializeFirebaseApp() {


        if (!firebase.apps.length) {
            firebase.initializeApp(this.firebaseConfig);
        } else {
            firebase.app(); // if already initialized, use that one
        }
    }

    async signInWithPhoneNumber() {

        let countryCode: any = this.f.countryCode.value;
        let phoneNumber: string = this.f.phoneNumber.value;
        const appVerifier: firebase.auth.RecaptchaVerifier = this.windowRef.recaptchaVerifier;

        this.appData.userCountryCode = countryCode;
        this.appData.userPhoneNumber = phoneNumber;

        // Check whether the value is an object
        if (typeof countryCode != "string") { countryCode = countryCode["name"]; }

        countryCode = countryCode.match(/(\d+)/).map(Number)[0];
        phoneNumber = "+" + countryCode + phoneNumber

        if (!this.helper.validatePhoneNumber(phoneNumber)) {
            this.notify.showError("Invalid Phone Number", "Please check the phone number.", true);
            return;
        }

        // Check whether the phone number is registered.  Only registered users can login with phone number
        const isPhoneRegistered = (await this.auth.isPhoneNumberRegistered(phoneNumber)).data;
        if (!isPhoneRegistered) {
            this.notify.showError("Phone Number Not Registered", "Please create an account with this phone number.", true);
            return;
        }

        try {
            // STEP 3: Call firebase to initiate SMS to send OTP number
            // NOTE: Calling this method resets the reCaptcha. 
            // Switch to different view on UI when reCaptcha is passed, before calling signInWithPhoneNumber method
            firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
                .then(confirmationResult => {
                    // STEP 4: Retain the confirmationResult to validate the OTP when the user enters it.
                    // Once the OTP is entered by user, use confirmationResult.confirm(code) to verify the entered code.
                    this.confirmationResult = confirmationResult;
                    this.showOTPPhone = false;
                })
                .catch(error => {
                    // Failed to send SMS
                    console.log('Failed to send SMS.', error);
                    this.notify.showError("SMS Exception", "Failed to send SMS. " + error?.message, true);
                })
        } catch (error) {
            console.log('error', error);
            this.notify.showError("Unknown Exception", error?.message, true);
        }
    }

    // Call this method to verify user entered OTP
    verifyOTP() {
        // STEP 5: Verify user entered OTP with confirmationResult
        const code: string = this.f.OTP.value.toString();

        this.confirmationResult.confirm(code).then(
            async (userData: firebase.auth.UserCredential) => {

                // STEP 6: Collect user information for local storage.
                await this.updateUserInfo(userData);

                // STEP 7: Add newly generated token to user's tokens collection within 5 seconds
                /**
                 * When a user logs in using email authentication, the API saves token in database for various reasons.
                 * But when a user logs in using Phone/OTP, the authentication communication happens between browser and firebase.
                 * This will skip the token management at serverside.  To fill this gap, call this.auth.onSignInWithPhone method 
                 * to perform token housekeeping at serverside
                 */
                setTimeout(async () => {
                    const syncedUserInfo = await this.auth.onSignInWithPhone(userData);
                    this.auth.onUserLogin(syncedUserInfo);
                })
            },
            error => {
                console.log("invalid opt code", error.message);
            }
        )
    }

    /**
     * Updates logged in user information like token
     */
    async updateUserInfo(userData: any) {
        const userInfo = {
            displayName: userData.user.displayName,
            email: userData.user.email,
            emailVerified: userData.user.emailVerified,
            phoneNumber: userData.user.phoneNumber,
            photoURL: userData.user.photoURL,
            id: userData.user.uid,
            token: await userData.user.getIdToken(true),
            refreshToken: userData.user.refreshToken,
            providerData: userData.user.providerData[0]
        }

        // const tokenParts:Array<string> = userInfo.token.split('.');
        // userInfo.token = `${tokenParts[0]}.${tokenParts[1]}.`
        // console.log('userInfo.token', userInfo.token);

        // Store user info in localStorage
        this.auth.saveUserProfile(userInfo);

        return userInfo;
    }


    // Sends email with email otp
    async sendEmailOTPLink() {


        const email = this.emailForm.controls.email.value;

        // Store the email so that it can be used to verify the otp
        this.appData.userEmail = email;

        try {
            const emailStatus = await this.auth.sendEmailOTPLink({ email: email });

            this.statusMessage = emailStatus.message;
        } catch (error) {

        }
    }


    /*
    // This method could be used with reCAPTCHA v3.
    submitPhoneNumber(e:Event){
        e.preventDefault();
        window['grecaptcha'].ready( async () => {
            try {
                // Trigger reCAPTCHA verification
                const reCaptchaResponse = await window['grecaptcha'].execute('6Ldo_NYaAAAAAPaXMObI7l4jBKdKQQA1FJGKYLsh', {action: 'submit'});

                console.log('recaptchaVerifier', this.windowRef.recaptchaVerifier);
                
                // Call server with phone number and recaptcha response to verify recaptcha and issue sms (otp)
                const status = await this.auth.signInWithPhone({pn: this.f.phoneNumber.value , recaptcha_token:reCaptchaResponse, recaptchaVerifier: this.windowRef.recaptchaVerifier})

                console.log('token',reCaptchaResponse, 'status', status);
                
            } catch (error) {
                console.log('reCAPTCHA security check failed.', error);
                
            }

        })
    }
    */

}