import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordRoutingModule } from './changePassword.routing';
import { ChangePasswordComponent } from './changePassword.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
    imports: [
        CommonModule,
        ChangePasswordRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
        InputTextModule
    ],
    declarations: [ChangePasswordComponent],
    providers: [],
    bootstrap: [ChangePasswordComponent]
})
export class ChangePasswordModule { }