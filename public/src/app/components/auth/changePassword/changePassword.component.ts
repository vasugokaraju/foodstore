import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationService } from '../../../services/validation.service'
import { NotifyService } from 'src/app/services/notify.service';
import { Helper } from '@app/helpers';


@Component({
    templateUrl: './changePassword.component.html'
})
export class ChangePasswordComponent implements OnInit, AfterViewInit {
    changePasswordForm: FormGroup;       // form group control
    submitted: boolean = false; // To enable/disable submit button

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    passwordMinLen: number = 8;
    passwordMaxLen: number = 100;

    // convenience getter for easy access to form fields
    get f() { return this.changePasswordForm.controls; }

    constructor(
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private helper: Helper
    ) { }

    ngOnInit() {

        // Clear session timeout message 
        this.notify.clear();

        // Initialize the form and its controls
        this.changePasswordForm = this.formBuilder.group({
            loginId: [
                { value: "", disabled: false },
                [
                    Validators.required,
                    Validators.minLength(3),
                    // ValidationService.emailValidator,        // Custom validator
                ]
            ],
            currentPassword: [
                { value: "", disabled: false },
                [
                    Validators.required, 
                    Validators.minLength(this.passwordMinLen), 
                    Validators.maxLength(this.passwordMaxLen)
                ]
            ],
            newPassword: [
                { value: "", disabled: false },
                [Validators.required, Validators.minLength(this.passwordMinLen), Validators.maxLength(this.passwordMaxLen)]
            ]
        })
    }


    public ngAfterViewInit(): void {
        // Add validation tooltip layer to fromgroup.
        ValidationService.bindFormControls(this.changePasswordForm);
    }

    async onSubmit() {

        // If the form is not valid, do not submit.
        if (this.changePasswordForm.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.", false, null, 10000);
            return;
        }

        this.submitted = true;

        try {
            const response = await this.auth.changePassword(this.f.loginId.value, this.f.currentPassword.value, this.f.newPassword.value);
            this.notify.showSuccess(response.title, response.message);
            this.auth.token = response.data.token;  // Set new token for further queries.

        } catch (error) {
            this.submitted = false;
            // this.notify.showError("Change of Password", "Failed to update your password.");            
        }

    }


    // Make sure the password has special characters
    checkPasswordStrength(password: string) {

        return this.helper.checkPasswordStrength(password, this.passwordMinLen, this.passwordMaxLen)

    }

}