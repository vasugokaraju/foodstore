import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChangePasswordComponent }   from '../../auth/changePassword/changePassword.component';

const routes: Routes = [
    { path: '', component: ChangePasswordComponent},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: []
})
export class ChangePasswordRoutingModule {

}