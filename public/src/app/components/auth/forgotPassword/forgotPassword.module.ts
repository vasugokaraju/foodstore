import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordRoutingModule } from './forgotPassword.routing';
import { ForgotPasswordComponent } from './forgotPassword.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';

@NgModule({
    imports: [
        CommonModule,
        ForgotPasswordRoutingModule,
        ButtonModule,
        FormsModule,
        CardModule,
        ReactiveFormsModule,
        InputTextModule
    ],
    declarations: [ForgotPasswordComponent],
    providers: [],
    bootstrap: [ForgotPasswordComponent]
})
export class ForgotPasswordModule { }