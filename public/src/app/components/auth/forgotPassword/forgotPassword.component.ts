import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationService } from '../../../services/validation.service'
import { NotifyService } from 'src/app/services/notify.service';
import { AppDataService } from '@app/services/appData.service';
import { ActivatedRoute } from '@angular/router';
import { Helper } from '@app/helpers';


@Component({
    templateUrl: './forgotPassword.component.html'
})
export class ForgotPasswordComponent implements OnInit, AfterViewInit {
    forgotPasswordForm: FormGroup;       // form group control
    resetPasswordForm: FormGroup;       // form group control

    submitted: boolean = false; // To enable/disable submit button

    @ViewChild("loginid") loginid: ElementRef;

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    passwordMinLen: number = 8;
    passwordMaxLen: number = 100;

    userEmail: string;

    viewMode: string = "email";

    // convenience getter for easy access to form fields
    get f() { return this.forgotPasswordForm.controls; }

    constructor(
        private route: ActivatedRoute,
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private notify: NotifyService,
        private appData: AppDataService,
        private helper: Helper
    ) { }

    async ngOnInit() {

        // Clear session timeout message 
        this.notify.clear();
        this.userEmail = this.appData.userEmail;

        // Initialize the forgot password form and its controls
        this.forgotPasswordForm = this.formBuilder.group({
            loginId: [
                { value: this.userEmail, disabled: false },
                [
                    Validators.required,
                    Validators.minLength(this.passwordMinLen),
                    Validators.email
                ]
            ]
        });

        // Initialize the form and its controls
        this.resetPasswordForm = this.formBuilder.group({
            loginId: [
                { value: this.userEmail, disabled: false },
                [
                    Validators.required,
                    Validators.email
                ]
            ],
            newPassword: [
                { value: "", disabled: false },
                [
                    Validators.required,
                    Validators.minLength(this.passwordMinLen),
                    Validators.maxLength(this.passwordMaxLen)
                ]
            ]
        });


        await this.verifyForgotPasswordLink();
    }


    public async ngAfterViewInit() {
        // Add validation tooltip layer to fromgroup.
        ValidationService.bindFormControls(this.forgotPasswordForm);
    }

    private async verifyForgotPasswordLink() {

        const params = Object.assign({}, this.route.snapshot.queryParams, { email: this.appData.userEmail })

        // Mode is available when user clicks on forgot password link in the email and this page is loaded
        if (params.mode && params.mode.toLowerCase()) {
            const status = await this.auth.verifyForgotPasswordLink(params);
            this.appData.userEmail = status.data.userEmail;
            this.viewMode = "password"
        }

    }


    async onSubmit() {

        // If the form is not valid, do not submit.
        if (this.forgotPasswordForm.invalid) {
            this.notify.showError("Validation Errors", "Please click on the marked fields to see error details.", false, null, 10000);
            return;
        }

        this.submitted = true;

        try {

            if (this.viewMode == "email") {

                const response = await this.auth.forgotPassword({ email: this.f.loginId.value });
                this.notify.showSuccess(response.title, response.message);

            }
            else if (this.viewMode == "password") {
                const response = await this.auth.resetPassword({
                    oobCode: this.route.snapshot.queryParams.oobCode,
                    email: this.appData.userEmail,
                    newPassword: this.resetPasswordForm.controls.newPassword.value
                });

                this.auth.onUserLogin(response);
            }

        } catch (error) {
            this.submitted = false;
            // this.notify.showError("Change of Password", "Failed to update your password.");            
        }

    }
    

    // Make sure the password has special characters
    checkPasswordStrength(password: string) {

        return this.helper.checkPasswordStrength(password, this.passwordMinLen, this.passwordMaxLen)

    }

}