import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ForgotPasswordComponent }   from '../../auth/forgotPassword/forgotPassword.component';

const routes: Routes = [
    { path: '', component: ForgotPasswordComponent},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: []
})
export class ForgotPasswordRoutingModule {

}