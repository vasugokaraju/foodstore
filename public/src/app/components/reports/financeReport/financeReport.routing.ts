import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FinanceReportComponent }   from '../../reports/financeReport/financeReport.component';
import { FinanceReportResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: FinanceReportComponent, resolve: {financeReportData:FinanceReportResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [FinanceReportResolver]
})
export class FinanceReportRoutingModule {

}