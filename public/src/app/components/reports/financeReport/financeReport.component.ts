import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helper } from '@app/helpers';
import { AppDataService } from '@app/services/appData.service';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { map, take } from 'rxjs/operators';
import { IFinanceReport } from '@app/interfaces/reportsInterfaces';

@Component({
    styleUrls: ['./financeReport.component.css'],
    templateUrl: './financeReport.component.html',
    encapsulation: ViewEncapsulation.None
})
export class FinanceReportComponent implements OnInit, AfterViewInit {
     financeReportData:any;

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    loading: boolean = true;
    @ViewChild('tblFinanceReport') tblFinanceReport: Table;
    @ViewChildren('filterElements') filterElements: QueryList<ElementRef>;

    // To hold data for list view
    financeReportListViewData:BehaviorSubject<IFinanceReport[]> = new BehaviorSubject<IFinanceReport[]>([]);
    

    totalRows:number = 0;
    rowsPerPage:number = 5;
    filter: any = {filters:{}, sortField:undefined, sortOrder:undefined};

    fileNamePrefix:string = "FinanceReport";

    constructor(
        private route: ActivatedRoute,
        public appDataService: AppDataService,
    ) {}

    ngOnInit() {
        // Preload data
        this.route.data
            .pipe(
                take(1),
                map((record: any) => {
                    let formatted: any = {"listViewData":[]};

                    if (record.financeReportData) {     // Transform the incoming object to fit the page needs
                        if (record.financeReportData) {
                            formatted.listViewData = record.financeReportData.data;
                        }
                    }
                    return formatted;
                })
            )
            .subscribe((resp: any) => {
                this.financeReportListViewData.next(resp.listViewData);
                this.totalRows = this.financeReportListViewData.value.length;
            });
    }

    ngAfterViewInit(): void {

    }


    ngOnDestroy(): void {
 
    }    


}