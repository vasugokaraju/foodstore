import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { FinanceReportRoutingModule } from './financeReport.routing';
import { FinanceReportComponent } from './financeReport.component';


@NgModule({
    imports: [
        CommonModule,
        FinanceReportRoutingModule,
        TableModule,
        PanelModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,

    ],
    declarations: [FinanceReportComponent],
    providers: [],
    bootstrap: [FinanceReportComponent]
})
export class FinanceReportModule { }