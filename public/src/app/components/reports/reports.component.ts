import { Component, OnInit, Input, ViewEncapsulation, AfterViewInit, AfterViewChecked} from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';
import { AppDataService } from '../../services/appData.service'; // Use this service to share data among components.

@Component({
    selector: 'my-reports',
    styleUrls: [ './reports.component.css' ],
    templateUrl: './reports.component.html'
})

export class ReportsComponent implements OnInit, AfterViewInit, AfterViewChecked{
    items: MenuItem[];
    
    constructor(
        private appData: AppDataService
        ){};

    ngOnInit(): void {

        // Subscribe for menu data
        this.appData.mainMenu.subscribe((data: MenuItem[]) => {
            this.items = data.filter( menuItem => menuItem.id === "REPORTS" )
        })        
    }

    ngAfterViewInit() {

    }

    ngAfterViewChecked(){
    }

}