import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { SummaryReportRoutingModule } from './summaryReport.routing';
import { SummaryReportComponent } from './summaryReport.component';


@NgModule({
    imports: [
        CommonModule,
        SummaryReportRoutingModule,
        TableModule,
        PanelModule,
        ButtonModule,
        FormsModule,
        ReactiveFormsModule,

    ],
    declarations: [SummaryReportComponent],
    providers: [],
    bootstrap: [SummaryReportComponent]
})
export class SummaryReportModule { }