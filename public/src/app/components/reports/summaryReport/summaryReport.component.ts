import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helper } from '@app/helpers';
import { AppDataService } from '@app/services/appData.service';
import { Table } from 'primeng/table';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { map, take } from 'rxjs/operators';
import { ISummaryReport } from '@app/interfaces/reportsInterfaces';

@Component({
    styleUrls: ['./summaryReport.component.css'],
    templateUrl: './summaryReport.component.html',
    encapsulation: ViewEncapsulation.None
})
export class SummaryReportComponent implements OnInit, AfterViewInit {
     summaryReportData:any;

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user came out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    changedControls: any = {};

    loading: boolean = true;
    @ViewChild('tblSummaryReport') tblSummaryReport: Table;
    @ViewChildren('filterElements') filterElements: QueryList<ElementRef>;

    // To hold data for list view
    summaryReportListViewData:BehaviorSubject<ISummaryReport[]> = new BehaviorSubject<ISummaryReport[]>([]);
    

    totalRows:number = 0;
    rowsPerPage:number = 5;
    filter: any = {filters:{}, sortField:undefined, sortOrder:undefined};

    fileNamePrefix:string = "SummaryReport";

    constructor(
        private route: ActivatedRoute,
        public appDataService: AppDataService,
    ) {}

    ngOnInit() {
        // Preload data
        this.route.data
            .pipe(
                take(1),
                map((record: any) => {
                    let formatted: any = {"listViewData":[]};

                    if (record.summaryReportData) {     // Transform the incoming object to fit the page needs
                        if (record.summaryReportData) {
                            formatted.listViewData = record.summaryReportData.data;
                        }
                    }
                    return formatted;
                })
            )
            .subscribe((resp: any) => {
                this.summaryReportListViewData.next(resp.listViewData);
                this.totalRows = this.summaryReportListViewData.value.length;
            });
    }

    ngAfterViewInit(): void {

    }


    ngOnDestroy(): void {
 
    }    


}