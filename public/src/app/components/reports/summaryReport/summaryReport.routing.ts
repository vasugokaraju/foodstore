import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SummaryReportComponent }   from '../../reports/summaryReport/summaryReport.component';
import { SummaryReportResolver } from '../../../services/common.resolve.service';

const routes: Routes = [
    { path: '', component: SummaryReportComponent, resolve: {summaryReportData:SummaryReportResolver}},   //, canActivate: [AuthGuard]
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ],
    providers: [SummaryReportResolver]
})
export class SummaryReportRoutingModule {

}