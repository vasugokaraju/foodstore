import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BulkUploadComponent } from './bulkUpload.component';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        SplitButtonModule,
        InputTextareaModule,
        TableModule
    ],
    declarations: [BulkUploadComponent],
    providers: [],
    exports:[CommonModule, BulkUploadComponent],    // Export the component so that it can be used as child
    bootstrap: [BulkUploadComponent]
})
export class BulkUploadModule { }