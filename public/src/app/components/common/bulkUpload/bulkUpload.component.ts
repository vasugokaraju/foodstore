import { Component, EventEmitter, Injectable, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { Helper } from '@app/helpers';
import { NotifyService } from '@app/services/notify.service';

@Injectable({ providedIn: "root" }) // allows this child component injectable in parent constructor

@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulkUpload.component.html',
  styleUrls: ['./bulkUpload.component.css']
})
export class BulkUploadComponent implements OnInit {
  @Input() templateFormat: any;
  @Input() fileNamePrefix: any = "";

  @Output() onUploadSubmit: EventEmitter<any> = new EventEmitter();  // Sends communication to Parent component

  formatTemplates: Array<any> = [
    {
      label: 'Download JSON Format Template', command: () => {
        // Download json tempalte
        this.downloadFormatTemplate('JSON');
      }
    },
  ];


  dataViewMode: number = 1;    // 1 = text; 2 = table
  userInputData: any = "";           // Contains raw inupt of csv data
  csvHeader: Array<any>;       // CSV header
  csvDataArray: Array<any>;    // Contains CSV data


  constructor(
    private helper: Helper,
    private notify: NotifyService
  ) { }


  ngOnInit(): void {
  }


  downloadFormatTemplate(format: any) {

    switch (format.toLowerCase()) {
      case "csv":
        const csvFields = Object.keys(this.templateFormat);
        this.helper.saveAsCSV([this.templateFormat], csvFields, this.fileNamePrefix + "BulkUploadTemplate.csv")
        break;
      case "json":
        this.helper.saveAsJSON([this.templateFormat], this.fileNamePrefix + "BulkUploadTemplate.json")
        break;
    }

  }

  // Toggles between text and table view.
  switchDataView() {

    if (this.dataViewMode == 1) {

      try {
        // Check if it is json
        let json = JSON.parse(this.userInputData);
        if (!Array.isArray(json)) json = [json];   // Convert to array if single record

        this.csvDataArray = this.helper.json2csv(json, Object.keys(json[0])).split(/\r?\n/);
        this.csvHeader = this.helper.csvToArray(this.csvDataArray.splice(0, 1)[0]);

        this.dataViewMode = 2;

      } catch (error) { //If not json, assume that it is csv
        this.csvDataArray = this.userInputData.split(/\r?\n/);
        this.csvHeader = this.csvDataArray.splice(0, 1)[0].split(",");

        this.dataViewMode = 2;
      }

    }
    else {
      this.dataViewMode = 1;
    }

  }

  onUploadSubmitClick() {
    const str: string = this.userInputData.toString().trim();
    const isJSON: boolean = (str.indexOf("[") == 0 || str.indexOf("{") == 0);

    if (isJSON) {
      // If user input data is json, send it as it
      try {
        // Check if it is json
        let json = JSON.parse(this.userInputData);
        if (!Array.isArray(json)) json = [json];   // Convert to array if single record
        this.onUploadSubmit.emit(json);

      } catch (error) {
        this.notify.showError("Invalid JSON", "Please check JSON. Make sure no trailing commas.");
        return;
      }

    }
    else {
      try {
        //If not json, assume that it is csv and convert it to json
        const json = this.helper.csv2json(this.userInputData);
        this.onUploadSubmit.emit(json);
      } catch (error) {
        this.notify.showError("Invalid CSV", "Please check CSV data.");
        return;
      }
    }
  }


}