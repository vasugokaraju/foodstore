import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeetingWidgetComponent } from './meetingWidget.component';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        SplitButtonModule,
        InputTextareaModule,
        TableModule
    ],
    declarations: [MeetingWidgetComponent],
    providers: [],
    exports:[CommonModule, MeetingWidgetComponent],    // Export the component so that it can be used as child
    bootstrap: [MeetingWidgetComponent]
})
export class MeetingWidgetModule { }