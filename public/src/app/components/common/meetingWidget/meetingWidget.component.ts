import { Component, Injectable, OnInit } from '@angular/core';
import { MEETING_INVITATION } from '@app/interfaces/IAppInterfaces';
import { AppDataService } from '@app/services/appData.service';
import { AuthService } from '@app/services/auth.service';
import { MeetingService } from '@app/services/meeting.service';
import { NotifyService } from '@app/services/notify.service';
import { AngularAgoraRtcService, Stream } from 'angular-agora-rtc'; // Add
import firebase from "firebase/app";
import "firebase/firestore";


/**
 * USAGE
 * <app-meeting-widget>
 * </app-meeting-widget>
 */

@Injectable({ providedIn: "root" }) // allows this child component injectable in parent constructor

@Component({
  selector: 'app-meeting-widget',
  templateUrl: './meetingWidget.component.html',
  styleUrls: ['./meetingWidget.component.css']
})
export class MeetingWidgetComponent implements OnInit {

  personName: string = "";
  personPhone: string = "";
  meetingId: string = "";

  contacts: any[] = [
    { mtngId: "xxxxxxxxxxxxxx", name: "Shambho", ph: "+17342722151" },
    { mtngId: "yyyyyyyyyyyyyy", name: "Devi", ph: "+11795728579" },
  ];

  private app: firebase.app.App;
  private db: firebase.firestore.Firestore;

  private unsubscribeMeeting: any;

  callerId: string;    // Holds unique id to participate in meeting

  firebaseAppConfig: any = {};

  meetingInvitation: any = {}

  callStatusMessage: string = "";

  _MEETING_INVITATION = MEETING_INVITATION;

  localStream: Stream // Add

  constructor(
    public appDataService: AppDataService,
    private meetingService: MeetingService,
    private authService: AuthService,
    private notify: NotifyService,
    private agoraService: AngularAgoraRtcService
  ) {

    this.agoraService.createClient();
  }


  initializeFirebaseApp() {

    // Get firebase app reference, if exists.  Create new one, if not.
    this.app = (!firebase.apps.length) ?
      firebase.initializeApp(this.firebaseAppConfig) :
      firebase.app(); // if already initialized, use that one

    this.db = firebase.firestore(this.app);
    try {
      this.db.useEmulator("127.0.0.1", 8080);  // Remove this line when deployed to production.  
    } catch (error) {

    }


    // firebase.firestore.setLogLevel('debug')

    return this.app;
  }

  async ngOnInit() {


    this.personName = this.appDataService.userName;
    this.personPhone = this.appDataService.userPhoneNumber;

    // Get firebase config
    // Route preload is best way but this is a child component thus no route
    this.firebaseAppConfig = (await this.authService.getFirebaseAppConfig()).data;

    // Initialize firebase app
    this.initializeFirebaseApp();

    try {
      /**
       * Create a record in 'meeting' collection to make/receive phone calls handshake communication
       * 
       * A record might already be exist with same phone number. 
       * In such case existing data should not be overwritten as it could a pending call
       * rcvrPh always belongs to this user thus overwritting does not effect any
       * 
       * If it is first time creating a record, at least one field is necessary to create record as "id" field will be deleted from data
       * 
       */
      const _newRec = {
        "id": this.personPhone,
        // "clrPh": "",
        // "rcvrPh": "",
        // "chnl": "",
        // "tkn": "",
        // "clStts": 0,
        // "strtTm": new Date(),
        "endTm": new Date().toISOString()    // Just to comply with firebase minimum one field requirement to create record.
      };

      await this.meetingService.update(_newRec);

      // Initialize realtime listener
      this.unsubscribeMeeting = this.db.collection("meeting").doc(this.personPhone)
        .onSnapshot((doc) => {

          const _meetingInvitation = doc.data();

          // If the user has left the meeting OR ended the meeting, 
          // do not respond to realtime updates unless it is a new invitation
          if (this.meetingInvitation &&
            (this.meetingInvitation.clStts == MEETING_INVITATION.LEFT || this.meetingInvitation.clStts == MEETING_INVITATION.ENDED) && // current status is accept or reject
            _meetingInvitation.clStts != MEETING_INVITATION.INVITE) {    // and it is not new invitation
            return;
          }

          this.meetingInvitation = _meetingInvitation;

          console.log("Current data this.meetingInvitation: ", this.meetingInvitation);
          this.setCallStatusMessage();
        });
    } catch (error) {
      // console.log('Meeting initialization error: ', error);
      this.notify.showError("Meeting Initialization Error", error.message);
    }

  }



  // This method gets called when realtime update happens (onSnapshot())
  // Set call status message
  setCallStatusMessage() {

    // Determine whether the logged in person is caller or receiver
    const callStatus: number = this.meetingInvitation.clStts;
    const callerPhone: string = this.meetingInvitation.clrPh;
    const receiverPhone: string = this.meetingInvitation.rcvrPh;
    const isCaller: boolean = (this.personPhone === callerPhone);


    if (callerPhone == receiverPhone) {
      this.callStatusMessage = `Self call is not permitted.`;
      return;
    }

    // FOR THE CALLER
    if (isCaller) {
      switch (callStatus) {
        case MEETING_INVITATION.INVITE:
          this.callStatusMessage = `You have invited ${receiverPhone} for a meeting.`;
          break;
        case MEETING_INVITATION.ACCEPTED:
          this.callStatusMessage = `${receiverPhone} has accepted your invitation.`;
          break;
        case MEETING_INVITATION.REJECTED:
          this.callStatusMessage = `${receiverPhone} has rejected your invitation.`;
          break;
        case MEETING_INVITATION.LEFT:
          this.callStatusMessage = `${receiverPhone} has left the meeting.`;
          break;
        case MEETING_INVITATION.ENDED:
          this.callStatusMessage = `You have ended the meeting.`;
          break;
        default:
          this.callStatusMessage = `NAMASKARAM`;
      }
    }

    // FOR THE RECEIVER
    if (!isCaller) {
      switch (callStatus) {
        case MEETING_INVITATION.INVITE:
          this.callStatusMessage = `You have been invited for a meeting by ${callerPhone}.`;
          break;
        case MEETING_INVITATION.ACCEPTED:
          this.callStatusMessage = `You have accepted invitation from ${callerPhone}.`;
          break;
        case MEETING_INVITATION.REJECTED:
          this.callStatusMessage = `You have rejected invitation from ${callerPhone}.`;
          break;
        case MEETING_INVITATION.LEFT:
          this.callStatusMessage = `${callerPhone} has left the meeting.`;
          break;
        case MEETING_INVITATION.ENDED:
          this.callStatusMessage = `${callerPhone} has ended the meeting.`;
          break;
        default:
          this.callStatusMessage = `NAMASKARAM`;
      }
    }

  }

  // Process meeting action
  async meetingAction(callStatus: MEETING_INVITATION, receiverPhone: string = undefined) {

    // Determine whether the logged in person is caller or receiver
    const callerPhone: string = (callStatus == MEETING_INVITATION.INVITE) ? this.personPhone : (this.meetingInvitation.clrPh || this.personPhone);
    const isCaller: boolean = (this.personPhone === callerPhone);
    let invitation: any = {};

    const temp = {
      callStatus: callStatus,
      receiverPhone: receiverPhone,
      callerPhone: callerPhone,
      isCaller: isCaller
    }

    console.log('temp', temp);


    // FOR THE CALLER
    // When caller INVITES for a meeting
    if (isCaller && callStatus == MEETING_INVITATION.INVITE) {

      invitation = {
        "id": receiverPhone,      // receiver's document id
        "clrPh": callerPhone,
        "rcvrPh": receiverPhone,  // receiver's document id
        "clStts": callStatus,
        "chnl": "",               // Channel will be created when receiver accepts the invitation
        "tkn": "",                // Token will be created when receiver accepts the invitation
        "strtTm": new Date(),      // Start Time. Just to comply with schema.  This will be created by server when receiver accepts the invitation
        "endTm": new Date()        // End Time. Just to comply with schema.  This will be created by server when receiver accepts the invitation
      };

      console.log('new invitation', invitation);

      this.meetingInvitation = Object.assign({}, invitation);

      this.setCallStatusMessage();
    }

    else if (isCaller) {
      // Caller LEFT, ENDED

      invitation = Object.assign({}, this.meetingInvitation, {
        "id": this.meetingInvitation.rcvrPh,   // receivers's document id
        "clStts": callStatus
      })

      this.meetingInvitation.clStts = callStatus;   // This is to prevent responding to live updates after leaving/ending the call
      this.setCallStatusMessage();

      switch (callStatus) {
        case MEETING_INVITATION.LEFT:
          this.callStatusMessage = `You have left the meeting`;
          break;
        case MEETING_INVITATION.ENDED:
          this.callStatusMessage = `You have ended the meeting`;
          break;
      }

    }

    // FOR THE RECEIVER
    else if (!isCaller) {

      // ACCEPT, REJECT, LEFT
      invitation = Object.assign({}, this.meetingInvitation, {
        "id": this.meetingInvitation.clrPh,   // caller's document id
        "clStts": callStatus
      });

      this.meetingInvitation.clStts = callStatus;   // This is to prevent responding to live updates after leaving/ending the call
      this.setCallStatusMessage();

      switch (callStatus) {
        case MEETING_INVITATION.LEFT:
          this.callStatusMessage = `You have left the meeting`;
          break;
      }

    }

    // For the local variable.  
    // This will anyway be updated once the onSnapshot() receives database changes.
    // In some cases like Caller ended meeting will not receive realtime update to Caller.
    // In such situation, this is useful to update the screen.
    // this.meetingInvitation.clStts = callStatus;

    // Update receiver's record with meeting invitation
    await this.meetingService.update(invitation);

  }


  ngOnDestroy(): void {
    try {
      this.unsubscribeMeeting();  // Unsubscribe to meeting listener          
    } catch (error) {

    }

  }


  // =================== AGORA VIDEO ==================

  startCall() {
    // Pass the channel key, channel name, and user ID to the method
    /**
     * Channel key: String used for broadcast security. For low security requirements, pass null as the parameter value.
     * Channel name: String that provides a unique channel name for the Agora session. This should be a numerical value 
     * for the Web SDK. The sample app uses channel.value (the value from the Channel UI text field).
     * 
     * User ID: The user ID is a 32-bit unsigned integer ranging from 1 to (2^32-1). If you set the user ID to null, 
     * the Agora server allocates a user ID and returns it in the onSuccess callback. If you decide to enter a specific user ID, 
     * make sure the integer is unique or an error will occur.
     */

    const userId = parseInt( this.personPhone.substr(2) );
    console.log('eeeeeeeeeeeee', this.meetingInvitation.tkn, this.meetingInvitation.chnl, null);
    
    this.agoraService.client.join(this.meetingInvitation.tkn, this.meetingInvitation.chnl, null, (uid) => {
      console.log('uid===============', uid);
      
      this.localStream = this.agoraService.createStream(uid, true, null, null, true, false);
      this.localStream.setVideoProfile('720p_3');
      this.subscribeToStreams();
    });
  }

  private subscribeToStreams() {
    this.localStream.on("accessAllowed", () => {
      console.log("accessAllowed");
    });
    // The user has denied access to the camera and mic.
    this.localStream.on("accessDenied", () => {
      console.log("accessDenied");
    });

    this.localStream.init(() => {
      console.log("getUserMedia successfully");
      this.localStream.play('agora_local');
      this.agoraService.client.publish(this.localStream, function (err) {
        console.log("Publish local stream error: " + err);
      });
      this.agoraService.client.on('stream-published', function (evt) {
        console.log("Publish local stream successfully");
      });
    }, function (err) {
      console.log("getUserMedia failed", err);
    });
  }

  // ==================================================

}