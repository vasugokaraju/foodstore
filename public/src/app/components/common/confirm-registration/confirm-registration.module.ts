import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmRegistration } from './confirm-registration.component';
import { ButtonModule } from 'primeng/button';
import { FileUploadModule } from 'primeng/fileupload';
import { MenubarModule } from 'primeng/menubar';


@NgModule({
    imports: [
        CommonModule,
        ButtonModule,
        FileUploadModule,
        MenubarModule
    ],
    declarations: [ConfirmRegistration],
    providers: [],
    exports:[CommonModule, ConfirmRegistration],    // Export the component so that it can be used as child
    bootstrap: [ConfirmRegistration]
})
export class ConfirmRegistrationModule { }