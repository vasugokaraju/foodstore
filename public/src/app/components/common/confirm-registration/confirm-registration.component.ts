import { Component, EventEmitter, Injectable, Input, OnInit, Output, ViewChildren } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Menubar } from 'primeng/menubar';

@Injectable({ providedIn: "root" }) // allows this child component injectable in parent constructor

@Component({
  selector: 'app-confirm-registration',
  templateUrl: './confirm-registration.component.html',
  styleUrls: ['./confirm-registration.component.css']
})
export class ConfirmRegistration implements OnInit {

  @Input() email:string = "";
  @Input() emailSubject:string = "";

  ngOnInit(): void {
  }


}