// https://github.com/josdejong/jsoneditor/blob/master/docs/api.md


import { AfterViewInit, Component, EventEmitter, Injectable, Input, OnInit, Output } from '@angular/core';
import { ADMIN_TOOLBAR_COMMANDS } from '@app/components/admin/admintoolbar/admintoolbar.component';
import { Helper } from '@app/helpers';
import { LANGUAGES } from '@app/interfaces/IAppInterfaces';
import { AppConfSchemaService } from '@app/services/appConf.schema.service';
import { AppDataService } from '@app/services/appData.service';
import { NotifyService } from '@app/services/notify.service';
import JSONEditor from 'jsoneditor';
import { MenuItem } from 'primeng/api';

@Injectable({ providedIn: "root" }) // allows this child component injectable in parent constructor

@Component({
  selector: 'app-json-editor',
  templateUrl: './jsonEditor.component.html',
  styleUrls: ['./jsonEditor.component.css']
})
export class JSONEditorComponent implements OnInit, AfterViewInit {
  private _jsonData: any;

  @Input() set jsonData(value: any) {
    this._jsonData = value;


    // Set default values for language and label to avoid schema validation error prompts
    if (this.editMode === ADMIN_TOOLBAR_COMMANDS.NEW) {
      // NOTE: Setting the language name instead of code is causing the field length schema exception.
      // this.jsonData.lng = this.languages[0];
      this.jsonData.lbl = this.appConfGroups[0];
    }
    else{
      // NOTE: Setting the language name instead of code is causing the field length schema exception.
      // const langName:any = Object.entries(LANGUAGES).find(([key, value]) => value === this._jsonData.lng);
      // this.jsonData.lng = (langName)? langName[0] : ""; // Set language Name instead of code for better user experience
    }

    try {
      this.jsonEditor?.set(this.jsonData);
      this.jsonEditor?.expandAll();
    } catch (error) {
      
    }

    // Set template based on on app config group name selected from the dropdown
    setTimeout(() => {
      const elemSelector: string = ".jsoneditor-tree > select";
      const elements: NodeListOf<Element> = document.querySelectorAll(elemSelector); // This should return two SELECT elements (languages and labels)

      if (elements && elements.length > 1) {
        // Bind to change event of 'labels' list
        elements.item(1).addEventListener('change', async (event: any) => {
          const templateId = event.target.options[event.target.options.selectedIndex].innerHTML;

          await this.setTemplate(templateId);
        });

      }

    }, 10);


    this.setSchema();
    this.setTemplate(this.jsonData.lbl || this.appConfGroups[0]);  // By default, get the template of first item in conf groups.  
  }

  get jsonData(): any {
    return this._jsonData;
  }

  @Input() jsonDataSchema: any;
  @Input() editMode: string;


  @Output() onJSONUpdateEmitter: EventEmitter<any> = new EventEmitter();  // Sends communication to Parent component

  jsonEditor: any;
  viewModes: MenuItem[];
  viewMode: string
  editorHeight: number = 650;
  editorFontSize: number = 13;
  isJSONValid: boolean = true;

  appConfGroups: string[] = [""];
  languages: string[] = [""];

  constructor(
    private notify: NotifyService,
    private helper: Helper,
    private appDataService: AppDataService,
    private appConfSchemaService: AppConfSchemaService
  ) {

    // Collect language names to use as enums
    this.languages = [];
    for (const lang in LANGUAGES) {
      this.languages.push(LANGUAGES[lang])
    }

    // Get appConfig group names
    this.appConfGroups = this.appDataService.appConfigGroups;
  }

  ngOnInit(): void {
    this.viewModes = [
      {
        id: "tree",
        label: 'Tree',
        styleClass: 'adminToolbarMenuSelected'
      },
      {
        id: "code",
        label: 'Code',
        styleClass: 'adminToolbarMenuUnselected'
      },
      {
        id: "form",
        label: 'Form',
        styleClass: 'adminToolbarMenuUnselected'
      },
      {
        id: "text",
        label: 'Text',
        styleClass: 'adminToolbarMenuUnselected'
      },
      {
        id: "view",
        label: 'View',
        styleClass: 'adminToolbarMenuUnselected'
      },
      {
        id: "preview",
        label: 'Preview',
        styleClass: 'adminToolbarMenuUnselected'
      }
    ];

  }

  ngAfterViewInit(): void {
    const readOnlyFields: string[] = ["id", "lng", "lbl", "data"];

    try {

      // Initialize json editor and load data if available
      const container = document.getElementById("jsoneditor")
      const options = {

        templates: [],

        onChange: (): any => {
          // console.log('onChange');

        },

        onChangeJSON: (json): any => {
          // console.log('onChangeJSON', json);

        },

        onChangeText: (json): any => {
          // console.log('onChangeText', json);

        },

        onEditable: (node: any): (boolean | { field: boolean, value: boolean }) => {

          let rtn: any = { field: false, value: true };   // DISABLE FIELD EDIT IN ALL MODES
          let depth: number = node.path?.length || -1;
          let readOnly: boolean = readOnlyFields.includes(node.field);

          // If edit mode, disable value edit for readonly fields
          if (this.editMode !== ADMIN_TOOLBAR_COMMANDS.NEW && readOnly && depth == 1) {
            rtn.value = false;
          }

          // For page labels schema, allow field editing from depth 2 onwards
          
          if(this._jsonData.lbl.endsWith("pagelabels") && depth > 1){
            rtn.field = true;
            rtn.value = true;
          }


          return rtn;
        },

        onClassName: (node: any): any => {

          // If this is not new record, disable the readonly fields.
          // return (this.editMode !== 'new' && readOnlyFields.includes(node.field) && node.path.length == 1) ? "disabled" : undefined;
          return (this.editMode !== 'new' && readOnlyFields.includes(node.field) && node.path.length == 1) ? "disabled" : undefined;
        },

        // This triggers only when json syntax is invalid.
        // This does not trigger for schema errors if "$async": true in the schema
        onValidationError: (errors: []): any => {

          // Set the flag based on the error count.
          this.isJSONValid = (errors.length == 0);
          errors.forEach((error: any) => {

            switch (error.type) {
              case 'validation': // schema validation error
                this.notify.showError("Schema Errors", (error.error?.message || error.message), true)
                break;
              case 'customValidation': // custom validation error
                console.log('customValidation error');
                break;
              case 'error':  // json parse error
                // console.log("Line", error.line);
                // console.log("Message", error.message);
                break;
            }
          });

          this.isJSONValid = (errors.length == 0);
        },

        onError: (error: any): any => {
          console.log('onError', error);

        },

        onValidate: (json) => {
          var errors = [];
          // console.log('onValidate', json);
          return errors;
        }
      }

      this.jsonEditor = new JSONEditor(container, options,); // Initialize editor
      this.jsonEditor.set(this.jsonData);                   // set data
      if (this.jsonDataSchema) {
        // this.jsonEditor.setSchema(this.jsonDataSchema);
        this.setSchema();
        this.jsonEditor.refresh();
      }

    } catch (error) {
      console.log('JSONEditor Error', error);

    }
  }

  // Sets schema based on edit mode.
  // Sets enum values for New records
  setSchema() {

    if (this.editMode === ADMIN_TOOLBAR_COMMANDS.NEW) {

      const _newSchema = this.helper.deepClone(this.jsonDataSchema);

      if (_newSchema) {
        // Limit the user input of lng and lbl to provided list.
        _newSchema.properties = Object.assign(
          {},
          _newSchema.properties,
          {
            lng: { enum: this.languages },
            lbl: { enum: this.appConfGroups }
          }
        );

        this.jsonEditor?.setSchema(_newSchema);

      }
    }
    else {
      this.jsonEditor?.setSchema(this.jsonDataSchema);
    }

  }

  // Set app config group specific template so that it can be selected from context menu in the json editor
  async setTemplate(templateId: string) {
    // Get the template based on the id and populate in 'data' array if empty
    
    
    const dataTemplate = await this.appConfSchemaService.getAppConfRecordTemplate(templateId);
    let _template:any = {
      text: `${templateId} data template`,
      title: 'Insert Data Template',
      className: 'jsoneditor-type-text'
    }

    // Depends on the template type, add the field/value to the template.
    if(templateId.endsWith("menu")){
      _template.value = dataTemplate; // Inserts this in the data[] array;
    }
    else if(templateId.endsWith("pagelabels")){
      // Adds field/value to data{} object
      _template.field = Object.keys(dataTemplate)[0]
      _template.value = dataTemplate[_template.field];
    }

    // Set app config group specific template so that it can be selected from context menu in the json editor
    this.jsonEditor.options.templates = [_template];
  }

  async onJSONUpdate() {

    let updatedJSON:any = this.jsonEditor.get();

    if (this.editMode !== ADMIN_TOOLBAR_COMMANDS.NEW){
      // If the edit mode is not new, make sure the document 'id' other non editable fields remain the same
      updatedJSON.id = this.jsonData.id;
      updatedJSON.lbl = this.jsonData.lbl;
      updatedJSON.lng = this.jsonData.lng;
    }
    
    this.onJSONUpdateEmitter.emit(updatedJSON);
  }

  onViewModeClicked(e: any) {

    if (!e.target) return;
    let id = "";

    try {
      // If target ID is not available, it could be one of the filter buttons.  Look for the parent button id
      id = (e.target.id === "") ? e.target.closest("button").id : e.target.id;
    } catch (error) {
      return;
    }

    if (!id || id === "") return;

    this.viewMode = id;
    this.underlineSelectedMenu(this.viewMode);
    this.jsonEditor.setMode(this.viewMode);   // Set new mode

    // Set editor height
    this.editorHeight = 650;
    // this.editorFontSize = (this.viewMode == "code")? 14.5: 15; // Cursor position is disturbed if font size is changed like this.
    this.jsonEditor.refresh();
    let elem: Element = document.getElementsByClassName('ace_content')[0];
    if (elem) elem.setAttribute("style", `font-size:${this.editorFontSize}px`);


    try {
      this.jsonEditor.expandAll();
    } catch (error) { }

  }

  underlineSelectedMenu(menuId: string) {
    if (!this.viewModes) return;

    this.viewModes.forEach(item => {
      item.styleClass = 'adminToolbarMenuUnselected'
      if (item.id == menuId) {
        item.styleClass = 'adminToolbarMenuSelected';
      }
    });

  }

  getLanguages(){
    let rtn:string = JSON.stringify(LANGUAGES);
    return rtn;
  }

}