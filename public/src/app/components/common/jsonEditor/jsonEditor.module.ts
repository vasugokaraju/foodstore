import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JSONEditorComponent } from './jsonEditor.component';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { MenubarModule } from 'primeng/menubar';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ButtonModule,
        InputTextareaModule,
        TableModule,
        MenubarModule
    ],
    declarations: [JSONEditorComponent],
    providers: [],
    exports:[CommonModule, JSONEditorComponent],    // Export the component so that it can be used as child
    bootstrap: [JSONEditorComponent]
})

export class JSONEditorModule { }