import { AfterViewInit, Component, NgZone, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router, Event as routeEvent, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { Title } from '@angular/platform-browser'
import { AppDataService } from './services/appData.service';
import { ValidationService } from './services/validation.service';
import { OverlayPanel } from 'primeng/overlaypanel';
import { NotifyService } from './services/notify.service';
import { fromEvent, merge } from 'rxjs';
import { filter, distinctUntilChanged } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
import { environment } from '@environments/environment';
import { SessionService } from './services/session.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, AfterViewInit {
    @ViewChildren(OverlayPanel) public opErrors: QueryList<OverlayPanel>;
    validationMessages: string[] = [];  // Feeds error messages data to template

    // To keep an eye on localStorage.  Logout if application critical variables are deleted using browser debug window
    private localStorage$ = fromEvent<StorageEvent>(window, "storage").pipe(
        filter(event => event.storageArea === localStorage)
    );


    // Events to observe to clear notifications.
    private dblclickEvent$ = fromEvent<MouseEvent>(window, "dblclick");
    private clickEvent$ = fromEvent<MouseEvent>(window, "click");

    private escapeKey$ = fromEvent<KeyboardEvent>(window, 'keyup').pipe(
        filter((e: KeyboardEvent, index: number) => e.key === 'Escape'),
        distinctUntilChanged()
    )

    // Observable to clear all notifications on double-click or escape key
    private clearMessages$ = merge(this.clickEvent$, this.dblclickEvent$, this.escapeKey$)


    inactiveDuration: number = -1;
    remainingSessionTime: number = -1;

    constructor(
        private router: Router,
        private title: Title,
        private notify: NotifyService,
        private auth: AuthService,
        public appData: AppDataService,
        public session: SessionService,
        public ngZone: NgZone
    ) {

        this.router.events.subscribe((event: routeEvent) => {
            if (event instanceof NavigationStart) {
                // Show loading indicator
            }

            if (event instanceof NavigationEnd) {
                // Hide loading indicator

                // TODO: When /home route is called, it is configured to redirect the request to /home/dashboard.  But the title is showing /home which is wrong.
                // Load route specific page titles here
                // this.title.setTitle(event.url);
                // this.notify.clear();

            }

            if (event instanceof NavigationError) {
                // Hide loading indicator

                // Present error to user
                console.log("NavigationError", event.error);
            }
        });

// This is to keep an eye on localstorage
/*
        this.localStorage$.subscribe(data => {
            if (environment.localStorageKeys.includes(data.key)) {
                this.notify.showError("Application Data Tampered", `Application data '${data.key}' has been tampered by user.  Please login again.`, true);
                this.auth.logout("/login");
            }
        })
*/

        // Clear all notifications on Escape key OR double-click
        this.clearMessages$.subscribe(data => {
            this.notify.clear();
        })
    }


    /**
     * Subscribes to `this.session.getUserInactivityDuration` and `this.session.getRemainingSessionTime` observables
     */
    ngOnInit() {
        // A value to determine whether to show/hide countdown message
        // In case the user resumed activity while the countdown warning message is visible, this value makes the warning message hidden
        // Otherwise the warning message is visible until inactivity counter starts again.
        this.session.inactivityDuration$.subscribe(elapsed => {
           /**
            * The inactive timer mechanism runs outside angular zone.
            * In this mode no outside changes are rendered on UI.
            * use ngZone.run() method to update UI.
            */
            this.ngZone.run(() => {
                this.inactiveDuration = elapsed

                // A value to determine whether to show/hide countdown message
                // This value is primarily to make the warning message visible for n number of seconds before session timeout
                this.remainingSessionTime = this.appData.inactivityThreshold - elapsed

                if (this.remainingSessionTime == 0) {
                    this.notify.clear();
                    if (this.appData.loggedIn) {
                        this.notify.showErrorMessage('Session Timed Out', ' due to inactivity.');
                        this.auth.logout("/welcome");
                    }
                }
            });
        });

    }

    public ngAfterViewInit(): void {

        /** 
         * Subscribe to ValidationService to allow the controls across the sytem to use the tooltip.
         * Other components can use ValidationService service to operate the tooltip (OverlayPanel).
         * See login.component.ts for sample.
         * 
         * e: This parameter receives 'false' to hide tooltip.  It may receive an array with click event and form control to show the tooltip
        */
        ValidationService.validationTooltip
            .subscribe((e: Array<any> | Boolean) => {

                if (e === false) {
                    Promise.resolve(null).then(() => this.opErrors.first.hide());
                }
                else if (Array.isArray(e)) {   // When user clicks on control, you get click event and the from control in array

                    let [event, form] = e;
                    let controlName = event.target.getAttribute('formControlName'); // Get the name of the input control that raised the click event

                    // Get validation error data for given component.
                    let controlErrorData = ValidationService.getControlErrorData(form, controlName);
                    // Get validation error info for given component.
                    let controlErrorInfo = ValidationService.getFormErrorInfo(form, controlErrorData);

                    this.validationMessages = [];   // Collect the user-friendly error messages to show in tooltip
                    Object.keys(controlErrorInfo[controlName]).forEach(item => {
                        this.validationMessages.push(controlErrorInfo[controlName][item]);
                    })

                    // If there are any validation errors, show them in tooltip
                    if (this.validationMessages.length > 0) {

                        /**
                         * OverlayPanel is causing ExpressionChangedAfterItHasBeenCheckedError 
                         * Check https://github.com/primefaces/primeng/issues/8839 for possible workaround
                         * Issue can be reproduced by entering invalid data in controls and 
                         * rapidly switching between controls by clicking on them while toggling shift key on keyboard.
                         */

                        /**
                         * Promise or setTimeout are the best methods to avoid ExpressionChangedAfterItHasBeenCheckedError 
                         * but the issue is with PrimeNG.
                         */

                        Promise.resolve(null).then(() => this.opErrors.first.show(event));

                    }
                }
            })
    }

}