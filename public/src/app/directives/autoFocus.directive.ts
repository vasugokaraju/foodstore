import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[autoFocus]',
})
export class AutoFocusDirective {

    constructor(el: ElementRef){
        el.nativeElement.focus();
        
    }
}