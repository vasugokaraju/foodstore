export class Menu {
    title: string;
    route: string;
    icon: string;
    active: boolean;
    dropDownClass: string;
    subMenu: any;
}