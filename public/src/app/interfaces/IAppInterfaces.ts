// Roles
export enum ROLES{ 
	"SUPERADMIN"=0,
	"MANAGER"=1.1,
	"SUPERVISOR"=2.2,
	"USER"=3.3
}

// Interface for notification data type
export interface INotification{
    severity:string,
    summary:string,
    detail:string,
    key:string,
    life:number,
    sticky:boolean,
    showTransitionOptions?:string,
    hideTransitionOptions?:string
}

export enum NOTIFICATION_SEVERITY{
    success = 'success',
    info = 'info',
    warn = 'warn',
    error = 'error'
}


// Interface for User Role
export interface IRole{
    code:number;
    name:string;
}

// Interface for Code-Name record
export interface ICodeName{
    code:string;
    name:string;
}


// Languages
export enum LANGUAGES{
    Telugu = 'te-IN',
    Kannada =  'kn-IN',
    Tamil =  'ta-IN',
    Malayalam =  'ml-IN',
    Hindi =  'hi-IN',
    English =  'en-US',
}

// File export options
export enum EXPORT_OPTIONS {
    JSON = "JSON",
    CSV = "CSV",
    EXCEL = "EXCEL",
    PDF = "PDF"
}

// Meeting call status
export enum MEETING_INVITATION {
    INVITE = 0,
    ACCEPTED = 1,
    REJECTED = 2,
    PROGRESS = 3,
    ENDED = 4,
    LEFT = 5
}