export interface ISummaryReport {
	field1?:string;
	field2?:number;
	field3?:boolean;
	field4:{
		};
	field5?:number;
	field6?:Date;
	field7?:Array<any>;
}


export interface IFinanceReport {
	field1?:string;
	field2?:number;
	field3?:boolean;
	field4:{
		};
	field5?:number;
	field6?:Date;
	field7?:Array<any>;
}