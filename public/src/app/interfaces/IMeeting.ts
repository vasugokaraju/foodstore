// Interface for complete record
// Firebase does not store id as part of collection, thus id field is not part of collection definition, thus need to be added externally.
// When read or readWhere queries are called, the api adds id field to each record.
export interface IMeeting {
    id:string;
	mtngId?:string;
	clrPh:string;
	rcvrPh:string;
	chnl?:string;
	tkn?:string;
	clStts?:number;
	strtTm?:Date;
	endTm?:Date;
}