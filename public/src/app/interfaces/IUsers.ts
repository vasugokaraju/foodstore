// Interface for complete record
// Firebase does not store id as part of collection, thus id field is not part of collection definition, thus need to be added externally.
// When read or readWhere queries are called, the api adds id field to each record.
export interface IUsers {
    id:string;
	displayName:string;
	email:string;
	password:string;
	phoneNumber:string;
	emailVerified?:boolean;
	disabled?:boolean;
	photoURL?:string;
	role:{
		code?:number;
		name?:string;
		};
	dob:Date;
	ssn?:number;
	address:{
		city:{
		colony:string;
		foundedon?:Date;
		};
		country:string;
		};
}