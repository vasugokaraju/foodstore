// Interface for complete record
// Firebase does not store id as part of collection, thus id field is not part of collection definition, thus need to be added externally.
// When read or readWhere queries are called, the api adds id field to each record.
export interface IProducts {
    id:string;
	name:string;
	desc?:string;
	price?:number;
}