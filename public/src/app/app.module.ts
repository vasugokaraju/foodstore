import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from '@app/app.component';
import { AppRoutingModule } from '@app/app.routes';

import { AppDataService } from '@app/services/appData.service';
import { DashboardService } from '@app/services/dashboard.service'
import { WindowService } from '@app/services/window.service'
import { UsersService } from '@app/services/users.service'
import { ProductsService } from '@app/services/products.service'
import { CartsService } from '@app/services/carts.service'
import { MeetingService } from '@app/services/meeting.service'
import { AppCodsService } from '@app/services/appCods.service'
import { AppConfService } from '@app/services/appConf.service'
import { ReportsService } from './services/reports.service';

import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { MenubarModule } from 'primeng/menubar';
import { MenuModule } from 'primeng/menu';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ToastModule } from 'primeng/toast';
import { RippleModule } from 'primeng/ripple';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { ProgressBarModule } from 'primeng/progressbar';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TabViewModule } from 'primeng/tabview'

import { WelcomeComponent } from '@app/pages/welcome.component';
import { HomeComponent } from '@app/components/home/home.component';
import { AuthComponent } from '@app/components/auth/auth.component';
import { AdminComponent } from '@app/components/admin/admin.component';
import { ReportsComponent } from '@app/components/reports/reports.component';
import { AppMenuComponent } from '@app/app.menu.component';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HTTPIntercept } from '@app/services/http.interceptor';
import { MessageService } from 'primeng/api';
import { ValidationService } from '@app/services/validation.service';

import { AuthenticationService } from '@app/services';
import { appInitializer } from '@app/helpers';
import { AuthService } from '@app/services/auth.service';
import { SessionService } from '@app/services/session.service';
import { TooltipModule } from 'primeng/tooltip';


import { HTTPTrafficFilterPipeModule } from '@app/pipes/httpTrafficFilter.pipe.module';

import { AutoFocusDirective } from './directives/autoFocus.directive';


// TODO: Import providers APP_INITIALIZER, HTTP_INTERCEPTORS from respective class to reduce deps services here

@NgModule({
  declarations: [
    AppComponent, WelcomeComponent, HomeComponent, AuthComponent, AdminComponent, ReportsComponent, AppMenuComponent, AutoFocusDirective
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, AppRoutingModule, ButtonModule, SplitButtonModule, DropdownModule, FormsModule, ReactiveFormsModule,
    MenubarModule, MenuModule, InputTextModule, TableModule, CardModule, ToastModule, RippleModule, OverlayPanelModule, MessageModule,MessagesModule,
    TooltipModule, ProgressBarModule, MultiSelectModule, InputNumberModule, InputSwitchModule, AutoCompleteModule, TabViewModule, HttpClientModule, HTTPTrafficFilterPipeModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HTTPIntercept, multi: true },
    { provide: APP_INITIALIZER, useFactory: appInitializer, multi: true, deps: [AuthService, SessionService, AppDataService] },
    AppDataService, DashboardService, MessageService, ValidationService, HTTPTrafficFilterPipeModule,
    WindowService,
    UsersService, 
    ProductsService, 
    CartsService, 
    MeetingService, 
    AppCodsService, 
    AppConfService, 
    ReportsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }