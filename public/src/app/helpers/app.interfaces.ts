/** Interface for http traffic record  */
export interface HTTPTrafficRecord{
    index:number;
    requestId:string;
    url:string;
    status:HTTP_REQUEST_STATUS;
    notify:boolean;
    method:string;
    priority:boolean;     // Determines whether this is a priority request.  token renewal is a priority request.
    sentAt:number;
    receivedAt?:number;
}


/** Interface for Status column filter in Traffic Monitor grid on App Monitors page */
export interface HTTPRequestStates {
    label: string,
    status: number
}


export enum HTTP_REQUEST_STATUS {
    PENDING = 0,
    RECEIVED = 1,
    CANCELLED = 2
}

export enum HTTP_REQUEST_FILTER {
    PENDING = 0,
    RECEIVED = 1,
    CANCELLED = 2,
    HIDDEN = 3,
    NOTIFICATION = 4
}

export enum FIRESTORE_QUERY_OPERATORS{
    "<" = "<",
    "<=" = "<=",
    "==" = "==",
    ">" = ">",
    ">=" = ">=",
    "!=" = "!=",
    "array-contains" = "array-contains",
    "array-contains-any" = "array-contains-any",
    "in-" = "in",
    "not-in" = "not-in"
}

// Interface to pass Where query parameters
export interface IWhereQuery{
    where:Array<any>;
    orderBy?:Array<any>;
    limit?:number;
    paginationRec?:any;
    direction?:number;
}