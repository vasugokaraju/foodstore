import { AuthenticationService } from '@app/services';
import { AppDataService } from '@app/services/appData.service';
import { AuthService } from '@app/services/auth.service'
import { SessionService } from '@app/services/session.service';

// TODO: Use @app path mapping throughout the application

export function appInitializer(
    auth: AuthService,
    session: SessionService,
    appData: AppDataService
) {
    return () => new Promise(resolve => {
        console.log("Initializing app...")

        // At the time of app initialization,
        // If token is expired, make sure loggedin status is false.
        if (auth.tokenLife <= 0) {
            auth.logout();  // trigger the logout method to clean up abandoned data.
        }
        else {

            try {
                // If userinfo is available in localStorage, load it.  This is needed if page is reloaded.
                let _userInfo = JSON.parse(localStorage.getItem('user'));
                auth.saveUserProfile(_userInfo);
            } catch (error) {}

            /**
             * At the time of app initialization, if token is available and valid
             * get new token so that new client session is initiated.
            */
            auth.stopTokenRenewTimer();
            // auth.renewToken();      // Start the token renewal timer
        }

        if (appData.loggedIn) {
            auth.startTokenRenewTimer();
            session.start();
        }

        resolve(true);

    });
}