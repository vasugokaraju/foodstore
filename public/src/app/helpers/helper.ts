import { ElementRef, Injectable, QueryList } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as jp from "jsonpath";
import { FIRESTORE_QUERY_OPERATORS } from './app.interfaces';
import { AppDataService } from '@app/services/appData.service';
import { Parser } from 'json2csv';
import { saveAs } from 'file-saver';

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

@Injectable({ providedIn: 'root' })
export class Helper {
    constructor(
        private appData: AppDataService,
    ) { }

    // Converts ISO date string to date object
    stringToDate<T>(data: Array<T>, dateFields: string[]): Array<T> {
        data.forEach(rec => {

            dateFields.forEach(path => {
                let dtValue = jp.query(rec, "$." + path)[0];
                jp.value(rec, "$." + path, new Date(dtValue));
            });
        });

        return data;
    }


    // Converts date object to ISO date string.
    dateToString<T>(data: T, dateFields: string[]): T {

        dateFields.forEach(path => {
            let dtValue = jp.query(data, "$." + path)[0];
            if (typeof dtValue.getMonth === 'function') jp.value(data, "$." + path, dtValue.toISOString());
        });

        return data;
    }


    // Builds JSON data out of FormGroup controls.
    formToJson<T>(form: FormGroup): T {
        let json: any = Object.keys(form.controls).reduce((aggregator: any, field: any) => {
            aggregator[field] = form.controls[field].value;
            return aggregator;
        }, {});

        let data: T = <T>json;

        return data;
    }


    // Turns the first letter to uppercase
    public initCaps(str: string) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }



    /**
     * Takes an object/JSON and returns an array of all paths.
     */
    public getPaths(obj: any, prefix: string = ''): any {
        let paths: Array<string> = [];

        Object.keys(obj).forEach(key => {
            let path = prefix + key;
            paths.push(path);
            if (typeof obj[key] === 'object') {
                paths.push(...this.getPaths(obj[key], path + "."));
            }
        })

        return paths;

    }

    /**
     * Updates JSON value at given path, including array
     */    
    public updateJSON(obj: any, keyPath: any, value: any) {

        keyPath = keyPath.split('.');  // split key path string
        let lastKeyIndex = keyPath.length - 1;
        for (var i = 0; i < lastKeyIndex; ++i) {
            const key = keyPath[i];

            // choose if nested object is array or hash based on if key is number
            if (!(key in obj)) obj[key] = parseInt(key) !== parseInt(key) ? {} : []
            obj = obj[key];
        }
        obj[keyPath[lastKeyIndex]] = value;
    }


    /**
     * Returns role name for given role code
     */
    public getRoleName(roleCode:number){
        const role = this.appData.roles.find(item => item.code == roleCode);
        return role? role.name:"";
    }


    /** Helper function to convert seconds to HH MM SS */
    seconds2HHMMSS(seconds: number) {
        if (isNaN(seconds)) return seconds;

        let date = new Date(0);
        date.setSeconds(seconds);
        return date.toISOString().substr(11, 8);
    }



    /**
     * Resets element value to blank.  Generally used to reset list view/grid filter fields.
     * @param element element reference
     */
    resetElementValue(element: any) {
        
        let elementType: string = (element.nativeElement) ? element.nativeElement.type : element.constructor.name.toLowerCase();
        
        switch (elementType) {
            case "text":
            case "number":
                element.nativeElement.value = null;
                break;
            case "dropdown":
                element.updateSelectedOption(null);
                element.inputFieldValue=null;
                break;
            case "calendar":
                element.value = null;
                element.inputFieldValue=null;
                break;
            case "inputswitch":
                element.value = null;
                element.checked = null;
                break;
        }
    }


    /**
     * Builds database query with filter values
     * Initially developed to build where query but later switched to table.filter() method
     */
     buildFlterQuery(elements:QueryList<ElementRef<any>>) {

        let query: Array<any> = [];
        
        if(!elements) return query;

        elements.forEach((element: any) => {
            // element.nativeElement.type for "input" controls
            // element.constructor.name.toLowerCase(); for controls like Calendar, InputSwitch
            let elementType: string = (element.nativeElement) ? element.nativeElement.type : element.constructor.name.toLowerCase();

            let databaseFieldName: string = '';
            let operator: string = '';

            switch (elementType) {
                case "text":
                case "number":
                    databaseFieldName = element.nativeElement.attributes["data-field-name"].value; // Value should be the name of the database collection field
                    const operatorCheck2 = element.nativeElement.value.substr(0, 2);    // Get the first two characters of the input
                    const operatorCheck1 = element.nativeElement.value.substr(0, 1);    // Get the first one characters of the input

                    operator = FIRESTORE_QUERY_OPERATORS[operatorCheck2] || FIRESTORE_QUERY_OPERATORS[operatorCheck1] || "==";
                    const regex = new RegExp(operator); // To remove operator part from search string
                    const searchValue = element.nativeElement.value.replace(regex, "")   // Remove the operator from search string

                    if (searchValue.length > 0) query.push([databaseFieldName, operator, (elementType == "text") ? searchValue : parseInt(searchValue)]);
                    break;

                case "dropdown":
                    databaseFieldName = element.el.nativeElement.attributes["data-field-name"].value;
                    // Update this condition as needed.
                    operator = ">=";
                    if (element.value) query.push([databaseFieldName, operator, element.value.code]);
                    break;

                case "calendar":
                    databaseFieldName = element.el.nativeElement.attributes["data-field-name"].value;
                    // Update this condition as needed.
                    operator = ">=";
                    if (element.value) query.push([databaseFieldName, operator, element.value]);
                    break;

                case "inputswitch":
                    databaseFieldName = document.getElementById(element.inputId).attributes["data-field-name"].value;
                    operator = "==";
                    query.push([databaseFieldName, operator, element.checked]);
                    break;

                default:
                    break;
            }

        });

        return query;
    }


    /**
     * Transforms primeng filter object to firebase where criteria
     */
    filter2Where(filter:any){
        let where = [];

        for(let f in filter ){
            where.push( [f, filter[f].matchMode, filter[f].value] );
        }

        // Sort the where criterias so that ">=" operator fields move to the top to comply with firebase where query
        where = where.sort((a, b) => a[1].localeCompare(b[1])).reverse();

        return where;
    }    



    public getMonthName(monthIndex:number){
        return monthNames[monthIndex];
    }

    /**
     * Provides start and end range for the given month
     * NOTE:  The month number will be incremented to facilitate queries against ISODate text format records in firebase
     */
    public getMonthRange(dt:Date){
        let dt1 = new Date(dt);
        dt1.setDate(1)

        let dt2 = new Date(dt);
        
        dt2.setHours(0);
        dt2.setMinutes(0);
        dt2.setSeconds(0);
        dt2.setMilliseconds(0)
        dt2.setDate(1)   // First day of given month
        dt2.setMonth(dt2.getMonth()+1)  // Now it is first day of next month
        dt2.setMilliseconds(-1); // Now it is last day of dt that was passed.

        // To facilitate string based date search in database, increment 0 based month number
        let month = (dt1.getMonth()+1).toString().padStart(2,'0');

        return `${dt1.getFullYear()}-${month}-01T00:00:00.000Z~${dt2.getFullYear()}-${month}-${dt2.getDate().toString().padStart(2,'0')}T23:59:59.999Z`;
    }


    /**
    * Extracts operstors from filter value and compares it against given value using the same operator
    * @param value - an alphanumeric string which might include operator
    */
    public compareAlphaNumeric(value:number, filter:string){
        let isMatch:boolean = true;     
        let operator:any = filter.match(/[^\d\.]+/);    // Get operator from filter value, if any
        operator = operator?operator[0].trim():"==";    // If operator available, get it and trim it.  If not use == as operator

        let num:any = filter.match(/[\d\.]+/);      // Get number from user entered filter value
        
        num = (num && num[0])? parseFloat(num[0]):"";

        // If a number is available in filter string, do comparison
        // If a number is NOT available in filter string, return true so that user can still see data as he/she might still be typing
        if(typeof num == "number"){ 
            try {
                isMatch = eval(value+operator+num);
            } catch (error) {
                // Ignore error as some are expected
            }
        }

        return isMatch;
    }


    // Duplicate object of any depth
    deepClone<T>(source: T): T {
        return Array.isArray(source)
            ? source.map(item => this.deepClone(item))
            : source instanceof Date
                ? new Date(source.getTime())
                : source && typeof source === 'object'
                    ? Object.getOwnPropertyNames(source).reduce((o, prop) => {
                        Object.defineProperty(o, prop, Object.getOwnPropertyDescriptor(source, prop));
                        o[prop] = this.deepClone(source[prop]);
                        return o;
                    }, Object.create(Object.getPrototypeOf(source)))
                    : source as T;
    }

    // Validate phone number to comply with E.164 standard
    validatePhoneNumber(phoneNum:string): boolean {
        const re = new RegExp(/^\+[1-9]\d{1,14}$/);
        return re.test(phoneNum);
    }

    // Takes an object and returns the value of "code" field.
    // This is useful to convert dropdown selected value which is object to its code value.
    /**
     * 
     * @param obj that contains key value pair like code and name
     * @param valueField is the name of field that that contains the value to be returned.
     * @returns 
     */
     objectToCode(obj:any, valueField:string = "code"){
        return (typeof obj !== "string")? obj[valueField] : obj;
    }


    /**
     * Takes array of objects and returns matching record
     * @param data contains an array to search in
     * @param valueField contains the field name to compare
     */
    codeToObject(data:Array<any>, valueField:string = "code", value:any){
        return data.find(item => item[valueField] == value);
    }


    /**
     * Copies values from code-name object to actual data object
     * This is usually done to copy from user selected value of autocomplete 
     * to data object before creating/updating record in db.
     * @param source provides {code:"", name:""} to copy data from
     * @param destination is actual data object that will be used to create/update db.
     * @param codeDataField contains the name of the filed to copy code destination to. ex: destination.codeDataField
     * @param nameDataField contains the name of the filed to copy name destination to. ex: destination.nameDataField
     */
    codeName2RecordMapping(source:any, destination:any, codeDataField:string, nameDataField:string){
        destination[codeDataField] = source[codeDataField];
        destination[nameDataField] = source[nameDataField];
        return destination;
    }


    // Make sure the password has special characters
    checkPasswordStrength(password: string, minLen:number, maxLen:number) {

        if (password.length < minLen || password.length > maxLen) return false;

        let hasUpperCase = /[A-Z]/.test(password);
        let hasLowerCase = /[a-z]/.test(password);
        let hasNumbers = /\d/.test(password);
        let hasNonalphas = /\W/.test(password);

        return (hasUpperCase && hasLowerCase && hasNumbers && hasNonalphas);
    }


    // Increase the last character to next char
    incrementString(key) {
        if (key === 'Z' || key === 'z') {
          return String.fromCharCode(key.charCodeAt() - 25) + String.fromCharCode(key.charCodeAt() - 25); // AA or aa
        } else {
          var lastChar = key.slice(-1);
          var sub = key.slice(0, -1);
          if (lastChar === 'Z' || lastChar === 'z') {
            // If a string of length > 1 ends in Z/z,
            // increment the string (excluding the last Z/z) recursively,
            // and append A/a (depending on casing) to it
            return this.incrementString(sub) + String.fromCharCode(lastChar.charCodeAt() - 25);
          } else {
            // (take till last char) append with (increment last char)
            return sub + String.fromCharCode(lastChar.charCodeAt() + 1);
          }
        }
      };
    
 
    /**
     * Converts data to csv format. Sets field names as headers
     * @param data provides an array of values
     * @param fields provides an array of field names
     * @returns an array of csv rows
     */
    public json2csv(data: Array<any>, fields: Array<string>): any {
        const opts = { fields };
        const parser = new Parser(opts);
        const csv = parser.parse(data); // Parse data array to csv

        return csv;
    }


    /**
     * Takes csv string with first row as header row and returns json array
     * @param data provides csv with first row as header
     */
    public csv2json(data:string){
        let csvDataArray = data.split(/\r?\n/);
        let csvHeader = csvDataArray.splice(0,1)[0].split(","); // Take the first row as header values
  
        let rows:Array<any> = [];

        csvDataArray.forEach(item => {
          const data = this.csvToArray(item);

          let row:any={};
          csvHeader.forEach((header, index) => {
            row[header] = data[index];
          });

          if(item.length > 0 ) rows.push(row);
        });

        return rows;
    }


    // Save (download) content as csv file
    saveAsCSV(data: Array<any>, fields: Array<string>, fileName:string){
        const csvData = this.json2csv(data, fields);
        const file = new File([csvData],fileName,{ type: "text/plain;charset=utf-8" });
        saveAs(file);
    }


    // Save (download) content as json file
    saveAsJSON(json: Array<any>, fileName:string){
        const jsonData = JSON.stringify(json, null, 2) ;
        const file = new File([jsonData],fileName,{ type: "text/plain;charset=utf-8" });
        saveAs(file);
    }


    // Converts text to csv value, including the text that might have quotes avoud text
    csvToArray(text) {
        var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
        var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    
        // Return NULL if input string is not well formed CSV string.
        if (!re_valid.test(text)) return null;
    
        var a = []; // Initialize array to receive values.
        text.replace(re_value, // "Walk" the string using replace with callback.
            function(m0, m1, m2, m3) {
    
                // Remove backslash from \' in single quoted values.
                if (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
    
                // Remove backslash from \" in double quoted values.
                else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
                else if (m3 !== undefined) a.push(m3);
                return ''; // Return empty string.
            });
    
        // Handle special case of empty last value.
        if (/,\s*$/.test(text)) a.push('');
        return a;
    };


    // Upgrades where query as firebase-starts-with query.
    firebaseStartsWith(where:Array<any>){
        let firstWhereCondition:Array<any> ;
        let _where:Array<any> = [];

        if (where && where.length > 0) {

            firstWhereCondition = (where[0] instanceof Array)? where[0] : where;

            if(firstWhereCondition[1] == ">="){
                _where.push(firstWhereCondition);
                _where.push( [firstWhereCondition[0], "<", this.incrementString(firstWhereCondition[2]) ] )
            }
            else {
                _where = where;
            }
        }
        else {
            _where = where;
        }

        return _where;
    }


    // Determines whether an action button can be shown
    showActionButton(route:string, role:string, action:string){
        const path:string =`$..[?(@.routerLink=="${route}")]['${role.toLowerCase()}']['${action.toLowerCase()}']`;
        const match = jp.query(this.appData.mainMenu.value, path );

        // Return false if no matches found.  Else, return matched value
        return (match.length == 0)? false : match[0];
    }

}