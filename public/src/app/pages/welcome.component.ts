import { Component } from '@angular/core';
import {AppDataService} from '../services/appData.service'

@Component({
    templateUrl: './welcome.component.html'
})
export class WelcomeComponent {

    constructor(
        private appDataService: AppDataService
    ){
        
    }
}