import { Injectable } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { AppDataService } from './appData.service';


@Injectable({ providedIn: "root" })
export class DeleteMeService {
    private defaultMenu: MenuItem[] = [
        {
            label: 'Register',
            icon: 'pi pi-fw pi-user-plus',
            routerLink: '/auth/register'
        },
        {
            label: 'Forgot Password',
            icon: 'pi pi-fw pi-key',
            routerLink: '/auth/forgot'
        },
        {
            label: 'Login',
            icon: 'pi pi-fw pi-sign-in',
            routerLink: '/auth/login'
        }
    ]
    private _menuData = new BehaviorSubject<MenuItem[]>(this.defaultMenu);

    constructor(
        private appData: AppDataService
    ) { }

    public getMenuData() {

        let menuData: MenuItem[];

        // AuthService.loggedIn is causing circular reference issue
        if (localStorage.getItem('loggedin') === 'true') {
            menuData = [
                { label: 'Home', routerLink: ['/home/dashboard'], icon: 'pi pi-fw pi-home' },
                {
                    label: 'Admin',
                    icon: 'pi pi-fw pi-user',
                    items: [
                        {
                            label: 'Users',
                            routerLink: '/admin/users'
                        },                    
                        {
                            label: 'Products',
                            routerLink: '/admin/products'
                        },                    
                        {
                            label: 'Carts',
                            routerLink: '/admin/carts'
                        },                    
                        {
                            label: 'Meeting',
                            routerLink: '/admin/meeting'
                        },                    
                        {
                            label: 'AppCods',
                            routerLink: '/admin/appcods'
                        },                    
                        {
                            label: 'AppConf',
                            routerLink: '/admin/appconf'
                        },                    
                    ]
                },
                {
                    label: 'Reports',
                    icon: 'pi pi-fw pi-file',
                    items: [
                        {
                            label: 'Report 1'
                        },
                        {
                            label: 'Report 2'
                        }
                    ]
                },
                {
                    label: '|'
                },
                {
                    icon: 'pi pi-fw pi-user',
                    items: [
                        {
                            label: 'Logout',
                            icon: 'pi pi-fw pi-sign-out',
                            routerLink: '/auth/logout'
                        },
                        {
                            label: 'Change Password',
                            icon: 'pi pi-fw pi-pencil',
                            routerLink: '/auth/changePassword'
                        },
                        {
                            label: 'App Monitor',
                            icon: 'pi pi-fw pi-sliders-h',
                            routerLink: '/admin/appmonitor'
                        }
                    ]
                },
                
            ];
        }
        else{
            menuData = this.defaultMenu;
        }

        const role = localStorage.getItem('role');
        let filteredMenu: Array<object>;
        
        filteredMenu = menuData;
        
        /*
        switch (role) {
            case "USER":
                // Delete some menus
                filteredMenu = this.removeMenuItems(menuData, ["Admin", "Reports", "App Monitor"]);
                break;
            default:
                filteredMenu = this.removeMenuItems(menuData, []);
                break;
        }
        */

        this._menuData.next(filteredMenu);
    }

    // Components can subscribe to the this observable to receive tooltip status
    public get menuData(): Observable<MenuItem[]> {
        return this._menuData.asObservable();
    }


    // Filters out unwanted menu items for the give user role
    private removeMenuItems(menu:Array<object>, removeItems:Array<string>){
        return menu.filter( (item:any, i:number) => {
            
            // Recurse through sub menus
            if(item.hasOwnProperty('items')) item.items = this.removeMenuItems(item.items, removeItems);
            
            // remove matching menu items.
            return !removeItems.includes(item.label);
        } );
    }
}