import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { HTTPService } from './http.service';
import {AuthService} from './auth.service'
import { map } from 'rxjs/internal/operators/map';
import { Helper } from '@app/helpers';
import { IMeeting } from '@app/interfaces';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({providedIn:"root"})
export class MeetingService{
    constructor(
        private http: HTTPService,
        private auth: AuthService,
        private helper: Helper
    ){}

    // This method is developed for MeetingResolver to load data before the UI is loaded.
    // Load meeting data
    public preload(): Promise<any> {
        // The preload service determines what is needed and provides all the data in the reponse.
        // It identifies the user based on the token
        const endpoint = `${environment.api.host}${environment.api.meetingPreload}`;

        let body:any = {
            whereQuery:{
                where:[],
                orderBy: ['clrPh','asc']
            }
        }

        // Get the preload data.
        return new Promise((resolve, reject) => {
            // Supress notifications on receiving response.
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting preload is completed.');
                    })

        })

    }

    // This method is developed to pull complete profile when user clicks on edit
    // Load meeting data
    public readOne(id:string): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.meetingReadOne}${id}`;

        return new Promise((resolve, reject) => {
            this.http.get(endpoint,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting readOne is completed.');
                    })

        })

    }


    /**
    * Find Meeting records based on where query
    */
     public readMany(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingReadMany}`;

        let body = data;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting readMany is completed.');
                    })

        })
    } 


    /**
    * Create Meeting record
    */
    public create(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingCreate}`;
        
        // Convert date object to ISO date string.
        data = this.helper.dateToString(data, ["strtTm","endTm"]);

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting create is completed.');
                    })

        })
    }


    /**
    * Update Meeting record
    */
    public update(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingUpdate}${data.id}`;

        // Convert date object to ISO date string.
        try{
            data = this.helper.dateToString(data, ["strtTm","endTm"]);
        }
        catch(e){}
        
        delete data.id;     // Id is not needed in the data

        return new Promise((resolve, reject) => {
            this.http.put(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting update is completed.');
                    })

        })   
    }

    /**
    * Delete Meeting record
    */
    public delete(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingDelete}${data.id}`;

        return new Promise((resolve, reject) => {
            this.http.delete(endpoint,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting delete is completed.');
                    })

        });

    }    


    /**
    * Download Meeting records
    */
    public download(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingDownload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting download is completed.');
                    })

        });           
    } 


    /**
    * Upload Meeting records
    */
    public upload(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.meetingUpload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Meeting upload is completed.');
                    })

        });          
    } 



}