import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { AppDataService } from './appData.service';
import { DashboardService } from './dashboard.service';
import { AuthService } from './auth.service';
import { UsersService } from './users.service'
import { ProductsService } from './products.service'
import { CartsService } from './carts.service'
import { MeetingService } from './meeting.service'
import { AppCodsService } from './appCods.service'
import { AppConfService } from './appConf.service'
import { ReportsService } from './reports.service';
import { IWhereQuery } from '@app/helpers/app.interfaces';


@Injectable()
export class DashboardResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private dashboardService: DashboardService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {

        const data = await this.dashboardService.preload();
        return data;

    }
}


// Phone Login resolver
@Injectable()
export class OTPLoginResolver implements Resolve<any> {
    constructor(
        private authService: AuthService,
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.authService.getFirebaseAppConfig()
        return data;
    }
}



// Login resolver
@Injectable()
export class LoginResolver implements Resolve<any> {
    constructor(
        private authService: AuthService,
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.authService.getFirebaseAppConfig()
        return data;
    }
}




// Users resolver
@Injectable()
export class UsersResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private usersService: UsersService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.usersService.preload();
        return data;
    }
}

// Products resolver
@Injectable()
export class ProductsResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private productsService: ProductsService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.productsService.preload();
        return data;
    }
}

// Carts resolver
@Injectable()
export class CartsResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private cartsService: CartsService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.cartsService.preload();
        return data;
    }
}

// Meeting resolver
@Injectable()
export class MeetingResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private meetingService: MeetingService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.meetingService.preload();
        return data;
    }
}

// AppCods resolver
@Injectable()
export class AppCodsResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private appcodsService: AppCodsService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.appcodsService.preload();
        return data;
    }
}

// AppConf resolver
@Injectable()
export class AppConfResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private appconfService: AppConfService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const data = await this.appconfService.preload();
        return data;
    }
}





// SummaryReport resolver
@Injectable()
export class SummaryReportResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private reportsService: ReportsService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const whereQuery:IWhereQuery = {where:[], orderBy:[]}
        const data = await this.reportsService.getSummaryReport(whereQuery);
        return data;
    }
}

// FinanceReport resolver
@Injectable()
export class FinanceReportResolver implements Resolve<any> {
    constructor(
        private appData: AppDataService,
        private reportsService: ReportsService
    ) {}

    async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        const whereQuery:IWhereQuery = {where:[], orderBy:[]}
        const data = await this.reportsService.getFinanceReport(whereQuery);
        return data;
    }
}