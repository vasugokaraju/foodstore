import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { HTTPService } from './http.service';
import {AuthService} from './auth.service'
import { map } from 'rxjs/internal/operators/map';
import { Helper } from '@app/helpers';
import { IProducts } from '@app/interfaces';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({providedIn:"root"})
export class ProductsService{
    constructor(
        private http: HTTPService,
        private auth: AuthService,
        private helper: Helper
    ){}

    // This method is developed for ProductsResolver to load data before the UI is loaded.
    // Load products data
    public preload(): Promise<any> {
        // The preload service determines what is needed and provides all the data in the reponse.
        // It identifies the user based on the token
        const endpoint = `${environment.api.host}${environment.api.productsPreload}`;

        let body:any = {
            whereQuery:{
                where:[],
                orderBy: ['name','asc']
            }
        }

        // Get the preload data.
        return new Promise((resolve, reject) => {
            // Supress notifications on receiving response.
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Products preload is completed.');
                    })

        })

    }

    // This method is developed to pull complete profile when user clicks on edit
    // Load products data
    public readOne(id:string): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.productsReadOne}${id}`;

        return new Promise((resolve, reject) => {
            this.http.get(endpoint,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Products readOne is completed.');
                    })

        })

    }


    /**
    * Find Products records based on where query
    */
     public readMany(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsReadMany}`;

        let body = data;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products readMany is completed.');
                    })

        })
    } 


    /**
    * Create Products record
    */
    public create(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsCreate}`;
        
        // Convert date object to ISO date string.
        data = this.helper.dateToString(data, []);

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products create is completed.');
                    })

        })
    }


    /**
    * Update Products record
    */
    public update(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsUpdate}${data.id}`;

        // Convert date object to ISO date string.
        try{
            data = this.helper.dateToString(data, []);
        }
        catch(e){}
        
        delete data.id;     // Id is not needed in the data

        return new Promise((resolve, reject) => {
            this.http.put(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products update is completed.');
                    })

        })   
    }

    /**
    * Delete Products record
    */
    public delete(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsDelete}${data.id}`;

        return new Promise((resolve, reject) => {
            this.http.delete(endpoint,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products delete is completed.');
                    })

        });

    }    


    /**
    * Download Products records
    */
    public download(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsDownload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products download is completed.');
                    })

        });           
    } 


    /**
    * Upload Products records
    */
    public upload(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsUpload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products upload is completed.');
                    })

        });          
    } 


    /**
    * Returns Products data in the form of code-name for the purpose of Autocomplete controls on UI.
    */
     public getProductsIdName(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.productsIdNameReadMany }`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Products ProductsIdNameReadMany is completed.');
                    })
        })
    }


}