import { Injectable } from '@angular/core'

@Injectable()
export class WindowService {
    
    // Returns global window object
    get windowRef() {
        return window;
    }

}