import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AppDataService } from './appData.service';
import { AuthService } from './auth.service';
import { Helper } from '@app/helpers';

@Injectable({
    providedIn: 'root'
  })
export class AuthGuard implements CanActivate {
    constructor(
        private appData: AppDataService,
        private auth: AuthService,
        private router: Router,
        private helper: Helper
        ) {}

    canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {

        // Check whether this role is allowed to access this route
        const allowed = this.helper.showActionButton(state.url, this.appData.userRole.name, "allow");

        if(this.appData.loggedIn && allowed){
            return true;
        }
        else{
          this.auth.redirectUrl = state.url;  // Store it so that requested page can be loaded after successful login.
          return this.router.navigateByUrl("/auth/login");
        }

    }
}