import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';


@Injectable({ providedIn: "root" })
export class ValidationService {

    // Holds the names of controls for which values are changed and onChange event is triggered
    // This helps to show the tool tip only after value is changed and user cames out of the control
    // This mechanism gives the user a chance to enter data without any validation error prompts
    private static changedControls: any = {};

    private static formGroup: FormGroup;

    private static tooltipStatus = new BehaviorSubject<any>(false);


    constructor() {

    }


    static showValidationTooltip(e: Event, form: AbstractControl) {
        this.tooltipStatus.next([e, form]);
    }

    static hideValidationTooltip() {
        this.tooltipStatus.next(false);
    }

    // Components can subscribe to the this observable to receive tooltip status
    static get validationTooltip(): Observable<boolean> {
        return this.tooltipStatus.asObservable();
    }


    // Returns user-friendly validation messages for given error
    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config = {
            required: 'Required',
            requiredtrue: 'Should be checked',
            pattern: `Should match ${validatorValue.requiredPattern} pattern.`, // Better to build a custom validator than using this
            minlength: `Minimum length ${validatorValue.requiredLength}`,   // For string input
            maxlength: `Maximum length ${validatorValue.requiredLength}`,   // For string input
            min: `Minimum ${validatorValue.requiredLength}`,   // For number input
            max: `Maximum ${validatorValue.requiredLength}`,   // For number input
            email: 'Invalid email address',
            invalidCreditCard: 'Is invalid credit card number',
            invalidEmailAddress: 'Invalid email address',
            invalidPassword: 'Invalid password. Password must be at least 6 characters long, and contain a number.',

        };

        return config[validatorName];
    }

    // Custom validation for credit card number
    static creditCardValidator(control: FormControl) {
        // Visa, MasterCard, American Express, Diners Club, Discover, JCB
        if (
            control.value.match(
                /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/
            )
        ) {
            return null;
        } else {
            return { invalidCreditCard: true };
        }
    }

    // Custom validation for email
    static emailValidator(control: FormControl) {
        // RFC 2822 compliant regex
        if (
            control.value.match(
                /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
            )
        ) {
            return null;
        } else {
            return { invalidEmailAddress: true };
        }
    }

    // Custom validation for password
    static passwordValidator(control: FormControl) {
        // {6,100}           - Assert password is between 6 and 100 characters
        // (?=.*[0-9])       - Assert a string has at least one number
        if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
            return null;
        } else {
            return { invalidPassword: true };
        }
    }

    // Returns error control and raw error data for all invalid controls in the given form
    static getFormErrorData(form: AbstractControl) {
        if (form instanceof FormControl) {
            // Return FormControl errors or null
            return form.errors ?? null;
        }
        if (form instanceof FormGroup) {
            const groupErrors = form.errors;
            // Form group can contain errors itself, in that case add'em
            const formErrors = groupErrors ? { groupErrors } : {};
            Object.keys(form.controls).forEach(key => {
                // Recursive call of the FormGroup fields
                const error = this.getFormErrorData(form.get(key));
                if (error !== null) {
                    // Only add error if not null
                    formErrors[key] = error;
                }
            });
            // Return FormGroup errors or null
            return Object.keys(formErrors).length > 0 ? formErrors : null;
        }
    }

    // Returns user-friendly error description for each error of each control in the given form
    static getFormErrorInfo(form: AbstractControl, errorData: any) {
        let errorInfo = {};

        // Loop through each control (that has error) in the form
        for (let controlName in errorData) {
            let control: AbstractControl = form.get(controlName);
            errorInfo[controlName] = {}

            // Loop through each error of the given component
            for (let exceptionPropName in errorData[controlName]) {

                // If data is changed by the user
                if (control.errors.hasOwnProperty(exceptionPropName)) {
                    errorInfo[controlName][exceptionPropName] = this.getValidatorErrorMessage(exceptionPropName, control.errors[exceptionPropName]);
                }
            }
        }

        return errorInfo;
    }

    // Returns error data for given control.  This is helpful to get active element error data to show tooltip.
    static getControlErrorData(form: AbstractControl, controlName: string) {
        // Return FormControl errors or null
        let control = form.get(controlName);
        let err = control.errors ?? null;
        let rtn = {};
        rtn[controlName] = err

        return rtn;
    }

    // Adds tooltip layer to given formgroup which automatically checks for validation errors as user enters data.
    static bindFormControls(formGroup: FormGroup) {

        this.formGroup = formGroup;
        this.changedControls = {};

        // Once the form is loaded and ready, subscribe to formgroup change events to capture the names of the controls for which value are changed and focus is removed.
        // This helps to show the tool tip only after value is changed and user came out of the control
        // This mechanism gives the user a chance to enter data without any validation error prompts
        if (document.querySelectorAll("input").length > 0) {

            fromEvent(document.querySelectorAll("input"), 'change').subscribe((e: any) => {
                this.changedControls[e.target.getAttribute("formcontrolname")] = true;
            })

            

            // Get the list of controls for which validation error messages are to be shown when clicked on respective control
            fromEvent(document.querySelectorAll("input"), 'click').subscribe((e: any) => {
                // if the data is changed and invalid, then show tooltip
                if (this.isDirtyAndErrors(e)) ValidationService.showValidationTooltip(e, this.formGroup);
            })


            fromEvent(document.querySelectorAll("input"), 'keyup').subscribe((e: KeyboardEvent) => {
                // As user types, validate the errors and update the error list in tooltip
                if (this.isDirtyAndErrors(e)) ValidationService.showValidationTooltip(e, this.formGroup);
                else ValidationService.hideValidationTooltip();  // If all errors are fixed, hide the tooltip
            })

            
            fromEvent(document.querySelectorAll("input"), 'blur').subscribe((e: any) => {
                // In case where tooltip is visible for a control and user presses tab and shifts focus to different control
                // The tooltip should be hidden.
                ValidationService.hideValidationTooltip();
            })


            // To hide the validation messages on mouseout
            fromEvent(document.querySelectorAll("input"), 'mouseout').subscribe((e: MouseEvent) => {
                ValidationService.hideValidationTooltip();
            })

        }
    }

    // Helper function to determine whether to show the validator tooltip
    static isDirtyAndErrors(e: any) {
        let controlName = e.target.getAttribute('formControlName');
        let control = this.formGroup.get(controlName);
        let rtn: boolean = false;


        /**
         * If the form's submitted status is false, validation tooltip is shown only if the data is changed 
         * and focus is shifted to another control ( onChange event). This behavior gives the user a chance 
         * to enter data without tooltip distraction.
         * 
         * If the form's submitted status is true, tooltip is shown when clicked/focused on control.
         */


        // this.changedControls contains fields for which data is changed and focus is moved out of control.
        // This helps to prevent tooltip while entering data first time in the control.
        rtn = (e.target.form && e.target.form.getAttribute("data-submitted") === 'true')? 
                    (control && control.errors !== null) : 
                    (this.changedControls.hasOwnProperty(controlName) && control && control.errors !== null);
        return rtn;
    }
}