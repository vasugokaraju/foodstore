import { Injectable } from '@angular/core';
import { MessageService, PrimeNGConfig } from 'primeng/api';
import { INotification, NOTIFICATION_SEVERITY } from '../interfaces/IAppInterfaces';
import { Helper } from '@app/helpers';

@Injectable({
    providedIn: 'root'
})
export class NotifyService {
    constructor(
        private messageService: MessageService,
        private primengConfig: PrimeNGConfig,
        private helper: Helper
    ) {
        this.primengConfig.ripple = true;
    }


    // Show notification using p-toast component
    show(options: INotification) {
        // 'key' is used to target a specific tag, for example <p-toast position="top-left" key="tl"></p-toast>
        if (!options.key) options.key = "top_right_toast";

        this.messageService.add(options);
    }

    // Show multiple messages at once
    showMultiple(options: INotification[]) {
        this.messageService.addAll(options);
    }

    // simple method to show success notification
    showSuccess(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.success, summary: summary, detail: detail, key: key, life: life, sticky: sticky });
        });
    }

    // simple method to show warn notification
    showInfo(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.info, summary: summary, detail: detail, key: key, life: life, sticky: sticky });
        });
    }

    // simple method to show warn notification
    showWarn(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.warn, summary: summary, detail: detail, key: key, life: life, sticky: sticky });
        });
    }

    // simple method to show error notification
    showError(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.error, summary: summary, detail: detail, key: key, life: life, sticky: sticky });
        })
    }

    showConfirm(summary: string, detail: string) {
        setTimeout( () => {
            this.messageService.clear();
            this.show({ severity: NOTIFICATION_SEVERITY.warn, summary: summary, detail: detail, key: 'c', life: 15000, sticky: true });
        })
    }

    // Show messages using p-messages component.
    showErrorMessage(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.error, summary: summary, detail: detail, key: "top_center_message", life: life, sticky: sticky });
        })
    }

    // Show messages using p-messages component.
    showWarningMessage(summary: string, detail: string, sticky: boolean = false, key: string = null, life: number = 15000) {
        setTimeout( () => {
            this.show({ severity: NOTIFICATION_SEVERITY.warn, summary: summary, detail: detail, key: "top_center_message", life: life, sticky: sticky });
        })
    }

     onConfirm() {
        this.messageService.clear('c');
    }

    onReject() {
        this.messageService.clear('c');
    }

    clear() {
        this.messageService.clear();
    }

    buildNotifications(errors:Array<any>, key:string="top_right_toast", life: number = 15000): Array<INotification>{
        let notifications:Array<INotification> = [];

        if(Array.isArray(errors)){

            errors.forEach(error => {
                notifications.push({
                    severity: NOTIFICATION_SEVERITY.error,
                    summary: error.title,
                    detail: this.helper.initCaps(error.path.replace(/body./,'') + ' ' + error.message.toLowerCase() + '.'),
                    key:key,
                    life: life,
                    sticky:true
                })
            });
            
        }
        else{
            let err:any = Object.assign({}, errors);
            notifications.push({
                severity: NOTIFICATION_SEVERITY.error,
                summary: err.schemaName,
                detail: err.message,
                key:key,
                life: life,
                sticky:true
            });
        }

        return notifications;
    }
}