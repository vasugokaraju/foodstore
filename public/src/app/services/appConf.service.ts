import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { HTTPService } from './http.service';
import {AuthService} from './auth.service'
import { map } from 'rxjs/internal/operators/map';
import { Helper } from '@app/helpers';
import { IAppConf } from '@app/interfaces';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({providedIn:"root"})
export class AppConfService{
    constructor(
        private http: HTTPService,
        private auth: AuthService,
        private helper: Helper
    ){}

    // This method is developed for AppConfResolver to load data before the UI is loaded.
    // Load appConf data
    public preload(): Promise<any> {
        // The preload service determines what is needed and provides all the data in the reponse.
        // It identifies the user based on the token
        const endpoint = `${environment.api.host}${environment.api.appConfPreload}`;

        let body:any = {
            whereQuery:{
                where:[],
                orderBy: ['lbl']
            }
        }

        // Get the preload data.
        return new Promise((resolve, reject) => {
            // Supress notifications on receiving response.
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf preload is completed.');
                    })

        })

    }

    // This method is developed to pull complete profile when user clicks on edit
    // Load appConf data
    public readOne(id:string): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.appConfReadOne}${id}`;

        return new Promise((resolve, reject) => {
            this.http.get(endpoint,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf readOne is completed.');
                    })

        })

    }


    /**
    * Find AppConf records based on where query
    */
     public readMany(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfReadMany}`;

        let body = data;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf readMany is completed.');
                    })

        })
    } 


    /**
    * Create AppConf record
    */
    public create(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfCreate}`;
        
        // Convert date object to ISO date string.
        data = this.helper.dateToString(data, []);

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf create is completed.');
                    })

        })
    }


    /**
    * Update AppConf record
    */
    public update(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfUpdate}${data.id}`;

        // Convert date object to ISO date string.
        try{
            data = this.helper.dateToString(data, []);
        }
        catch(e){}
        
        delete data.id;     // Id is not needed in the data

        return new Promise((resolve, reject) => {
            this.http.put(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf update is completed.');
                    })

        })   
    }

    /**
    * Delete AppConf record
    */
    public delete(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfDelete}${data.id}`;

        return new Promise((resolve, reject) => {
            this.http.delete(endpoint,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf delete is completed.');
                    })

        });

    }    


    /**
    * Download AppConf records
    */
    public download(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfDownload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf download is completed.');
                    })

        });           
    } 


    /**
    * Upload AppConf records
    */
    public upload(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.appConfUpload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('AppConf upload is completed.');
                    })

        });          
    } 



}