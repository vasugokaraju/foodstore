import { HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTPTrafficRecord, HTTP_REQUEST_STATUS } from '@app/helpers/app.interfaces';
import { environment } from '@environments/environment';
import { BehaviorSubject, EMPTY, Observable, ObservableInput, Subject, throwError } from 'rxjs';
import { catchError, filter, finalize, map, switchMap, take, takeUntil, tap } from 'rxjs/operators';
import { v4 as uuid } from 'uuid';
import { AppDataService } from './appData.service';
import { AuthService } from './auth.service';
import { HttpCancelService } from './http.service';
import { NotifyService } from './notify.service';


// TODO:  Break this service into multiple files like error.interceptor.ts, jwt.interceptor.ts
// TODO: If silent request fails, retry n number of times.

@Injectable({
    providedIn: 'root'
})
// This service helps to catch HTTP errors and also cancel pending requests
export class HTTPIntercept implements HttpInterceptor {

    constructor(
        private httpCancelService: HttpCancelService,
        private notify: NotifyService,
        private appData: AppDataService,
        private auth: AuthService
    ) { }

    private requestCounter: number = 0;

    refreshTokenInProgress = false;

    tokenRefreshedSource = new Subject();
    tokenRefreshed$ = this.tokenRefreshedSource.asObservable();
    tokenBehaviorSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    // Add headers to current request
    addHeaders(request: HttpRequest<any>, x_request_id: string) {

        // Add general headers
        // Set x-request-id to http request for unique identification
        let headers = {
            'Content-Type': 'application/json',
            'x-request-id': x_request_id      // Uniquely identify each request
        }

        // For login route, the Authorization header is used to pass user credentials and this header is set in the login component and it should not be changed
        if (!request.url.endsWith(environment.api.login) && this.auth.token) {

            headers['Authorization'] = `Bearer ${this.auth.token}`;     // Add token header
            headers['tknuid'] = this.auth.tknuid;     // Add token uid header
            headers['providerId'] = this.auth.providerId;     // Add providerId header

            // Add login status header to determine at the server whether the token should be validated.
            // If loggedin == false, the server ignores the request
            // If loggedin == true, the server attempts to validate the token
            // Helps to avoid storing tokens on the server-side to handle logout situation while the tokens are still valid.
            headers['loggedin'] = this.appData.loggedIn.toString();
        }

        const newReq = request.clone({ setHeaders: headers, withCredentials: true });    // Return cloned request with updated headers
        // this.examineRequest(newReq);
        return newReq;
    }

    handleErrorResponse(error: HttpErrorResponse, request?: HttpRequest<any>, next?: HttpHandler): Observable<HttpEvent<any>> {
        let msg = '';
        let title = '';

        // client-side error
        if (error.error instanceof ErrorEvent) {
            title = "Browser Error";
            msg = `Error: ${error.error.message}`;
        }
        else {
            // server-side errors
            switch (error.status) {
                case 400:   // Business error
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);
                    break;
                case 401:   // Invalid token error
                case 403:   // Access denied error
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);

                    // this.auth.logout("/login");     // Logout and redirect to login page.
                    break;
                case 404:   // Server error
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);
                    break;
                case 500:   // Server error
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);
                    break;
                case 503:   // Maintenance error
                    // Redirect to the maintenance page
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);
                    break;
                default:
                    title = (error.error.title || error.statusText);
                    msg = (error.error.message || error.message);
                    break;
            }
        }

        if(error.status == 422){    // Unprocessable Entity (Schema exceptions)
            this.notify.showMultiple(this.notify.buildNotifications(error.error.data))
        }
        else{
            this.notify.showError(title, msg, true);
        }

        return throwError(error.error);
    }

    handleSuccessResponse(request: HttpRequest<any>, event: HttpEvent<any>): HttpEvent<any> {
        if (event instanceof HttpResponse) {
            
            // this.examineResponse(event);

            /**
             * By default notifications are shown when response is received.
             * Some requests prefer not to show notifications. Such requests carry a custom http header called 'notification=false'
             * If this request has no specific instruction to hide the notification
             */
            if (request.headers.get("notification") !== "false") {

                if (event.body.hasOwnProperty("title") && event.body.hasOwnProperty("message")) {
                    // this.notify.showSuccess(event.body.title, event.body.message + ' ' + event.url);
                    this.notify.showSuccess(event.body.title, event.body.message);
                }
                else {
                    this.notify.showWarn("Warning", "Request is successful but expected data is missing.\n" + event.url + " " + JSON.stringify(event.body))
                }
            }
        }

        return event;
    }

    finalizeRequest(latestResp: HttpEvent<any>, error: HttpErrorResponse, httpTrafficRec: HTTPTrafficRecord) {

        // If this is cancelled request, skip the rest of the code
        if (latestResp.type === HttpEventType.Sent && !error) {

            // Propagate cancelled request
            httpTrafficRec.receivedAt = new Date().getTime();
            httpTrafficRec.status = HTTP_REQUEST_STATUS.CANCELLED;
            // httpTrafficRec.notify = false;
            this.appData.httpTraffic.next(httpTrafficRec)

            this.appData.loading = !(this.appData.regularRequestCount === 0);

            return;
        }

        // Propagate received request
        httpTrafficRec.receivedAt = new Date().getTime();
        httpTrafficRec.status = HTTP_REQUEST_STATUS.RECEIVED;
        this.appData.httpTraffic.next(httpTrafficRec);

        this.appData.loading = !(this.appData.regularRequestCount === 0);
        return;
    }


    updateTrafficData(request: HttpRequest<any>, x_request_id: string, priority: boolean = false): HTTPTrafficRecord {

        let notify:boolean;
        try {
            notify = !request.headers.has("notification")
        } catch (error) {
            notify = false;
        }
        
        // Store current request metadata locally
        let httpTrafficRec: HTTPTrafficRecord = {
            index: ++this.requestCounter,
            requestId: x_request_id,
            url: request.url,
            status: HTTP_REQUEST_STATUS.PENDING,
            notify: notify,   // this header exists only to hide the notification
            method: request.method,
            priority: priority,
            sentAt: new Date().getTime()
        }
        // Propagate pending request
        this.appData.httpTraffic.next(httpTrafficRec);

        return httpTrafficRec;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // BEFORE SENDING REQUEST

        let x_request_id = uuid();
        let latestResp: HttpEvent<any>;      // holds latest response object to check its cancel state in the finalize method
        let error: HttpErrorResponse;


        // AFTER RECEIVING RESPONSE

        // Token renew request
        if (request.url.endsWith(environment.api.renewToken)) {
            this.tokenBehaviorSubject.next(true);
            request = this.addHeaders(request, x_request_id);

            // Store current request metadata locally
            // This is only needed for the purpose of appMonitor page.
            let httpTrafficRec: HTTPTrafficRecord = this.updateTrafficData(request, x_request_id, true);

            return next.handle(request).pipe(
                tap((response: HttpEvent<any>) => {
                    latestResp = response;      // get the response object to check its cancel state in the finalize method
                    return response;
                }),
                catchError(err => {
                    error = err;
                    return this.handleErrorResponse(err, request, next);
                }),
                finalize(() => {
                    this.tokenBehaviorSubject.next(false);
                    return this.finalizeRequest(latestResp, error, httpTrafficRec);
                })
            )
        }
        else {  // All other requests

            /** 
             * By default the pending requests are cancelled.
             * There might be some requests like renewToken that happens periodically behind the scenes.
             * When such requests are triggered, the system holds the requests that are generated by user activity.
             */
            return this.tokenBehaviorSubject.pipe(
                // allows requests to go through only if token renewal is not in progress
                filter(isTokenRenewalInProgress => isTokenRenewalInProgress === false),
                // take one if there are multiple waiting
                take(1),
                // Cancel all pending requests if onCancelPendingRequests is triggered
                takeUntil(this.httpCancelService.onCancelPendingRequests()),
                // switchMap is not cancelling any pending requests as the value is not Observable
                switchMap(() => {
                    request = this.addHeaders(request, x_request_id);

                    // Store current request metadata locally
                    // This is only needed for the purpose of appMonitor page.
                    let httpTrafficRec: HTTPTrafficRecord = this.updateTrafficData(request, x_request_id);

                    /** 
                    * This variable is to control the show/hide state of loading animation for the user initiated requests.
                    * The loader animation should remain visible until all pending requests are returned.
                    * 
                    * Silent requests initiated by the timers should not be counted as loading animation is muted for them.
                    */
                    if (httpTrafficRec.notify === true) this.appData.loading = true;


                    // Send request and handle the response
                    return next.handle(request)
                        .pipe(

                            tap((response: HttpEvent<any>) => {
                                latestResp = response;      // get the response object to check its cancel state in the finalize method
                                return response;
                            }),

                            // Handle http error response
                            catchError((err: HttpErrorResponse) => {
                                error = err;
                                return this.handleErrorResponse(err, request, next);
                            }),

                            // Show notification for successful responses
                            map(event => {
                                return this.handleSuccessResponse(request, event);
                            }),

                            // Cancel all pending requests
                            takeUntil(this.httpCancelService.onCancelPendingRequests()),

                            finalize(() => {
                                return this.finalizeRequest(latestResp, error, httpTrafficRec);
                            })
                        )

                }),
                finalize(() => {
                    return;
                })
            )
        }
    }

    examineRequest(req:HttpRequest<any>){
        console.info(`*****************OUT GOING ${req.urlWithParams}******************`);
        console.log('URL:', req.urlWithParams, '\nwithCredentials:', req.withCredentials);
        console.log('HEADERS:');
        req.headers.keys().map(key => {console.log( `${key}: ${req.headers.get(key)}`) });
        console.info('********************************************');
    }

    examineResponse(res:HttpEvent<any>){
       
        if (res instanceof HttpResponse) {
            console.info(`*****************IN COMING ${res.url}******************`);
            console.log("Response", res);
            console.log('HEADERS:');
            res.headers.keys().map(key => {console.log( `${key}: ${res.headers.get(key)}`) });
    
            console.info('********************************************');
        }
        
    }

}



// export const AuthInterceptorProvider = {
//     provide: HTTP_INTERCEPTORS,
//     useClass: AuthInterceptor,
//     multi: true
// };