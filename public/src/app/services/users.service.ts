import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { HTTPService } from './http.service';
import {AuthService} from './auth.service'
import { map } from 'rxjs/internal/operators/map';
import { Helper } from '@app/helpers';
import { IUsers } from '@app/interfaces';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({providedIn:"root"})
export class UsersService{
    constructor(
        private http: HTTPService,
        private auth: AuthService,
        private helper: Helper
    ){}

    // This method is developed for UsersResolver to load data before the UI is loaded.
    // Load users data
    public preload(): Promise<any> {
        // The preload service determines what is needed and provides all the data in the reponse.
        // It identifies the user based on the token
        const endpoint = `${environment.api.host}${environment.api.usersPreload}`;

        let body:any = {
            whereQuery:{
                where:[],
                orderBy: ['displayName','asc']
            }
        }

        // Get the preload data.
        return new Promise((resolve, reject) => {
            // Supress notifications on receiving response.
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        // Transform data like date strings to date objects
                        response.data.usersData = this.helper.stringToDate<IUsers>(response.data.usersData, ["dob","address.city.foundedon"]);
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Users preload is completed.');
                    })

        })

    }

    // This method is developed to pull complete profile when user clicks on edit
    // Load users data
    public readOne(id:string): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.usersReadOne}${id}`;

        return new Promise((resolve, reject) => {
            this.http.get(endpoint,{},true,false)
                .subscribe(
                    response => {
                        // Transform data like date strings to date objects
                        response.data = this.helper.stringToDate<IUsers>(response.data, ["dob","address.city.foundedon"]);
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Users readOne is completed.');
                    })

        })

    }


    /**
    * Find Users records based on where query
    */
     public readMany(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersReadMany}`;

        let body = data;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        // Transform data like date strings to date objects
                        response.data = this.helper.stringToDate<IUsers>(response.data, ["dob","address.city.foundedon"]);
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users readMany is completed.');
                    })

        })
    } 


    /**
    * Create Users record
    */
    public create(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersCreate}`;
        
        // Convert date object to ISO date string.
        data = this.helper.dateToString(data, ["dob","address.city.foundedon"]);

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users create is completed.');
                    })

        })
    }


    /**
    * Update Users record
    */
    public update(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersUpdate}${data.id}`;

        // Convert date object to ISO date string.
        try{
            data = this.helper.dateToString(data, ["dob","address.city.foundedon"]);
        }
        catch(e){}
        
        delete data.id;     // Id is not needed in the data

        return new Promise((resolve, reject) => {
            this.http.put(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users update is completed.');
                    })

        })   
    }

    /**
    * Delete Users record
    */
    public delete(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersDelete}${data.id}`;

        return new Promise((resolve, reject) => {
            this.http.delete(endpoint,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users delete is completed.');
                    })

        });

    }    


    /**
    * Download Users records
    */
    public download(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersDownload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users download is completed.');
                    })

        });           
    } 


    /**
    * Upload Users records
    */
    public upload(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersUpload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users upload is completed.');
                    })

        });          
    } 


    /**
    * Returns Users data in the form of code-name for the purpose of Autocomplete controls on UI.
    */
     public getUsersIdDisplayName(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.usersIdDisplayNameReadMany }`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Users UsersIdDisplayNameReadMany is completed.');
                    })
        })
    }


}