import { Injectable } from '@angular/core';
import { HTTPTrafficRecord, HTTP_REQUEST_STATUS } from '@app/helpers/app.interfaces';
import { IRole } from '@app/interfaces/IAppInterfaces';
import { MenuItem } from 'primeng/api/menuitem';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';


@Injectable()
export class AppDataService {

    constructor() {

        /** 
         * Count the Regular and Priority requests that are in progress
         */
        this.httpTraffic$
            .pipe(filter(data => data.url !== null))
            .subscribe((data: HTTPTrafficRecord) => {
                // console.log('url', data.url.split('/api')[1], 'status', data.status, 'notify', data.notify, 'priority', data.priority);
                
                // if (data.status !== HTTP_REQUEST_STATUS.CANCELLED && data.priority === true)
                if (data.priority === true)
                    (data.status === HTTP_REQUEST_STATUS.PENDING) ? this._priorityRequestCount++ : this._priorityRequestCount--;

                if (data.priority === false) {
                    (data.status === HTTP_REQUEST_STATUS.PENDING) ? this._regularRequestCount++ : this._regularRequestCount--;
                }
            })
    }

    /** 
     * This variable is to control the show/hide state of loading animation for the user initiated requests.
     * The loader animation should remain visible until all pending requests are returned.
     * 
     * Silent requests initiated by the timers should not be counted as loading animation is muted for them.
     */
    loading: boolean = false;           // Loading animator show/hide switch

    private _regularRequestCount: number = 0;
    public get regularRequestCount() { return this._regularRequestCount };

    private _priorityRequestCount: number = 0;
    public get priorityRequestCount() { return this._priorityRequestCount };


    // Returns roles array, if available
    public get roles():Array<any>{
        try {
            this._roles = (this._roles && this._roles.length > 0)? this._roles : JSON.parse(localStorage.getItem('roles'));
            return this._roles;
        } catch (error) {
            return []
        }
    }

    // Sets roles to service variable and also stores in localstorge to handle browser is reload scenario
    public set roles(roles:Array<any>){
        this._roles = roles;
        localStorage.setItem('roles', JSON.stringify(roles));
    }

    /** Get method to read user login status.  
     * This is created primarily to avoid circular reference between auth.service and other services.
     * Since this service does not consume any other service, there will not be circural reference issue.
     */
    /**Provides user login status. */
    public get loggedIn(): boolean { return (localStorage.getItem("loggedin") === 'true') }
    public set loggedIn(v:boolean) { localStorage.setItem("loggedin",v.toString()); }


    // Returns logged in user email
    public get userEmail(): string{
        return localStorage.getItem("userEmail") || undefined;
    }
    // Sets logged in user email
    public set userEmail(eml:string){
        localStorage.setItem("userEmail", eml);
    }


    // Returns logged in user phone
    public get userPhoneNumber(): string{
        return localStorage.getItem("userPhoneNumber") || undefined;
    }
    // Sets logged in user phone
    public set userPhoneNumber(ph:string){
        localStorage.setItem("userPhoneNumber", ph);
    }


    // Returns logged in user country code
    public get userCountryCode(): string{
        return localStorage.getItem("userCountryCode") || undefined;
    }
    // Sets logged in user country code
    public set userCountryCode(countryCode:string){
        localStorage.setItem("userCountryCode", countryCode);
    }


    // Returns logged in user role
    public get userRole(): IRole{
        return localStorage.getItem("userRole")? JSON.parse(localStorage.getItem("userRole")) : undefined;
    }
    // Sets logged in user role
    public set userRole(role:IRole){
        localStorage.setItem("userRole", JSON.stringify(role));
    }


    // Holds http traffic data
    httpTraffic: BehaviorSubject<HTTPTrafficRecord> = new BehaviorSubject<HTTPTrafficRecord>({ index: 0, requestId: null, url: null, status: HTTP_REQUEST_STATUS.PENDING, notify: null, method: '', priority:false, sentAt: null, receivedAt: null });
    // Observable for traffic data
    httpTraffic$: Observable<HTTPTrafficRecord> = this.httpTraffic.asObservable();

    /** Holds http traffic record data */
    public httpTrafficData: HTTPTrafficRecord[] = [];

    /** Max number of records to hold in the above array. */
    public httpTrafficMaxRows: number = 100;           // Max httpTraffic records

    loginId: string = '';        // Current user's login id
    userName: string = '';
    email:string = '';

    userProfile: any;    // Holds user profile provided by API after successful login.
    _roles:Array<any>;   // Holds user roles related to the logged in user
    uiEntitlements: any;

    inactivityThreshold: number = 300;   // duration in seconds for session timeout due to user inactivity
    countdownThreshold: number = 60;       // seconds. Warning countdown starts when session remaining time reaches this number
    tokenRenewThreshold: number = 10;    // seconds.  Token gets renewed when its remaining life reaches this number

    // ======================================MENU===================================
    appConfigGroups:string[] = ["burgermenu","downloadmenu","mainmenu","redemptionmenu","homepagelabels"];

    public defaultMenu: MenuItem[] = [
        {
            label: 'Register',
            icon: 'pi pi-fw pi-user-plus',
            routerLink: '/auth/register'
        },
        {
            label: 'Forgot Password',
            icon: 'pi pi-fw pi-key',
            routerLink: '/auth/forgot'
        },
        {
            label: 'Login',
            icon: 'pi pi-fw pi-sign-in',
            routerLink: '/auth/login'
        }
    ]
    
    mainMenu: BehaviorSubject<MenuItem[]> = new BehaviorSubject<MenuItem[]>(this.defaultMenu);

    // =========================================================================


    showTopNav: boolean = true;   // Show or hide top navigation

    // Application wide dialog window
    showDialog: boolean = false;
    dialogHeader: any = "";
    dialogContent: any = "";

    showSessionExpireDialog: boolean = false;
    sessionDuration: number = 1440;    // minutes
    sessionCountdownDuration: number = 10;  // Count down duration in seconds
    sessionLastMinuteCountDown: number = 60;   // A running counter to show last x seconds on screen

    gridMaxRows: number = 25;
    gridPageLinks: number = 10;

}