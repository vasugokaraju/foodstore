import { Injectable, NgZone } from '@angular/core';
import { fromEvent, interval, merge, Observable, Subscription } from 'rxjs';
import { switchMap, take, tap, skipWhile } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { NotifyService } from './notify.service';
import { SessionService } from './session.service';

@Injectable({providedIn: 'root'})
export class InactiveTimerService {

    // Events to watch for to implement inactive/inactivity alert before system logout
    private inactiveTimerEvents: Array<any>[] = [
        [document, 'click'],
        [document, 'wheel'],
        [document, 'scroll'],
        [document, 'mousemove'],
        [document, 'keyup'],
        [window, 'resize'],
        [window, 'scroll'],
        [window, 'mousemove']
    ];
    private inactiveTimers$: Observable<any>[] = [];    // Array to hold fromEvent objects before merged
    private inactiveTimer$: Observable<any>;            // Merged fromEvent objects
    public inactiveObservable: Observable<any>;
    private inactiveTimerSubscription: Subscription;

    private inactivityThreshold: number = 10;    // seconds. Threshold for the inactive session before logout
    private counterThreshold: number = 10;    // seconds.  Threshold to display countdown message before session ends.
    private _timeLeftForSession: number;
    private _inactiveDuration:number;       // inactivity duration since last action


    constructor(
        private _ngZone: NgZone,
        private notify: NotifyService,
        private auth: AuthService,
        private session: SessionService
    ) {

        this.inactiveTimerEvents.forEach(evnt => {
            this.inactiveTimers$.push(fromEvent(evnt[0], evnt[1]));
        });

        this.inactiveTimer$ = merge(...this.inactiveTimers$);   // merge all event observables into one
    }


    public get timeLeftForSession(){
        return this._timeLeftForSession;
    }

    public get inactiveDuration(){
        return this._inactiveDuration;
    }

    private createObservable(): void {

        // Run outside angular for better performance.
        this._ngZone.runOutsideAngular(() => {

            this.inactiveObservable = this.inactiveTimer$
                .pipe(
                    switchMap(ev => {
                        // console.log('mouse events skipped');
                        return interval(1000).pipe(take(this.inactivityThreshold));
                    }),
                    tap(elapsed => { return (this.auth.loggedIn && this.isShowCountdown(elapsed)) }),
                    skipWhile((elapsed) => { return elapsed != this.inactivityThreshold - 1 })
                );

            this.inactivityObservable();

        })
    }

    private isShowCountdown(elapsed: number) {
        this._inactiveDuration = elapsed;
        this._timeLeftForSession = this.inactivityThreshold - elapsed;
        let showCountdown = (this._timeLeftForSession <= this.counterThreshold);

        console.log(`You have been inactive for ${this._inactiveDuration + 1} seconds.`);

        return showCountdown;
    }

    public inactivityObservable() {
        this.inactiveTimerSubscription = this.inactiveObservable.subscribe(x => {
            console.log(`User inactive for ${x + 1} sec`);
            this.unsubscribeObservable();
        });
    }

    public unsubscribeObservable() {
        console.log("Inactive timer unsubscriebd.");
        this.inactiveTimerSubscription.unsubscribe();

        /**
        * The inactive timer mechanism run outside angular zone.  In this mode no angular UI changes are rendered
        * use ngZone.run() method for UI changes to take effect.
        * 
        */
        this._ngZone.run(() => {
            this.notify.clear();
            this.session.startCountdown();
        });

    }


    public start() {
        this.createObservable();
        console.log("Inactive timer subscription started");
    }


    public stop() {
        if (this.inactiveTimerSubscription && !this.inactiveTimerSubscription.closed) {
            this.unsubscribeObservable();
        }
    }
}