import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { HTTPService } from './http.service';
import { IWhereQuery } from '@app/helpers/app.interfaces';


@Injectable({providedIn:"root"})
export class ReportsService{
    constructor(
        private http: HTTPService,
    ){}

    /**
    * Get summaryReport data
    */
     public getSummaryReport(whereQuery:IWhereQuery): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.summaryReport }`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,{whereQuery:whereQuery},{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('summaryReport request is completed.');
                    })

        })
    } 
    /**
    * Get financeReport data
    */
     public getFinanceReport(whereQuery:IWhereQuery): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.financeReport }`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,{whereQuery:whereQuery},{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('financeReport request is completed.');
                    })

        })
    } 
}