import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, interval, Observable, Subscription } from 'rxjs';
import { map, skipWhile, take, tap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { AppDataService } from './appData.service';
import { HTTPService } from './http.service';
import { NotifyService } from './notify.service';
import { SessionService } from './session.service';


@Injectable({ providedIn: "root" })
export class AuthService {
    redirectUrl: string = ''

    constructor(
        private httpService: HTTPService,
        private appData: AppDataService,
        private notify: NotifyService,
        private router: Router,
        private session: SessionService
    ) { }


    /** An observable to provide token's remaining life */
    private _tokenElapsedTime: BehaviorSubject<number> = new BehaviorSubject<number>(-1);
    private tokenRenewTimerSubscription: Subscription;

    public get tokenElapsedTime$() {
        return this._tokenElapsedTime.asObservable();
    }


    public get token(): string { return localStorage.getItem("token"); }

    public set token(v: string) {
        if (!v) {
            this.notify.showError("Token Exception", "Invalid token.");
            this.logout();
        }
        else {
            try {
                // JSON.parse(atob(v.split(".")[1]));
                localStorage.setItem("token", v);
                localStorage.setItem("loggedin", "true");   // Stores the login status in localstore
            } catch (error) {
                this.notify.showError("Token Exception", "User data format in the token is invalid.");
            }
        }
    }


    public get tknuid(): string { return localStorage.getItem("tknuid"); }
    public set tknuid(v: string) {
        localStorage.setItem("tknuid", v);
    }


    public get providerId(): string { return localStorage.getItem("providerId"); }
    public set providerId(v: string) {
        localStorage.setItem("providerId", v);
    }

    public get userProfile(): any {
        return JSON.parse(localStorage.getItem("user"));
    }

    // To test http interceptor
    public test(counter: number): Observable<any> {
        const endpoint = `${environment.api.host}${environment.api.test}?counter=${counter}`;
        return this.httpService.post(endpoint, { count: counter }, {})
    }

    // Prepares the UI on successful login
    public onUserLogin(response: any) {

        // Save user profile in localStorage
        this.saveUserProfile(response.data.userInfo);

        // Store user roles in appData
        this.appData.roles = response.data.roles;

        // Store user menu in appData
        this.appData.mainMenu.next(response.data.mainMenu);
        
        // Also store the menu in localstorage to reload when page is refreshed
        localStorage.setItem("mainMenu", JSON.stringify(response.data.mainMenu));

        // Set loggedin status as true
        this.appData.loggedIn = true;


        if (this.token) {

            // Trigger token auto renew timer
            // this.auth.renewToken();
            this.startTokenRenewTimer();

            // Observable timer to show alert when inactivity goes beyond threshold
            this.session.stop();  // stop if timer is running already. 
            this.session.start();

            // Redirect to redirectUrl page if available, else rdirect to dashboard
            (this.redirectUrl && this.redirectUrl.length > 0) ? this.router.navigateByUrl(this.redirectUrl) : this.router.navigateByUrl("/home/dashboard");
            this.redirectUrl = '';
        }


    }


    public login(login: string, password: string): Promise<any> {

        let credentials = this.encode(`${login}:${password}`);

        const endpoint = `${environment.api.host}${environment.api.login}`;

        let body = {}
        let httpOptions = {
            headers: new HttpHeaders({
                Authorization: credentials
            })
        }

        // 'Success' notification is not needed for login verification as the user will be redirected to a different page
        // Notification is shown if login fails.

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, body, httpOptions, false, false)
            .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        console.log('HTTP request completed');
                    }
                )
        });

    }

    // Gets firebase app config from server
    public getFirebaseAppConfig(): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.gfac}`;
        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, {}, {}, false, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    })
        });
    }

    // Sign in with phone
    public onSignInWithPhone(data: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.onSignInWithPhone}`;
        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, false, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    })
        });
    }


    // Check whether the phone number is registered.
    public isPhoneNumberRegistered(phoneNumber: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.isPhoneNumberRegistered}/${phoneNumber}`;
        return new Promise((resolve, reject) => {
            this.httpService.get(endpoint, {}, false, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }



    // This function is called send send email verification email on demand.
    public sendEmailVerification(data: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.sendEmailVerification}`;
        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, false, true)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    // This function is called when user clicks on link sent to his/her email after initial registration
    public verifyEmailConfirmation(token: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.verifyRegisteredEmail}?token=${token}`;
        return new Promise((resolve, reject) => {
            this.httpService.get(endpoint, {}, false, true)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    // This function is called to send email otp
    public sendEmailOTPLink(data: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.sendEmailOTPLink}`;
        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, false, true)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    // This function is called to send email otp
    public verifyEmailOTPLink(data: any): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.verifyEmailOTPLink}`;
        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, false, true)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }



    // Creates password upton verifying email
    public createPassword(data:any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.createPassword}`;

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });

    }


    public changePassword(login: string, currentPassword: string, newPassword: string): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.changePassword}`;

        let body = { login: login, currentPassword: currentPassword, newPassword: newPassword };

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, body, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    public forgotPassword(data:any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.sendForgotPasswordLink}`;

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }
    


    public verifyForgotPasswordLink(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.verifyForgotPasswordLink}`;

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }

    
    /**
     * Resets password
     * @param data new password data
     * @returns new password creation status
     */
    public resetPassword(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.resetPassword}`;

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    
    /**
     * Send Federated credential to api to login again and store token id and get user roles
     * @param data Federated credential
     * @returns user information
     */
     public federatedLogin(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.federatedLogin}`;

        return new Promise((resolve, reject) => {
            this.httpService.post(endpoint, data, {}, true, false)
                .subscribe(
                    resp => {
                        return resolve(resp);
                    },
                    error => {
                        return reject(error);
                    }
                )
        });
    }


    public logout(redirectUrl: string = "") {
        this.stopTokenRenewTimer()
        this.session.stop(); // Stop the session timer

        environment.localStorageKeys.forEach(key => {
            localStorage.removeItem(key);
        });

        this.appData.userName = "";
        this.appData.email = "";

        this.appData.mainMenu.next(this.appData.defaultMenu);

        if (redirectUrl) this.router.navigateByUrl(redirectUrl)
    }

    // Stores token and user data in localStore and setes timer to renew token 1 minute before it expires
    public saveUserProfile(userInfo: any) {
        // Store token in localStore
        this.token = userInfo.token;
        if (userInfo.tknuid) this.tknuid = userInfo.tknuid;
        if (userInfo.providerId) this.providerId = userInfo.providerId;
        this.appData.userName = userInfo.displayName;  // To show the name on the page.

        if (this.token) {
            let jwtToken = JSON.parse(atob(this.token.split(".")[1]));
            this.appData.userProfile = jwtToken;
            this.appData.userName = jwtToken.name;
            this.appData.email = jwtToken.email;
            this.appData.userRole = jwtToken.role;
            this.appData.userPhoneNumber = jwtToken.phone_number;

            // localStorage.setItem("user", JSON.stringify(jwtToken.data));
            localStorage.setItem("user", JSON.stringify(Object.assign(userInfo, jwtToken)));
        }
        else {
            this.logout();
        }

    }


    // Renew token
    public renewToken() {

        if (this.appData.loggedIn) {

            const endpoint = `${environment.api.host}${environment.api.renewToken}`;

            this.httpService.post(endpoint, {}, {}, false, false).subscribe(data => {
                this.saveUserProfile(data.data); // Store latest token and user profile in localstore
                console.log('Token has been renewed.');
                this.startTokenRenewTimer()          // call this method again for next iteration
                return;
            });

        }
    }

    // TODO:  Testing new method.
    // Renew token - testing new method
    public refreshToken(): Observable<any> {


        const endpoint = `${environment.api.host}${environment.api.renewToken}`;
        return this.httpService.post(endpoint, {}, {}, false, false)

    }


    public registerUser(data: any): Observable<any> {

        const endpoint = `${environment.api.host}${environment.api.register}`;

        let body = data;

        return this.httpService.post(endpoint, body)

    }

    /** Provides the number of seconds remaining for the token before it expires. */
    public get tokenLife(): number {
        let life = 0;
        let jwtToken: any;

        try {
            if (this.token) {
                jwtToken = JSON.parse(atob(this.token.split(".")[1]));
                const expires = new Date(jwtToken.exp * 1000);
                life = Math.round((expires.getTime() - new Date().getTime()) / 1000)
            }
            return life > 0 ? life : 0;
        } catch (error) {
            return 0;
        }

    }


    /** Provides actual life of a token in seconds.*/
    public get tokenLifespan(): number {
        let life: number = 0;
        let jwtToken: any;

        try {
            if (this.token) {
                jwtToken = JSON.parse(atob(this.token.split(".")[1]));
                const iat = new Date(jwtToken.iat * 1000);
                const exp = new Date(jwtToken.exp * 1000);

                life = (exp.getTime() - iat.getTime()) / 1000;
            }
            return life;
        } catch (error) {
            return 0;
        }

    }

    /** Provides actual life of a token in seconds.*/
    public get tokenExpTime(): string {
        let jwtToken: any;

        try {
            if (this.token) {
                jwtToken = JSON.parse(atob(this.token.split(".")[1]));
                const exp = new Date(jwtToken.exp * 1000);
                return exp.toISOString().split("T")[1].substr(0, 8);
            }

        } catch (error) {
            return error.message;
        }

    }

    public get tokenIatTime(): string {
        let jwtToken: any;

        try {
            if (this.token) {
                jwtToken = JSON.parse(atob(this.token.split(".")[1]));
                const iat = new Date(jwtToken.iat * 1000);
                return iat.toISOString().split("T")[1].substr(0, 8);
            }

        } catch (error) {
            return error.message;
        }

    }



    /** Starts a timer that provides token's remaining life */
    public startTokenRenewTimer() {
        // return; // TODO: remove this after testing
        // console.log('Token renew timer started.');

        this.stopTokenRenewTimer(); // just in case

        this.tokenRenewTimerSubscription = interval(1000)
            .pipe(
                take(this.tokenLifespan),
                map(elapsed => ++elapsed),  // Since the counter starts with 0
                skipWhile(elapsed => {

                    // time left for renewal = (Remaining life) - threshold time
                    let tokenRenewTime = (this.tokenLifespan - elapsed) - this.appData.tokenRenewThreshold;

                    this._tokenElapsedTime.next(elapsed); // token's remaining life

                    return tokenRenewTime > 0;
                })
            )
            .subscribe(counter => {
                this.stopTokenRenewTimer();
                // console.log('Time to renew token.');
                this.renewToken();
            });
    }


    /** Stops the tokenLife timer. */
    public stopTokenRenewTimer() {
        if (this.tokenRenewTimerSubscription && !this.tokenRenewTimerSubscription.closed) {
            // console.log("Token renew timer stopped.");
            this.tokenRenewTimerSubscription.unsubscribe();
        }
    }

    // Encode a string with base64
    private encode(str: any): string {
        try {
            return btoa(str);
        }
        catch (e) {
            return e;
        }
    }

    // Encode a string with base64
    private decode(str: any): string {
        try {
            return atob(str);
        }
        catch (e) {
            return e;
        }
    }
}