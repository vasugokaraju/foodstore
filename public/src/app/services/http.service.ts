import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmptyError, Observable, Subject, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HTTPService {

  constructor(
    private http: HttpClient,
    private httpCancelService: HttpCancelService
  ) { }

  /**
   * Facilitates GET requests.
   * This function facilitates the following services
   * 1. Cancels pending requests by default.  Use `cancelPending` param to alter the behavior.
   * 2. Shows notifications by default. Use `notification` param to alter the behavior.
   * 3. Makes GET requests and returns an observable
   *
   * @param endpoint - api url endpoint
   * @param options - request headers and other options
   * @param cancelPending - flag to alter the default cancel pending requests behavior
   * @param notification - flag to alter the default behavior of notifications
   */
  public get(endpoint: string, options: any = {}, cancelPending: boolean = true, notification: boolean = true): Observable<any> {

    // Prepare the request before calling the endpoint
    options = this.prepareRequest(options, cancelPending, notification)

    // Make new http request
    return this.http.get(endpoint, options)
      .pipe(catchError((error: ErrorEvent) => {
        let err: any;
        if (error instanceof ErrorEvent) {
          // client-side error
          console.log('Local HTTP error');
        }
        else {
          // server-side error
        }

        // return new Observable<EmptyError>();
        return throwError(error);   // Forward the error to caller

      }))
      .pipe(map(data => { return data; }
      ))
  }

  /**
   * Facilitates POST requests.
   * This function facilitates the following services
   * 1. Cancels pending requests by default.  Use `cancelPending` param to alter the behavior.
   * 2. Shows notifications by default. Use `notification` param to alter the behavior.
   * 3. Makes POST requests and returns an observable
   *
   * @param endpoint - api url endpoint
   * @param body - body of the request
   * @param options - request headers and other options
   * @param cancelPending - flag to alter the default cancel pending requests behavior
   * @param notification - flag to alter the default behavior of notifications
   */
  public post(endpoint: string, body: any, options: any = {}, cancelPending: boolean = true, notification: boolean = true): Observable<any> {

    // Prepare the request before calling the endpoint
    options = this.prepareRequest(options, cancelPending, notification)

    // Make new http request
    return this.http.post(endpoint, body, options)
      .pipe(catchError((error: ErrorEvent) => {
        let err: any;
        if (error instanceof ErrorEvent) {
          // client-side error
          console.log('Local HTTP error');
        }
        else{
          // server-side error
        }

        // return new Observable<EmptyError>();
        return throwError(error);   // Forward the error to caller
      }))
      .pipe(map(data => { return data; }
      ))
  }



  /**
   * Facilitates PUT requests.
   * This function facilitates the following services
   * 1. Cancels pending requests by default.  Use `cancelPending` param to alter the behavior.
   * 2. Shows notifications by default. Use `notification` param to alter the behavior.
   * 3. Makes Put requests and returns an observable
   *
   * @param endpoint - api url endpoint
   * @param body - body of the request
   * @param options - request headers and other options
   * @param cancelPending - flag to alter the default cancel pending requests behavior
   * @param notification - flag to alter the default behavior of notifications
   */
   public put(endpoint: string, body: any, options: any = {}, cancelPending: boolean = true, notification: boolean = true): Observable<any> {

    // Prepare the request before calling the endpoint
    options = this.prepareRequest(options, cancelPending, notification)

    // Make new http request
    return this.http.put(endpoint, body, options)
      .pipe(catchError((error: ErrorEvent) => {
        let err: any;
        if (error instanceof ErrorEvent) {
          // client-side error
          console.log('Local HTTP error');
        }
        else {
          // server-side error
        }

        // return new Observable<EmptyError>();
        return throwError(error);   // Forward the error to caller

      }))
      .pipe(map(data => { return data; }
      ))
  }



  /**
   * Facilitates DELETE requests.
   * This function facilitates the following services
   * 1. Cancels pending requests by default.  Use `cancelPending` param to alter the behavior.
   * 2. Shows notifications by default. Use `notification` param to alter the behavior.
   * 3. Makes DELETE requests and returns an observable
   *
   * @param endpoint - api url endpoint
   * @param body - body of the request
   * @param options - request headers and other options
   * @param cancelPending - flag to alter the default cancel pending requests behavior
   * @param notification - flag to alter the default behavior of notifications
   */
   public delete(endpoint: string, options: any = {}, cancelPending: boolean = true, notification: boolean = true): Observable<any> {

    // Prepare the request before calling the endpoint
    options = this.prepareRequest(options, cancelPending, notification)

    // Make new http request
    return this.http.delete(endpoint, options)
      .pipe(catchError((error: ErrorEvent) => {
        let err: any;
        if (error instanceof ErrorEvent) {
          // client-side error
          console.log('Local HTTP error');
        }
        else {
          // server-side error
        }

        // return new Observable<EmptyError>();
        return throwError(error);   // Forward the error to caller

      }))
      .pipe(map(data => { return data; }
      ))
  }




  /**
   * Prepares the request before calling the endpoint
   * @param body 
   * @param options - request headers and other options
   * @param cancelPending - flag to alter the default cancel pending requests behavior
   * @param notification - flag to alter the default behavior of notifications
   */
  private prepareRequest(options: any, cancelPending: boolean, notification: boolean) {


    // https://weblog.west-wind.com/posts/2019/Apr/07/Creating-a-custom-HttpInterceptor-to-handle-withCredentials
    // options.withCredentials = true;

    // By default notifications are shown (by the http interceptor) when response is received.
    // If notification is false, a "notification" custom header is added to http headers.
    // The custom header will be used by http interceptor to hide the notification for the specific request.
    if (!notification) {
      options.headers = (options.headers) ? options.headers.append("notification", "false") : new HttpHeaders({ "notification": "false" });
    }

    // Cancel the pending requests when a new request is made.
    if (cancelPending) {
      console.log('Cancelling pending requests.');
      this.httpCancelService.cancelPendingRequests();   // Trigger to http.interceptor to cancel all pending requests
    }

    return options
  }
}


@Injectable({
  providedIn: 'root'
})
export class HttpCancelService {
  private cancelPendingRequests$ = new Subject<void>()

  constructor() { }

  /** Cancels all pending Http requests. */
  public cancelPendingRequests() {
    this.cancelPendingRequests$.next()
  }

  public onCancelPendingRequests() {
    return this.cancelPendingRequests$.asObservable()
  }

}