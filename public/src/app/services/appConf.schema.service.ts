import { Injectable } from '@angular/core';


@Injectable({ providedIn: "root" })
export class AppConfSchemaService {
    constructor() { }

    private _schemas: any = {

        "menu_schema": {
            "$schema": "http://json-schema.org/draft-07/schema#",
            "$id": "http://example.com/schemas/appConfSchema#",
            "title": "Menu Configuration",
            "description": "This schema is to validate menu json data.",
            "$async": false,

            "type": "object",
            "properties": {
                "lng": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15,
                    "description": "Language",
                    "label": "Language"
                },
                "lbl": {
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 50,
                    "description": "Label that represent the data",
                    "label": "Label"
                },
                "data": {
                    "type": "array",
                    "maxItems": 30,
                    "description": "Menu items",
                    "label": "Menu Items",
                    "items": {
                        "type": "object",
                        "properties":
                        {
                            "id": {
                                "type": "string",
                                "minLength": 2,
                                "maxLength": 20,
                                "description": "Unique ID for the menu item.",
                                "label": "Menu Item ID"
                            },
                            "icon": {
                                "type": "string",
                                "minLength": 5,
                                "maxLength": 30,
                                "description": "CSS class names for menu item Icon.",
                                "label": "Menu Icon"
                            },
                            "label": {
                                "type": "string",
                                "minLength": 3,
                                "maxLength": 30,
                                "description": "Menu item label to display",
                                "label": "Menu Item Label"
                            },
                            "routerLink": {
                                "type": "string",
                                "minLength": 5,
                                "maxLength": 100,
                                "description": "Link to the page",
                                "label": "Page Link"
                            },
                            "allow": {
                                "type": "object",
                                "description": "Indicates forbidden users of the route",
                                "label": "Forbidden Users"
                            },
                            "items": {
                                "type": "array",
                                "description": "Sub-Menu items",
                                "label": "Sub-Menu Items",
                                "items": 
                                    {
                                        "type": "object",
                                        "properties":
                                        {
                                            "id": {
                                                "type": "string",
                                                "minLength": 2,
                                                "maxLength": 20,
                                                "description": "Unique ID for the menu item.",
                                                "label": "Menu Item ID"
                                            },
                                            "icon": {
                                                "type": "string",
                                                "minLength": 5,
                                                "maxLength": 30,
                                                "description": "CSS class names for menu item Icon.",
                                                "label": "Menu Icon"
                                            },
                                            "label": {
                                                "type": "string",
                                                "minLength": 3,
                                                "maxLength": 30,
                                                "description": "Menu item label to display",
                                                "label": "Menu Item Label"
                                            },
                                            "routerLink": {
                                                "type": "string",
                                                "minLength": 5,
                                                "maxLength": 100,
                                                "description": "Link to the page",
                                                "label": "Page Link"
                                            },
                                            "allow": {
                                                "type": "object",
                                                "description": "Indicates forbidden users of the route",
                                                "label": "Forbidden Users"
                                            },
                                            // jsonEditor on appConfig page uses predefined templates to make it easy for the user.  
                                            // The template has "items" for the purpose of first level elements.  
                                            // Since the same template can also be used for sub level elements, the schema is adjusted to allow "items" field though it is not needed.                                            
                                            "items": {
                                                "type": "array",
                                                "description": "Sub-Sub-Menu items",
                                                "label": "Sub Sub-Menu Items"
                                            }
                                        },
                                        "required": ["label"]
                                    }
                                
                            }
                        },
                        "required": ["label"]
                    }

                },
                "id": {
                    "type": "string",
                    "description": "Record unique identifier.",
                    "label": "AppConf Id"
                },
            },
            "required": ["lbl", "lng", "data"],
            "additionalProperties": false
        },
        "page_labels_schema": {
            "$schema": "http://json-schema.org/draft-07/schema#",
            "$id": "http://example.com/schemas/appConfSchema#",
            "title": "Page Labels Configuration",
            "description": "This schema is to validate page labels json data.",
            "$async": false,

            "type": "object",
            "properties": {
                "lng": {
                    "type": "string",
                    "minLength": 5,
                    "maxLength": 15,
                    "description": "Language",
                    "label": "Language"
                },
                "lbl": {
                    "type": "string",
                    "minLength": 3,
                    "maxLength": 50,
                    "description": "Label that represent the data",
                    "label": "Label"
                },
                "data": {
                    "type": "object",
                    "description": "Page labels",
                    "label": "Page Labels",
                },
                "id": {
                    "type": "string",
                    "description": "Record unique identifier.",
                    "label": "AppConf Id"
                },
            },
            "required": ["lbl", "lng", "data"],
            "additionalProperties": false
        }        

    }

    private _templates: any = {

        "menu_data_template": {
            "id": "Unique ID",
            "icon": "icon css",
            "label": "Label to display",
            "routerLink": "/path-to-page",
            "items": []
        },

        "page_labels_data_template": {
            "label_name": "Label Value"
        }

    }


    /**
     * Provides schema based on given schema id.  This is used to validate json data in appConf json editor.
     * @param schemaID to retrieve
     * @returns schema
     */
    public getAppConfSchema(schemaID: string): Promise<any> {

        let schema: any;

        switch (schemaID) {
            case "burgermenu":
            case "downloadmenu":
            case "mainmenu":
            case "redemptionmenu":
                schema = this._schemas["menu_schema"];
                break;
            case "homepagelabels":
                schema = this._schemas["page_labels_schema"];
                break;
        }

        return Promise.resolve(schema);
    }


    /**
     * Provides new record template to be used to used in appConf json editor
     * This helps user to create new records without running into schema validation issues
     * @param schemaID to retrieve template
     * @returns json record tempalte
     */
    public getAppConfRecordTemplate(templateId: string): Promise<any> {
        let template: any;

        switch (templateId) {
            case "burgermenu":
            case "downloadmenu":
            case "mainmenu":
            case "redemptionmenu":
                template = this._templates["menu_data_template"];
                break;
            case "homepagelabels":
                template = this._templates["page_labels_data_template"];
                break;
        }
        return Promise.resolve(template);
    }

}