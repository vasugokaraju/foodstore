import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, fromEvent, interval, merge, Observable, Subscription } from 'rxjs';
import { map, skipWhile, switchMap, take, tap } from 'rxjs/operators';
import { AppDataService } from './appData.service';


/**
 * This service offers a timer that is responsible for terminating inactive session. 
 * When the timer (user inactivity) reaches `inactivityThreshold`, session is logged out and a message is shown.
 * 
 * If the user resumes activity before the timer reaches `inactivityThreshold`, the timer is reset and the process restarts.
 */
@Injectable({ providedIn: 'root' })
export class SessionService {

    //#region SESSION TIMEOUT
    /**
     * List of events to watch for user activity
     */
    // SESSION_TIMEOUT::STEP1
    // Events to watch for to implement inactive/inactivity alert before system logout
    private inactiveTimerEvents: Array<any>[] = [
        [document, 'click'], [document, 'wheel'], [document, 'scroll'], [document, 'mousemove'],
        [document, 'keyup'], [window, 'resize'], [window, 'scroll'], [window, 'mousemove']
    ];

    /** An array to hold observables of DOM events that are chosen to watch for user activity*/
    private userActivityEvents$: Observable<any>[] = [];                  // Array to hold fromEvent objects before merged

    /** A Subscription variable to cncel subscription to inactive timer. */
    private inactiveTimerSubscription: Subscription;                      // To cancel subscription

    /** Observable to provide user inactivity, in seconds, since last action. */
    private _inactivityDuration: BehaviorSubject<number> = new BehaviorSubject<number>(0);


    /** Provides public observable for the local `inactivityDuration` variable to read user inactivity duration. */
    public get inactivityDuration$() {
        return this._inactivityDuration.asObservable();
    }

    
    constructor(
        private ngZone: NgZone,
        private appData: AppDataService
    ) {

        // SESSION_TIMEOUT::STEP2
        /**
         * Create observable for each document/window event that is considered for user activity observation. 
         * Then push the observables into this.inactiveTimers$ array to merge them later.
         */
        this.inactiveTimerEvents.forEach(evnt => {
            this.userActivityEvents$.push(fromEvent(evnt[0], evnt[1]));
        });

    }

    // SESSION_TIMEOUT::STEP5
    /**
     * Cancels subscription to inactive timer.
     */
    public unsubscribeToInactiveTimer() {
        console.log("Inactivity timer is stopped.");
        this.inactiveTimerSubscription.unsubscribe();
        this._inactivityDuration.next(0);
    }

    /**
     * Starts the inactivity timer that is responsible for inactive session timeout.
     */
    public start() {

        // Run outside angular for better performance.
        this.ngZone.runOutsideAngular(() => {

            /**
             * SESSION_TIMEOUT::STEP3
             * Merge all event observable and create new observable
             * which emits a value when inactive count (seconds) reaches inactive threshold
             */
            this.inactiveTimerSubscription = merge(...this.userActivityEvents$)
                .pipe(
                    // Ignore previous inactive seconds count and start fresh when user stops activity
                    switchMap(ev => {
                        // console.log('indicates user activity.');
                        this._inactivityDuration.next(0);
                        return interval(1000).pipe(take(this.appData.inactivityThreshold + 1));     // Count upto inactivity threshold.
                    }),
                    map(elapsed => ++elapsed),  // Since the counter starts with 0
                    tap(elapsed => {
                        this._inactivityDuration.next(elapsed);


                        return elapsed; // 'tap' operator does not effect the original value, thus this 'elapsed' still contains the original 0 based count
                    }),
                    // Send data to subscribers only when the inactivity count reached inactivity threshold
                    skipWhile((elapsed) => {
                        // console.log(`sec of user inactivity. ${this.appData.inactivityThreshold + this.countdownDuration} sec threshold`);
                        return (this.appData.loggedIn && elapsed != this.appData.inactivityThreshold)
                    })
                )
                /**
                 * SESSION_TIMEOUT::STEP4
                 * Subscribe to the above merged observable which gives subscription (this.inactiveTimerSubscription)
                 * Use the subscription to cancel the subscriber.
                 * 
                 * When data is received, it indicates that inactive timer reached its threshold 
                 * and it is not going to send any data.
                 */
                .subscribe(x => {
                    console.log('End of Session');
                    this.unsubscribeToInactiveTimer();   // Unsubscribe
                });
        });

        // Trigger fake document mousemove event to start the inactive timer
        // Otherwise timer waits until user activity
        document.dispatchEvent(new Event('mousemove'));

        console.log("Inactivity timer is started.");
    }


    /**
     * Stops the inactivity timer that is responsible for inactive session timeout
     */
    public stop() {
        if (this.inactiveTimerSubscription && !this.inactiveTimerSubscription.closed) {
            this.unsubscribeToInactiveTimer();
        }
    }

    
}