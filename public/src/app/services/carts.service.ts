import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { HTTPService } from './http.service';
import {AuthService} from './auth.service'
import { map } from 'rxjs/internal/operators/map';
import { Helper } from '@app/helpers';
import { ICarts } from '@app/interfaces';
import { throwError } from 'rxjs/internal/observable/throwError';


@Injectable({providedIn:"root"})
export class CartsService{
    constructor(
        private http: HTTPService,
        private auth: AuthService,
        private helper: Helper
    ){}

    // This method is developed for CartsResolver to load data before the UI is loaded.
    // Load carts data
    public preload(): Promise<any> {
        // The preload service determines what is needed and provides all the data in the reponse.
        // It identifies the user based on the token
        const endpoint = `${environment.api.host}${environment.api.cartsPreload}`;

        let body:any = {
            whereQuery:{
                where:[],
                orderBy: ['customerId','asc']
            }
        }

        // Get the preload data.
        return new Promise((resolve, reject) => {
            // Supress notifications on receiving response.
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Carts preload is completed.');
                    })

        })

    }

    // This method is developed to pull complete profile when user clicks on edit
    // Load carts data
    public readOne(id:string): Promise<any> {
        const endpoint = `${environment.api.host}${environment.api.cartsReadOne}${id}`;

        return new Promise((resolve, reject) => {
            this.http.get(endpoint,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response.data);
                    },
                    error => {
                        if(error.code === 'E1405'){     // No records found error
                            return resolve([]);
                        }

                        return reject(error);
                    },
                    () => {
                        // console.log('Carts readOne is completed.');
                    })

        })

    }


    /**
    * Find Carts records based on where query
    */
     public readMany(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsReadMany}`;

        let body = data;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,body,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts readMany is completed.');
                    })

        })
    } 


    /**
    * Create Carts record
    */
    public create(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsCreate}`;
        
        // Convert date object to ISO date string.
        data = this.helper.dateToString(data, []);

        return new Promise((resolve, reject) => {
            this.http.post(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts create is completed.');
                    })

        })
    }


    /**
    * Update Carts record
    */
    public update(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsUpdate}${data.id}`;

        // Convert date object to ISO date string.
        try{
            data = this.helper.dateToString(data, []);
        }
        catch(e){}
        
        delete data.id;     // Id is not needed in the data

        return new Promise((resolve, reject) => {
            this.http.put(endpoint,data,{},true,false)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts update is completed.');
                    })

        })   
    }

    /**
    * Delete Carts record
    */
    public delete(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsDelete}${data.id}`;

        return new Promise((resolve, reject) => {
            this.http.delete(endpoint,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts delete is completed.');
                    })

        });

    }    


    /**
    * Download Carts records
    */
    public download(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsDownload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts download is completed.');
                    })

        });           
    } 


    /**
    * Upload Carts records
    */
    public upload(data: any): Promise<any> {

        const endpoint = `${environment.api.host}${environment.api.cartsUpload}`;

        return new Promise((resolve, reject) => {
            this.http.post(endpoint, data,{},true,true)
                .subscribe(
                    response => {
                        return resolve(response);
                    },
                    error => {
                        return reject(error);
                    },
                    () => {
                        // console.log('Carts upload is completed.');
                    })

        });          
    } 



}