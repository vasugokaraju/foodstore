import { Injectable } from '@angular/core';


@Injectable({providedIn:"root"})
export class DashboardService{
    constructor(){}

    // This method is developed for DashboardResolver to load data before the UI is loaded.
    // Load dashboard data
    preload(): Promise<any> {
        console.log('in DashboardService');
        let data = [
            {name: 'Book1', author: 'Author1'},
            {name: 'Book2', author: 'Author2'},
            {name: 'Book3', author: 'Author3'},
            {name: 'Book4', author: 'Author4'},
            {name: 'Book5', author: 'Author5'}
        ];
        return Promise.resolve({widget:{title:"widget1", data:data}});
    }
}