# Compile project for production and build /dist folder
npx ng build --prod

# Compile project for production and webpack stats data gets exported to dist folder
npx ng build --prod --stats-json

# Find the size of /dist folder
du -sh ./dist/foodstore/

# Clean npm cache
npm cache clean --force

# Remove package-lock.json
rm -rf package-lock.json

# Remove node_modules
rm -rf node_modules/