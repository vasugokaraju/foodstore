#!/usr/bin/env bash
#!/bin/bash

# Kill firebase emulator related ports

kill -9 $(lsof -t -i:8080)
kill -9 $(lsof -t -i:8085)
kill -9 $(lsof -t -i:9000)