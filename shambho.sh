#!/usr/bin/env bash
#!/bin/bash

# BUILDS THE CODE BASED ON THE GIVEN CONFIGURATION

appDataFile=./projectConfig.json
outputFile=./codebase.zip
# endpoint=https://192.168.1.212:1443/firebase
endpoint=https://172.19.0.2:1443/firebase
schemaError=0

# Delete the existing outputfile
rm -rf $outputFile

curl -k --header "Content-Type: application/json" --request POST --data @$appDataFile --out $outputFile $endpoint

unzip -o $outputFile

chmod +x shambho.sh createServiceAccount.sh createWebApp.sh showAllConfig.sh uiDebug.sh deployFunctions.sh deployPublic.sh killPorts.sh
# chmod +x ./functions/getWebAppConfig.sh
chmod +x ./functions/getRuntimeConfig.sh
chmod +x ./functions/setEmailConfig.sh

# Delete sample files
rm -rf projectConfigSample*


# Special case for appConf since it has jsonEditor as UI and supports Object and Array as value for "data" field.
# Move _appConf.component.html to appConf.component.html.
# This is to replace default grid UI with JSON Editor UI.
mv ./public/src/app/components/admin/appConf/_appConf.component.html ./public/src/app/components/admin/appConf/appConf.component.html
mv ./public/src/app/components/admin/appConf/_appConf.component.ts ./public/src/app/components/admin/appConf/appConf.component.ts
mv ./public/src/app/components/admin/appConf/_appConf.module.ts ./public/src/app/components/admin/appConf/appConf.module.ts
mv ./functions/src/schemas/_appConfSchema.ts ./functions/src/schemas/appConfSchema.ts