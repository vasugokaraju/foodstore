# BUILD UI CODE FOR PRODUCTION RELEASE
# cd public && npm run build

# Deploy Public Web to preview channel
# The channel will be created under the site specified in the firebase.jon file ( ex:  "site":"foodstore-d4030")
# If no site specified, it goes to default site.
# firebase hosting:channel:deploy foodstore-d4030-app-v1-preview-channel

# Deploy Public Web to PRODUCTION (https://foodstore-d4030.web.app/)
# firebase hosting:clone foodstore-d4030:foodstore-d4030-app-v1-preview-channel foodstore-d4030:live

# Deploy all functions
# firebase deploy --only functions

# Deploy with debug option to see the log
# firebase deploy --debug

# Deploy specific functions
# firebase deploy --only functions:helloWorld