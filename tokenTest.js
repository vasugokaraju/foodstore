// Validates Identity and Access Management (IAM) Credentials

const admin = require('firebase-admin');
// const fetch = require('node-fetch');

// get an access token from your service account credentials (GOOGLE_APPLICATION_CREDENTIALS)
async function  getAccessToken() {
  return admin.credential.applicationDefault().getAccessToken()
      .then(accessToken => {
        return accessToken.access_token;
      })
      .catch(err => {
        console.error('Unable to get access token');
        console.error(err);
      });
}

// List existing projects
// async function listProjects(accessToken) {
//   const uri = 'https://firebase.googleapis.com/v1beta1/availableProjects';
//   const options = {
//     method: 'GET',
//     headers: {
//       'Authorization': 'Bearer ' + accessToken,
//     },
//   };

//   try {
//     const rawResponse = await fetch(uri, options);
//     const resp = await rawResponse.json();
//     const projects = resp['projectInfo'];

//     for (let i in projects) {
//       const project = projects[i];
//       console.log('Project ' + i);
//       console.log('ID: ' + project['project']);
//       console.log('Display Name: ' + project['displayName']);
//       console.log('');
//     }
//   } catch(err) {
//     console.error(err);
//   }
// }
  

// async function addWebApp(projectId, displayName, accessToken) {
//     const uri = 'https://firebase.googleapis.com/v1beta1/projects/' + projectId + '/webApps';
//     const options = {
//       method: 'POST',
//       headers: {
//         'Authorization': 'Bearer ' + accessToken,
//       },
//       body: JSON.stringify({
//         'displayName': displayName
//       }),
//     };
  
//     try {
//       const rawResponse = await fetch(uri, options);
//       const resp = await rawResponse.json();
//       console.log(resp);
//     } catch(err) {
//       console.error(err['message']);
//     }
//   }


(async () => {

    try {

      console.log("");
      console.log('\nprojectId: ', admin.credential.applicationDefault().projectId);
      console.log('\nclientEmail: ', admin.credential.applicationDefault().clientEmail);

        // Get the token
        let accessToken = await getAccessToken()

        console.log('\nToken:', accessToken);

        console.log("");

    } catch (error) {
        console.log("Error", error);
    }

})();