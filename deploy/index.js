const projectsData = [
    {
        "userEmail": "vasugokaraju@gmail.com",
        "serviceAccountId": "foodstore-service-1",
        "serviceAccountName": "Foodstore Service Account 1",
        "serviceAccountKeyFile": "/home/node/foodstore-d4030--foodstore-service-1--key.json",
        "remoteConfigFile": "REMOTE CONFIG FILE WILL BE SET ON SUCCESSFUL VERIFICATION OF CONFIGURATION",
        "firebaseProjectId": "foodstore-d4030",
        "firebaseProjectName": "FoodStore",
        "projectLocation":"us-central1",

        "webAppId": "1:792224779387:web:f6a2b6770546afcbb326de",

        "projectRootFolder":"/workspaces/foodstore",
        "functionsRootFolder": "/functions",
        "publicRootFolder": "/public",
        
        "userHomeFolder":"/home/node/",

        "notes":[]
    },
    {
        "userEmail": "managudifoundation@gmail.com",
        "serviceAccountId": "managudi-admin-web-service-1",
        "serviceAccountName": "Mana Gudi Web Admin Service Account 1",
        "serviceAccountKeyFile": "/home/node/mana-gudi-admin-web--managudi-admin-web-service-1--key.json",
        "remoteConfigFile": "REMOTE CONFIG FILE WILL BE SET ON SUCCESSFUL VERIFICATION OF CONFIGURATION",
        "firebaseProjectId": "mana-gudi-admin-web",
        "firebaseProjectName": "Mana Gudi Admin Web Project",
        "projectLocation":"us-central1",

        "webAppId": "1:664301156249:web:fcebc41726d53a2e9f0508",

        "projectRootFolder":"/workspaces/mana-gudi-admin-website",
        "functionsRootFolder": "/functions",
        "publicRootFolder": "/public",
        
        "userHomeFolder":"/home/node/",

        "notes": [
            "Use managudi credentials for both GCloud and Firebase.  Having difficulty uploading functions using vasugokaraju credentials, though permissions given.  Need to investigate more."
        ]

    }


]

let allRoutes = [];
let deployRoutes = [];
let batchSize = 10;          // batch size for routes deployment

function init() {
    let socket = io();

    // Subscribe to timestamp socket to receive timestamp from server.
    socket.on('timestamp', function (timestamp) {
        $('.timestamp').html("Timestamp: " + timestamp);
    });

    // Subscribe to output socket to receive timestamp from server.
    socket.on('output', function (response) {
        addToOutput(response.message);
    });

    // Load projects
    loadProjects();

    // show run tab
    switchTab("run");

    // Load project related settings
    onProjectSelection();
}

// Deploys functions in batches
function deployInBatches() {
    let _deployRoutes = [...deployRoutes];
    let batches = [];
    let promises = [];

    // utility function for sleeping
    const sleep = (n) => new Promise((res) => setTimeout(res, n));

    // Build promise for each batch
    let batchCount = 0;
    for (i = 0, j = _deployRoutes.length; i < j; i += batchSize) {
        batches.push(_deployRoutes.slice(i, i + batchSize));

        promises.push(
            async () => {
                return new Promise(async (resolve, reject) => {
                    try {
                        batchCount++;
                        setStatus(`-----------------Running batch ${batchCount} of ${batches.length}`);
                        await runCommand('DEPLOY_FUNCTIONS', batches[batchCount-1]);
                        setStatus(`-----------------Completed batch ${batchCount} of ${batches.length}`)
                        return resolve(true);
                    } catch (error) {
                        setStatus('Batch executin failed.', error);
                        return reject(false);
                    }

                });
            }
        )        
    }

    // Run batches one after the other
    return ( async function(){
        await promises.reduce(async (previousPromise, nextAsyncFunction) => {
            await previousPromise;
            await nextAsyncFunction();
            await sleep(2000);      // Wait for few seconds before running next batch.
        }, Promise.resolve());
            
        return Promise.resolve(true);
    })();

}

// Executes pre-defined shall command in the backend
async function runCommand(command, routes) {


    // Switch to Shell
    switchTab("shell");

    const requestStartTime = new Date();
    // const projectId = $("#ddlProjects option:selected").val(),
    
    let data = getProjectConfig();
    
    data.command = ( command !== "DEPLOY_PUBLIC" )? command : $('input[name="hostingDestination"]:checked').val();

    $(`button`).prop("disabled", true);
    $(`#spin${command}`).show("slow");

    const options = {
        url: "/shell"
    }


    addToOutput("=========== START " + command + "===========")
    setStatus('Command ' + command + ' is issued at ' + requestStartTime.toTimeString());

    if (command == "DEPLOY_FUNCTIONS") { 
        data.routes = routes; 
        addToOutput("Deploying " + data.routes.toString());
    };  // Provide the reoutes to deploy

    return $.post(options, data, (resp) => {

        if (resp.hasOwnProperty("routes")) {
            allRoutes = resp.routes;
            $("#selRoutes").find("option").remove();
            
            resp.routes.forEach(item => {
                let o = new Option(item, item);
                $(o).html(item);
                $("#selRoutes").append(o);
            });

            $("#allRoutesCount").html("(" + resp.routes.length + ")");
        }

        if(resp.hasOwnProperty("channelURL")){
            $("#aHostingChannel").attr("href", resp.channelURL).html(resp.channelURL);
        }

    }, "json")
        .done(() => {
            $(`button`).prop("disabled", false);
            $(`#spin${command}`).hide("slow");

            setStatus('Command ' + command + ' is completed in ' + timeDiff(requestStartTime, new Date()));
            addToOutput("=========== END " + command + "===========")
            return Promise.resolve(true);

        }).fail(function (err) {
            console.log('Command', command, ' failed at', new Date().toString());
            console.log("error", err);
            addToOutput("=========== ERROR " + command + "===========")
            return Promise.reject(false);
        })
        .always(function () {
            addToOutput("=========== FINAL " + command + "===========")
            return Promise.resolve(true);
        });
}

// Gets project configuration using gcloud and firebase cli commands
async function verifyProjectConfig() {

    // Switch to Shell
    switchTab("shell");

    const data = getProjectConfig();

    // $(`button`).prop("disabled", true);
    // $(`#spin${command}`).show("slow");

    const options = {
        url: "/verifyProjectConfig"
    }

    addToOutput("=========== START PROJECT CONFIG REQUEST ===========")

    return $.post(options, data, (resp) => {
            
        renderConfigInfoView(resp.projectConfig);

        let projectConfig = getProjectConfig();
        // projectConfig.serviceAccountKeyFile = resp.projectConfig.serviceAccountKeyFile;
        projectConfig.remoteConfigFile = resp.projectConfig.remoteConfigFile;

        console.log('projectsData', projectsData);
        // Switch to config
        switchTab("config");        
    }, "json")
        .done(() => {
            // $(`button`).prop("disabled", false);
            // $(`#spin${command}`).hide("slow");

            // setStatus('Command ' + command + ' is completed in ' + timeDiff(requestStartTime, new Date()));
            addToOutput("=========== END PROJECT CONFIG REQUEST ===========")

            return Promise.resolve(true);

        }).fail(function (err) {

            console.log("error", err);
            addToOutput("=========== ERROR " + err + "===========")
            return Promise.reject(false);
        })
        .always(function () {
            addToOutput("=========== FINAL ===========")
            return Promise.resolve(true);
        });
}

// Gets firebase config OR runtime configuration from .runtimeconfig.json
async function getConfig(which) {


    // Switch to Shell
    switchTab("shell");


    let data = getProjectConfig();
    data.which = which

    // $(`button`).prop("disabled", true);
    // $(`#spin${command}`).show("slow");

    const options = {
        url: "/getConfig",
    }

    addToOutput("=========== START RUNTIME CONFIG REQUEST ===========")

    return $.post(options, data, (resp) => {

        (which == "runtime")? $("#runtimeConfigJSON").val(resp.configJSON) : $("#firebaseConfigJSON").val(resp.configJSON);

        // Switch to config
        switchTab("config");       

    }, "json")
        .done(() => {
            // $(`button`).prop("disabled", false);
            // $(`#spin${command}`).hide("slow");

            // setStatus('Command ' + command + ' is completed in ' + timeDiff(requestStartTime, new Date()));
            addToOutput("=========== END RUNTIME CONFIG REQUEST ===========")

            return Promise.resolve(true);

        }).fail(function (err) {

            console.log("error", err);
            addToOutput("=========== ERROR " + err + "===========")
            return Promise.reject(false);
        })
        .always(function () {
            addToOutput("=========== FINAL ===========")
            return Promise.resolve(true);
        });
}



// Saves runtime configuration in .runtimeconfig.json
async function setConfig() {

    // Switch to Shell
    switchTab("shell");


    let data = getProjectConfig();
    data.config = $("#runtimeConfigJSON").val();
    // $(`button`).prop("disabled", true);
    // $(`#spin${command}`).show("slow");

    const options = {
        url: "/setConfig",
    }

    addToOutput("=========== START SET RUNTIME CONFIG REQUEST ===========")

    return $.post(options, data, (resp) => {

        switchTab("config");
    }, "json")
        .done(() => {
            // $(`button`).prop("disabled", false);
            // $(`#spin${command}`).hide("slow");

            // setStatus('Command ' + command + ' is completed in ' + timeDiff(requestStartTime, new Date()));
            addToOutput("=========== END SET RUNTIME CONFIG REQUEST ===========")

            return Promise.resolve(true);

        }).fail(function (err) {

            console.log("error", err);
            addToOutput("=========== ERROR " + err + "===========")
            return Promise.reject(false);
        })
        .always(function () {
            addToOutput("=========== FINAL ===========")
            return Promise.resolve(true);
        });
}



function getProjectConfig(){

    const projectId = $("#ddlProjects option:selected").val();
    return projectsData.filter(project => project.firebaseProjectId == projectId)[0];

}

// Creates service account
async function createServiceAccount() {

    // Switch to Shell
    switchTab("shell");

    const projectId = $("#ddlProjects option:selected").val();
    const projectInfo = projectsData.filter(project => project.firebaseProjectId == projectId)[0];

    const data = projectInfo;

    // $(`button`).prop("disabled", true);
    // $(`#spin${command}`).show("slow");

    const options = {
        url: "/createService"
    }

    addToOutput("=========== START SERVICE CREATION REQUEST ===========")

    return $.post(options, data, (resp) => {
            
        
    }, "json")
        .done(() => {
            addToOutput("=========== END SERVICE CREATION REQUEST ===========")

            return Promise.resolve(true);

        }).fail(function (err) {
            addToOutput("=========== ERROR " + err + "===========")
            return Promise.reject(false);
        })
        .always(function () {
            addToOutput("=========== FINAL ===========")
            return Promise.resolve(true);
        });
}


function renderConfigInfoView(config){
    
    // Populate all the fields and Color code respective field labels based on the status
    Object.keys(config).forEach( key => {
        
        const val = config[key].toString();

        $("#"+key).html(val);

        const isInvalid = val.match( /(false|NOT FOUND|INVALID)/ )

        const $label =  (key == "serviceAccountId")?  $("#"+key).parent().prev() : $("#"+key).prev();
        $label.removeClass("text-success").removeClass("text-danger");
        (isInvalid)? $label.addClass("text-danger") : $label.addClass("text-success");
    });

    // Make suggestions in the second pass, if any.
    if(!config.gCloudLoggedIn) $("#gCloudLoggedIn").html("Please login into gcloud using 'gcloud auth login' cli command.");
    if(!config.firebaseLoggedIn) $("#firebaseLoggedIn").html("Please login into firebase using 'firebase login' cli command.");

    // Show/Hide service [create] link
    (config.serviceAccountId.includes("SERVICE ACCOUNT NOT FOUND") || !config.serviceAccountKeyFile)?
        $("#createService").show() : 
        $("#createService").hide();


    setFolderPaths();

    switchTab('config');
}

// Creates gcloud service
function createService(){
    console.log('createService');
}

function clearOutput() {
    $("#output").html("");
}

function addToOutput(msg) {
    const $output = $("#output");
    msg = msg.replace(/(|\[\[|36m|33m|90m|1m|22m)/g, '');

    $output.html($output.html() + msg + "\n");
    if ($output.length)
        $output.scrollTop($output[0].scrollHeight - $output.height());

}

function loadProjects() {
    let $projects = $("#ddlProjects");
    $.each(projectsData, function () {
        $projects.append($("<option />").val(this.firebaseProjectId).text(this.firebaseProjectName));
    });

}

// Select routes using user given filter criteria
function filterRoutes(filter) {
    try {
        // Select the routes that match user filter criteria
        deployRoutes = allRoutes.filter(item => isIncluded(item, [(filter)]));

        $('#selDeployRoutes').find('option').remove();  // Clear

        deployRoutes.forEach(item => {
            let o = new Option(item, item);
            $(o).html(item);
            $("#selDeployRoutes").append(o);
        });

        $("#filteredRoutesCount").html("(" + deployRoutes.length + ")");

    } catch (error) {
        console.log(error);
    }

}

// Checks whether the given filename matches any of the provided include regular expressions
function isIncluded(fileName, includeList) {
    let rtn = false;

    for (let i = includeList.length; i > 0;) {
        let include = includeList[--i];
        let rx = new RegExp(include);

        if (rx.test(fileName)) {
            rtn = true;
            break;
        }
    }

    return rtn;
}

function timeDiff(start, end) {

    // let diffMs = (end - start); // milliseconds between now & Christmas
    // let diffDays = Math.floor(diffMs / 86400000); // days
    // let diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
    let diffMins = Math.round((end.getTime() - start.getTime()) / 60000, 0); // minutes
    let diffSecs = Math.round(seconds = (end.getTime() - start.getTime()) / 1000, 0); // seconds
    return (diffMins + " minutes, " + diffSecs + " seconds");

}

function setStatus(status) {
    $("#divStatus").html(status);
    addToOutput(status);
}

function switchTab(elemId){
 
    // make tab active
    $(".active").removeClass("active");
    $("#"+elemId).addClass("active");

    $(".tabBody").hide();   // Hide all tabs body sections
    $("#div"+elemId).show();    // Show selected tab's body

}

function onProjectSelection(){

    const projectInfo = getProjectConfig();

    $("#aHosting").attr("href", `https://console.firebase.google.com/u/1/project/${projectInfo.firebaseProjectId}/hosting/sites`);
    
    setFolderPaths();
}

/**
 * Sets source and destination folders
 */
function setFolderPaths(){
    const projectInfo = getProjectConfig();

    // $("#spnFunctionsBuildSource").html(`SOURCE: ${projectInfo.projectRootFolder}${projectInfo.functionsRootFolder}/src`);
    // $("#spnFunctionsBuildDest").html(`DEST: ${projectInfo.projectRootFolder}${projectInfo.functionsRootFolder}/lib`);

    $(".projectRootFolder").html(projectInfo.projectRootFolder);
    $(".functionsRootFolder").html(projectInfo.functionsRootFolder);
    $(".publicRootFolder").html(projectInfo.publicRootFolder);
    $(".firebaseProjectId").html(projectInfo.firebaseProjectId);
    $(".projectLocation").html(projectInfo.projectLocation);
    
    $("#aGCloudFunctions").attr("href",`https://console.cloud.google.com/functions/list?project=${projectInfo.firebaseProjectId}&folder=&organizationId=`);
    $("#aFirebaseFunctions").attr("href",`https://console.firebase.google.com/u/1/project/${projectInfo.firebaseProjectId}/functions/list`);
    $("#aLogsExplorer").attr("href",`https://console.cloud.google.com/logs/query?project=${projectInfo.firebaseProjectId}`);
    
    $("#aHostingProduction").attr("href",`https://${projectInfo.firebaseProjectId}.web.app`);
    $("#aHostingProduction").html(`https://${projectInfo.firebaseProjectId}.web.app`);
    $("#aHostingChannel").attr("href",`https://${projectInfo.firebaseProjectId}.web.app`);

}