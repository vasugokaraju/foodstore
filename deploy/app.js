// npm run start
// Runing this file using vscode launch is causing issues reading routes in Build API step.
// TODO:  find out the reasons for the above issue.

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const path = require('path');
const { Server } = require("socket.io");
const io = new Server(server);
const shell = require('shelljs');
// const bodyParser = require('body-parser')

let timestampInterval;

// const commands = [
//   {
//     projectId: "foodstore-d4030",
//     commands: {
//       // Change to functions folder && compile functions && collect route names for deployment
//       "functionsBuild": "cd /workspaces/foodstore/functions && npm run deploy && firebase emulators:exec --only functions ls",
//       "functionsDeploy": "cd /workspaces/foodstore && firebase deploy --only ",  // Deploy given functions (APIs)
//       "publicBuild": "cd /workspaces/foodstore/public && npm run deploy",
//       "publicDeploy": "cd /workspaces/foodstore && firebase deploy --only hosting",
//     }
//   }
// ]


const GCLOUD = {
  LIST: "gcloud auth list",
  USE: "gcloud config set project ",
  SERVICE_LIST: "gcloud iam service-accounts list",
  PROJECT_ID: "gcloud config get-value project",
  ACCOUNT_ID: "gcloud config get-value account",
  CREATE_SERVICE: "gcloud iam service-accounts create SERVICE_NAME --display-name='DISPLAY_NAME' --description='DESCRIPTION'",
  DELETE_SERVICE: "gcloud iam service-accounts delete ",
  GRANT_SERVICE_PERMISSIONS: "gcloud projects add-iam-policy-binding PROJECT_ID --member='serviceAccount:SERVICE_ACCOUNT_EMAIL' ",
  SAVE_SERVICE_KEY_FILE: "gcloud iam service-accounts keys create SERVICE_KEY_FILENAME --iam-account=SERVICE_ACCOUNT_EMAIL",
  GET_SERVICE_ROLES: `gcloud projects get-iam-policy PROJECT_ID`,
  SET_GOOGLE_APPLICATION_CREDENTIALS: 'export GOOGLE_APPLICATION_CREDENTIALS="SERVICE_KEY_FILENAME" && echo $GOOGLE_APPLICATION_CREDENTIALS '
}


const FIREBASE = {
  LIST: "firebase login:list",
  USE: "firebase use ",
  GET_WEB_APP_ID: "firebase apps:sdkconfig WEB ",
  BUILD_FUNCTIONS: " && npm run deploy && firebase emulators:exec --only functions ls",
  DEPLOY_FUNCTIONS: " && firebase deploy --only ",
  BUILD_PUBLIC: " && npm run deploy",
  DEPLOY_PUBLIC: " && firebase deploy --only hosting",
  CREATE_CHANNEL: " && firebase hosting:channel:deploy CHANNEL_NAME"
}


const ENVIRONMENT = {
  CREATE_BASHRC: `echo -e 'export GOOGLE_APPLICATION_CREDENTIALS="SERVICE_KEY_FILENAME"' >> USER_HOME_ROOT.bashrc`,
  SOURCE_BASHRC: `source USER_HOME_ROOT.bashrc && echo $GOOGLE_APPLICATION_CREDENTIALS`,
  SERVICE_ACCOUNT_KEY_FILE: 'echo $GOOGLE_APPLICATION_CREDENTIALS',
  KEY_FILE_EXISTS: " source USER_HOME_ROOT.bashrc && [[ -f $GOOGLE_APPLICATION_CREDENTIALS ]] && echo $GOOGLE_APPLICATION_CREDENTIALS",
  RUNTIME_CONFIG_FILE_EXISTS: " [[ -f PROJECT_ROOT_FOLDER-FUNCTIONS_ROOT_FOLDER/.runtimeconfig.json ]] && echo PROJECT_ROOT_FOLDER-FUNCTIONS_ROOT_FOLDER/.runtimeconfig.json",
  READ_RUNTIME_CONFIG: "cat PROJECT_ROOT_FOLDER-FUNCTIONS_ROOT_FOLDER/.runtimeconfig.json",
  WRITE_RUNTIME_CONFIG: "echo 'RUNTIME_CONFIG' > PROJECT_ROOT_FOLDER-FUNCTIONS_ROOT_FOLDER/.runtimeconfig.json"
}

let routes = [];    // Holds the route names for deployment purpose

// console.log('bodyParser',bodyParser);
// parse application/x-www-form-urlencoded
app.use(express.json());
app.use(express.urlencoded({ extended: true }))


app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
});

// function getCommandData(params) {
//   const data = commands.filter(command => command.projectId == params.projectId)[0]
//   return data;
// }

// Sets GOOGLE_APPLICATION_CREDENTIALS environment variable with current project service key file
// async function setGOOGLE_APPLICATION_CREDENTIALS(keyFile){
//   let command = GCLOUD.SET_GOOGLE_APPLICATION_CREDENTIALS
//                 .replace("SERVICE_KEY_FILENAME",keyFile);
//   let keyFileExists = await isExist(command, keyFile);

//   return keyFileExists
// }

app.post('/shell', async (req, res) => {

  req.setTimeout(0);  // Make the request wait untis res.send() is called. No timeout.

  let commandPart1 = "cd " + req.body.projectRootFolder;

  switch (req.body.command) {
    case "BUILD_PUBLIC":
      commandPart1 += req.body.publicRootFolder + " && "
      break;
    case "BUILD_FUNCTIONS":
      commandPart1 += req.body.functionsRootFolder + " && "
      break;
    default:
      commandPart1 += " && "
      break;
  }

  let command = (commandPart1 + GCLOUD.SET_GOOGLE_APPLICATION_CREDENTIALS + FIREBASE[req.body.command])
    .replace(/SERVICE_KEY_FILENAME/g, req.body.serviceAccountKeyFile)
    .replace(/CHANNEL_NAME/g, getChannelName(req))


  if (req.body.command === "DEPLOY_FUNCTIONS") {
    command += "functions:" + req.body.routes.join(",functions:");
  }

  io.emit("output", { message: command });

  let child = shell.exec(command, { async: true });
  routes = [];

  // Gets triggered for each line returned by shell
  child.stdout.on("data", (data) => stdOutData(data, req, res));

  child.stdout.on("pause", stdOutPause);

  child.stdout.on("resume", stdOutResume);

  child.stdout.on("end", stdOutResume);

  child.stdout.on("end", stdOutEnd);

  // Triggers at last
  child.stdout.on("close", (data) => stdOutClose(data, req, res));

  // On shell command execution error
  child.stderr.on("data", stdErrData);

});


function getChannelName(req) {
  return `${req.body.firebaseProjectId}-preview-channel`
}

/**
 * Gets project configuration
 */
app.post('/verifyProjectConfig', async (req, res) => {

  req.setTimeout(0);  // Make the request wait untis res.send() is called. No timeout.

  const projectConfig = await verifyProjectConfig(req)

  io.emit("\n\n VERIFICATION PROCESS IS COMPLETED. PLEASE SWITCH TO CONFIG TAB\n\n\n\n ")

  // Let the emit go first
  setTimeout(() => {
    res.send({ projectConfig: projectConfig });
  }, 0);

});


/**
 * Creates service account
 */
app.post('/createService', async (req, res) => {

  req.setTimeout(0);  // Make the request wait untis res.send() is called. No timeout.

  const projectConfig = await createService(req)

  io.emit("\n\n SERVICE HAS BEEN CREATED \n\n\n\n ")

  // Let the emit go first
  setTimeout(() => {
    res.send({ projectConfig: projectConfig });
  }, 0);


});


/**
 * Get .runtimeconfig.json json
 */
app.post('/getConfig', async (req, res) => {

  let configJSON;
  const commandPart1 = "cd " + req.body.projectRootFolder + " && "
  let command;

  if (req.body.which == "runtime") {

    command = ENVIRONMENT.READ_RUNTIME_CONFIG
      .replace(/PROJECT_ROOT_FOLDER-/g, req.body.projectRootFolder)
      .replace(/FUNCTIONS_ROOT_FOLDER/g, req.body.functionsRootFolder);

    let runtimeConfigJSON;
    try {
      configJSON = JSON.stringify(JSON.parse(await getValue(command, "")), null, 2);
    } catch (error) {
      configJSON = JSON.stringify(error);
    }

  }
  else {
    // Firebase Runtime config
    command = commandPart1 + FIREBASE.GET_WEB_APP_ID + req.body.webAppId
    configJSON = await getValue(command, "");

    if (configJSON.includes("firebase.initializeApp")) {
      configJSON = JSON.stringify(JSON.parse(configJSON.split(/\(|\)/)[1]), null, 2);
    }
    else {
      configJSON = "WEB APP NOT FOUND";
    }

  }

  res.send({ configJSON: configJSON });

});


/**
 * Set .runtimeconfig.json json
 */
app.post('/setConfig', async (req, res) => {

  const commandPart1 = "cd " + req.body.projectRootFolder + req.body.functionsRootFolder + " && "

  command = commandPart1 + ENVIRONMENT.WRITE_RUNTIME_CONFIG
    .replace("RUNTIME_CONFIG", req.body.config)
    .replace(/PROJECT_ROOT_FOLDER-/g, req.body.projectRootFolder)
    .replace(/FUNCTIONS_ROOT_FOLDER/g, req.body.functionsRootFolder);

  try {
    let dd = await isExist(command, "");

    res.send(true);
  } catch (error) {
    res.send(error);
  }


});




app.get('/index.js', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.js'));
});

server.listen(3000, () => {
  console.log('listening on http://localhost:3000');

});


// Create connection socket
io.on('connection', (socket) => {
  console.log('a user connected');

  clearInterval(timestampInterval);

  // Emit timestamp every second
  timestampInterval = setInterval(() => {
    // Start sending timestamp on connection from client
    io.emit("timestamp", new Date().toISOString())
  }, 5000);


  socket.on('disconnect', () => {
    console.log('user disconnected');
  });

});


// Handle shell sdtout data
function stdOutData(message, req, res) {

  if (req.body.command === "BUILD_FUNCTIONS" && message.toString().indexOf("function initialized") !== -1) {

    const lines = message.split(/✔/g)
    
    lines.forEach(line => {
      if(line.includes("function initialized")){
        const routeName = line.toString().split("-")[2].split("]")[0];
        routes.push(routeName);
      }
    })

  }

  if (req.body.command === "CREATE_CHANNEL"){
    if(message.includes("Channel URL")){
      let channelURL = message.split("https")[1].split(".web.app")[0];
      res.channelURL = `https${channelURL}.web.app`;
    }
  }
  

  message = message.replace(/[^a-zA-Z -_~!@#$%^&*()_+=-[]\\|<>,.\/?{}:\r\n\t]/g, "").replace(/(mmi|mm|1m|32m|39m|36mi|22m)/g, "");

  io.emit("output", { message: message });
}


// Handle shell sdtout pause
function stdOutPause(data) {
  console.log('on pause');
}


// Handle shell sdtout resume
function stdOutResume(data) {
  console.log('on resume');
}


// Handle shell sdtout end
function stdOutEnd(data) {
  console.log('on end');
}


// Handle shell sdtout close
function stdOutClose(data, req, res) {
  console.log('on close');
  let resp = { status: true }; 
  
  if (res.hasOwnProperty("channelURL")) resp.channelURL = res.channelURL;

  if (req.body.command === "BUILD_FUNCTIONS") {
    resp.routes = routes;
  }

  res.send(resp);   // End the request by responding to it.
}

// Handle shell sdterr pause
function stdErrData(message) {
  console.log('on error data');
  io.emit("output", { message: message });
}

/**
 * Collects project information
 */
async function verifyProjectConfig(req) {

  let projectConfig = {}

  const commandPart1 = "cd " + req.body.projectRootFolder + " && "
  let command;

  try {

    // GCLOUD
    // Checks whether user logged into gcloud account
    projectConfig.gCloudLoggedIn = await isExist(GCLOUD.LIST, req.body.userEmail);

    // Check GCloud account id is same as the given userEmail
    command = commandPart1 + GCLOUD.ACCOUNT_ID;
    projectConfig.gCloudAccountId = (await getValue(command, "")) == req.body.userEmail ? req.body.userEmail : false;


    // Set given project id as gcloud default project
    command = commandPart1 + GCLOUD.USE + req.body.firebaseProjectId;
    projectConfig.gCloudProjectId = (await isExist(command, req.body.firebaseProjectId)) ? "INVALID GCLOUD PROJECT ID" : req.body.firebaseProjectId;


    // Confirm the given service account exists
    command = commandPart1 + GCLOUD.SERVICE_LIST;
    let lookFor = `${req.body.serviceAccountId}@${req.body.firebaseProjectId}.iam.gserviceaccount.com`;
    projectConfig.serviceAccountId = (await isExist(command, lookFor)) ? req.body.serviceAccountId : "SERVICE ACCOUNT NOT FOUND<br/>" + lookFor;


    // Check whether service account key file exists
    command = commandPart1 + ENVIRONMENT.KEY_FILE_EXISTS
      .replace("USER_HOME_ROOT", req.body.userHomeFolder);

    projectConfig.serviceAccountKeyFile = await getValue(command, "");
    projectConfig.serviceAccountKeyFile = (projectConfig.serviceAccountKeyFile == "") ? false : projectConfig.serviceAccountKeyFile;


    // Get service account roles
    command = commandPart1 + GCLOUD.GET_SERVICE_ROLES
      .replace("PROJECT_ID", req.body.firebaseProjectId);
    let rawOutput1 = await getValue(command, "");


    // Extract roles from output
    const _email = getServiceAccountEmail(req)
    rawOutput1 = rawOutput1.split('etag:')[0].split("- members:").reduce((reduced, current) => {
      if (current.includes(_email)) reduced.push(current.split('role:')[1]);
      return reduced;
    }, [])

    projectConfig.serviceAccountRoles = rawOutput1.join("; ")



    // FIREBASE

    // Check whether user logged into firebase account
    projectConfig.firebaseLoggedIn = await isExist(FIREBASE.LIST, req.body.userEmail);


    // Set given project id as firebase default project
    command = commandPart1 + FIREBASE.USE + req.body.firebaseProjectId;
    projectConfig.firebaseProjectId = (await isExist(command, "Invalid project selection")) ? "INVALID FIREBASE PROJECT ID" : req.body.firebaseProjectId;


    // Firebase web app id
    command = commandPart1 + FIREBASE.GET_WEB_APP_ID
    projectConfig.firebaseWebAppId = (await isExist(command, req.body.webAppId)) ? req.body.webAppId : "WEB APP NOT FOUND";



    // Check firebase .runtimeconfig.json file exists
    command = ENVIRONMENT.RUNTIME_CONFIG_FILE_EXISTS
      .replace(/PROJECT_ROOT_FOLDER-/g, req.body.projectRootFolder)
      .replace(/FUNCTIONS_ROOT_FOLDER/g, req.body.functionsRootFolder);

    lookFor = `${req.body.projectRootFolder}${req.body.functionsRootFolder}/.runtimeconfig.json`;
    projectConfig.runtimeConfig = (await isExist(command, lookFor)) ? lookFor : "RUNTIMECONFIG FILE NOT FOUND";


    return projectConfig;

  } catch (error) {
    return Promise.reject(error)
  }

}




// Create service account
async function createService(req) {

  const commandPart1 = "cd " + req.body.projectRootFolder + " && "
  let command;

  try {

    const serviceAccountEmail = getServiceAccountEmail(req);

    // Delete service account if exists. Only then we could create certificate file
    command = GCLOUD.DELETE_SERVICE + serviceAccountEmail + " --quiet";
    let serviceAccountDeleted = await isExist(command, /(deleted service account|NOT_FOUND)/);
    if (serviceAccountDeleted) io.emit("output", { message: "Service account is not found or deleted." });


    // Create service account
    command = commandPart1 + GCLOUD.CREATE_SERVICE
      .replace("SERVICE_NAME", req.body.serviceAccountId)
      .replace("DISPLAY_NAME", req.body.serviceAccountName)
      .replace("DESCRIPTION", req.body.serviceAccountName)

    const serviceAccountCreated = await isExist(command, "Created service account");
    if (serviceAccountCreated) io.emit("output", { message: "Service account has been created." });


    // Grant permissions to the service account
    // https://cloud.google.com/logging/docs/access-control
    // roles/logging.admin or roles/logging.logWriter
    command = commandPart1 + GCLOUD.GRANT_SERVICE_PERMISSIONS
      .replace("PROJECT_ID", req.body.firebaseProjectId)
      .replace("SERVICE_ACCOUNT_EMAIL", serviceAccountEmail);

    const serviceAccountOwnerRole = await isExist(command + "--role='roles/owner'", "role: roles/owner");
    if (serviceAccountOwnerRole) io.emit("output", { message: "\n'roles/owner' role has been added to service account.\n" });

    const serviceAccountLoggingRole = await isExist(command + "--role='roles/logging.admin'", "role: roles/logging.admin");
    if (serviceAccountLoggingRole) io.emit("output", { message: "\n'roles/logging.admin' role has been added to service account.\n" });


    // Generate key file and place it under given folder
    // The key file can only be downloaded once. (https://cloud.google.com/iam/docs/creating-managing-service-account-keys)
    const key_fileName = `${req.body.userHomeFolder}${req.body.firebaseProjectId}--${req.body.serviceAccountId}--key.json`;
    command = commandPart1 + GCLOUD.SAVE_SERVICE_KEY_FILE
      .replace("SERVICE_KEY_FILENAME", key_fileName)
      .replace("SERVICE_ACCOUNT_EMAIL", serviceAccountEmail);

    const serviceKeyFile = await isExist(command, "created key");
    if (serviceKeyFile) io.emit("output", { message: "\n Service Key file is saved as " + key_fileName });


    // Set GOOGLE_APPLICATION_CREDENTIALS environment variable
    command = commandPart1 + ENVIRONMENT.CREATE_BASHRC
      .replace("SERVICE_KEY_FILENAME", key_fileName)
      .replace("USER_HOME_ROOT", req.body.userHomeFolder);

    const createBashrcFile = await isExist(command, "created key");
    if (createBashrcFile) io.emit("output", { message: "\nGOOGLE_APPLICATION_CREDENTIALS environment variable is added to .bashrc file" + key_fileName });


    // Load .bashrc file and reload it
    command = commandPart1 + ENVIRONMENT.SOURCE_BASHRC
      .replace("USER_HOME_ROOT", req.body.userHomeFolder);

    const loadBashrcFile = await isExist(command, key_fileName);
    if (loadBashrcFile) io.emit("output", { message: "\n.bashrc file is reloaded and $GOOGLE_APPLICATION_CREDENTIALS contains service key file path." });


    io.emit("output", { message: "\n\n*****SERVICE HAS BEEN CREATED. VERIFY PROJECT CONFIGURATION AGAIN*****\n" });

    return true;

  } catch (error) {
    return Promise.reject(error)
  }

}


// Runs given shell command and looks for given lookup value in output messages and returns true/false
/**
 * Checks whether user logged into gcloud account
 * @command - provides shell command to execute
 * @lookFor - regular expression to match value(s) in command output
 */
async function isExist(command, lookFor) {

  console.log('=======command:: ', command);
  return new Promise((resolve, reject) => {

    try {
      io.emit("output", { message: command });

      let child = shell.exec(command, { async: true });
      let returnValue = false;
      let rx = new RegExp(lookFor);


      // Check whether the user is logged in using gcloud cli
      child.stdout.on("data", (message) => {
        io.emit("output", { message: message });

        if (rx.test(message)) returnValue = true;
      });


      // Check for lookFor message in errors
      child.stderr.on("data", (message) => {
        console.log('-----------on stderror', message);
        io.emit("output", { message: message });

        if (rx.test(message)) returnValue = true;
      });



      // When shell command completed
      child.stdout.on("close", (message) => {
        // console.log('=======close', command, message);
        return resolve(returnValue);
      });


    } catch (error) {
      return Promise.reject(error);
    }

  })

}

// Runs given command and return the message that contains given lookup value.
/**
 * Executes given shell command returns output if lookup value found.
 * If lookup value is blank, all the output is returned.
 * @command - provides shell command to execute
 * @lookFor - regular expression to match value(s) in command output
 */
async function getValue(command, lookFor) {

  return new Promise((resolve, reject) => {
    try {

      io.emit("output", { message: command });

      let child = shell.exec(command, { async: true });
      let returnValue = "";

      // Check whether the user is logged in using gcloud cli
      child.stdout.on("data", (message) => {
        io.emit("output", { message: message });

        if (lookFor == "") { returnValue += message + "\n"; }
        else {
          let rx = new RegExp(lookFor);
          if (rx.test(message)) returnValue = message;
        }

      });

      // When process ends
      child.stdout.on("close", (message) => {
        // console.log('-------close', command, message);
        returnValue = returnValue.replace(/(\r\n|\r|\n)/g, "").trim();
        return resolve(returnValue);
      });

    } catch (error) {
      return Promise.reject(error);
    }

  })

}

function getServiceAccountEmail(req) {
  return `${req.body.serviceAccountId}@${req.body.firebaseProjectId}.iam.gserviceaccount.com`;
}