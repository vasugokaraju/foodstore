#!/usr/bin/env bash

# THIS FILE CREATES GOOGLE SERVICE ACCOUNT AND PLACES THE .JSON FILE UNDER ~/ FILDER AND ADDS EXPORT STATEMENT IN .bashrc FILE


# USAGE
# ./createServiceAccount.sh "my-test-service-1" "My Test Service Account 1" "This is a test service account."


# PARAMETERS
# @service_name = Name of the service
# @service_dsiplay = Display Name of the service
# @service_desc = Description of the service


# VARIABLES
# $project_id = Current project that the user is already logged into.  Service will be created under this project.
# $service_account = Google service account details
# $key_filename = Private key file name


# Assign command line parameters to variables
service_name=$1
service_dsiplay=$2
service_desc=$3

# Get the user confirmation about current project under which the new service will be created.
# https://cloud.google.com/sdk/gcloud/reference/config/get-value
project_id=$(gcloud config get-value project)
home_dir=~/
key_filename="$home_dir$project_id--$service_name--key.json"


// Create service account
createServiceAcct(){
    gcloud iam service-accounts create $service_name --display-name="$service_dsiplay" --description="$service_desc"
}

listServiceAccount(){
    # List all service accounts
    # https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/list
    gcloud iam service-accounts list
}

clear
echo "Create '$service_name' service account in '$project_id' project?" [N/y]
read project_confirmation
if [ "$project_confirmation" != "${project_confirmation#[Yy]}" ] ;then

    # Create google service account
    if createServiceAcct
    then
        # Grant permissions to the service account
        # https://cloud.google.com/logging/docs/access-control
        # roles/logging.admin or roles/logging.logWriter
        gcloud projects add-iam-policy-binding $project_id --member="serviceAccount:$service_name@$project_id.iam.gserviceaccount.com" --role="roles/owner"
        gcloud projects add-iam-policy-binding $project_id --member="serviceAccount:$service_name@$project_id.iam.gserviceaccount.com" --role="roles/logging.admin"

        # Generate key file and place it under user home folder (~/)
        # The key file can only be downloaded once. (https://cloud.google.com/iam/docs/creating-managing-service-account-keys)
        gcloud iam service-accounts keys create $key_filename --iam-account=$service_name@$project_id.iam.gserviceaccount.com

        # Add export statement in .bashrc file
        echo -e "\nexport GOOGLE_APPLICATION_CREDENTIALS=\"$key_filename\"" >> $home_dir.bashrc

        # Load the new path
        source $home_dir.bashrc # not reflecting the changes.
        export GOOGLE_APPLICATION_CREDENTIALS="$key_filename"

        # Show the APIs & Services Console URL
        echo
        echo
        echo "Your new service can be found here"
        echo "https://console.cloud.google.com/apis/credentials?project=$project_id"

        # Call function to list existing service accounts under this project.
        listServiceAccount

        # Run the following command to populate GOOGLE_APPLICATION_CREDENTIALS variable
        # source ~/.bashrc

    else 
        echo
        echo "Service account name '$service_name' already exists in '$project_id' project.  Please change and try again."
        echo

        # Call function to list existing service accounts under this project.
        listServiceAccount
    fi

else
    echo "Please switch to desired project using 'gcloud config set project \"your-project-id\"' and try to create the service again."
fi


# To delete a service account
# https://cloud.google.com/sdk/gcloud/reference/iam/service-accounts/delete
# Y | gcloud iam service-accounts delete my-test-service-1@my-firebase-project-18725.iam.gserviceaccount.com