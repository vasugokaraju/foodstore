### =====================AppCods======================
@token=
@appCodsId={{createAppCods.response.body.data.id}}

### --------------------CREATE appCods
# @name createAppCods
POST {{host}}appCodsCreate HTTP/1/1
Authorization:Bearer {{idToken}}
Cookie: {{cookies}}
Content-Type: application/json

{
 "lang": "en-US",
 "code": "5e9cosj59orin7c2ncjy06uzhhvvx28andfro055lzs6ne8jnx",
 "titl": "h2t2w38ati",
 "msg": "samamyj3n2ldk7ydta49",
 "httpCode": "lmfmr4w102bnebpdxmfu",
 "actn": 0
}

### --------------------READ appCods
GET {{host}}appCodsRead/{{appCodsId}} HTTP/1/1
Authorization:Bearer {{idToken}}
Cookie: {{cookies}}

### --------------------UPDATE appCods
# @name updateAppCods
PUT {{host}}appCodsUpdate/{{appCodsId}} HTTP/1/1
Authorization:Bearer {{idToken}}
Cookie: {{cookies}}
Content-Type: application/json

{
 "lang": "en-US",
 "code": "3akerdyehz8pqpdtjtslm05usqxcve7hwn9wtslzwycogkzd3t",
 "titl": "vnvw11kx8w",
 "msg": "09qxxoybg0v9jbms06de",
 "httpCode": "qzpylfmtfwoofhy8rcis",
 "actn": 0
}

### --------------------DELETE appCods
DELETE {{host}}appCodsDelete/{{appCodsId}} HTTP/1/1
Authorization:Bearer {{idToken}}
Cookie: {{cookies}}

### ================================================