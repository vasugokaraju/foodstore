import 'mocha';
import chai from 'chai';
import chaiHttp = require('chai-http');

const server = "http://localhost:5000/api";
// const server = "/api";

const testUser:any = {
 "displayName": "Liza",
 "email": "Nikita35@gmail.com",
 "password": "VBVT6khDXf9qYbX",
 "phoneNumber": "+19958178000",
 "emailVerified": true,
 "disabled": false,
 "photoURL": "http://myphotosdfsf.com",
 "role": {
  "code": 0,
  "name": "SUPERADMIN"
 },
 "dob": "2020-11-14T09:03:21.442Z",
 "ssn": 324717111,
 "address": {
  "city": {
   "colony": "New Sylvester",
   "foundedon": "2021-03-03T16:08:58.693Z"
  },
  "country": "Malta"
 }
};

chai.use(chaiHttp);
// const expect = chai.expect;
chai.should();

let resp: any;
let encodedCredentials: string;
let token: string;
let cookies: string;
let id:string;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // To supress certificate expiration error.  It happens because of self signed certs for local dev env.

export default class TestAuthService {

    constructor(){

    }

    public async login(): Promise<any>{

        try {
            // Encode login password.  This step should be done on client side
            resp = await chai.request(server).post("/encode/" + `${testUser.email}:${testUser.password}`);
            encodedCredentials = resp.body.data;
    
            resp.should.have.status(200);
    
            // Login using the encoded credentials
            resp = await chai.request(server).post("/login").set("Authorization", encodedCredentials);
    
            // If user not found, register user
            if (resp.body.code == "auth/user-not-found") {
                // Create new user
                resp = await chai.request(server)
                    .post("/register")
                    .set("Content-Type", "application/json")
                    .send(testUser);
    
                resp.should.have.status(201);
    
                // Encode login password.  This step should be done on client side
                resp = await chai.request(server).post("/encode/" + `${testUser.email}:${testUser.password}`);
                encodedCredentials = resp.body.data;
    
                resp.should.have.status(200);
    
                // Login using the encoded credentials
                resp = await chai.request(server).post("/login").set("Authorization", encodedCredentials);
            }
    
            token = resp.body.data.userInfo.token;
            cookies = resp.headers["set-cookie"].toString();
            id = resp.body.data.userInfo.id;
        
            return Promise.resolve({
                token:token, 
                cookies:cookies, 
                id:id, 
                encodedCredentials:encodedCredentials,
                login:testUser.email,
                password:testUser.password,
                server:server
            });
    
        } catch (error) {
            token = "";
            return Promise.reject();
        }

    }

}