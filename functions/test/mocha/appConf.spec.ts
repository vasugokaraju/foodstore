import 'mocha';
import chai from 'chai';
import chaiHttp = require('chai-http');
const appConfSampleData = require('./appConfSampleData.json');
import TestAuthService from './testAuthService';

let testAuthServiceimport = new TestAuthService();

let server = "";  // This value will be provided by testAuthService in the before() call.

chai.use(chaiHttp);
// const expect = chai.expect;
chai.should();

let authInfo:any = {};
let resp: any;
let appConfId:string;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // To supress certificate expiration error.  It happens because of self signed certs for local dev env.

// ************************* LOGIN TO GET TOKEN *************************
// Login and get token.  Register the test user if not done already
before(async () => {
    authInfo = await testAuthServiceimport.login();
    server = authInfo.server;
});

// **************************************************************

describe("AppConf Tests", async () => {

    describe('Data Validity Tests', async () => {

        let createTest = it("Should create AppConf", async () => {

            try {
                resp = await chai.request(server)
                    .post("/appConfCreate/")
                    .set({ "Authorization": `Bearer ${authInfo.token}` })
                    .set("Cookie", `${authInfo.cookies}`)
                    .send(appConfSampleData[0]);

                printResponse(createTest);

                resp.should.have.status(201)
                appConfId = resp.body.data.id;

                return Promise.resolve(true);
            } catch (e) {
                authInfo.token = '';
                return Promise.reject(e);
            }

        })

        let readTest = it("Should read AppConf", async () => {

            try {
                resp = await chai.request(server)
                .get("/appConfRead/" + appConfId)
                .set({ "Authorization": `Bearer ${authInfo.token}` })
                .set("Cookie", `${authInfo.cookies}`);

                printResponse(readTest);

                resp.should.have.status(200)
                
                return Promise.resolve(true);
            } catch (e) {
                authInfo.token = '';
                return Promise.reject(e);
            }

        })

        let updateTest = it("Should update AppConf", async () => {

            try {
                resp = await chai.request(server)
                    .put("/appConfUpdate/" + appConfId)
                    .set({ "Authorization": `Bearer ${authInfo.token}` })
                    .set("Cookie", `${authInfo.cookies}`)
                    .send(appConfSampleData[1]);

                printResponse(updateTest);

                resp.should.have.status(200)
                
                return Promise.resolve(true);
            } catch (e) {
                authInfo.token = '';
                return Promise.reject(e);
            }

        })

/*
        let deleteTest = it("Should delete AppConf", async () => {

            try {
                resp = await chai.request(server).delete("/appConfDelete/" + appConfId)
                    .set({ "Authorization": `Bearer ${authInfo.token}` })
                    .set("Cookie", `${authInfo.cookies}`);

                printResponse(deleteTest);

                resp.should.have.status(200)
                
                return Promise.resolve(true);
            } catch (e) {
                authInfo.token = '';
                return Promise.reject(e);
            }

        })
*/
    })

    describe('Data Integrity Tests', async () => {

    });

});

function printResponse(myTest?: any) {

    if (myTest) console.log("Test Title: ", myTest.title);

    console.log('\nresponse.status:', JSON.stringify(resp.status, null, " "));
    console.log('response.body:', JSON.stringify(resp.body, null, " "));
}