import 'mocha';
import chai from 'chai';
import chaiHttp = require('chai-http');
const sampleData = require('./productsSampleData.json');
import TestAuthService from './testAuthService';

let server = "";  // This value will be provided by testAuthService in the before() call.

let testAuthServiceimport = new TestAuthService();

chai.use(chaiHttp);
// const expect = chai.expect;
chai.should();

let resp: any;
let authInfo:any = {};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // To supress certificate expiration error.  It happens because of self signed certs for local dev env.

// ************************* LOGIN TO GET TOKEN *************************
// Login and get token.  Register the test user if not done already
before(async () => {
    authInfo = await testAuthServiceimport.login();
    server = authInfo.server;
});

// **************************************************************

describe("Upload Products Sample Data", async () => {

    sampleData.forEach(function(record:any, indx:number) {

        const createTest = it("Should create Products record " + indx, async () => {

            try {
                resp = await chai.request(server)
                    .post("/productsCreate/")
                    .set({ "Authorization": `Bearer ${authInfo.token}` })
                    .set("Cookie", `${authInfo.cookies}`)
                    .send(record);
   
                printResponse(createTest);

                resp.should.have.status(201)
                console.log('resp.body.data.id', resp.body.data);

                return Promise.resolve(true);
            } catch (e) {
                authInfo.token = '';
                return Promise.reject(e);
            }

        });

      });

});


function printResponse(myTest?: any) {

    if (myTest) console.log("Test Title: ", myTest.title);

    console.log('\nresponse.status:', JSON.stringify(resp.status, null, " "));
    console.log('response.body:', JSON.stringify(resp.body, null, " "));
}