#!/usr/bin/env bash

# THIS SCRIPT RETRIEVES FIREBASE WEB APP CONFIG AND STORES IN /functions/.runtimeconfig.json
# THIS CONFIG GETS UPLOADED TO PRODUCTION WHEN WEB APP IS DEPLOYED

# USAGE
# ./getRuntimeConfig.sh 1:642322825162:web:90371b4c54a7d937099954

# If the web_app_id is not provided by the user, the default app id will be taken
web_app_id=$1    # Get firebase web app id using 'firebase apps:list WEB'

# Check whether the parent folder name is /functions
parent_dir=$(pwd)

if [[ $parent_dir != *"/functions"* ]]; then
  echo "Please run this file from /functions folder."
else
    echo "Retrieving .runtimeconfig.json content from firebase..."

    # Get config into a variable
    config=$(firebase apps:sdkconfig WEB $web_app_id)

    # Extract JSON from config string
    substring="$(echo $config| cut -d'(' -f 2)"   # cut string where '('' starts.
    JSON="{ \"fireconf\":  ${substring//[);]/} }"

    # Write to file    
    rm -rf .runtimeconfig.json

    # Read json to variables to set functions configuration
    projectId=$( jq -r  '.fireconf.projectId' <<< "${JSON}" )
    appId=$( jq -r  '.fireconf.appId' <<< "${JSON}" )
    storageBucket=$( jq -r  '.fireconf.storageBucket' <<< "${JSON}" )
    locationId=$( jq -r  '.fireconf.locationId' <<< "${JSON}" )
    apiKey=$( jq -r  '.fireconf.apiKey' <<< "${JSON}" )
    authDomain=$( jq -r  '.fireconf.authDomain' <<< "${JSON}" )
    messagingSenderId=$( jq -r  '.fireconf.messagingSenderId' <<< "${JSON}" )

    firebase functions:config:set   fireconf.apikey="${apiKey}" \
                                fireconf.authdomain="${authDomain}" \
                                fireconf.projectid="${projectId}" \
                                fireconf.storagebucket="${storageBucket}" \
                                fireconf.messagingsenderid="${messagingSenderId}" \
                                fireconf.appid="${appId}" \
                                fireconf.measurementid="" \
                                fireconf.locationid="${locationId}"

    # Show the runtime configuration that is being deployed
    echo "Runtime configuration for functions"
    firebase functions:config:get

    firebase functions:config:get > .runtimeconfig.json
fi