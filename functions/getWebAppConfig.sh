#!/usr/bin/env bash

# THIS SCRIPT RETRIEVES FIREBASE WEB APP CONFIG AND STORES IN /functions/.runtimeconfig.json
# THIS CONFIG GETS UPLOADED TO PRODUCTION WHEN WEB APP IS DEPLOYED

# USAGE
# ./getWebAppConfig.sh 1:642322825162:web:90371b4c54a7d937099954

web_app_id=$1    # Get firebase web app id using 'firebase apps:list WEB'

# Check whether the parent folder name is /functions
parent_dir=$(pwd)

if [[ $parent_dir != *"/functions"* ]]; then
  echo "Please run this file from /functions folder."
else
    # Get config into a variable
    config=$(firebase apps:sdkconfig WEB $web_app_id)

    # Extract JSON from config string
    substring="$(echo $config| cut -d'(' -f 2)"   # cut string where '('' starts.
    JSON="{ \"fireconf\":  ${substring//[);]/} }"

    # Write to file    
    rm -rf .runtimeconfig.json
    echo $JSON >> .runtimeconfig.json
fi