"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.products = void 0;
const productsRoutes_1 = require("./productsRoutes");
exports.products = [
    productsRoutes_1.productsPreload,
    productsRoutes_1.productsCreate,
    productsRoutes_1.productsReadOne,
    productsRoutes_1.productsReadMany,
    productsRoutes_1.productsUpdate,
    productsRoutes_1.productsDelete,
    productsRoutes_1.productsDownload,
    productsRoutes_1.productsUpload,
    productsRoutes_1.productsIdNameReadMany,
];
//# sourceMappingURL=index.js.map