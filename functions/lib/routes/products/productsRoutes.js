"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productsIdNameReadMany = exports.productsUpload = exports.productsDownload = exports.productsDelete = exports.productsUpdate = exports.productsCreate = exports.productsReadMany = exports.productsReadOne = exports.productsPreload = void 0;
const typedi_1 = require("typedi");
const express_1 = require("express");
const productsService_1 = __importDefault(require("../../services/productsService"));
const errorService_1 = __importDefault(require("../../services/errorService"));
const middleware_1 = __importDefault(require("../../middleware"));
const routerProductsPreload = express_1.Router();
const routerProductsReadOne = express_1.Router();
const routerProductsReadMany = express_1.Router();
const routerProductsCreate = express_1.Router();
const routerProductsUpdate = express_1.Router();
const routerProductsDelete = express_1.Router();
const routerProductsDownload = express_1.Router();
const routerProductsUpload = express_1.Router();
const routerGetProductsIdNameReadMany = express_1.Router();
const productsService = typedi_1.Container.get(productsService_1.default);
let errorService = typedi_1.Container.get(errorService_1.default);
/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerProductsPreload.post('/:id?', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("products", req, res, next); }, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await productsService.preload(req.userProfile, req.body.whereQuery);
        if (result) {
            res.appSuccess("S0000", result);
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerProductsReadOne.get('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("products", req, res, next); }, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        // const result = await productsService.read(req.params.id? req.params : req.query);  // id || query parameters
        const result = await productsService.readOne(req.params.id);
        if (result.length != 0) {
            let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
            res.appSuccess("S1400", result, customMessage); // Record(s) found.
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
/**
 * Finds records using where clause
 */
routerProductsReadMany.post('/', middleware_1.default.validateToken, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await productsService.readMany(req.body.whereQuery);
        let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
        res.appSuccess("S1400", result, customMessage); // Record(s) found.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerProductsCreate.post('/', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("products", req, res, next); }, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const data = req.body;
        const result = await productsService.create(data);
        res.appSuccess("S1401", result, "1 record has been created."); //Record has been created.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerProductsUpdate.put('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("products", req, res, next); }, async (req, res) => {
    try {
        const id = req.params.id;
        const data = req.body;
        const result = await productsService.update(id, data);
        res.appSuccess("S1402", result, "1 record has been updated.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerProductsDelete.delete('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("products", req, res, next); }, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const id = req.params.id;
        const result = await productsService.delete(id);
        res.appSuccess("S1403", result.value, "1 record has been deleted.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Downloads module data
*/
routerProductsDownload.post('/', middleware_1.default.validateToken, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await productsService.download(req.body);
        res.appSuccess("S1403", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Uploads module data
*/
routerProductsUpload.post('/', middleware_1.default.validateToken, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await productsService.upload(req);
        res.appSuccess("S1404", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
* Returns Products data in the form of code-name for the purpose of Autocomplete controls on UI.
*/
routerGetProductsIdNameReadMany.post('/', middleware_1.default.validateToken, middleware_1.default.productsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await productsService.getProductsIdNameReadMany(req);
        let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
        res.appSuccess("S0000", result, customMessage); // Record(s) found.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
exports.productsPreload = { name: 'productsPreload', router: routerProductsPreload };
exports.productsReadOne = { name: 'productsReadOne', router: routerProductsReadOne };
exports.productsReadMany = { name: "productsReadMany", router: routerProductsReadMany };
exports.productsCreate = { name: "productsCreate", router: routerProductsCreate };
exports.productsUpdate = { name: "productsUpdate", router: routerProductsUpdate };
exports.productsDelete = { name: "productsDelete", router: routerProductsDelete };
exports.productsDownload = { name: "productsDownload", router: routerProductsDownload };
exports.productsUpload = { name: "productsUpload", router: routerProductsUpload };
exports.productsIdNameReadMany = { name: "productsIdNameReadMany", router: routerGetProductsIdNameReadMany };
//# sourceMappingURL=productsRoutes.js.map