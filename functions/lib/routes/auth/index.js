"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.auth = void 0;
const authRoutes_1 = require("./authRoutes");
exports.auth = [authRoutes_1.register, authRoutes_1.registerID3, authRoutes_1.verifyID3, authRoutes_1.login, authRoutes_1.logout, authRoutes_1.createPassword, authRoutes_1.changePassword, authRoutes_1.encode, authRoutes_1.decode, authRoutes_1.renewToken, authRoutes_1.hash, authRoutes_1.test, authRoutes_1.gfac, authRoutes_1.onSignInWithPhone, authRoutes_1.isPhoneNumberRegistered, authRoutes_1.verifyRegisteredEmail, authRoutes_1.sendEmailVerification, authRoutes_1.sendEmailOTPLink, authRoutes_1.verifyEmailOTPLink, authRoutes_1.sendForgotPasswordLink, authRoutes_1.verifyForgotPasswordLink, authRoutes_1.resetPassword, authRoutes_1.federatedLogin];
//# sourceMappingURL=index.js.map