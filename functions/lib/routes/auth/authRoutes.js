"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.federatedLogin = exports.resetPassword = exports.verifyForgotPasswordLink = exports.sendForgotPasswordLink = exports.verifyEmailOTPLink = exports.sendEmailOTPLink = exports.sendEmailVerification = exports.verifyRegisteredEmail = exports.isPhoneNumberRegistered = exports.onSignInWithPhone = exports.gfac = exports.hash = exports.decode = exports.encode = exports.test = exports.renewToken = exports.changePassword = exports.createPassword = exports.logout = exports.login = exports.verifyID3 = exports.registerID3 = exports.register = void 0;
const express_1 = require("express");
const typedi_1 = require("typedi");
const middleware_1 = __importDefault(require("../../middleware"));
const errorService_1 = __importDefault(require("../../services/errorService"));
const authService_1 = __importDefault(require("../../services/authService"));
const usersService_1 = __importDefault(require("../../services/usersService"));
const routerRegister = express_1.Router();
const routerRegisterID3 = express_1.Router(); // for Admin App registration
const routerVerifyID3 = express_1.Router(); // for Admin App user verification
const routerAuthLogin = express_1.Router();
const routerAuthLogout = express_1.Router();
const routerAuthChangePassword = express_1.Router();
const routerAuthCreatePassword = express_1.Router();
const routerAuthRenewToken = express_1.Router();
const routerAuthTest_Get = express_1.Router();
const routerAuthEncode = express_1.Router();
const routerAuthDecode = express_1.Router();
const routerAuthHash = express_1.Router();
const routerGFAC = express_1.Router(); // Get Firebase App Configuration
const routerOnSignInwithPhoneNumber = express_1.Router(); // Sign in with phone number and reCaptcha verifier
const routerIsPhoneNumberRegistered = express_1.Router();
const routerSendEmailVerification = express_1.Router();
const routerVerifyRegisteredEmail = express_1.Router();
const routerSendEmailOTPLink = express_1.Router();
const routerVerifyEmailOTPLink = express_1.Router();
const routerSendForgotPasswordLink = express_1.Router();
const routerVerifyForgotPasswordLink = express_1.Router();
const routerResetPassword = express_1.Router();
const routerFederatedLogin = express_1.Router();
let errorService = typedi_1.Container.get(errorService_1.default);
let authService = typedi_1.Container.get(authService_1.default);
let usersService = typedi_1.Container.get(usersService_1.default);
routerRegister.post('/', (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, async (req, res) => {
    try {
        const data = req.body;
        // TODO:  Set the role as USER for all public registrations
        const result = await usersService.create(data);
        res.appSuccess("S1401", result, "1 record has been created."); //Record has been created.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Route to register admin app users
routerRegisterID3.post('/', (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, async (req, res) => {
    try {
        const data = req.body;
        data.disabled = true; // Admin has to manually verify and enable the user
        data.emailVerified = false;
        // Registers app admin
        // FID, IMEI and Gmail are called as "ID3".
        // https://www.notion.so/Authentication-Page-bdcd5473080c45bd853c928b493c032d
        const result = await usersService.create(data);
        res.appSuccess("S1401", result, "1 record has been created."); //Record has been created.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Route to register admin app users
routerVerifyID3.post('/', (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, async (req, res) => {
    try {
        const data = req.body;
        // TODO: Validate ID3 and return the status
        const result = {
            status: true,
            data: data
        };
        res.appSuccess("S0000", result); // Query has been successful.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Validate user credentials using Basic-Auth and generate JWT token based on user's Consumer credentials and return token
routerAuthLogin.post('/', async (req, res) => {
    try {
        const userInfo = await authService.login(req, res);
        res.appSuccess("S1000", userInfo); // Credentials are valid
    }
    catch (e) {
        res.appFail(errorService.build(e, null)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerAuthLogout.post('/', middleware_1.default.validateRequest, async (req, res) => {
    try {
        const status = await authService.logout(req, res);
        res.appSuccess("S1008", { loggedOut: status }); // Logged out
    }
    catch (e) {
        res.appFail(errorService.build(e, null)); // Unknown error during string encode attempt.  Please try again.
    }
});
// // A route for testing
// route.post('/test', middleware.validateToken, async (req: Request, res: Response) => {
//     setTimeout(() => {
//         res.appSuccess("S0000", Object.assign({ status: "success"},req.body));
//     }, (parseInt(req.body.delay) * 1000) || 0);
// })
// Validate user credentials using Basic-Auth and generate JWT token based on user's Consumer credentials and return token
routerAuthTest_Get.get('/', async (req, res) => {
    res.appSuccess("S0000", { status: "success", timestamp: Date.now() });
});
routerAuthCreatePassword.post('/', async (req, res) => {
    try {
        const data = await authService.createPassword(req, res);
        res.appSuccess("S1017", Object.assign({ status: "success" }, data)); // Password has been changed.
    }
    catch (e) {
        res.appFail(errorService.build(e, null)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerAuthChangePassword.post('/', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("auth", req, res, next); }, async (req, res) => {
    try {
        const data = await authService.changePassword(req, res);
        res.appSuccess("S1003", Object.assign({ status: "success" }, data)); // Password has been changed.
    }
    catch (e) {
        res.appFail(errorService.build(e, null)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerAuthRenewToken.post('/', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("auth", req, res, next); }, async (req, res) => {
    try {
        const response = await authService.renewToken(req, res); // Generate token with user info
        setTimeout(() => {
            res.appSuccess("S1004", response); // Token has been renewed.
        }, 0);
    }
    catch (e) {
        res.appFail(errorService.build(e, null)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerAuthEncode.post('/:value', async (req, res) => {
    try {
        res.appSuccess("S1005", await authService.encode(req)); //String has been encoded with base64.
    }
    catch (e) {
        res.appFail(errorService.build(e, req.params.value)); //Unknown error during string encode attempt.  Please try again.
    }
});
routerAuthDecode.post('/:value', async (req, res) => {
    try {
        res.appSuccess("S1006", await authService.decode(req)); // String has been decoded with base64.
    }
    catch (e) {
        res.appFail(errorService.build(e, req.params.value)); //Unknown error during string decode attempt.  Please try again.
    }
});
routerAuthHash.post('/:value', async (req, res) => {
    try {
        res.appSuccess("S1007", await authService.hash(req.params.value)); // Hash string with n salt rounds
    }
    catch (e) {
        res.appFail(errorService.build(e, req.params.value)); //Unknown error during string hash attempt.  Please try again.
    }
});
// Returns firebase app configuration
routerGFAC.post('/', async (req, res) => {
    try {
        res.appSuccess("S0000", authService.getFirebaseAppConfig());
    }
    catch (e) {
        res.appFail(errorService.build(e, req.params.value)); //Unknown error during string hash attempt.  Please try again.
    }
});
// This route is called when user successfully logged in using his/her phone number on the client
// The intent is to store the token ID under respective users's tokens collection for token management
routerOnSignInwithPhoneNumber.post('/', async (req, res) => {
    try {
        const data = await authService.onSignInWithPhoneNumber(req, res);
        res.appSuccess("S0000", data);
    }
    catch (e) {
        res.appFail(errorService.build(e, req.params.value)); //Unknown error during string hash attempt.  Please try again.
    }
});
routerIsPhoneNumberRegistered.get('/:phoneNumber', async (req, res) => {
    try {
        const result = await usersService.isPhoneNumberRegistered(req.params.phoneNumber);
        res.appSuccess("S0000", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerVerifyRegisteredEmail.get('/', async (req, res) => {
    try {
        const token = req.query["token"];
        const result = await authService.verifyRegisteredEmail(token);
        res.appSuccess("S1016", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Sends verification email.  This is used to send email again as user demands
routerSendEmailVerification.post('/', async (req, res) => {
    try {
        const result = await authService.sendEmailVerificationEmail(req.body.email, req.body.lang || "en-US");
        res.appSuccess("S1014", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Sends verification email.  This is used to send email again as user demands
routerSendEmailOTPLink.post('/', async (req, res) => {
    try {
        const result = await authService.sendEmailOTPLink(req.body.email);
        res.appSuccess("S1015", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Sends verification email.  This is used to send email again as user demands
routerVerifyEmailOTPLink.post('/', async (req, res) => {
    try {
        const result = await authService.verifyEmailOTPLink(req.body.email, req.body, res);
        res.appSuccess("S1000", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Sends forgot password verification email link
routerSendForgotPasswordLink.post('/', async (req, res) => {
    try {
        const result = await authService.sendForgotPasswordLink(req.body.email);
        res.appSuccess("S1018", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Verifies forgot password link
routerVerifyForgotPasswordLink.post('/', async (req, res) => {
    try {
        const result = await authService.verifyForgotPasswordLink(req.body.email, req.body, res);
        res.appSuccess("S1016", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Reset password.  Last step in forgot password process
routerResetPassword.post('/', async (req, res) => {
    try {
        const result = await authService.resetPassword(req, res);
        res.appSuccess("S1020", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
// Reset password.  Last step in forgot password process
routerFederatedLogin.post('/', async (req, res) => {
    try {
        const result = await authService.federatedLogin(req, res);
        res.appSuccess("S1021", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
exports.register = { name: 'register', router: routerRegister };
exports.registerID3 = { name: 'registerID3', router: routerRegisterID3 };
exports.verifyID3 = { name: 'verifyID3', router: routerVerifyID3 };
exports.login = { name: 'login', router: routerAuthLogin };
exports.logout = { name: 'logout', router: routerAuthLogout };
exports.createPassword = { name: "createPassword", router: routerAuthCreatePassword };
exports.changePassword = { name: "changePassword", router: routerAuthChangePassword };
exports.renewToken = { name: "renewToken", router: routerAuthRenewToken };
exports.test = { name: "test", router: routerAuthTest_Get };
exports.encode = { name: "encode", router: routerAuthEncode };
exports.decode = { name: "decode", router: routerAuthDecode };
exports.hash = { name: "hash", router: routerAuthHash };
exports.gfac = { name: "gfac", router: routerGFAC };
exports.onSignInWithPhone = { name: "onSignInWithPhone", router: routerOnSignInwithPhoneNumber };
exports.isPhoneNumberRegistered = { name: "isPhoneNumberRegistered", router: routerIsPhoneNumberRegistered };
exports.verifyRegisteredEmail = { name: "verifyRegisteredEmail", router: routerVerifyRegisteredEmail };
exports.sendEmailVerification = { name: "sendEmailVerification", router: routerSendEmailVerification };
exports.sendEmailOTPLink = { name: "sendEmailOTPLink", router: routerSendEmailOTPLink };
exports.verifyEmailOTPLink = { name: "verifyEmailOTPLink", router: routerVerifyEmailOTPLink };
exports.sendForgotPasswordLink = { name: "sendForgotPasswordLink", router: routerSendForgotPasswordLink };
exports.verifyForgotPasswordLink = { name: "verifyForgotPasswordLink", router: routerVerifyForgotPasswordLink };
exports.resetPassword = { name: "resetPassword", router: routerResetPassword };
exports.federatedLogin = { name: "federatedLogin", router: routerFederatedLogin };
//# sourceMappingURL=authRoutes.js.map