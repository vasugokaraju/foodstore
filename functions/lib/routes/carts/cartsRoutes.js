"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cartsUpload = exports.cartsDownload = exports.cartsDelete = exports.cartsUpdate = exports.cartsCreate = exports.cartsReadMany = exports.cartsReadOne = exports.cartsPreload = void 0;
const typedi_1 = require("typedi");
const express_1 = require("express");
const cartsService_1 = __importDefault(require("../../services/cartsService"));
const errorService_1 = __importDefault(require("../../services/errorService"));
const middleware_1 = __importDefault(require("../../middleware"));
const routerCartsPreload = express_1.Router();
const routerCartsReadOne = express_1.Router();
const routerCartsReadMany = express_1.Router();
const routerCartsCreate = express_1.Router();
const routerCartsUpdate = express_1.Router();
const routerCartsDelete = express_1.Router();
const routerCartsDownload = express_1.Router();
const routerCartsUpload = express_1.Router();
const cartsService = typedi_1.Container.get(cartsService_1.default);
let errorService = typedi_1.Container.get(errorService_1.default);
/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerCartsPreload.post('/:id?', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("carts", req, res, next); }, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await cartsService.preload(req.userProfile, req.body.whereQuery);
        if (result) {
            res.appSuccess("S0000", result);
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerCartsReadOne.get('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("carts", req, res, next); }, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        // const result = await cartsService.read(req.params.id? req.params : req.query);  // id || query parameters
        const result = await cartsService.readOne(req.params.id);
        if (result.length != 0) {
            let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
            res.appSuccess("S1400", result, customMessage); // Record(s) found.
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
/**
 * Finds records using where clause
 */
routerCartsReadMany.post('/', middleware_1.default.validateToken, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await cartsService.readMany(req.body.whereQuery);
        let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
        res.appSuccess("S1400", result, customMessage); // Record(s) found.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerCartsCreate.post('/', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("carts", req, res, next); }, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const data = req.body;
        const result = await cartsService.create(data);
        res.appSuccess("S1401", result, "1 record has been created."); //Record has been created.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerCartsUpdate.put('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("carts", req, res, next); }, async (req, res) => {
    try {
        const id = req.params.id;
        const data = req.body;
        const result = await cartsService.update(id, data);
        res.appSuccess("S1402", result, "1 record has been updated.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerCartsDelete.delete('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("carts", req, res, next); }, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const id = req.params.id;
        const result = await cartsService.delete(id);
        res.appSuccess("S1403", result.value, "1 record has been deleted.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Downloads module data
*/
routerCartsDownload.post('/', middleware_1.default.validateToken, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await cartsService.download(req.body);
        res.appSuccess("S1403", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Uploads module data
*/
routerCartsUpload.post('/', middleware_1.default.validateToken, middleware_1.default.cartsEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await cartsService.upload(req);
        res.appSuccess("S1404", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
exports.cartsPreload = { name: 'cartsPreload', router: routerCartsPreload };
exports.cartsReadOne = { name: 'cartsReadOne', router: routerCartsReadOne };
exports.cartsReadMany = { name: "cartsReadMany", router: routerCartsReadMany };
exports.cartsCreate = { name: "cartsCreate", router: routerCartsCreate };
exports.cartsUpdate = { name: "cartsUpdate", router: routerCartsUpdate };
exports.cartsDelete = { name: "cartsDelete", router: routerCartsDelete };
exports.cartsDownload = { name: "cartsDownload", router: routerCartsDownload };
exports.cartsUpload = { name: "cartsUpload", router: routerCartsUpload };
//# sourceMappingURL=cartsRoutes.js.map