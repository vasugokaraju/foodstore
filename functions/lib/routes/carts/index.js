"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.carts = void 0;
const cartsRoutes_1 = require("./cartsRoutes");
exports.carts = [
    cartsRoutes_1.cartsPreload,
    cartsRoutes_1.cartsCreate,
    cartsRoutes_1.cartsReadOne,
    cartsRoutes_1.cartsReadMany,
    cartsRoutes_1.cartsUpdate,
    cartsRoutes_1.cartsDelete,
    cartsRoutes_1.cartsDownload,
    cartsRoutes_1.cartsUpload,
];
//# sourceMappingURL=index.js.map