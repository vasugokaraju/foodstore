"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.usersIdDisplayNameReadMany = exports.usersUpload = exports.usersDownload = exports.usersDelete = exports.usersUpdate = exports.usersCreate = exports.usersReadMany = exports.usersReadOne = exports.usersPreload = void 0;
const typedi_1 = require("typedi");
const express_1 = require("express");
const usersService_1 = __importDefault(require("../../services/usersService"));
const errorService_1 = __importDefault(require("../../services/errorService"));
const middleware_1 = __importDefault(require("../../middleware"));
const routerUsersPreload = express_1.Router();
const routerUsersReadOne = express_1.Router();
const routerUsersReadMany = express_1.Router();
const routerUsersCreate = express_1.Router();
const routerUsersUpdate = express_1.Router();
const routerUsersDelete = express_1.Router();
const routerUsersDownload = express_1.Router();
const routerUsersUpload = express_1.Router();
const routerGetUsersIdDisplayNameReadMany = express_1.Router();
const usersService = typedi_1.Container.get(usersService_1.default);
let errorService = typedi_1.Container.get(errorService_1.default);
/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerUsersPreload.post('/:id?', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.preload(req.userProfile, req.body.whereQuery);
        if (result) {
            res.appSuccess("S0000", result);
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
routerUsersReadOne.get('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        // const result = await usersService.read(req.params.id? req.params : req.query);  // id || query parameters
        const result = await usersService.readOne(req.params.id);
        if (result.length != 0) {
            let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
            res.appSuccess("S1400", result, customMessage); // Record(s) found.
        }
        else {
            res.appFail(errorService.build("E1405", req)); // No matching record found in database.
        }
    }
    catch (e) {
        res.appFail(errorService.build(e, req)); // Unknown error during string encode attempt.  Please try again.
    }
});
/**
 * Finds records using where clause
 */
routerUsersReadMany.post('/', middleware_1.default.validateToken, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.readMany(req.body.whereQuery);
        let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
        res.appSuccess("S1400", result, customMessage); // Record(s) found.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerUsersCreate.post('/', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const data = req.body;
        const result = await usersService.create(data);
        res.appSuccess("S1401", result, "1 record has been created."); //Record has been created.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerUsersUpdate.put('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        // the validateToken middleware verifies the token and stores current user profile in req.userProfile
        // the second parameter contains new profile (full or partial)
        const result = await usersService.update(req.userProfile, req.body);
        res.appSuccess("S1402", result, "1 record has been updated.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
routerUsersDelete.delete('/:id', middleware_1.default.validateToken, (req, res, next) => { middleware_1.default.validateRequest("users", req, res, next); }, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.delete(req.params.id);
        res.appSuccess("S1403", result.value, "1 record has been deleted.");
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Downloads module data
*/
routerUsersDownload.post('/', middleware_1.default.validateToken, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.download(req.body);
        res.appSuccess("S1403", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
  * Uploads module data
*/
routerUsersUpload.post('/', middleware_1.default.validateToken, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.upload(req);
        res.appSuccess("S1404", result);
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
/**
* Returns Users data in the form of code-name for the purpose of Autocomplete controls on UI.
*/
routerGetUsersIdDisplayNameReadMany.post('/', middleware_1.default.validateToken, middleware_1.default.usersEntitlements, middleware_1.default.accessControlCheck, async (req, res) => {
    try {
        const result = await usersService.getUsersIdDisplayNameReadMany(req);
        let customMessage = (result.length == 1) ? "1 record found." : result.length + " records found.";
        res.appSuccess("S0000", result, customMessage); // Record(s) found.
    }
    catch (e) {
        res.appFail(errorService.build(e, req));
    }
});
exports.usersPreload = { name: 'usersPreload', router: routerUsersPreload };
exports.usersReadOne = { name: 'usersReadOne', router: routerUsersReadOne };
exports.usersReadMany = { name: "usersReadMany", router: routerUsersReadMany };
exports.usersCreate = { name: "usersCreate", router: routerUsersCreate };
exports.usersUpdate = { name: "usersUpdate", router: routerUsersUpdate };
exports.usersDelete = { name: "usersDelete", router: routerUsersDelete };
exports.usersDownload = { name: "usersDownload", router: routerUsersDownload };
exports.usersUpload = { name: "usersUpload", router: routerUsersUpload };
exports.usersIdDisplayNameReadMany = { name: "usersIdDisplayNameReadMany", router: routerGetUsersIdDisplayNameReadMany };
//# sourceMappingURL=usersRoutes.js.map