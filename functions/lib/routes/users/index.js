"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.users = void 0;
const usersRoutes_1 = require("./usersRoutes");
exports.users = [
    usersRoutes_1.usersPreload,
    usersRoutes_1.usersCreate,
    usersRoutes_1.usersReadOne,
    usersRoutes_1.usersReadMany,
    usersRoutes_1.usersUpdate,
    usersRoutes_1.usersDelete,
    usersRoutes_1.usersDownload,
    usersRoutes_1.usersUpload,
    usersRoutes_1.usersIdDisplayNameReadMany,
];
//# sourceMappingURL=index.js.map