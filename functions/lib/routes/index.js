"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.routes = void 0;
const auth_1 = require("./auth");
const users_1 = require("./users");
const products_1 = require("./products");
const carts_1 = require("./carts");
const meeting_1 = require("./meeting");
const appCods_1 = require("./appCods");
const appConf_1 = require("./appConf");
const reports_1 = require("./reports");
const uploadFiles_1 = require("./uploadFiles");
const baseArray = [];
exports.routes = baseArray.concat(users_1.users, products_1.products, carts_1.carts, meeting_1.meeting, appCods_1.appCods, appConf_1.appConf, auth_1.auth, reports_1.reports, uploadFiles_1.upload);
/*
export default () => {
    const app = Router();

    usersRoute(app);         // Load users route
    productsRoute(app);         // Load products route
    cartsRoute(app);         // Load carts route
    meetingRoute(app);         // Load meeting route
    appCodsRoute(app);         // Load appCods route
    appConfRoute(app);         // Load appConf route
    authRoute(app);       // Load authentication route

    return app;
}
*/ 
//# sourceMappingURL=index.js.map