"use strict";
/**
 * Attach custom responses to req
 * @param {*} req Express req Object
 * @param {*} res  Express res Object
 * @param {*} next  Express next Function
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
// import {HTTP_STATUS,DATATYPES} from "../loaders/enums";
// import Helper from "../helpers/helper";
const logger_1 = __importDefault(require("../loaders/logger"));
const errorService_1 = __importDefault(require("../services/errorService"));
const cacheService_1 = __importDefault(require("../services/cacheService"));
// import AuditService from "../services/auditService";
const appEssentials = async (req, res, next) => {
    // let helper: Helper = Container.get(Helper);
    let errorService = typedi_1.Container.get(errorService_1.default);
    let cacheService = typedi_1.Container.get(cacheService_1.default);
    // let auditService : AuditService = Container.get(AuditService);
    try {
        // appSuccess signature is added to Response interface in @types/express/index.d.ts
        res.appSuccess = async function (successCode, data, customMessage) {
            try {
                // let successCodes:any = Container.get("SuccessCodes");
                // let successInfo:IAppSuccessCode = <IAppSuccessCode>successCodes[successCode];
                let successInfo = await cacheService.getAppCode(successCode, "en-US"); // Provides app code details for the given code.
                let msg = customMessage ? customMessage : successInfo.message;
                res.status(successInfo.httpCode).json({ "status": 'success', title: successInfo.title, "message": msg, "data": data });
            }
            catch (error) {
                // Requested success code is not available
                res.appFail(errorService.build(error, req));
            }
            // Make an attempt to add audit log. Do not wait for response.
            try {
                if (req.method.toLowerCase() != "get") {
                    // let logRec = helper.getAuditLogRecord(req, data);
                    // auditService.log(logRec);
                }
            }
            catch (e) { }
        };
        // appFail signature is added to Response interface in @types/express/index.d.ts
        res.appFail = async function (error) {
            let errorResolve = await errorService.resolve(error);
            res.status(errorResolve.statusCode).json(errorResolve.response);
            if (errorResolve.logAction > 0)
                logger_1.default.error(JSON.stringify(errorResolve.log, null, " ")); // Dump all the info about the error to the log
        };
        return next();
    }
    catch (e) {
        return next(errorService.build(e, req));
    }
};
exports.default = appEssentials;
//# sourceMappingURL=appEssentials.js.map