"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const errorService_1 = __importDefault(require("../services/errorService"));
const helper_1 = __importDefault(require("../helpers/helper"));
const firebaseError_1 = __importDefault(require("../classes/firebaseError"));
/**
 * Firebase idTokens are time bound and they must be verified
 */
const validateToken = async (req, res, next) => {
    let errorService = typedi_1.Container.get(errorService_1.default);
    // let usersService: UsersService = Container.get(UsersService);
    const admin = typedi_1.Container.get('admin');
    const helper = typedi_1.Container.get(helper_1.default);
    try {
        // Check the loggedin flag
        // if (req.headers.loggedin !== "true") return next("E1000");  // Please login
        if (!req.headers.authorization || req.headers.authorization.split(" ").length != 2)
            return next("E1002");
        const token = await helper.getToken(req);
        try {
            // Verify whether the token is expired.
            req.userProfile = await admin.auth().verifyIdToken(token); // Make current user profile available to entire app
        }
        catch (error) {
            return next(errorService.build("E1040", error)); // Invalid Token. Usuall happens when token is generated by firebase and validated by local emulator during federated login testing
        }
        // Token gives uid or user_id but we want it as "id" as the rest of the code looks for it.
        req.userProfile["id"] = req.userProfile.uid;
        // Get the token uid from cookie to pull the record from tokens subcollection.
        const token_uid = helper.getCookie('tknuid', req) || req.get('tknuid'); // Chrome and cross domain cookies are making it difficult, thus headers
        // Get the providerId from cookie
        const providerId = helper.getCookie('providerId', req) || req.get('providerId');
        // If token uid is not an invalid value
        if (!!token_uid) {
            // Verify whether the token is rovoked.  Token shouild be revoked when the user signs out to prevent unauthorized usage.
            const tokenRec = await admin.firestore().collection('users').doc(req.userProfile.id).collection('tokens').doc(token_uid).get();
            req.userProfile.tokenUID = token_uid;
            if (!tokenRec.exists)
                return next(errorService.build("E1105", null));
        }
        else {
            return next(errorService.build("E1105", null));
        }
        // Check providerId exists
        if (!!providerId) {
            req.userProfile.providerId = providerId;
        }
        else {
            return next(errorService.build("E1104", null));
        }
        next();
    }
    catch (e) {
        const firebaseError = helper.getFirebaseError(e);
        if (firebaseError instanceof firebaseError_1.default) {
            return next(errorService.build(firebaseError, null));
        }
        else if (e.errorCode) {
            return next(errorService.build(e, null));
        }
        else {
            return next(errorService.build("E1001", e)); // Token expired. Please login to renew.
        }
    }
};
exports.default = validateToken;
//# sourceMappingURL=validateToken.js.map