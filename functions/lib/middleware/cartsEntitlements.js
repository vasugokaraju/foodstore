"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const enums_1 = require("../loaders/enums");
const errorService_1 = __importDefault(require("../services/errorService"));
const entitlementService_1 = __importDefault(require("../services/entitlementService"));
// This file plays major role in limiting access to data.
// The incoming query is manipulated here based on many factors including user role, role hierarchy
const cartsEntitlement = async (req, res, next) => {
    let errorService = typedi_1.Container.get(errorService_1.default);
    let entitlementService = typedi_1.Container.get(entitlementService_1.default);
    try {
        // ----------------------- COMMON ENTITLEMENT VALIDATIONS -----------------------
        // Collect User provided query/params parameters
        // req.query.findQuery = await entitlementService.buildQueryParams(req);   // Returns req.query.findQuery[]
        // SUPERADMIN role should be exempted from the following validations.
        if (req.userProfile.role.code === enums_1.ROLES.SUPERADMIN) {
            return next();
        }
        // ----------------------- ROLESPECIFIC ENTITLEMENT VALIDATIONS -----------------------
        // Perform other entitlement checks to deny access to data that the user is not entitled to.
        // These methods are expected to reject the request with proper error code
        // await entitlementService.someEntitlementCheck(req);
        // Influence query to filter out data that the user is not entitled to.
        await entitlementService.enforceCartsEntitlements(req);
        return next();
    }
    catch (e) {
        if (e.stack && e.stack.indexOf("api-query-params") != -1) {
            return next(errorService.build("E1200", e)); // Invalid Payload
        }
        else {
            return next(errorService.build(e, null));
        }
    }
};
exports.default = cartsEntitlement;
//# sourceMappingURL=cartsEntitlements.js.map