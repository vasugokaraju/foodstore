"use strict";
/**
 * Attach custom responses to req
 * @param {*} req Express req Object
 * @param {*} res  Express res Object
 * @param {*} next  Express next Function
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const helper_1 = __importDefault(require("../helpers/helper"));
const errorService_1 = __importDefault(require("../services/errorService"));
const validationService_1 = __importDefault(require("../services/validationService"));
const validateRequest = async (routeName, req, res, next) => {
    let helper = typedi_1.Container.get(helper_1.default);
    let errorService = typedi_1.Container.get(errorService_1.default);
    let validationService = typedi_1.Container.get(validationService_1.default);
    try {
        // Process and consolidate the req.query and req.params fields to a manageable object.
        req.findQuery = await helper.buildQueryParams(req);
        // let route = helper.getRouteName(req);
        await validationService.validate(routeName + "Schema", req);
        return next();
    }
    catch (e) {
        return next(errorService.build(e, null));
    }
};
exports.default = validateRequest;
//# sourceMappingURL=validateRequest.js.map