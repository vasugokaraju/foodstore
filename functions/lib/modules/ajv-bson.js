"use strict";
/**
 * INSPIRED BY https://www.npmjs.com/package/ajv-bsontype
 */
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
// Adds new keyword 'bsontype' to ajv schema validator to validate bsontypes
module.exports = function (ajv) {
    // Extracts the name of the datatype by hacking Object.prototype
    const type = function (v) {
        if (v === null) {
            return 'null';
        }
        if (v === undefined) {
            return 'undefined';
        }
        const s = Object.prototype.toString.call(v);
        const t = s.match(/\[object (.*?)\]/)[1].toLowerCase();
        if (t === 'number') {
            if (isNaN(v)) {
                return 'nan';
            }
            if (!isFinite(v)) {
                return 'infinity';
            }
        }
        return t;
    };
    // Checks the datatype of the value provided by user
    const $type = function (a) {
        const t = type(a);
        return function (b) {
            switch (b) {
                case 1:
                case 'double':
                    return (t === 'number') && ((a + '').indexOf('.') !== -1);
                case 2:
                case 'string':
                    return t === 'string';
                case 3:
                case 'object':
                    return t === 'object';
                case 4:
                case 'array':
                    return t === 'array';
                case 6:
                case 'undefined':
                    return ['null', 'undefined'].includes(t);
                case 7:
                case 'objectId':
                    return (t === 'object') && (a._bsontype === 'ObjectID');
                case 8:
                case 'bool':
                case 'boolean':
                    return t === 'boolean';
                case 9:
                case 'date':
                    return t === 'date';
                case 10:
                case 'null':
                    return t === 'null';
                case 11:
                case 'regex':
                    return ['regex', 'regexp'].includes(t);
                case 16:
                case 'int':
                    return (t === 'number') && (a <= 2147483647) && ((a + '').indexOf('.') === -1);
                case 18:
                case 'long':
                    return (t === 'number') && (a > 2147483647) && (a <= 9223372036854775807) && ((a + '').indexOf('.') === -1);
                case 19:
                case 'decimal':
                    return (t === 'object') && (a._bsontype === 'Decimal128');
                case 20:
                case 'number':
                    return t === 'number';
                default: return false;
            }
        };
    };
    // Main function that is used as addKeyword function
    let validate = (schema, data) => {
        if (validate.errors === null)
            validate.errors = [];
        // User may provide data in the form of string for fields of type objectId, Decimal and Date
        // For such scenarios, convert the value from string to respective data type to make the remaining logic work.
        switch (schema.toLowerCase()) {
            case "date":
                data = (typeof data === "string") ? new Date(data) : data; // If the provided date is string, convert it to object.
                break;
            case "objectid":
                try {
                    data = new bson_1.ObjectID(data); // If the provided objectId is string, convert it to object. If bad data, don't raise any exception.  It will be handled further
                }
                catch (_a) { }
                break;
            case "decimal":
                data = (typeof data === "number") ? bson_1.Decimal128.fromString(data.toString()) : data; // If the provided decimal is number, convert it to decimal object.
                break;
        }
        const v = $type(data);
        let msg, passed;
        if (Array.isArray(schema)) {
            msg = schema.join(', ');
            passed = schema.some(v);
        }
        else {
            msg = schema;
            passed = v(schema);
        }
        if (!passed) {
            validate.errors.push({
                keyword: 'bsonType',
                params: {
                    bsonType: schema
                },
                message: `should be ${msg} but received "${data}"`
            });
        }
        return passed;
    };
    ajv.addKeyword('bsonType', {
        errors: true,
        validate
    });
};
//# sourceMappingURL=ajv-bson.js.map