"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errorService_1 = __importDefault(require("../services/errorService"));
const typedi_1 = require("typedi");
const enums_1 = require("../loaders/enums");
const config_1 = __importDefault(require("../config"));
const api_query_params_1 = __importDefault(require("api-query-params"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const firebaseError_1 = __importDefault(require("../classes/firebaseError"));
const crypto_1 = __importDefault(require("crypto"));
const firebase_admin_1 = require("firebase-admin");
const jp = require('jsonpath');
class Helper {
    constructor(roleHierarchy = enums_1.ROLES) {
        this.roleHierarchy = roleHierarchy;
        this.monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        this.algorithm = 'aes-256-ctr';
        this.iv = crypto_1.default.randomBytes(16);
        this.replacerFunc = () => {
            const visited = new WeakSet();
            return (key, value) => {
                if (typeof value === "object" && value !== null) {
                    if (visited.has(value)) {
                        return;
                    }
                    visited.add(value);
                }
                return value;
            };
        };
    }
    // Returns full path of given request
    fullPath(req) {
        return req.protocol + '://' + req.get('host') + req.originalUrl;
    }
    // Encode a string using Base64 encoding
    encodeBase64(str) {
        return Buffer.from(str).toString('base64');
    }
    // Decode a string that was encrypted using Base64 encoding
    decodeBase64(base64String) {
        return Buffer.from(base64String, 'base64').toString('ascii');
    }
    encrypt(text) {
        let iv = crypto_1.default.randomBytes(16);
        let cipher = crypto_1.default.createCipheriv('aes-256-cbc', Buffer.from(config_1.default.cryptoSecret), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        const urlSafe = (iv.toString('hex') + ':' + encrypted.toString('hex')).replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/g, '');
        return urlSafe;
    }
    ;
    decrypt(hash) {
        // Undo url safe to get actual encrypted string
        hash = hash.replace(/\-/g, '+').replace(/\_/g, '/');
        while (hash.length % 4)
            hash += '=';
        let textParts = hash.split(':');
        let iv = Buffer.from(textParts.shift(), 'hex');
        let encryptedText = Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto_1.default.createDecipheriv('aes-256-cbc', Buffer.from(config_1.default.cryptoSecret), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }
    ;
    // Returns User information (iss) from JWT token present in req object
    getUserInfoFromToken(req) {
        let token = req.headers.authorization;
        let userInfo = JSON.parse(this.decodeBase64(token.split(".")[1]));
        return userInfo;
    }
    // Decode a string that was encrypted using Base64 encoding
    createPassword(length) {
        var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        return retVal;
    }
    // Returns data type of given variable
    getDataType(myVar) {
        // let varType;
        if (typeof myVar === enums_1.DATATYPES.string || myVar instanceof String) {
            return enums_1.DATATYPES.string;
        }
        else if (Array.isArray(myVar)) {
            return enums_1.DATATYPES.array;
        }
        else if (typeof myVar === enums_1.DATATYPES.object) {
            return enums_1.DATATYPES.object;
        }
        else if (typeof myVar === enums_1.DATATYPES.number) {
            return enums_1.DATATYPES.number;
        }
        else if (typeof myVar === enums_1.DATATYPES.bigint) {
            return enums_1.DATATYPES.bigint;
        }
        else if (typeof myVar === enums_1.DATATYPES.symbol) {
            return enums_1.DATATYPES.symbol;
        }
        else if (typeof myVar === enums_1.DATATYPES.function) {
            return enums_1.DATATYPES.function;
        }
        else if (typeof myVar === enums_1.DATATYPES.boolean) {
            return enums_1.DATATYPES.boolean;
        }
        else if (typeof myVar === enums_1.DATATYPES.undefined) {
            return enums_1.DATATYPES.undefined;
        }
        return enums_1.DATATYPES.undefined;
    }
    // Returns Mongo objectid
    getObjectId(id) {
        try {
            // let _id = new ObjectID(id);
            let _id = {};
            return _id;
        }
        catch (e) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            throw errorService.build("E1206", [e.message, id]);
        }
    }
    // Turns the first letter to uppercase
    initCaps(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
    // Builds audit log record structure
    // action:AUDIT_LOG_ACTION, comments:string, sourceId:string, sourceName:string, record:any, modifiedBy:string
    getAuditLogRecord(req, rec) {
        let comments = "";
        let moduleName = req.originalUrl.split(/[/?&]+/)[2];
        if (req.method.toLowerCase() == "post") {
            comments = moduleName + " is created.";
        }
        else if (req.method.toLowerCase() == "put") {
            comments = moduleName + " is updated.";
        }
        else if (req.method.toLowerCase() == "delete") {
            comments = moduleName + " is deleted.";
        }
        else {
            comments = "Unknown action.";
        }
        return {
            "Action": req.method,
            "ModifiedTime": new Date(),
            "Comments": comments,
            "SourceId": rec._id.toString(),
            "SourceName": moduleName,
            "SerializedEntity": JSON.stringify(rec),
            "ModifiedBy": req.userProfile ? req.userProfile.consumerUsername : 'User info not available'
        };
    }
    // Takes an array of objectid strings and convert them to ObjectId objects.
    getArrayOfObjectIds(arrayOfStrings) {
        let objectIdStrings = arrayOfStrings;
        const objectIdObjects = objectIdStrings.map((id) => {
            try {
                return this.getObjectId(id);
            }
            catch (e) {
                let errorService = typedi_1.Container.get(errorService_1.default);
                throw errorService.build(e, null);
            }
        });
        return objectIdObjects;
    }
    // Takes an object and list of field names and convert objectid string to ObjectId object.
    stringToObjectId(params, objectIdFieldsList) {
        try {
            objectIdFieldsList.split(",").forEach((field) => {
                if (!params[field])
                    return;
                let val = params[field];
                if (this.getDataType(val) == enums_1.DATATYPES.string) {
                    params[field] = this.getObjectId(val);
                }
                else if (this.getDataType(val) == enums_1.DATATYPES.array) {
                    params[field] = this.getArrayOfObjectIds(val);
                }
                else if (this.getDataType(val) == enums_1.DATATYPES.object) { // when operators like $in,$nin etc are present in query
                    if (val.$in)
                        val.$in = this.getArrayOfObjectIds(val.$in);
                    else if (val.$nin)
                        val.$nin = this.getArrayOfObjectIds(val.$nin);
                    else if (val.$all)
                        val.$all = this.getArrayOfObjectIds(val.$all);
                }
            });
            return params;
        }
        catch (e) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            throw errorService.build(e, arguments);
        }
    }
    // Translates query string to IFindQuery type
    toFindQuery(params) {
        // Build find query object
        let findQuery = {
            project: (params.projection || {}),
            sort: (params.sort || {}),
            skip: params.hasOwnProperty("skip") ? parseInt(params.skip) : 0,
            limit: params.hasOwnProperty("limit") ? parseInt(params.limit) : config_1.default.pageLimit,
            id: (params.id || ""),
            query: params.filter
        };
        return findQuery;
    }
    // Translates query string to Update/Delete find query
    toFindQuery2(params, objectIdFields) {
        // Convert objectid strings to ObjectId objects.
        if (params.hasOwnProperty('filter')) {
            params.filter = this.stringToObjectId(params.filter, objectIdFields);
        }
        else {
            params = this.stringToObjectId(params, objectIdFields);
        }
        // Build find query object
        let findQuery = {
            find: params.filter // params.filter will be undefined in some cases. when that is true, this key will be removed from json
        };
        return findQuery;
    }
    // Converts comma separated query values to arrays
    comma2Array(query) {
        Object.keys(query).map((field) => {
            try {
                if (field != "filter" && field != "fields" && query[field].indexOf(",") != -1) { // If the value contains comma, split it as array
                    query[field] = query[field].split(",");
                }
            }
            catch (e) { }
        });
        return query;
    }
    // Takes two arrays and returns the items that do not exist in lookIn array.
    getNotExisting(lookIn, lookFor) {
        let notExist = [];
        lookFor.forEach((item) => {
            if (!lookIn.includes(item))
                notExist.push(item);
        });
        return notExist;
    }
    isNumeric(value) {
        let regExp = /^[0-9]+$/;
        let result = value.toString().match(regExp);
        return result != null ? true : false;
    }
    // Convert user provided query and params to an object
    async buildQueryParams(req) {
        let findQuery;
        try {
            let params = req.params;
            // When user does not pass any optional route parameter value, for example route.customer('/:_id?') 
            // Express creates the variable _id with undefined as value in req.params object.
            // Remove such variable
            Object.keys(params).forEach(key => params[key] === undefined && delete params[key]);
            params = Object.assign({}, params, req.query); // Merge both req.params and req.query
            // Construct the find query
            // The castParams settings make sure the data type of value remain as is.
            // Ex, phone number is of type string.  But aqp converts it as number if phone number is passed as number unless it is specified in castParams.
            // At this step, the findQuery variable gets new field calld "filter" which contains the find criteria.
            findQuery = api_query_params_1.default(params, { castParams: { skip: "int", limit: "int" } });
            return await Promise.resolve(findQuery);
        }
        catch (e) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            await Promise.reject(errorService.build(e, req));
        }
    }
    // Returns the method name from route uri
    getRouteName(req) {
        let routeSegments = req.baseUrl.split("/");
        return routeSegments[routeSegments.length - 1];
    }
    // Verify password
    async verifyPassword(storedPassword, givenPassword) {
        return await bcrypt_1.default.compare(givenPassword, storedPassword);
    }
    async hash(str) {
        // return await bcrypt.hash(str, saltRounds);
        return crypto_1.default.createHash('sha256').update(str).digest('hex');
    }
    async sha256(str, salt) {
        return crypto_1.default.createHash('sha256').update(str).digest('hex');
        // return crypto.createHmac('sha512', salt).update(str).digest('hex');
    }
    // Returns hierarchy number for given role
    getRoleHierarchy(role) {
        return this.roleHierarchy[role.code];
    }
    /**
     * The firebase functions:config:set command does not allow capital letters as part of namespace
     * But the firebase config field names have uppercase letters and are case sensitive.
     * Rebuild the firebaseConfig with proper field names
     */
    formatFirebaseConfig(conf) {
        let firebaseConfig = {};
        firebaseConfig.apiKey = conf.apikey || conf.apiKey;
        firebaseConfig.authDomain = conf.authdomain || conf.authDomain;
        firebaseConfig.projectId = conf.projectid || conf.projectId;
        firebaseConfig.storageBucket = conf.storagebucket || conf.storageBucket;
        firebaseConfig.messagingSenderId = conf.messagingsenderid || conf.messagingSenderId;
        firebaseConfig.measurementId = conf.measurementid || conf.measurementId || "";
        return firebaseConfig;
    }
    /**
     * In the absence of official FirebaseError class.
     * Returns custom FirebaseError object if the provided error is caused by Firebase
     * https://github.com/firebase/firebase-admin-node/issues/403
     */
    getFirebaseError(e) {
        console.log('getFirebaseError():', e);
        if (e.code && e.code.startsWith('auth/')) {
            return new firebaseError_1.default(e.code, e.message);
        }
        else {
            return e;
        }
    }
    getCookie(cookieName, req) {
        const { headers: { cookie } } = req;
        if (cookie === undefined)
            return '';
        const cookies = cookie.split(/[\s,;]+/);
        let val = undefined;
        for (let index = 0; index < cookies.length; index++) {
            if (cookies[index].startsWith(cookieName)) {
                val = cookies[index].split("=")[1];
                break;
            }
        }
        return val;
    }
    // Extract token from authentication header
    getToken(req) {
        let errorService = typedi_1.Container.get(errorService_1.default);
        if (!req.headers.authorization || req.headers.authorization.split(" ").length != 2)
            return Promise.reject(errorService.build("E1002", null)); // Token is not provided
        return req.headers.authorization.split(" ")[1];
    }
    // Collection useragent properties to identify the device type
    getUserAgent(req) {
        let userAgentInfo = {};
        Object.keys(req.useragent).forEach(item => {
            if (req.useragent[item] === true)
                userAgentInfo[item] = true;
        });
        return userAgentInfo;
    }
    // Returns UTC time
    getUTC() {
        let now = new Date;
        return Date.UTC(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds());
    }
    // Convert Object to Map
    object2JSON(obj) {
        let json = {};
        Object.keys(obj).forEach(item => {
            json[item] = obj[item];
        });
        json = JSON.parse(JSON.stringify(json, this.replacerFunc()));
        return json;
    }
    // Delete firestore document
    // https://stackoverflow.com/questions/49286764/delete-a-document-with-all-subcollections-and-nested-subcollections-in-firestore
    async deleteDocument(doc) {
        const collections = await doc.listCollections();
        await Promise.all(collections.map(collection => this.deleteCollection(collection)));
        await doc.delete();
    }
    // Delete firestore (sub)collection
    // https://stackoverflow.com/questions/49286764/delete-a-document-with-all-subcollections-and-nested-subcollections-in-firestore
    async deleteCollection(collection) {
        const query = collection.limit(100);
        while (true) {
            const snap = await query.get();
            if (snap.empty) {
                return;
            }
            await Promise.all(snap.docs.map(doc => this.deleteDocument(doc.ref)));
        }
    }
    userProfileSegments(profileData) {
        const config = typedi_1.Container.get('config');
        let authFields = {};
        let customClaimsFileds = {};
        // ************ COLLECT AUTHENTICATION RELATED FIELDS
        config.authenticationFields.forEach((authField) => {
            // Collect the authentication fileds that are to be stored in firebase project
            if (profileData.hasOwnProperty(authField))
                authFields[authField] = profileData[authField];
        });
        // ************ COLLECT CUSTOM CLAIMS RELATED FIELDS
        config.customClaimsFields.forEach((claimField) => {
            // Collect the custom user claims fileds that are to be stored in customClaims file that is part of authentication
            if (profileData.hasOwnProperty(claimField))
                customClaimsFileds[claimField] = profileData[claimField];
        });
        // ************ COLLECT REMAINING FIELDS AS EXTENDED PROFILE FIELDS
        [...config.authenticationFields, ...config.customClaimsFields].forEach((auth_claim_field) => {
            // Delete all the reserved fields to store the remaining as extended user profile data
            delete profileData[auth_claim_field];
        });
        return { authFields: authFields, customClaimsFileds: customClaimsFileds, extendedProfileFields: profileData };
    }
    // Sets cookie to response object. Helps to manage options better.
    setCookie(res, name, value, options = { maxAge: 3600 * 24 }) {
        const opts = Object.assign({ maxAge: 3600 * 24, samesite: "None", secure: true }, options);
        return res.cookie(name, value, opts);
    }
    // Sets header to response object
    setHeader(res, header, value) {
        return res.header(header, value);
    }
    // Merges two objects based on a key and returns the records that match the obj1
    mergeByKey(obj1, key1, obj2, key2) {
        return obj1.map((item1) => (Object.assign(Object.assign({}, obj2.find((item2) => (item2[key2] === item1[key1]) && item2)), item1)));
    }
    removeDuplicateWhereCriterias(where) {
        let holder = {};
        return where.reduce((accumulator, currentValue) => {
            const key = currentValue.join("");
            // If key already exists, it indicates that this is duplicate criteria
            if (!holder.hasOwnProperty(key))
                accumulator.push(currentValue);
            holder[key] = true;
            return accumulator;
        }, []);
    }
    removeDuplicateOrderByCriterias(where) {
        let holder = {};
        return where.reduce((accumulator, currentValue) => {
            const key = currentValue[0];
            // If key already exists, it indicates that this is duplicate criteria
            if (!holder.hasOwnProperty(key))
                accumulator.push(currentValue);
            holder[key] = true;
            return accumulator;
        }, []);
    }
    /**
     * Makes where queries on given collections
     * NOTE:  FIREBASE RAISES EXCEPTION IF THERE IS NO INDEX ON THE FILEDS USED IN WHERE CONDITION.
     * FIREBASE/FIRESTORE AUTOMATICALLY CREATES INDEX ON EACH SINGLE FIELD
     * CUSTOM INDEX NEEDS MINIMUM TWO FIELDS
     */
    async where(collection, query) {
        // Get query reference to collection
        let whereQuery = collection;
        try {
            let { where, orderBy, limit, paginationRec, direction } = query; // Extract query parts
            // **************************************** WHERE
            if (where[0] instanceof Array)
                where = this.removeDuplicateWhereCriterias(where);
            if (orderBy && orderBy[0] instanceof Array)
                orderBy = this.removeDuplicateOrderByCriterias(orderBy);
            if (where && where.length > 0) {
                if (where[0] instanceof Array) { // If there are multiple where conditions (array or arrays)
                    for (let w of where) { // add each where condition to query
                        // https://stackoverflow.com/questions/48036975/firestore-multiple-conditional-where-clauses
                        whereQuery = whereQuery.where(...w);
                    }
                }
                else {
                    whereQuery = whereQuery.where(...where);
                }
            }
            // **************************************** ORDER BY
            if (orderBy && orderBy[0] instanceof Array) {
                // It's an array of array
                for (let o of orderBy) {
                    // https://stackoverflow.com/questions/48036975/firestore-multiple-conditional-where-clauses
                    whereQuery = whereQuery.orderBy(...o);
                }
            }
            else if (orderBy) {
                whereQuery = whereQuery.orderBy(...orderBy);
            }
            // **************************************** LIMIT
            whereQuery = whereQuery.limit(limit ? limit : parseInt(config_1.default.pageLimit.toString()));
            // **************************************** PAGINATION DIRECTION
            if (paginationRec && Object.keys(paginationRec).length > 0 && direction) {
                switch (direction) {
                    case 0: // First call
                        break;
                    case 1: // NEXT
                        let paginationValues = [];
                        // Extract values of the paginationRec fields involved in orderBy criteria
                        if (orderBy && orderBy[0] instanceof Array) {
                            // It's an array of arrays
                            for (let o of orderBy) {
                                const paginationFieldValue = jp.query(paginationRec, "$." + o[0]);
                                paginationValues.push(paginationFieldValue[0]);
                            }
                        }
                        else if (orderBy) {
                            const paginationFieldValue = jp.query(paginationRec, "$." + orderBy[0]);
                            paginationValues.push(paginationFieldValue);
                        }
                        if (paginationValues.length > 0)
                            whereQuery = whereQuery.startAfter(...paginationValues);
                        break;
                    case -1: // PREVIOUS
                        break;
                }
            }
            // console.log('where',JSON.stringify(where,null,2));
            // console.log('orderBy',JSON.stringify(orderBy,null,2));
            // console.log('paginationRec',JSON.stringify(paginationRec,null,2));
            // console.log('direction',direction);
            const snapshot = await whereQuery.get();
            const data = snapshot.docs.map((dataItem) => {
                return Object.assign({}, dataItem.data(), { id: dataItem.id });
            }); // Extract actual data from snapshot
            return Promise.resolve(data);
        }
        catch (e) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            const fireError = this.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(errorService.build(fireError, query));
            }
            else {
                return Promise.reject(errorService.build(e, query));
            }
        }
    }
    // Enforces default where criteria to limit the number of records retrieved.
    enforceUserWhereCriteria(req) {
        // Do not add 'where' criteria for Create and Update operations
        if (req.baseUrl.toLowerCase().endsWith("create") || req.baseUrl.toLowerCase().endsWith("update"))
            return;
        if (req.body.whereQuery && req.body.whereQuery.where && Array.isArray(req.body.whereQuery.where) && req.body.whereQuery.where.length > 0) {
            // Check whether it is array of arrays
            if (Array.isArray(req.body.whereQuery.where[0])) {
                // Put role criteria on the top
                req.body.whereQuery.where.unshift(["role.code", ">=", (parseFloat(req.userProfile.role.code))]);
            }
            else {
                // Create array or arrays
                req.body.whereQuery.where = [["role.code", ">=", (parseFloat(req.userProfile.role.code))], req.body.whereQuery.where];
            }
        }
        else {
            // make sure the whereQuery contains 'where' array to make the query succeed
            req.body.whereQuery = Object.assign({}, { where: [] }, req.body.whereQuery);
            req.body.whereQuery.where = ["role.code", ">=", (parseFloat(req.userProfile.role.code))];
        }
        // Set orderBy criteria
        if (req.body.whereQuery && req.body.whereQuery.orderBy && Array.isArray(req.body.whereQuery.orderBy) && req.body.whereQuery.orderBy.length > 0) {
            // Check whether it is array of arrays
            if (Array.isArray(req.body.whereQuery.orderBy[0])) {
                // Check orderBy contains 'role' criteria, if not do not add role criteria again
                const roleCriteriaExists = req.body.whereQuery.orderBy.find((element) => {
                    return element[0].toString().startsWith("role");
                });
                // Put role criteria on the top
                if (!roleCriteriaExists)
                    req.body.whereQuery.orderBy.unshift(["role.code", "asc"]);
            }
            else {
                // Create array or arrays
                req.body.whereQuery.orderBy = [["role.code", "asc"], req.body.whereQuery.orderBy];
            }
        }
        else {
            req.body.whereQuery = Object.assign({}, { orderBy: [] }, req.body.whereQuery);
            req.body.whereQuery.orderBy = ["role.code", "asc"];
        }
        return req;
    }
    /**
     * Performs OR operations on given firebase collection
     * @param collection
     * @param query
     * ======================TO BE TESTED=================
     */
    async whereOr(collection, queries) {
        const promises = queries.map(query => {
            return new Promise(async (resolve, reject) => {
                resolve(await this.where(collection, query));
            });
        });
        return Promise.all(promises)
            .then(values => {
            // Merge all results as one dataset.
            values.reduce((prevVal, curVal) => {
                return prevVal.concat(curVal);
            }, []);
        });
    }
    /**
     * Takes an object/JSON and returns an array of all paths.
     */
    getPaths(obj, prefix = '') {
        let paths = [];
        Object.keys(obj).forEach(key => {
            let path = prefix + key;
            paths.push(path);
            if (typeof obj[key] === 'object') {
                paths.push(...this.getPaths(obj[key], path + "."));
            }
        });
        return paths;
    }
    /**
     * Updates JSON value at given path, including array
     */
    updateJSON(obj, keyPath, value) {
        keyPath = keyPath.split('.'); // split key path string
        let lastKeyIndex = keyPath.length - 1;
        for (var i = 0; i < lastKeyIndex; ++i) {
            const key = keyPath[i];
            // choose if nested object is array or hash based on if key is number
            if (!(key in obj))
                obj[key] = parseInt(key) !== parseInt(key) ? {} : [];
            obj = obj[key];
        }
        obj[keyPath[lastKeyIndex]] = value;
    }
    /**
     * Gives roles as array of objects for the benifit of UI
     */
    getRoles(userRole) {
        let roles = [];
        let code;
        for (const role in enums_1.ROLES) {
            code = parseFloat(role);
            if (!Number.isNaN(code)) {
                // Return the roles that are less or equal in hierarchy of the user's own role
                // Role 0 is higher in hierarchy than role 1
                if (code >= userRole)
                    roles.push({ code: code, name: enums_1.ROLES[code] });
            }
        }
        return roles;
    }
    // Converts ISO date string to date object
    stringToDate(data, dateFields) {
        data.forEach(rec => {
            dateFields.forEach(path => {
                let dtValue = jp.query(rec, "$." + path)[0];
                jp.value(rec, "$." + path, new Date(dtValue));
            });
        });
        return data;
    }
    // Converts ISO date string to firebase timestamp object
    stringToTimestamp(data, dateFields) {
        dateFields.forEach(path => {
            let dtValue = jp.query(data, "$." + path)[0];
            if (dtValue)
                jp.value(data, "$." + path, firebase_admin_1.firestore.Timestamp.fromDate(new Date(dtValue)));
        });
        return data;
    }
    // Converts date object to ISO date string.
    dateToString(data, dateFields) {
        dateFields.forEach(path => {
            let dtValue = jp.query(data, "$." + path)[0];
            if (typeof dtValue.getMonth === 'function')
                jp.value(data, "$." + path, dtValue.toISOString());
        });
        return data;
    }
    // Converts firebase timestamps to ISO date string
    timestampToString(data, dateFields) {
        data.forEach(rec => {
            dateFields.forEach(path => {
                let dtValue = jp.query(rec, "$." + path)[0];
                if (dtValue && typeof dtValue.toDate === 'function')
                    jp.value(rec, "$." + path, dtValue.toDate().toISOString());
            });
        });
        return data;
    }
    /**
     * Makes where queries on given collections
     * NOTE:  FIREBASE RAISES EXCEPTION IF THERE IS NO INDEX ON THE FILEDS USED IN WHERE CONDITION.
     * FIREBASE/FIRESTORE AUTOMATICALLY CREATES INDEX ON EACH SINGLE FIELD
     * CUSTOM INDEX NEEDS MINIMUM TWO FIELDS
     * Returns where query
     */
    async getWhereQuery(collection, query) {
        // Get query reference to collection
        let whereQuery = collection;
        try {
            let { where, orderBy, limit, paginationRec, direction } = query; // Extract query parts
            // **************************************** WHERE
            if (where[0] instanceof Array)
                where = this.removeDuplicateWhereCriterias(where);
            if (orderBy && orderBy[0] instanceof Array)
                orderBy = this.removeDuplicateOrderByCriterias(orderBy);
            if (where && where.length > 0) {
                if (where[0] instanceof Array) { // If there are multiple where conditions (array or arrays)
                    for (let w of where) { // add each where condition to query
                        // https://stackoverflow.com/questions/48036975/firestore-multiple-conditional-where-clauses
                        whereQuery = whereQuery.where(...w);
                    }
                }
                else {
                    whereQuery = whereQuery.where(...where);
                }
            }
            // **************************************** ORDER BY
            if (orderBy && orderBy[0] instanceof Array) {
                // It's an array of array
                for (let o of orderBy) {
                    // https://stackoverflow.com/questions/48036975/firestore-multiple-conditional-where-clauses
                    whereQuery = whereQuery.orderBy(...o);
                }
            }
            else if (orderBy) {
                whereQuery = whereQuery.orderBy(...orderBy);
            }
            // **************************************** LIMIT
            whereQuery = whereQuery.limit(limit ? limit : parseInt(config_1.default.pageLimit.toString()));
            // **************************************** PAGINATION DIRECTION
            if (paginationRec && Object.keys(paginationRec).length > 0 && direction) {
                switch (direction) {
                    case 0: // First call
                        break;
                    case 1: // NEXT
                        let paginationValues = [];
                        // Extract values of the paginationRec fields involved in orderBy criteria
                        if (orderBy && orderBy[0] instanceof Array) {
                            // It's an array of arrays
                            for (let o of orderBy) {
                                const paginationFieldValue = jp.query(paginationRec, "$." + o[0])[0];
                                paginationValues.push(paginationFieldValue);
                            }
                        }
                        else if (orderBy) {
                            const paginationFieldValue = jp.query(paginationRec, "$." + orderBy[0])[0];
                            paginationValues.push(paginationFieldValue);
                        }
                        if (paginationValues.length > 0)
                            whereQuery = whereQuery.startAfter(...paginationValues);
                        break;
                    case -1: // PREVIOUS
                        break;
                }
            }
            // console.log('where',JSON.stringify(where,null,2));
            // console.log('orderBy',JSON.stringify(orderBy,null,2));
            // console.log('paginationRec',JSON.stringify(paginationRec,null,2));
            // console.log('direction',direction);
            return Promise.resolve(whereQuery);
        }
        catch (e) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            const fireError = this.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(errorService.build(fireError, query));
            }
            else {
                return Promise.reject(errorService.build(e, query));
            }
        }
    }
    /**
     * Insurts bulk data
     * @param db provides collection reference
     * @param collection provides collection name
     * @param data proivdes bulk data
     * @returns bulk insert status
     */
    async bulkInsert(db, collection, data) {
        try {
            // const snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentReference> = await whereQuery.get();
            // Calculate the number of batches that it takes to update the records
            const batchLimit = 500;
            let recCounter = 1;
            let batchCounter = 0;
            let batchCount = Math.ceil(data.length / batchLimit);
            // Create n number of batches based on record count
            let batches = [];
            for (let i = 0; i < batchCount; i++) {
                batches.push(db.batch());
            }
            ;
            let batch = batches[batchCounter]; // First batch to collect records
            // Add records to one or more batches
            data.map((rec) => {
                let docRef;
                // If docId key is available, use it as document id.
                if (rec.hasOwnProperty('docId')) {
                    docRef = collection.doc(rec.docId);
                    delete rec.docId;
                }
                else {
                    docRef = collection.doc();
                }
                batch.set(docRef, rec); // set new record for insert
                // assign new batch once the batch limit is reached
                if (recCounter == batchLimit) {
                    recCounter = 0; // it will be incremented to 1 by recCounter++
                    batch = batches[++batchCounter];
                }
                recCounter++;
            });
            // Create promises to commit all batches
            let promises = batches.map(batch => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const result = await batch.commit();
                        return resolve(result);
                    }
                    catch (error) {
                        return reject(error);
                    }
                });
            });
            // Commit all batches
            const result = await Promise.all(promises);
            return Promise.resolve(result);
        }
        catch (error) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            const fireError = this.getFirebaseError(error);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(errorService.build(fireError, null));
            }
            else {
                return Promise.reject(errorService.build(error, null));
            }
        }
    }
    /**
     * Bulk Update
     * @param collection in question
     * @param where criteria to select records
     * @param update fields
     */
    async bulkUpdate(db, collection, where, update) {
        try {
            // Get where query
            const whereQuery = await this.getWhereQuery(collection, where);
            const snapshot = await whereQuery.get();
            // Calculate the number of batches that it takes to update the records
            const batchLimit = 500;
            let recCounter = 1;
            let batchCounter = 0;
            let batchCount = Math.ceil(snapshot.docs.length / batchLimit);
            // Create n number of batches based on record count
            let batches = [];
            for (let i = 0; i < batchCount; i++) {
                batches.push(db.batch());
            }
            ;
            let batch = batches[batchCounter]; // First batch to collect records
            // Add records to one or more batches
            snapshot.docs.map((rec) => {
                batch.update(rec.ref, update);
                // assign new batch once the batch limit is reached
                if (recCounter == batchLimit) {
                    recCounter = 0; // it will be incremented to 1 by recCounter++
                    batch = batches[++batchCounter];
                }
                recCounter++;
            });
            // Create promises to commit all batches
            let promises = batches.map(batch => {
                return new Promise(async (resolve, reject) => {
                    try {
                        const result = await batch.commit();
                        return resolve(result);
                    }
                    catch (error) {
                        return reject(error);
                    }
                });
            });
            // Commit all batches
            const result = await Promise.all(promises);
            return Promise.resolve(result);
        }
        catch (error) {
            let errorService = typedi_1.Container.get(errorService_1.default);
            const fireError = this.getFirebaseError(error);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(errorService.build(fireError, null));
            }
            else {
                return Promise.reject(errorService.build(error, null));
            }
        }
    }
    // TODO:  to be tested
    // Takes an array of objects and returns unique telements based on given key
    getUniqueObjects(arr, key) {
        return [...new Map(arr.map(item => [item[key], item])).values()];
    }
    // ------------ Increase the last character to next char ---------------
    incrementString(key) {
        const charAt = key.length;
        if (key === 'Z' || key === 'z') {
            return String.fromCharCode(key.charCodeAt(charAt) - 25) + String.fromCharCode(key.charCodeAt(charAt) - 25); // AA or aa
        }
        else {
            var lastChar = key.slice(-1);
            var sub = key.slice(0, -1);
            if (lastChar === 'Z' || lastChar === 'z') {
                // If a string of length > 1 ends in Z/z,
                // increment the string (excluding the last Z/z) recursively,
                // and append A/a (depending on casing) to it
                return this.incrementString(sub) + String.fromCharCode(lastChar.charCodeAt(charAt) - 25);
            }
            else {
                // (take till last char) append with (increment last char)
                return sub + String.fromCharCode(lastChar.charCodeAt(charAt) + 1);
            }
        }
    }
    ;
    // Duplicate object of any depth
    /*
    deepClone<T>(source: T): T {
        return Array.isArray(source)
            ? source.map(item => this.deepClone(item))
            : source instanceof Date
                ? new Date(source.getTime())
                : source && typeof source === 'object'
                    ? Object.getOwnPropertyNames(source).reduce((o, prop) => {
                        Object.defineProperty(o, prop, Object.getOwnPropertyDescriptor(source, prop)!);
                        o[prop] = this.deepClone(source[prop]);
                        return o;
                    }, Object.create(Object.getPrototypeOf(source)))
                    : source as T;
    }
    */
    /**
     * Returns menu items that that the Role has access to
     * @param menuItems provides an array of menu items
     * @param role provides user role
     */
    getRoleMenuItems(menuItems, role) {
        role = role.toLowerCase();
        const roleMenu = menuItems.reduce((reduced, menuItem) => {
            // check whether the Role has access to this menu item
            let allowed = (menuItem.hasOwnProperty(role) && menuItem[role].allow);
            let _menuItem;
            // If this menu item is allowed, remove other role's data
            if (allowed) {
                _menuItem = {
                    icon: menuItem.icon,
                    id: menuItem.id,
                    items: menuItem.items,
                    label: menuItem.label,
                    routerLink: menuItem.routerLink
                };
                _menuItem[role] = menuItem[role]; // User spefic data
            }
            // If allowed, check whether the menu item has submenu items
            if (_menuItem && _menuItem.hasOwnProperty("items") && _menuItem.items && allowed) {
                _menuItem.items = this.getRoleMenuItems(_menuItem.items, role);
            }
            if (_menuItem)
                reduced.push(_menuItem);
            return reduced;
        }, []);
        return roleMenu;
    }
}
exports.default = Helper;
//# sourceMappingURL=helper.js.map