"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const functions = require("firebase-functions");
const expressService_1 = __importDefault(require("./services/expressService"));
const firebase_1 = require("./loaders/firebase");
/*
  Initialize before calling middleware, routes to avoid Error: The default Firebase app does not exist.
  Make sure you call initializeApp() before using any of the Firebase services.
  The error happes because the admin.firestore().collection('????') is called in the class constructor before app is initialized.
*/
firebase_1.initFirebase();
const middleware_1 = __importDefault(require("./middleware"));
const errorService_1 = __importDefault(require("./services/errorService"));
const express_useragent_1 = __importDefault(require("express-useragent"));
const logger_1 = __importDefault(require("./loaders/logger"));
const routes_1 = require("./routes"); // Get all routes
const cors = require("cors");
// THIS IS TO HELP DEPLOYING FUNCTIONS EASILY
if (((_a = process.env.NODE_ENV) === null || _a === void 0 ? void 0 : _a.toString().toUpperCase()) === "DEVELOPMENT") {
    /*
    let routesNames:string = "'" + routes.map(route => route.name).join("','") + "'" ;
    routesNames = "./deployFunctions.sh " + routesNames
    
    console.log('\n=======================DEPLOYMENT COMMAND==========================\n');
    // console.log(routesNames);
  
    // RBAC
    let rbacEntries:any[] = [];
    routes.forEach( item => {
      let json:any={}
      
      json[item.name] = {admin:true, manager:true, supervisor:true, user:true}
  
      rbacEntries.push(json)
    })
  
    let rbacRecord:any = {
      "docId":"rbac:en-us",
      "lng": "en-US",
      "lbl": "rbac",
      "data": rbacEntries
    }
  
    console.log('\n=======================RBAC ENTRIES==========================\n');
    console.log(JSON.stringify(rbacRecord));
    */
}
// Create express app for each route
routes_1.routes.forEach((route) => {
    const errorService = typedi_1.Container.get(errorService_1.default);
    const app = expressService_1.default();
    require('./loaders').default({ expressApp: app });
    const config = typedi_1.Container.get('config');
    // https://stackoverflow.com/questions/43871637/no-access-control-allow-origin-header-is-present-on-the-requested-resource-whe
    // Set http headers to response object
    app.use(function (req, res, next) {
        // For Chrome browser to accept cookies, the req.headers.origin must be set as value of Access-Control-Allow-Origin header
        // Without security cookies, the app cannot accept requests
        // Make sure the req.headers.origin is part of allowed origins.
        let origin = (config.allowedOrigins.indexOf(req.headers.origin) != -1) ? req.headers.origin : "unauthorized.origin";
        if (req.headers.origin == undefined) {
            origin = "*";
        }
        // console.log('req.url',req.url, 'req.method', req.method);
        // console.log('req.headers.cookie',req.headers.cookie);
        // console.log('req.headers.set-cookie',req.headers['set-cookie']);
        // console.log('req.headers',req.headers);
        // console.log('allowed origins:',config.allowedOrigins, 'req.headers.origin:', req.headers.origin, 'origin:', origin);
        // Handle headers
        res.header("Access-Control-Allow-Origin", origin);
        res.header("Access-Control-Allow-Credentials", "true");
        res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        res.header("Access-Control-Max-Age", "86400");
        res.header("Access-Control-Allow-Headers", "tknuid, providerId, Origin, X-Requested-With, X-Auth-Token, Content-Type, Content-Length, X-JSON, x-request-id, Authorization, Accept, Access-Control-Request-Method, Access-Control-Allow-Headers");
        res.header("Allow-Origin-With-Credentials", "true"); // https://weblog.west-wind.com/posts/2019/Apr/07/Creating-a-custom-HttpInterceptor-to-handle-withCredentials
        next();
    });
    // add cors middleware
    app.use(cors({
        origin: function (origin, callback) {
            // TODO: Test this with AJAX and implement error handling mechanism
            // allow requests with no origin 
            // (like mobile apps or curl requests)
            if (!origin)
                return callback(null, true);
            if (config.allowedOrigins.indexOf(origin) === -1) {
                let msg = 'The CORS policy for this site does not allow access from the specified Origin: ' + origin;
                logger_1.default.error({ allowedOrigins: config.allowedOrigins, requestOrigin: origin, msg: msg });
                return callback(new Error(msg), false);
            }
            return callback(null, true);
        }
    }));
    app.use(express_useragent_1.default.express());
    app.use(middleware_1.default.appEssentials);
    // Catch all error handler for all the routes.
    route.router.use((error, req, res, next) => {
        res.appFail(errorService.build(error, null));
    });
    // console.log(`"${route.name}",`);
    // Use this IF condition to pick the newly created functions to upload using "firebase deploy" so that they can be accessed using custom domains.
    // if ( ['test','user'].findIndex((rte) => {return route.name.startsWith(rte)}) != -1 ) {
    // export routes individually for cloud functions
    // The /api/ part is added to make the functions accessible through custom functions.
    // /api/ also improves readability
    app.use("/api/" + route.name, route.router);
    // https://stackoverflow.com/questions/56128645/cannot-get-error-on-firebase-cloud-functions-express-sample
    // Function will be registered/exported with the 'route.name' value.
    // For example: to access 'test' function directly, use the following url structure
    // https://us-central1-foodstore-d4030.cloudfunctions.net/test/api/test
    // This is to make each function independent.
    // If all functions related to a route were to be exported under one name, 
    // scaling that instance to handle traffic would hit the 2000 instance limit.
    // To access this function using /api/<name> path, use rewrites in firebase.json file.
    exports[route.name] = functions.https.onRequest(app);
    // }
});
// ******************** Triggers *******************
// On User Create Trigger
exports.onUserCreated = functions.auth.user().onCreate(user => {
    console.log('User Created', user);
});
// On User Delete Trigger
exports.onUserDeletion = functions.auth.user().onDelete(user => {
    console.log('User Deleted', user);
});
//# sourceMappingURL=index.js.map