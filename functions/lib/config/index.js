"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
const envFound = dotenv_1.default.config({ path: __dirname + "/../.env" });
if (!envFound) {
    // This error should crash whole process
    throw new Error("⚠️  Couldn't find .env file  ⚠️");
}
exports.default = {
    appName: process.env.APP_NAME,
    protocol: process.env.API_PROTOCOL,
    host: process.env.API_HOST,
    port: process.env.API_PORT,
    allowedOrigins: process.env.ALLOWED_ORIGINS,
    ui_host: process.env.UI_HOST,
    databaseURL: process.env.MONGODB_URI,
    auditIndex: process.env.AUDIT_INDEX,
    auditType: process.env.AUDIT_TYPE,
    saltRounds: process.env.SALT_ROUNDS,
    secret: process.env.TOKEN_SECRET,
    cryptoSecret: process.env.CRYPTO_SECRET,
    pageLimit: 5,
    tokenLife: process.env.TOKEN_LIFE,
    authenticationFields: ["displayName", "email", "emailVerified", "phoneNumber", "password", "photoURL", "disabled"],
    customClaimsFields: ["role"],
    storage_bucket: process.env.STORAGE_BUCKET,
    storage_root: process.env.STORAGE_ROOT,
    agora_app_id: process.env.AGORA_APP_ID,
    agora_app_certificate: process.env.AGORA_APP_CERTIFICATE,
    defaultLanguage: "en-US",
    api: {
        prefix: '/api',
    },
    /**
     * Used by winston logger
     */
    logs: {
        level: process.env.LOG_LEVEL || 'silly',
    },
};
//# sourceMappingURL=index.js.map