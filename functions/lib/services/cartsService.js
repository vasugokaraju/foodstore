"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = require("typedi");
// import {ICarts, INewCarts, IUpdateCarts} from '../interfaces/ICartsInterface';
const cartsRepository_1 = __importDefault(require("../repositories/cartsRepository"));
const errorService_1 = __importDefault(require("./errorService"));
const middleware_1 = __importDefault(require("../middleware"));
// import firebase from "firebase/app";
// import "firebase/auth";
let CartsService = class CartsService {
    constructor(errorService = typedi_1.Container.get(errorService_1.default)) {
        this.errorService = errorService;
        this.errorService = typedi_1.Container.get(errorService_1.default);
    }
    /**
      * Handles Carts preload operations
    */
    async preload(userProfile, whereQuery) {
        try {
            // Retrieve Carts records for preload
            let cartsInfo = await this.readMany(whereQuery); // Get the records of people who are lower in hierarchy.
            const result = {
                cartsData: cartsInfo
            };
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
     * Helps to validate record.  This method is used as schema keyword validate function.
     * @param id
     */
    async idExists(id) {
        let result = await this.readOne(id);
        // if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
        return Promise.resolve((result.length != 0));
    }
    /**
    * Handles Carts single read operations
    */
    async readOne(id) {
        try {
            let result;
            if (!id)
                return Promise.resolve([]);
            result = await cartsRepository_1.default.read(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts where query operations
    */
    async readMany(where) {
        try {
            let result;
            const whereQuery = where;
            result = await cartsRepository_1.default.readWhere(whereQuery);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts Create operations
    * @param data
    */
    async create(data) {
        try {
            const result = await cartsRepository_1.default.create(data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts Update operations
    * @param id
    * @param data
    */
    async update(id, data) {
        try {
            const result = await cartsRepository_1.default.update(id, data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts Delete operations
    * @param id
    */
    async delete(id) {
        try {
            const result = await cartsRepository_1.default.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts Download operations
    * @param id
    */
    async download(query) {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data
            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Carts Upload operations
    * @param id
    */
    async upload(req) {
        try {
            return middleware_1.default.validateRequest("carts", {
                method: "UPLOAD",
                body: req.body,
                params: {}
            }, {}, async (err) => {
                if (err) {
                    return Promise.reject(this.errorService.build(err, null));
                }
                else {
                    await cartsRepository_1.default.bulkInsert(req.body.bulkData);
                    return Promise.resolve(true);
                }
            });
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
};
CartsService = __decorate([
    typedi_1.Service("carts_service"),
    __metadata("design:paramtypes", [errorService_1.default])
], CartsService);
exports.default = CartsService;
//# sourceMappingURL=cartsService.js.map