"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = require("typedi");
const helper_1 = __importDefault(require("../helpers/helper"));
const errorService_1 = __importDefault(require("./errorService"));
const usersService_1 = __importDefault(require("./usersService"));
const api_query_params_1 = __importDefault(require("api-query-params"));
const enums_1 = require("../loaders/enums");
// import {DATATYPES} from "../loaders/enums";
let EntitlementService = class EntitlementService {
    constructor(helper = typedi_1.Container.get(helper_1.default), errorService = typedi_1.Container.get(errorService_1.default), usersService = typedi_1.Container.get(usersService_1.default)) {
        this.helper = helper;
        this.errorService = errorService;
        this.usersService = usersService;
    }
    // Convert user provided query and params to an object
    async buildQueryParams(req) {
        let findQuery;
        try {
            let params = req.query;
            // When user does not pass any _id, the route.??('/:_id') creates the variable _id with undefined as value
            if (req.params.hasOwnProperty('_id') && req.params._id != undefined)
                params._id = req.params._id;
            // The castParams settings make sure the data type of value remain as is.
            // Ex, phone number is of type string.  But aqp converts it as number if phone number is passed as number unless it is specified in castParams.
            findQuery = api_query_params_1.default(params, { castParams: { phone: "string", zip: "string" } });
            return await Promise.resolve(findQuery);
        }
        catch (e) {
            await Promise.reject(this.errorService.build(e, req));
        }
    }
    // implement entitlement enforcement here for the other modules as needed.
    async enforceUsersEntitlements(req) {
        switch (req.method.toUpperCase()) {
            case "GET":
                // If users's role is of type USER, make sure only his/her record is retreived at the max
                if (req.userProfile.role.code == enums_1.ROLES.USER) {
                    if (req.body.whereQuery == undefined)
                        req.body.whereQuery = { where: [] };
                    req.body.whereQuery.where.push(["email", "==", req.userProfile.email]);
                    req.body.whereQuery.where.push(["phoneNumber", "==", req.userProfile.phone_number]);
                    this.helper.enforceUserWhereCriteria(req);
                }
            // Do not put break here.  Let it continue
            case "PUT":
                // The less the role value is the higher the hierarchy
                // If the logged in user is changing a role to higher or equal to his/her own, reject it.
                if (req.method.toUpperCase() === "PUT" && parseFloat(req.userProfile.role.code) >= parseFloat(req.body.role.code)) {
                    // This situation may arise when user updates his/her own record.  Ignore this in such situation
                    if (req.userProfile.user_id !== req.body.id) {
                        return Promise.reject(this.errorService.build("E1026", req.body));
                    }
                }
            case "DELETE":
                // GET, PUT & DELETE routes force the user to provide id value, otherwise the request failes at route level
                // Check whether the user in question is lower in Role hierarchy than the requester.
                if (req.params.id != undefined && req.params.id.trim().length > 0) {
                    // If the requester id (logged in id) and the requesting id (the id in question) are not the same
                    if (req.userProfile.id !== req.params.id) {
                        // Query the database and get the user record in question to verify the role
                        const subject = await this.usersService.readOne(req.params.id);
                        // Role numbers go from 0 to N while 0 is the highest role in hierarchy.
                        // Requester can access records of other users that are less in hierarchy than the requester.
                        // Raise an unauthorized data acccess exception if requester's role is less or equal in hierarchy than the subject's role
                        if (parseFloat(subject[0].role.code) <= parseFloat(req.userProfile.id)) {
                            return Promise.reject(this.errorService.build("E1003", { unauthorized: req.params.id }));
                        }
                    }
                }
                // Do not let the user to delete his/her own record
                if (req.baseUrl.toLowerCase().endsWith("delete")) {
                    if (req.userProfile.id == req.params.id) {
                        return Promise.reject(this.errorService.build("E1025", { unauthorized: req.params.id }));
                    }
                }
                break;
            case "POST":
                // If the logged in user's role is lower than the new user's role, reject creating the new record.
                // (Note: the less the role number is the higher the hierarchy)
                if (req.baseUrl.toLowerCase().endsWith("create") && parseFloat(req.body.role.code) <= parseFloat(req.userProfile.role.code)) {
                    return Promise.reject(this.errorService.build("E1024", req.body));
                }
                // If users's role is of type USER, make sure only his/her record is retreived at the max
                if (req.userProfile.role.code == enums_1.ROLES.USER) {
                    if (req.body.whereQuery == undefined)
                        req.body.whereQuery = { where: [] };
                    req.body.whereQuery.where.push(["email", "==", req.userProfile.email]);
                    req.body.whereQuery.where.push(["phoneNumber", "==", req.userProfile.phone_number]);
                    this.helper.enforceUserWhereCriteria(req);
                }
                // MAKE SURE 'role' FIELD IS INDEXED IN FIREBASE TO AVOID QUERY FAILURES.
                // ALSO MAKE SURE THE usersService read method contains 'role' parameter as part of WHERE query.
                if (req.baseUrl.toLowerCase().endsWith("readmany") || req.baseUrl.toLowerCase().endsWith("preload")) {
                    // Limit the records to the role level of the user.
                    // Other than own record, records of the same role cannot be edited in the UI.
                    this.helper.enforceUserWhereCriteria(req);
                }
                break;
            default:
                return Promise.reject(false);
        }
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceProductsEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceCartsEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceMeetingEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceAppCodsEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceAppConfEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceSummaryReportEntitlements(req) {
        return Promise.resolve(true);
    }
    // Improve this as needed
    async enforceFinanceReportEntitlements(req) {
        return Promise.resolve(true);
    }
};
EntitlementService = __decorate([
    typedi_1.Service(),
    __metadata("design:paramtypes", [helper_1.default,
        errorService_1.default,
        usersService_1.default])
], EntitlementService);
exports.default = EntitlementService;
//# sourceMappingURL=entitlementService.js.map