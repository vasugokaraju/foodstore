"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = require("typedi");
const helper_1 = __importDefault(require("../helpers/helper"));
const errorService_1 = __importDefault(require("./errorService"));
const ajv_1 = require("ajv");
let ValidationService = class ValidationService {
    constructor(
    // private helper: Helper = Container.get(Helper),
    errorService = typedi_1.Container.get(errorService_1.default), ajv = typedi_1.Container.get("AJV")) {
        this.errorService = errorService;
        this.ajv = ajv;
        // TODO:  Need to find out why class constructor parameters are not being initialized.
        this.errorService = typedi_1.Container.get(errorService_1.default);
        // Initialize keywords
        this.ajv.addKeyword("idExists", {
            validate: this.checkIdExists,
            errors: true,
            async: true
        });
        // Hash password value
        this.ajv.addKeyword("hashIt", {
            validate: this.hashPassword,
            errors: true,
            async: true,
            $data: true
        });
        this.ajv.addKeyword('isNotEmpty', {
            validate: this.isStringNotEmpty,
            errors: true,
            async: false,
            $data: true
        });
        // ajv-errors module should be loaded after custom keywords are added.
        // Otherwise custom errorMessage does not work for the custom keywords.
        // https://github.com/ajv-validator/ajv-errors/issues/32
        require('ajv-errors')(ajv, { singleError: false });
    }
    // Validate given data against given schema
    async validate(schemaName, data) {
        try {
            // let schema: any = this.ajv.getSchema(schemaName);
            this.ajv.errors = []; // Errors array must be reset as it retains old ones.
            let valid = await this.ajv.validate(schemaName, data);
            if (!valid) {
                valid.errors = this.ajv.errors;
                return Promise.reject(this.errorService.build(valid, { schemaName: schemaName }));
            }
            return Promise.resolve(true);
        }
        catch (e) { // Promise.reject() in async keyword function will land here
            e.schemaName = schemaName;
            return Promise.reject(this.errorService.build(e, { schemaName: schemaName }));
        }
    }
    // Checks whether _id exists in given collection
    // This function is used by schema keyword "idExists"
    async checkIdExists(schema, data) {
        try {
            let errors = [];
            // Load the CRUD service based on the schema that triggers this method.
            // Services are loaded based on the name given to service (named service). For example @Service("customers_service")
            let modelService = typedi_1.Container.get(schema.collection.toLowerCase() + "_service");
            let found = await modelService.idExists(data);
            if (found) {
                return Promise.resolve(true);
            }
            else {
                errors.push({
                    keyword: 'idExists',
                    dataPath: '',
                    schemaPath: '',
                    params: { errorCode: schema.errorCode || "E1405" },
                    message: schema.collection + " id '" + data + "' does not exist.",
                    // parentSchema:{},
                    schema: schema,
                    data: data
                });
                return Promise.reject(new ajv_1.ValidationError(errors));
            }
        }
        catch (e) {
            return Promise.reject(new ajv_1.ValidationError(e));
        }
    }
    // Hash the password before storing in db.
    async hashPassword(schema, data) {
        const helper = typedi_1.Container.get(helper_1.default);
        try {
            // let errors:Array<any> = [];
            schema.password = await helper.hash(data);
            return Promise.resolve(true);
        }
        catch (e) {
            return Promise.reject(new ajv_1.ValidationError(e));
        }
    }
    // Checks whether string is blank.
    isStringNotEmpty(schema, data) {
        return (typeof data === 'string' && data.trim() !== '');
    }
};
ValidationService = __decorate([
    typedi_1.Service(),
    __metadata("design:paramtypes", [errorService_1.default, Object])
], ValidationService);
exports.default = ValidationService;
//# sourceMappingURL=validationService.js.map