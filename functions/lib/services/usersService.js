"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = require("typedi");
// import {IUsers, INewUsers, IUpdateUsers} from '../interfaces/IUsersInterface';
const usersRepository_1 = __importDefault(require("../repositories/usersRepository"));
const errorService_1 = __importDefault(require("./errorService"));
// const { Parser } = require('json2csv');
const enums_1 = require("../loaders/enums");
const middleware_1 = __importDefault(require("../middleware"));
// import firebase from "firebase/app";
// import "firebase/auth";
let UsersService = class UsersService {
    constructor(errorService = typedi_1.Container.get(errorService_1.default)) {
        this.errorService = errorService;
        this.errorService = typedi_1.Container.get(errorService_1.default);
    }
    /**
      * Handles Users preload operations
    */
    async preload(userProfile, whereQuery) {
        try {
            let result = {};
            // If the user role is of type USER, limit the data to his/her record
            if (userProfile.role.code == enums_1.ROLES.USER) {
                const requesterInfo = await this.readOne(userProfile.id); // Get the record of the requester (logged in persion);
                result.usersData = requesterInfo;
            }
            // for the users of higher role, there might be multiple roles as they access to users of lower roles
            // The entitlement middleware will enforce where criteria on all requests
            else {
                let usersInfo = await this.readMany(whereQuery); // Get the records of people who are lower in hierarchy.
                result.usersData = usersInfo;
            }
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
     * Helps to validate record.  This method is used as schema keyword validate function.
     * @param id
     */
    async idExists(id) {
        let result = await this.readOne(id);
        // if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
        return Promise.resolve((result.length != 0));
    }
    /**
    * Handles Users single read operations
    */
    async readOne(id) {
        try {
            let result;
            if (!id)
                return Promise.resolve([]);
            result = await usersRepository_1.default.read(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users where query operations
    */
    async readMany(where) {
        try {
            let result;
            const whereQuery = where;
            result = await usersRepository_1.default.readWhere(whereQuery);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users Create operations
    * @param data
    */
    async create(newProfile, id = "", skipAuthentication = false, lang = "en-US") {
        try {
            const result = await usersRepository_1.default.create(newProfile, id, skipAuthentication, lang);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users Update operations
    * @param currentProfile
    * @param newProfile
    */
    async update(currentProfile, newProfile) {
        try {
            // delete password field from newProfile
            delete newProfile.password;
            const result = await usersRepository_1.default.update(currentProfile, newProfile);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users Delete operations
    * @param id
    */
    async delete(id) {
        try {
            const result = await usersRepository_1.default.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users Download operations
    * @param id
    */
    async download(query) {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data
            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Users Upload operations
    * @param id
    */
    async upload(req) {
        try {
            return middleware_1.default.validateRequest("users", {
                method: "UPLOAD",
                body: req.body,
                params: {}
            }, {}, async (err) => {
                if (err) {
                    return Promise.reject(this.errorService.build(err, null));
                }
                else {
                    await usersRepository_1.default.bulkInsert(req.body.bulkData);
                    return Promise.resolve(true);
                }
            });
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
     * Checks whether the given phone number is registered.
     * Initially developed for phone authentication purpose
     */
    async isPhoneNumberRegistered(phoneNumber) {
        try {
            return usersRepository_1.default.isPhoneNumberRegistered(phoneNumber);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Returns Users data in the form of code-name for the purpose of Autocomplete controls on UI.
    * @param whereQuery provides search criteria
    * @returns [ {code:"", name:""} ]
    */
    async getUsersIdDisplayNameReadMany(req) {
        try {
            // Generally whereQuery is enhanced by entitlement process.
            // In a firestore compound query, range (<, <=, >, >=) and not equals (!=, not-in) comparisons must all filter on the same field.
            // Due to this limitation, conditions added by entitlement process are ignored.
            let whereQuery = req.body.autoFilterQuery;
            // Re-arrange the user provided wherequery for security reasons
            whereQuery.where[0][0] = "displayName";
            whereQuery.where[1][0] = "displayName";
            let data = await usersRepository_1.default.readWhere(whereQuery);
            data = data.map(item => {
                return { customerId: item.id, customerName: item.displayName };
            });
            return Promise.resolve(data);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
};
UsersService = __decorate([
    typedi_1.Service("users_service"),
    __metadata("design:paramtypes", [errorService_1.default])
], UsersService);
exports.default = UsersService;
//# sourceMappingURL=usersService.js.map