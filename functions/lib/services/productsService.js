"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const typedi_1 = require("typedi");
// import {IProducts, INewProducts, IUpdateProducts} from '../interfaces/IProductsInterface';
const productsRepository_1 = __importDefault(require("../repositories/productsRepository"));
const errorService_1 = __importDefault(require("./errorService"));
const middleware_1 = __importDefault(require("../middleware"));
// import firebase from "firebase/app";
// import "firebase/auth";
let ProductsService = class ProductsService {
    constructor(errorService = typedi_1.Container.get(errorService_1.default)) {
        this.errorService = errorService;
        this.errorService = typedi_1.Container.get(errorService_1.default);
    }
    /**
      * Handles Products preload operations
    */
    async preload(userProfile, whereQuery) {
        try {
            // Retrieve Products records for preload
            let productsInfo = await this.readMany(whereQuery); // Get the records of people who are lower in hierarchy.
            const result = {
                productsData: productsInfo
            };
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
     * Helps to validate record.  This method is used as schema keyword validate function.
     * @param id
     */
    async idExists(id) {
        let result = await this.readOne(id);
        // if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
        return Promise.resolve((result.length != 0));
    }
    /**
    * Handles Products single read operations
    */
    async readOne(id) {
        try {
            let result;
            if (!id)
                return Promise.resolve([]);
            result = await productsRepository_1.default.read(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products where query operations
    */
    async readMany(where) {
        try {
            let result;
            const whereQuery = where;
            result = await productsRepository_1.default.readWhere(whereQuery);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products Create operations
    * @param data
    */
    async create(data) {
        try {
            const result = await productsRepository_1.default.create(data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products Update operations
    * @param id
    * @param data
    */
    async update(id, data) {
        try {
            const result = await productsRepository_1.default.update(id, data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products Delete operations
    * @param id
    */
    async delete(id) {
        try {
            const result = await productsRepository_1.default.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products Download operations
    * @param id
    */
    async download(query) {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data
            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Handles Products Upload operations
    * @param id
    */
    async upload(req) {
        try {
            return middleware_1.default.validateRequest("products", {
                method: "UPLOAD",
                body: req.body,
                params: {}
            }, {}, async (err) => {
                if (err) {
                    return Promise.reject(this.errorService.build(err, null));
                }
                else {
                    await productsRepository_1.default.bulkInsert(req.body.bulkData);
                    return Promise.resolve(true);
                }
            });
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    /**
    * Returns Products data in the form of code-name for the purpose of Autocomplete controls on UI.
    * @param whereQuery provides search criteria
    * @returns [ {code:"", name:""} ]
    */
    async getProductsIdNameReadMany(req) {
        try {
            // Generally whereQuery is enhanced by entitlement process.
            // In a firestore compound query, range (<, <=, >, >=) and not equals (!=, not-in) comparisons must all filter on the same field.
            // Due to this limitation, conditions added by entitlement process are ignored.
            let whereQuery = req.body.autoFilterQuery;
            // Re-arrange the user provided wherequery for security reasons
            whereQuery.where[0][0] = "name";
            whereQuery.where[1][0] = "name";
            let data = await productsRepository_1.default.readWhere(whereQuery);
            data = data.map(item => {
                return { productId: item.id, productName: item.name };
            });
            return Promise.resolve(data);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
};
ProductsService = __decorate([
    typedi_1.Service("products_service"),
    __metadata("design:paramtypes", [errorService_1.default])
], ProductsService);
exports.default = ProductsService;
//# sourceMappingURL=productsService.js.map