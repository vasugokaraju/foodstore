"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const helper_1 = __importDefault(require("../helpers/helper"));
const firebaseError_1 = __importDefault(require("../classes/firebaseError"));
const errorService_1 = __importDefault(require("../services/errorService"));
const admin = require("firebase-admin");
// const products = admin.firestore().collection('products');
class productsRepository {
    constructor(errorService = typedi_1.Container.get(errorService_1.default), products = admin.firestore().collection('products'), helper = typedi_1.Container.get(helper_1.default)) {
        this.errorService = errorService;
        this.products = products;
        this.helper = helper;
        this.errorService = typedi_1.Container.get(errorService_1.default);
    }
    // Retrieve one record.
    async read(id) {
        try {
            if (id != undefined && id.trim().length > 0) { // If valid id is provided
                // Get the record
                const doc = await this.products.doc(id).get();
                if (doc.exists) { // If record exists
                    const data = doc.data(); // Extract data
                    data.id = id; // add id back to returning dataset for better handling of the data
                    const transformed = this.helper.timestampToString([data], []);
                    return Promise.resolve(transformed);
                }
                else {
                    return Promise.resolve([]); // If no record exists
                }
            }
            else {
                return Promise.resolve([]); // If the id parameter is not present
            }
        }
        catch (e) {
            // Handle any exception
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(this.errorService.build(fireError, { id: id }));
            }
            else {
                return Promise.reject(this.errorService.build(e, { id: id }));
            }
        }
    }
    // Find records based on provided WHERE condition
    // https://cloud.google.com/appengine/docs/standard/go111/datastore/query-restrictions
    // Cannot have inequality filters on multiple properties
    // Properties used in inequality filters must be sorted first
    async readWhere(query) {
        try {
            // Get where query
            const whereQuery = await this.helper.getWhereQuery(admin.firestore().collection('products'), query);
            const snapshot = await whereQuery.get();
            // Extract actual data from snapshot
            let data = snapshot.docs.map((dataItem) => {
                return Object.assign({}, dataItem.data(), { id: dataItem.id });
            });
            // Convert date objects to ISO string
            data = this.helper.timestampToString(data, []);
            return Promise.resolve(data);
        }
        catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(this.errorService.build(fireError, query));
            }
            else {
                return Promise.reject(this.errorService.build(e, query));
            }
        }
    }
    /**
     * Create new Products.
     * @param rec new Products data
     * @param lang
     * @returns
     */
    async create(rec, lang = "en-US") {
        try {
            const data = await this.products.add(rec);
            return Promise.resolve(Object.assign({}, rec, { id: data.id }));
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    // Update record
    async update(id, updateRec) {
        try {
            const updatedData = await this.products.doc(id).set(updateRec, { merge: true });
            return Promise.resolve(updatedData);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    // Delete record
    async delete(id) {
        try {
            const deletedData = await this.products.doc(id).delete();
            return Promise.resolve(deletedData);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, id));
        }
    }
    // Bulk Insert
    async bulkInsert(data) {
        try {
            const db = admin.firestore();
            await this.helper.bulkInsert(db, admin.firestore().collection('products'), data);
            return Promise.resolve(true);
        }
        catch (error) {
            return Promise.reject(this.errorService.build(error, null));
        }
    }
}
exports.default = new productsRepository();
//# sourceMappingURL=productsRepository.js.map