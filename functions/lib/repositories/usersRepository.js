"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const helper_1 = __importDefault(require("../helpers/helper"));
const firebaseError_1 = __importDefault(require("../classes/firebaseError"));
const errorService_1 = __importDefault(require("../services/errorService"));
const config_1 = __importDefault(require("../config"));
const emailService_1 = __importDefault(require("../services/emailService"));
const enums_1 = require("../loaders/enums");
const admin = require("firebase-admin");
// const users = admin.firestore().collection('users');
class usersRepository {
    constructor(errorService = typedi_1.Container.get(errorService_1.default), users = admin.firestore().collection('users'), helper = typedi_1.Container.get(helper_1.default), emailService = typedi_1.Container.get(emailService_1.default)) {
        this.errorService = errorService;
        this.users = users;
        this.helper = helper;
        this.emailService = emailService;
        this.errorService = typedi_1.Container.get(errorService_1.default);
    }
    // Retrieve one record.
    async read(id) {
        try {
            if (id != undefined && id.trim().length > 0) { // If valid id is provided
                // Get the record
                const doc = await this.users.doc(id).get();
                if (doc.exists) { // If record exists
                    const data = doc.data(); // Extract data
                    data.id = id; // add id back to returning dataset for better handling of the data
                    const transformed = this.helper.timestampToString([data], ["dob", "address.city.foundedon"]);
                    return Promise.resolve(transformed);
                }
                else {
                    return Promise.resolve([]); // If no record exists
                }
            }
            else {
                return Promise.resolve([]); // If the id parameter is not present
            }
        }
        catch (e) {
            // Handle any exception
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(this.errorService.build(fireError, { id: id }));
            }
            else {
                return Promise.reject(this.errorService.build(e, { id: id }));
            }
        }
    }
    // Find records based on provided WHERE condition
    // https://cloud.google.com/appengine/docs/standard/go111/datastore/query-restrictions
    // Cannot have inequality filters on multiple properties
    // Properties used in inequality filters must be sorted first
    async readWhere(query) {
        try {
            // Get where query
            const whereQuery = await this.helper.getWhereQuery(admin.firestore().collection('users'), query);
            const snapshot = await whereQuery.get();
            // Extract actual data from snapshot
            let data = snapshot.docs.map((dataItem) => {
                return Object.assign({}, dataItem.data(), { id: dataItem.id });
            });
            // Convert date objects to ISO string
            data = this.helper.timestampToString(data, ["dob", "address.city.foundedon"]);
            return Promise.resolve(data);
        }
        catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(this.errorService.build(fireError, query));
            }
            else {
                return Promise.reject(this.errorService.build(e, query));
            }
        }
    }
    /**
     * Create new user.
     * @param newRec new Users data
     * @param id to use as record identifier
     * @param skipAuthentication to skip creating authentication record. Useful when user logs in using federated logins
     * @param lang
     * @returns
     */
    async create(newRec, id = "", skipAuthentication = false, lang = "en-US") {
        const profileSegments = this.helper.userProfileSegments(newRec);
        try {
            // ************ CREATE AUTHENTICATION RECORD FOR THE USER
            if (!skipAuthentication && profileSegments.hasOwnProperty('authFields') && Object.keys(profileSegments.authFields).length > 0) {
                // Password may not be present at all times.
                if (profileSegments.authFields.password) {
                    profileSegments.authFields.password = await this.helper.hash(profileSegments.authFields.password); // Hash it for safety
                }
                if (!profileSegments.authFields.photoURL || profileSegments.authFields.photoURL.length == 0) {
                    delete profileSegments.authFields.photoURL;
                }
                profileSegments.authFields.email = profileSegments.authFields.email.toLowerCase();
                // Create new user and get uid
                const registeredUser = await admin.auth().createUser(profileSegments.authFields);
                id = registeredUser.uid;
            }
            // ************ UPDATE NEW USER WITH CUSTOM CLAIMS
            await admin.auth().setCustomUserClaims(id, profileSegments.customClaimsFileds);
            // ************ CREATE RECORD IN USERS COLLECTION WITH ALL THE FIELDS TO ENABLE QUERYING
            // The data that is stored as Authentication data cannot be queried on fields.  Only pagination is allowed.  It becomes expensive over the period.
            // Authentication data is meant for Firebase authentication.
            // Store all the user data in collection for eash mangement.
            let userData = Object.assign({}, profileSegments.authFields, profileSegments.customClaimsFileds, profileSegments.extendedProfileFields);
            delete userData.password; // Do not store password in users collection
            delete userData.id; // No need to store the Id field.  This is only for the sake of external use.
            // Convert date strings to firebase timestamps
            const transformedData = this.helper.stringToTimestamp(userData, ["dob", "address.city.foundedon"]);
            await this.users.doc(id).set(transformedData); // Create user record
            // --------------SEND EMAIL FOR REGISTRATION CONFIRMATION-------------
            const str = `${id}~~${userData.email}`;
            const token = this.helper.encrypt(str);
            const emailData = {
                name: userData.displayName,
                confirm_link: `${config_1.default.ui_host}/auth/verifyRegisteredEmail?token=${token}`
            };
            await this.emailService.send(lang, [userData.email], enums_1.EMAIL_TEMPLATES.EMAIL_VERIFICATION, emailData);
            // ------------------------------------------------------------------
            // When the authentication record is created for the user, it provides an unique reference key.
            // Use the same key to create user record in the Users collection.
            return Promise.resolve({ id: id });
        }
        catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof firebaseError_1.default) {
                return Promise.reject(this.errorService.build(fireError, newRec));
            }
            else {
                return Promise.reject(this.errorService.build(e, newRec));
            }
        }
    }
    // Update record
    // Note: idToken is gets invalidated if the authentication provider is email address and it is updated.
    async update(currentProfile, newProfile) {
        newProfile = this.helper.userProfileSegments(newProfile);
        const id = newProfile.extendedProfileFields.id;
        try {
            // ************ UPDATE USER AUTHENTICATION DATA
            if (newProfile.hasOwnProperty('authFields') && Object.keys(newProfile.authFields).length > 0) {
                // Do not update the providers and password as it invalidates tokens.
                // There is a dedicated process to make such updates.
                delete newProfile.authFields.email;
                delete newProfile.authFields.password;
                delete newProfile.authFields.phoneNumber;
                await admin.auth().updateUser(id, newProfile.authFields);
            }
            // ************ UPDATE CUSTOM CLAIMS DATA IF AVAILABLE
            if (newProfile.hasOwnProperty('customClaimsFileds') && Object.keys(newProfile.customClaimsFileds).length > 0)
                await admin.auth().setCustomUserClaims(id, newProfile.customClaimsFileds);
            // ************ UPDATE USER EXTENDED DATA
            let userData = Object.assign({}, newProfile.authFields, newProfile.customClaimsFileds, newProfile.extendedProfileFields);
            delete userData.password; // Do not store password in users collection
            delete userData.id; // No need to store the Id field.  This is only for the sake of external use.
            // Convert date strings to firebase timestamps
            const transformedData = this.helper.stringToTimestamp(userData, ["dob", "address.city.foundedon"]);
            await this.users.doc(id).update(transformedData);
            return Promise.resolve(true);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }
    // Delete record
    async delete(id) {
        try {
            // ********** DELETE AUTHENTICATION RECORD INCLUDING CUSTOM USER CLAIMS FOR THE GIVEN USER
            admin.auth().deleteUser(id);
            // ********** DELETE USER DOCUMENT AND ITS SUBCOLLECTIONS FROM USERS COLLECTION
            await this.helper.deleteDocument(this.users.doc(id));
            return Promise.resolve(true);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, id));
        }
    }
    /**
     * Checks whether the given phone number is registered.
     * Initially developed for phone authentication purpose
     */
    // Find the user by phone number from authentication data
    async isPhoneNumberRegistered(phoneNumber) {
        try {
            // Check whether any user exists with given phone number
            await admin.auth().getUserByPhoneNumber(phoneNumber);
            return Promise.resolve(true);
        }
        catch (error) {
            const fireError = this.helper.getFirebaseError(error);
            if (fireError instanceof firebaseError_1.default) {
                if (fireError.code == "auth/user-not-found") {
                    return Promise.resolve(false);
                }
                else {
                    return Promise.reject(this.errorService.build(fireError, phoneNumber));
                }
            }
            else {
                return Promise.reject(this.errorService.build(error, phoneNumber));
            }
        }
    }
    // Bulk Insert
    async bulkInsert(data) {
        try {
            const db = admin.firestore();
            await this.helper.bulkInsert(db, admin.firestore().collection('users'), data);
            return Promise.resolve(true);
        }
        catch (error) {
            return Promise.reject(this.errorService.build(error, null));
        }
    }
}
exports.default = new usersRepository();
//# sourceMappingURL=usersRepository.js.map