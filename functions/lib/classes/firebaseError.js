"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * In the absence of official FirebaseError class
 * https://github.com/firebase/firebase-admin-node/issues/403
 */
class FirebaseError extends Error {
    constructor(code, message) {
        super();
        this.code = code;
        this.message = message;
    }
}
exports.default = FirebaseError;
//# sourceMappingURL=firebaseError.js.map