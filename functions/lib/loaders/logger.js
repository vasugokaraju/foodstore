"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
let winston = require('winston');
const admin = require("firebase-admin");
const config_1 = __importDefault(require("../config"));
// Imports the Google Cloud client library for Winston
const logging_winston_1 = require("@google-cloud/logging-winston");
let serviceAccountConfig = Object.assign({}, admin.credential.applicationDefault());
const loggingWinston = new logging_winston_1.LoggingWinston({ projectId: serviceAccountConfig.projectId });
const transports = [];
if (process.env.NODE_ENV !== 'development') {
    // Print logs to google cloud logging
    transports.push(loggingWinston);
}
else {
    // Prints logs to console window.
    transports.push(new winston.transports.Console({
        format: winston.format.combine(winston.format.cli(), winston.format.splat())
    }));
}
const LoggerInstance = winston.createLogger({
    level: config_1.default.logs.level,
    levels: winston.config.npm.levels,
    format: winston.format.combine(winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss'
    }), winston.format.errors({ stack: true }), winston.format.splat(), winston.format.printf((info) => "\n\n" + info.message + "\n____________________________________________________________________________________________")),
    transports
});
exports.default = LoggerInstance;
//# sourceMappingURL=logger.js.map