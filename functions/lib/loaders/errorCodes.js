"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
// import Logger from './logger';
const errorService_1 = __importDefault(require("../services/errorService"));
const fs = require('fs');
exports.default = async () => {
    return new Promise((resolve, reject) => {
        let errorService = typedi_1.Container.get(errorService_1.default);
        try {
            // Read from .json file
            let file = __dirname + "/errorCodesData.json";
            let data = fs.readFileSync(file).toString();
            const ErrorCodes = JSON.parse(data);
            typedi_1.Container.set("ErrorCodes", ErrorCodes);
            return resolve(ErrorCodes);
        }
        catch (e) {
            typedi_1.Container.set("ErrorCodes", {});
            return reject(errorService.build(e, null));
        }
    });
};
//# sourceMappingURL=errorCodes.js.map