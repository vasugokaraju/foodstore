"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initFirebase = void 0;
/**
  * This should be loaded before routes are loaded.  So, it will be imported in the main /index.ts file.
**/
const typedi_1 = __importDefault(require("typedi"));
const admin = require("firebase-admin");
const helper_1 = __importDefault(require("../helpers/helper"));
const app_1 = __importDefault(require("firebase/app"));
require("firebase/auth");
const functions = require('firebase-functions');
const helper = typedi_1.default.get(helper_1.default);
const initFirebase = () => {
    // console.log('**********************Initialize Firebase App');
    // ********** INITIALIZE FIREBASE APP
    // https://firebase.google.com/docs/reference/js/firebase.app
    // Default credentials are taken using GOOGLE_APPLICATION_CREDENTIALS OS environment variable
    // https://firebase.google.com/docs/admin/setup#initialize_the_sdk
    // NOTE: add service setting depends on your usecase. (i.e. firestore)
    // const firebaseConfig: any = functions.config().fireconf;
    // console.log('firebaseConfig', firebaseConfig);
    // admin.initializeApp({
    //   credential: admin.credential.applicationDefault(),
    //   projectId:firebaseConfig.projectId
    //   // databaseURL: ""
    // });
    try {
        admin.initializeApp();
        typedi_1.default.set('admin', admin);
    }
    catch (error) {
        console.log('initializeApp error', error);
    }
    // const firestoreSettings: any = Object.assign({}, admin.firestore())
    // console.log('databaseURL:', firestoreSettings._settings.servicePath + ":" + firestoreSettings._settings.port);
    // console.log('firebaseConfig.projectId', firebaseConfig.projectId);
    // ********** INITIALIZE FIREBASE AUTH SERVICE
    // https://firebase.google.com/docs/reference/js/firebase.auth
    /*
      The auth module is generally used in clients.  Taking advantage of this javascript library on server side.
      Credentials are taken from /functions/.runtimeconfig.json
      Environment specific credentials are laoded during bootstrap using firebaseConfigSet.sh (firebaseConfig.development, firebaseConfig.production)
  
      NOTE:  The above method is not used anymore.  Now the getRuntimeConfig.sh gets the config and writes to .runtimeconfig.json directly.
             Make sure web_app_id variable is set with your firebase web app id in getRuntimeConfig.sh.
             Get firebase web app id using 'firebase apps:list WEB' command
    */
    const runtimeConfig = helper.formatFirebaseConfig(functions.config().fireconf);
    app_1.default.initializeApp(runtimeConfig);
    // Store firebase app config in container to provide the same to web client.
    // This is to avoid hardcoding the configuration in client app
    typedi_1.default.set('firebaseAppConfig', runtimeConfig);
    // let auth: firebase.auth.Auth;
    if (process.env.NODE_ENV == 'development') {
        // The following code is causing the warning
        // WARNING: You are using the Auth Emulator, which is intended for local testing only.  Do not use with production credentials.
        // firebase.initializeApp(firebaseConfig);
        const auth = app_1.default.auth();
        auth.useEmulator("http://127.0.0.1:9099"); // Use local emulators for development
    }
};
exports.initFirebase = initFirebase;
//# sourceMappingURL=firebase.js.map