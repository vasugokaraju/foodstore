"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
const errorService_1 = __importDefault(require("../services/errorService"));
const fs = require('fs');
exports.default = async () => {
    return new Promise((resolve, reject) => {
        let errorService = typedi_1.Container.get(errorService_1.default);
        try {
            // Read from .json file
            let file = __dirname + "/successCodesData.json";
            let data = fs.readFileSync(file).toString();
            const SuccessCodes = JSON.parse(data);
            typedi_1.Container.set("SuccessCodes", SuccessCodes);
            return resolve(SuccessCodes);
        }
        catch (e) {
            typedi_1.Container.set("SuccessCodes", {});
            return reject(errorService.build(e, null));
        }
    });
};
//# sourceMappingURL=successCodes.js.map