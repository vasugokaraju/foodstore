"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMAIL_TEMPLATES = exports.DATATYPES = exports.ROLES = exports.HTTP_STATUS = void 0;
// HTTP statuses to use in responses.
var HTTP_STATUS;
(function (HTTP_STATUS) {
    HTTP_STATUS[HTTP_STATUS["OK"] = 200] = "OK";
    HTTP_STATUS[HTTP_STATUS["Created"] = 201] = "Created";
    HTTP_STATUS[HTTP_STATUS["Accepted"] = 202] = "Accepted";
    HTTP_STATUS[HTTP_STATUS["Non_Authoritative_Information"] = 203] = "Non_Authoritative_Information";
    HTTP_STATUS[HTTP_STATUS["No_Content"] = 204] = "No_Content";
    HTTP_STATUS[HTTP_STATUS["Bad_Request"] = 400] = "Bad_Request";
    HTTP_STATUS[HTTP_STATUS["Unauthorized"] = 401] = "Unauthorized";
    HTTP_STATUS[HTTP_STATUS["Forbidden"] = 403] = "Forbidden";
    HTTP_STATUS[HTTP_STATUS["Not_Found"] = 404] = "Not_Found";
    HTTP_STATUS[HTTP_STATUS["Method_Not_Allowed"] = 405] = "Method_Not_Allowed";
    HTTP_STATUS[HTTP_STATUS["Duplicate_Key"] = 409] = "Duplicate_Key";
    HTTP_STATUS[HTTP_STATUS["Invalid_Input"] = 422] = "Invalid_Input";
    HTTP_STATUS[HTTP_STATUS["Internal_Server_Error"] = 500] = "Internal_Server_Error";
    HTTP_STATUS[HTTP_STATUS["Not_Implemented"] = 501] = "Not_Implemented";
    HTTP_STATUS[HTTP_STATUS["Service_Unavailable"] = 503] = "Service_Unavailable";
})(HTTP_STATUS = exports.HTTP_STATUS || (exports.HTTP_STATUS = {}));
// Roles
var ROLES;
(function (ROLES) {
    ROLES[ROLES["SUPERADMIN"] = 0] = "SUPERADMIN";
    ROLES[ROLES["MANAGER"] = 1.1] = "MANAGER";
    ROLES[ROLES["SUPERVISOR"] = 2.2] = "SUPERVISOR";
    ROLES[ROLES["USER"] = 3.3] = "USER";
})(ROLES = exports.ROLES || (exports.ROLES = {}));
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
var DATATYPES;
(function (DATATYPES) {
    DATATYPES["string"] = "string";
    DATATYPES["number"] = "number";
    DATATYPES["boolean"] = "boolean";
    DATATYPES["undefined"] = "undefined";
    DATATYPES["date"] = "date";
    DATATYPES["object"] = "object";
    DATATYPES["bigint"] = "bigint";
    DATATYPES["symbol"] = "symbol";
    DATATYPES["function"] = "function";
    DATATYPES["array"] = "array";
})(DATATYPES = exports.DATATYPES || (exports.DATATYPES = {}));
var EMAIL_TEMPLATES;
(function (EMAIL_TEMPLATES) {
    EMAIL_TEMPLATES["WELCOME"] = "welcome";
    EMAIL_TEMPLATES["EMAIL_VERIFICATION"] = "email-verification";
    EMAIL_TEMPLATES["EMAIL_OTP_LOGIN"] = "email-opt-login";
    EMAIL_TEMPLATES["FORGOT_PASSWORD"] = "forgot_password";
})(EMAIL_TEMPLATES = exports.EMAIL_TEMPLATES || (exports.EMAIL_TEMPLATES = {}));
//# sourceMappingURL=enums.js.map