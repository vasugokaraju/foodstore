"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
// import expressLoader from './express';
// import mongodbClient from './mongodbClient';
// import mongodbCollections from './mongodbCollections';
const logger_1 = __importDefault(require("./logger"));
const jsonDataLoader_1 = __importDefault(require("./jsonDataLoader"));
const errorService_1 = __importDefault(require("../services/errorService"));
const schemas_1 = __importDefault(require("./schemas"));
const config_1 = __importDefault(require("../config"));
// import initialSetup from "./initialSetup";
exports.default = async ({ expressApp }) => {
    let errorService = typedi_1.Container.get(errorService_1.default);
    try {
        // Load config
        typedi_1.Container.set("config", config_1.default);
        // Load app error codes
        await jsonDataLoader_1.default();
        // Logger.info('Error Codes are loaded.');
        // await initialSetup(mongoConnection);
        // Logger.warn('Initial setup is completed. Please comment "await initialSetup(mongoConnection)" in loaders/index.ts');
        await schemas_1.default();
        // Logger.info('Data validation schemas are loaded.');
        // await expressLoader({ app: expressApp });
        // Logger.info('Express loaded');
    }
    catch (e) {
        let errorData = errorService.build(e, null);
        let errorResolve = await errorService.resolve(errorData);
        logger_1.default.error(JSON.stringify(errorResolve.log, null, " "));
        // Terminate load process as one of the modules had failed to load.
        process.exit();
    }
};
//# sourceMappingURL=index.js.map