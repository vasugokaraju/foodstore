"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typedi_1 = require("typedi");
// import Logger from './logger';
const errorService_1 = __importDefault(require("../services/errorService"));
const fs_1 = __importDefault(require("fs"));
const ajv_1 = __importDefault(require("ajv"));
let ajv = new ajv_1.default({ allErrors: true, async: "es7", jsonPointers: true, $data: true });
require('../modules/ajv-bson')(ajv);
exports.default = async () => {
    return new Promise((resolve, reject) => {
        let errorService = typedi_1.Container.get(errorService_1.default);
        try {
            // STEP1: Get the list of schema file names from /schema folder
            let schemaFolder = __dirname + "/../schemas";
            let schemaFiles = fs_1.default.readdirSync(schemaFolder).filter(file => !file.endsWith(".js.map"));
            // let schemas:Array<any> = [];
            // Load all schemas
            // Although addSchema does not compile schemas, explicit compilation is not required - the schema will be compiled when it is used first time.
            schemaFiles.forEach(file => {
                let schema = require("../schemas/" + file).default; // get each schema
                let schemaName = file.split(".")[0];
                // Because of Firebase way of loading each route as a function, this code is causing errors. trycatch is a workaround
                try {
                    ajv.addSchema(schema, schemaName); // Add each schema. They will be compiled on first use.
                }
                catch (error) {
                }
            });
            typedi_1.Container.set("AJV", ajv);
            return resolve(true);
        }
        catch (e) {
            typedi_1.Container.set("StaticCodeSnippets", {});
            return reject(errorService.build(e, null));
        }
    });
};
//# sourceMappingURL=schemas.js.map