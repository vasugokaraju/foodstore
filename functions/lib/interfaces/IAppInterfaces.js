"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MEETING_INVITATION = exports.LANGUAGES = void 0;
// Languages
var LANGUAGES;
(function (LANGUAGES) {
    LANGUAGES["Telugu"] = "te-IN";
    LANGUAGES["Kannada"] = "kn-IN";
    LANGUAGES["Tamil"] = "ta-IN";
    LANGUAGES["Malayalam"] = "ml-IN";
    LANGUAGES["Hindi"] = "hi-IN";
    LANGUAGES["English"] = "en-US";
})(LANGUAGES = exports.LANGUAGES || (exports.LANGUAGES = {}));
// Meeting call status
var MEETING_INVITATION;
(function (MEETING_INVITATION) {
    MEETING_INVITATION[MEETING_INVITATION["INVITE"] = 0] = "INVITE";
    MEETING_INVITATION[MEETING_INVITATION["ACCEPTED"] = 1] = "ACCEPTED";
    MEETING_INVITATION[MEETING_INVITATION["REJECTED"] = 2] = "REJECTED";
    MEETING_INVITATION[MEETING_INVITATION["PROGRESS"] = 3] = "PROGRESS";
    MEETING_INVITATION[MEETING_INVITATION["ENDED"] = 4] = "ENDED";
    MEETING_INVITATION[MEETING_INVITATION["LEFT"] = 5] = "LEFT";
})(MEETING_INVITATION = exports.MEETING_INVITATION || (exports.MEETING_INVITATION = {}));
//# sourceMappingURL=IAppInterfaces.js.map