#!/usr/bin/env bash

# Run this file under /functions folder to set functions configuration and create .runtimeconfig.json file
firebase functions:config:set   fireconf.apikey="YOUR API KEY" \
                                fireconf.authdomain="YOUR AUTH DOMAIN" \
                                fireconf.projectid="YOUR PROJECT ID" \
                                fireconf.storagebucket="YOUR STORAGE BUCKET" \
                                fireconf.messagingsenderid="YOUR MESSAGING SENDER ID" \
                                fireconf.appid="YOUR APP ID" \
                                fireconf.measurementid="YOUR MEASUREMENT ID"

firebase functions:config:get > .runtimeconfig.json