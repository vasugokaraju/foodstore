import 'reflect-metadata';
import { Service } from 'typedi';
import { IWhereQuery} from "../interfaces/IAppInterfaces";
import { ISummaryReport, IFinanceReport,  } from "../interfaces/IReportsInterfaces"

// import ErrorService from "./errorService";

@Service()
export default class ReportsService {
    constructor(
        // private errorService: ErrorService = Container.get(ErrorService),
        // private helper: Helper = Container.get(Helper),
    ) {
    }

    /**
    *   Provides summaryReport data
    */
    public async summaryReport(userProfile:any, whereQuery:IWhereQuery): Promise<any> {
        
        // Retrieve data and poplate the reportData variable
        // DATABASE QUERY GOES HERE

        // Above code should provide data for this variable.
        const reportData:ISummaryReport[] = [
            {field1:"summaryReport", field2:1, field3:true,field4:{}, field5:2.2, field6:new Date(), field7:["array1"]}
        ];   

        return reportData;
    }

    /**
    *   Provides financeReport data
    */
    public async financeReport(userProfile:any, whereQuery:IWhereQuery): Promise<any> {
        
        // Retrieve data and poplate the reportData variable
        // DATABASE QUERY GOES HERE

        // Above code should provide data for this variable.
        const reportData:IFinanceReport[] = [
            {field1:"financeReport", field2:1, field3:true,field4:{}, field5:2.2, field6:new Date(), field7:["array1"]}
        ];   

        return reportData;
    }


}