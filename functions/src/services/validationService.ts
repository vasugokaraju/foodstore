import 'reflect-metadata';
import { Service, Container } from 'typedi';
import Helper from "../helpers/helper";
import ErrorService from "./errorService";
import { Ajv, ValidationError } from 'ajv';
import { ICRUDServiceInterface } from '../interfaces/IAppInterfaces';

@Service()
export default class ValidationService {
    constructor(
        // private helper: Helper = Container.get(Helper),
        private errorService: ErrorService = Container.get(ErrorService),
        private ajv: Ajv = Container.get("AJV")
    ) {
        // TODO:  Need to find out why class constructor parameters are not being initialized.
        this.errorService = Container.get(ErrorService);

        // Initialize keywords
        this.ajv.addKeyword("idExists", {
            validate: this.checkIdExists,
            errors: true,
            async: true
        });

        // Hash password value
        this.ajv.addKeyword("hashIt", {
            validate: this.hashPassword,
            errors: true,
            async: true,
            $data: true
        });

        this.ajv.addKeyword('isNotEmpty', {
            validate: this.isStringNotEmpty,
            errors: true,
            async: false,
            $data: true
        })        

        // ajv-errors module should be loaded after custom keywords are added.
        // Otherwise custom errorMessage does not work for the custom keywords.
        // https://github.com/ajv-validator/ajv-errors/issues/32
        require('ajv-errors')(ajv, { singleError: false });
    }

    // Validate given data against given schema
    public async validate(schemaName: any, data: any): Promise<any> {
        try {

            // let schema: any = this.ajv.getSchema(schemaName);

            this.ajv.errors = [];   // Errors array must be reset as it retains old ones.
            let valid = await this.ajv.validate(schemaName, data);

            if (!valid) {
                valid.errors = this.ajv.errors;
                return Promise.reject(this.errorService.build(valid, { schemaName: schemaName }));
            }

            return Promise.resolve(true);
        } catch (e) {   // Promise.reject() in async keyword function will land here
            e.schemaName = schemaName;
            return Promise.reject(this.errorService.build(e, { schemaName: schemaName }));
        }
    }

    // Checks whether _id exists in given collection
    // This function is used by schema keyword "idExists"
    public async checkIdExists(schema: any, data: any): Promise<boolean> {

        try {
            let errors:Array<any> = [];

            // Load the CRUD service based on the schema that triggers this method.
            // Services are loaded based on the name given to service (named service). For example @Service("customers_service")
            let modelService: ICRUDServiceInterface = Container.get(schema.collection.toLowerCase() + "_service");

            let found = await modelService.idExists(data);

            if (found) {
                return Promise.resolve(true);
            }
            else {
                errors.push({
                    keyword: 'idExists',
                    dataPath: '',
                    schemaPath: '', // AJV fills this field with full schema path
                    params: { errorCode: schema.errorCode || "E1405" }, // params: the object with the additional information about error that can be used to create
                    message: schema.collection + " id '" + data + "' does not exist.",
                    // parentSchema:{},
                    schema: schema,
                    data: data
                })
                return Promise.reject(new ValidationError(errors));
            }

        } catch (e) {
            return Promise.reject(new ValidationError(e));
        }
    }

    // Hash the password before storing in db.
    public async hashPassword(schema: any, data: any): Promise<boolean> {
        const helper = Container.get(Helper);

        try {
            // let errors:Array<any> = [];
            schema.password = await helper.hash(data);

            return Promise.resolve(true);

        } catch (e) {
            return Promise.reject(new ValidationError(e));
        }
    }

    // Checks whether string is blank.
    public isStringNotEmpty(schema: any, data: any): boolean {
        return (typeof data === 'string' && data.trim() !== '')
    }

}