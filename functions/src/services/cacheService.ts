// https://www.npmjs.com/package/node-cache

import 'reflect-metadata';
import { Container, Service } from 'typedi';
import NodeCache, { ValueSetItem } from 'node-cache';
import { IAppCode, IAppConf } from '../interfaces';
// import FirebaseError from '../classes/firebaseError';
import admin = require('firebase-admin');

const cacheOpts: any = {
    stdTTl: 60 * 60,
    maxKeys: 100
}

const myCache = new NodeCache(cacheOpts);

// Fired when a key has been added or changed. You will get the key and the value as callback argument.
myCache.on("set", function (key, value) {
    // ... do something ...
});

// Fired when a key has been removed manually or due to expiry. You will get the key and the deleted value as callback arguments.
myCache.on("del", function (key, value) {
    // ... do something ...
});

// Fired when a key expires. You will get the key and value as callback argument.
myCache.on("expired", function (key, value) {
    // ... do something ...
});

// Fired when the cache has been flushed.
myCache.on("flush", function () {
    // ... do something ...
});

// Fired when the cache stats has been flushed.
myCache.on("flush_stats", function () {
    // ... do something ...
});

/**
 * Manages server side cache
 */
@Service()
export default class CacheService {
    constructor(
        // private appCodesService: AppCodsService = Container.get(AppCodsService),
        // private appConfService: AppConfService = Container.get(AppConfService),
        
        // TODO: Use FirebaseFirestore.CollectionReference as appCodes type instead of any
        private appCodes:any = admin.firestore().collection('appCods'),
        private appConf:any = admin.firestore().collection('appConf'),
    ) {

    }

    /**
     * Creates cache key/value pair
     * @param key - name of the cache variable
     * @param val - value
     */
    public set(key: string, value: any, options: any = undefined): boolean {
        // Store given key/value.  Set options if available.  Blank options object removes default options
        const status =  (options)?  myCache.set(key, value, options) : myCache.set(key, value);
        return status;
    }

    /**
     * Stores multiple cache key/value pairs at once
     * @param data - array of key/value pairs to be sored in cache
     */
    public mset(data: ValueSetItem[]): boolean {
        const status = myCache.mset(data);
        return status;
    }

    /**
     * Gets given cache value
     * @param key - name of the key to retrieve
     */
    public get(key: string): any {

        // Call database to get value, if not present in cache.

        // Store in cache

        // Return value
        return myCache.get(key);
    }

    /**
     * Returns app code value from database if not available in cache
     * @param appCode - name of the key to retrieve
     */
    public async getAppCode(appCode: string, lang:string = 'en-US'): Promise<IAppCode> {

        const key:string = `${appCode.toLowerCase()}:${lang}`.toLowerCase().replace(/\//g,"_");    // make sure the key is lower case
        let rtn:IAppCode = { code: "" , title: "" , message: "", httpCode: 200, action: 0 };    // To satisfy IAppCode interface

        // Check whether value exist in cache
        if (this.has(key)) {
            console.log(appCode, 'value provided from cache');
            rtn = this.get(key);
        }
        else {
            // Call database to get value, if not present in cache.
            try {
                // Get the record from database
                const doc: admin.firestore.DocumentData = await this.appCodes.doc(key).get();

                if (doc.exists) {                   // If record exists
                    // Transform
                    const data = doc.data();        // Extract data
                    console.log(appCode, 'value provided from db');

                    // Transform to IAppCode type
                    rtn = { code: appCode , title: data.titl , message: data.msg, httpCode: data.httpCode, action: data.actn };
                    this.set(key,rtn);              // Store in cache for future use.
                }
                else {

                    // Requested appcode is not found in Cache and Database

                    // Temporary solution until appcodes are loaded into database for the first time
                    // Container.get("ErrorCodes") are loaded from .json files.  This is old solution
                    const errorCodes:any = Container.get("ErrorCodes");
                    const successCodes:any = Container.get("SuccessCodes");
                    rtn = successCodes[appCode] || errorCodes[appCode];

                    // FINAL SOLUTION
                    // rtn = { "code": "E0001" , 
                    //         "title":"Error Code Exception", 
                    //         "message":"App code is not available in master list OR App codes are not loaded. (Missing App Code:" + appCode + ")", 
                    //         "httpCode":500, "action":1 };
                            
                    // return Promise.reject(false);   // If no record exists
                }

            } catch (error) {
                return Promise.reject(error);
            }
        }

        return Promise.resolve(rtn);
    }

    /**
     * Returns app configuration value from database if not available in cache
     * @param key - name of the key to retrieve
     */
    public async getAppConf(pageRoute: string, lang:string = 'en-US'): Promise<any> {

        const key:string = `${pageRoute.toLowerCase()}:${lang}`.toLowerCase().replace(/\//g,"_");    // make sure the key is lower case
        let rtn:IAppConf = { lng: "" , lbl: "" , data: {} };    // To satisfy IAppCode interface

        // Check whether value exist in cache
        if (this.has(key)) {
            rtn = this.get(key);
        }
        else {
            // Call database to get value, if not present in cache.
            try {
                // Get the record from database
                const doc: admin.firestore.DocumentData = await this.appConf.doc(key).get();

                if (doc.exists) {                   // If record exists
                    // Transform
                    rtn = doc.data();        // Extract data
                    this.set(key,rtn);              // Store in cache for future use.
                }
                else {
                    return Promise.reject(new Error("Requested App Config Not Available. " + key));
                }

            } catch (error) {
                return Promise.reject(error);
            }
        }

        return Promise.resolve(rtn);
    }

    /**
     * Gets email template from database to store in cache
     * @param templateId - name of the key to retrieve
     */
    public async getEmailTemplate(templateId: string): Promise<any> {

        // Check whether value exist in cache
        if (this.has(templateId)) return this.get(templateId);
        else {
            // Call database to get value, if not present in cache.
            try {
                
            } catch (error) {
                
            }
        }

        return "some value";
    }


    /**
     * Gets multiple keys
     * @param keys - Array of keys to retrieve
     */
    public mget(keys: string[]): any {
        return myCache.mget(keys);
    }


    /**
     * Deletes one or many keys depends on provided value (string/string[])
     * @param key - name of the key to delete
     */
    public del(key: any): any {
        return myCache.del(key);
    }


    /**
     * Get the cached value and remove the key from the cache.
     * @param key - name of the key to retrieve
     */
    public take(key: string): any {
        return myCache.take(key);
    }


    /**
     * Updates ttl for given key
     * @param key - key to update ttl
     * @param ttl - new time to live
     */
    public setTTL(key: string, ttl: number): any {
        return myCache.ttl(key, ttl);
    }

    /**
     * Return ttl for given key
     * @param key - key to retrieve ttl for
     */
    public getTTL(key: string): any {
        return myCache.getTtl(key);
    }


    /**
     * Returns list of keys
     */
    public keys(): any {
        return myCache.keys();
    }


    /**
     * Returns boolean indicating if the key is cached.
     * @param key - key to check
     */
    public has(key: string): any {
        return myCache.has(key);
    }



    /**
     * Returns the statistics.
     */
    public stats(): any {
        return myCache.getStats();
    }


    /**
     * Flush all data.
     */
    public flushAll(): any {
        return myCache.flushAll();
    }


    /**
     * Flush all data.
     */
    public flushStats(): any {
        return myCache.flushStats();
    }

}