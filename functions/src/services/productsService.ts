import 'reflect-metadata';
import {Container, Service} from 'typedi';
// import config from "../config"
import { ICRUDServiceInterface, IWhereQuery} from "../interfaces/IAppInterfaces";
// import {IProducts, INewProducts, IUpdateProducts} from '../interfaces/IProductsInterface';
import ProductsRepository from '../repositories/productsRepository'
import ErrorService from "./errorService";
// const { Parser } = require('json2csv');
import { IProducts } from '../interfaces';
import middleware from "../middleware";

// import firebase from "firebase/app";
// import "firebase/auth";



@Service("products_service")
export default class ProductsService implements ICRUDServiceInterface {
    
    constructor(
        private errorService:ErrorService = Container.get(ErrorService)
    ){
        this.errorService = Container.get(ErrorService);
    }

    /**
      * Handles Products preload operations
    */
    public async preload(userProfile:any, whereQuery:IWhereQuery): Promise<any> {

        try {
            // Retrieve Products records for preload
            let productsInfo:IProducts[] = await this.readMany(whereQuery);     // Get the records of people who are lower in hierarchy.

            const result = {
                productsData: productsInfo
            };

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    /**
	 * Helps to validate record.  This method is used as schema keyword validate function.
	 * @param id
	 */
	public async idExists(id:string): Promise<any>{
		let result = await this.readOne(id);
		// if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
		return Promise.resolve((result.length != 0));
	}


    /**
    * Handles Products single read operations
    */
    public async readOne(id:string): Promise<any> {
        try {
            let result:any;

            if(!id) return Promise.resolve([]);

            result = await ProductsRepository.read(id);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Products where query operations
    */
     public async readMany(where:IWhereQuery): Promise<any> {
        try {
            let result:any;

            const whereQuery:IWhereQuery = where;
            result = await ProductsRepository.readWhere(whereQuery);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Products Create operations
    * @param data
    */
    public async create(data: any): Promise<any> {
        try {

            const result = await ProductsRepository.create(data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    /**
    * Handles Products Update operations
    * @param id
    * @param data
    */
    public async update(id: string, data: any): Promise<any> {
        try {


            const result = await ProductsRepository.update(id, data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Products Delete operations
    * @param id
    */
    public async delete(id: string): Promise<any> {
        try {
            const result = await ProductsRepository.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Products Download operations
    * @param id
    */
    public async download(query: any): Promise<any> {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data

            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Products Upload operations
    * @param id
    */
    public async upload(req: any): Promise<any> {

        try {
            return middleware.validateRequest("products", {
                method:"UPLOAD",
                body:req.body,
                params:{}
            },{}, async (err:any) => {

                if(err){
                    return Promise.reject(this.errorService.build(err,null));
                }
                else{
                    await ProductsRepository.bulkInsert(req.body.bulkData)
                    return Promise.resolve(true);
                }

            })
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }

    }



    /**
    * Returns Products data in the form of code-name for the purpose of Autocomplete controls on UI.
    * @param whereQuery provides search criteria
    * @returns [ {code:"", name:""} ]
    */
    public async getProductsIdNameReadMany(req:any): Promise<any> {

        try {
            // Generally whereQuery is enhanced by entitlement process.
            // In a firestore compound query, range (<, <=, >, >=) and not equals (!=, not-in) comparisons must all filter on the same field.
            // Due to this limitation, conditions added by entitlement process are ignored.
            let whereQuery: IWhereQuery = req.body.autoFilterQuery;

            // Re-arrange the user provided wherequery for security reasons
            whereQuery.where[0][0] = "name";
            whereQuery.where[1][0] = "name";

            let data:Array<any> = await ProductsRepository.readWhere(whereQuery);
            data = data.map( item => { 
                return { productId:item.id, productName:item.name }
            });

            return Promise.resolve(data);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


}