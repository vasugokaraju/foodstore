import 'reflect-metadata';
import { Container, Service } from 'typedi';
import ErrorService from "./errorService";
import Helper from '../helpers/helper';
import config from '../config';
import Admin from 'firebase-admin';
import { File, GetSignedUrlConfig } from '@google-cloud/storage';
// import firebase from 'firebase/app';

@Service("storage_service")
export default class StorageService {
    constructor(
        private errorService: ErrorService = Container.get(ErrorService),
        private helper: Helper = Container.get(Helper),
        private admin: Admin.app.App = Container.get('admin')
    ) {
    }


    /**
     * Expects file upload
     * Saves to google cloud storage and returns file url
     * 
     * @param req - express request object
     * @param res - express response object
     */

    public async save(req: any): Promise<any> {
        try {

            // PREPARE FILE FOR STORAGE NAME
            const bucketName: string = config.storage_bucket; // + "/" + req.body.file_category;
            const folderName: string = req.body.folder_path || "";
            const monthName: string = this.helper.monthNames[new Date().getMonth() - 1];
            let fileName: string = '';


            let promises: Array<Promise<any>> = [];
            //const file: any = req.files[0];

            promises = req.files.map(async (file: any) => {
                const dt: Date = new Date();
                const fileNamePrefix: string = dt.getFullYear() + "_" + monthName;
                const time: string = dt.getTime().toString();

                // Build file name
                if (file.originalname.indexOf(".") != -1) {   //If . exits in original file name
                    fileName = `${req.body.employee_id}--`
                    fileName += file.originalname.toString().replace(/.([^.]*)$/, "--" + `${req.body.timesheet_entry_id}--` + time + ".$1");
                }
                else {
                    fileName += "_" + time;
                }
                fileName = fileNamePrefix + "--" + fileName

                const fileBuffer: Buffer = file.buffer;

                // SAVE FILE IN BUCKET
                const newFile: File = this.admin.storage().bucket(bucketName).file(folderName + "/" + fileName);

                return newFile.save(fileBuffer, { contentType: file.mimetype }).then(
                    resp => {
                        // RETURN FILE URL. FILE CANNOT BE ACCESSED DIRECTLY
                        console.log('newFile.publicUrl()', newFile.publicUrl());
                        
                        return Promise.resolve(newFile.publicUrl().split(bucketName)[1]);
                    },
                    err => Promise.reject(err)
                );
            });

            // Execute all promises (file uploads).  .save() returns void.
            const publicURLs = await Promise.all(promises);
            return Promise.resolve({ fileURLs: publicURLs });

        } catch (e) {
            return Promise.reject(this.errorService.build("E1021", e));
        }
    }

    /**
     * Expects file url to delete
     * 
     * @param fileURL
     */
    public async delete(fileURL: any): Promise<any> {

        try {
            // PREPARE FOR FILE DELETION
            const bucketName: string = config.storage_bucket;
            fileURL = fileURL.substr(1);
            const deleteFile: File = this.admin.storage().bucket(bucketName).file(fileURL);
            await deleteFile.delete();
            Promise.resolve(true);
        } catch (e) {
            return Promise.reject(this.errorService.build("E1020", fileURL));
        }
    }


    /**
     * Read file from storage
     */
    public async getDownloadURL(filePath: any): Promise<any> {
        try {
            // PREPARE FILE FOR STORAGE NAME
            const bucketName: string = config.storage_bucket;

            // READ FILE FROM BUCKET
            filePath = filePath.substr(1);    // Remove first forward slash (/)
            const file: File = this.admin.storage().bucket(bucketName).file(filePath);
            const metadata: any = await file.getMetadata();

            let dt: Date = new Date();
            dt.setSeconds(dt.getSeconds() + 15);    // Make the link expire in n seconds.

            // let buf:any = '';
            const config2: GetSignedUrlConfig = { action: "read", expires: dt };
            const dwonloadURL: any = await file.getSignedUrl(config2);

            let fileInfo: any = {
                url: dwonloadURL[0],
                fileName: metadata[0].name.replace(/\//g, '--'),
                metadata: {
                    'contentType': metadata[0].contentType,
                    'contentLength': metadata[0].size,
                    'eTag': metadata[0].etag
                }

            }
            return fileInfo;

        } catch (error) {
            console.log('getDownloadURL error', error);

            return Promise.reject(this.errorService.build("E1022", error));
        }

    }


    /**
     *
     * @param filePath - contains google cloud storage file path
     * @returns file buffer and file metadata
     */
    public async getDownloadBuffer(filePath: any): Promise<any> {
        try {
            // PREPARE FILE FOR STORAGE NAME
            const bucketName: string = config.storage_bucket;

            // READ FILE FROM BUCKET
            filePath = filePath.substr(1);    // Remove first forward slash (/)
            const file: File = this.admin.storage().bucket(bucketName).file(filePath);
            const metadata: any = await file.getMetadata();

            return file.download().then((content) => {
                let fileInfo: any = {
                    buffer: content[0],
                    fileName: metadata[0].name.replace(/\//g, '--'),
                    metadata: {
                        'contentType': metadata[0].contentType,
                        'contentLength': metadata[0].size,
                        'eTag': metadata[0].etag
                    }

                }

                return Promise.resolve(fileInfo);
            });

        } catch (error) {
            console.log('getDownloadBuffer error', error);
            return Promise.reject(this.errorService.build("E1023", error));
        }
    }

}