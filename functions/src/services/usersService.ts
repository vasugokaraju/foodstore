import 'reflect-metadata';
import {Container, Service} from 'typedi';
// import config from "../config"
import { ICRUDServiceInterface, IWhereQuery} from "../interfaces/IAppInterfaces";
// import {IUsers, INewUsers, IUpdateUsers} from '../interfaces/IUsersInterface';
import UsersRepository from '../repositories/usersRepository'
import ErrorService from "./errorService";
// const { Parser } = require('json2csv');
import { ROLES } from '../loaders/enums';
import { IUsers } from '../interfaces';
import middleware from "../middleware";

// import firebase from "firebase/app";
// import "firebase/auth";



@Service("users_service")
export default class UsersService implements ICRUDServiceInterface {
    
    constructor(
        private errorService:ErrorService = Container.get(ErrorService)
    ){
        this.errorService = Container.get(ErrorService);
    }

    /**
      * Handles Users preload operations
    */
    public async preload(userProfile:any, whereQuery:IWhereQuery): Promise<any> {

        try {
            let result: any = {};

            // If the user role is of type USER, limit the data to his/her record
            if (userProfile.role.code == ROLES.USER) {
                const requesterInfo:IUsers = await this.readOne(userProfile.id); // Get the record of the requester (logged in persion);
                result.usersData = requesterInfo;
            }

            // for the users of higher role, there might be multiple roles as they access to users of lower roles
            // The entitlement middleware will enforce where criteria on all requests
            else {  
                let usersInfo:IUsers[] = await this.readMany(whereQuery);     // Get the records of people who are lower in hierarchy.
                result.usersData = usersInfo;
            }

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    /**
	 * Helps to validate record.  This method is used as schema keyword validate function.
	 * @param id
	 */
	public async idExists(id:string): Promise<any>{
		let result = await this.readOne(id);
		// if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
		return Promise.resolve((result.length != 0));
	}


    /**
    * Handles Users single read operations
    */
    public async readOne(id:string): Promise<any> {
        try {
            let result:any;

            if(!id) return Promise.resolve([]);

            result = await UsersRepository.read(id);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Users where query operations
    */
     public async readMany(where:IWhereQuery): Promise<any> {
        try {
            let result:any;

            const whereQuery:IWhereQuery = where;
            result = await UsersRepository.readWhere(whereQuery);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Users Create operations
    * @param data
    */
    public async create(newProfile: any, id:string="", skipAuthentication:boolean = false, lang:string = "en-US"): Promise<any> {
        try {
            const result = await UsersRepository.create(newProfile, id, skipAuthentication, lang);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    /**
    * Handles Users Update operations
    * @param currentProfile
    * @param newProfile
    */
    public async update(currentProfile: any, newProfile: any): Promise<any> {
        try {
            // delete password field from newProfile
            delete newProfile.password;
            const result = await UsersRepository.update(currentProfile, newProfile);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Users Delete operations
    * @param id
    */
    public async delete(id: string): Promise<any> {
        try {
            const result = await UsersRepository.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Users Download operations
    * @param id
    */
    public async download(query: any): Promise<any> {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data

            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Users Upload operations
    * @param id
    */
    public async upload(req: any): Promise<any> {

        try {
            return middleware.validateRequest("users", {
                method:"UPLOAD",
                body:req.body,
                params:{}
            },{}, async (err:any) => {

                if(err){
                    return Promise.reject(this.errorService.build(err,null));
                }
                else{
                    await UsersRepository.bulkInsert(req.body.bulkData)
                    return Promise.resolve(true);
                }

            })
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }

    }


    /**
     * Checks whether the given phone number is registered.
     * Initially developed for phone authentication purpose
     */
     public async isPhoneNumberRegistered(phoneNumber: string): Promise<any> {
        try {
            return UsersRepository.isPhoneNumberRegistered(phoneNumber)
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Returns Users data in the form of code-name for the purpose of Autocomplete controls on UI.
    * @param whereQuery provides search criteria
    * @returns [ {code:"", name:""} ]
    */
    public async getUsersIdDisplayNameReadMany(req:any): Promise<any> {

        try {
            // Generally whereQuery is enhanced by entitlement process.
            // In a firestore compound query, range (<, <=, >, >=) and not equals (!=, not-in) comparisons must all filter on the same field.
            // Due to this limitation, conditions added by entitlement process are ignored.
            let whereQuery: IWhereQuery = req.body.autoFilterQuery;

            // Re-arrange the user provided wherequery for security reasons
            whereQuery.where[0][0] = "displayName";
            whereQuery.where[1][0] = "displayName";

            let data:Array<any> = await UsersRepository.readWhere(whereQuery);
            data = data.map( item => { 
                return { customerId:item.id, customerName:item.displayName }
            });

            return Promise.resolve(data);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


}