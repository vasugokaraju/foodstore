import 'reflect-metadata';
import { Container, Service } from 'typedi';
// import { IFindQuery } from "../interfaces/IAppInterfaces";
import { Request, Response } from 'express';
import ErrorService from "./errorService";
// import UsersService from "./usersService";
import Helper from '../helpers/helper';
import jwt, { TokenExpiredError } from 'jsonwebtoken'
import config from '../config';
import FirebaseError from '../classes/firebaseError';
import Admin, { firestore } from 'firebase-admin';
import firebase from 'firebase/app';
import EmailService from './emailService';
import { EMAIL_TEMPLATES, ROLES } from '../loaders/enums';
import UsersService from './usersService';
import CacheService from './cacheService';
import { IRole } from '../interfaces';
const jp = require('jsonpath');

// const rp = require('request-promise')

@Service("auth_service")
export default class AuthService {
    constructor(
        private errorService: ErrorService = Container.get(ErrorService),
        // private usersService: UsersService = Container.get(UsersService),
        private helper: Helper = Container.get(Helper),
        private admin: Admin.app.App = Container.get('admin'),
        private emailService: EmailService = Container.get(EmailService),
        private userService: UsersService = Container.get(UsersService),
        private cacheService: CacheService = Container.get(CacheService)
    ) {
    }

    /**
     * Expects user id and password
     * Delets old tokens if any
     * 
     * @param req - express request object
     * @param res - express response object
     */

    public async login(req: Request, res: Response): Promise<any> {
        try {

            // Extract login and password from headers
            const [login, password] = this.helper.decodeBase64(req.headers.authorization!).split(":");
            const hashedPassword = await this.helper.hash(password);    // Hash the password as the stored password is hashed.

            let userInfo: any;
            const userCredentials = await firebase.auth().signInWithEmailAndPassword(login.toLowerCase(), hashedPassword);
            if (userCredentials.user) {

                let user = userCredentials.user

                // Check whether this email is verified.
                if (!user.emailVerified) return Promise.reject(this.errorService.build("E1036", null));     // This email is not verified yet.
                
                userInfo = {
                    displayName: user.displayName,
                    email: user.email,
                    emailVerified: user.emailVerified,
                    phoneNumber: user.phoneNumber,
                    photoURL: user.photoURL,
                    id: user.uid,
                    token: await user.getIdToken(true),
                    refreshToken: user.refreshToken,
                    providerData: user.providerData
                }

                await this.tokenHouseKeeping(req, res, userInfo);

                // Set providerId as cookie to learn which authentication method is used to login
                // https://stackoverflow.com/questions/39291878/firebase-auth-get-provider-id
                // First entry in the providerData[] is the last used provider
                this.helper.setCookie(res, 'providerId', userInfo.providerData[0].providerId);
                this.helper.setHeader(res, 'providerId', userInfo.providerData[0].providerId);
                userInfo.providerId = userInfo.providerData[0].providerId;

                // Get user profile
                const userProfile = await this.admin.firestore().collection('users').doc(user.uid).get();
                userInfo = Object.assign({}, userProfile.data(), userInfo);
            }

            // Get allowed roles based on user's role
            const roles = this.helper.getRoles(parseFloat(userInfo.role.code));

            // Get user menu data
            const userMainMenu = await this.getMenuByRole("mainmenu",userInfo.role,config.defaultLanguage);

            return Promise.resolve({userInfo: userInfo, roles:roles, mainMenu:userMainMenu});

        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }

    /**
     * Sends email to verify given email.  The target emails must be registered.  This is generally called during registration process.
     * @param req 
     * @param res 
     */
     public async sendEmailVerificationEmail(email: string, lang: string = config.defaultLanguage): Promise<any> {
        try {

            // Get user profile to make sure the user is already registered, before sending confirmation email.
            const userAuthInfo: any = await this.admin.auth().getUserByEmail(email);
            
            if (userAuthInfo.emailVerified) return Promise.reject(this.errorService.build("E1035", null));       // This email has already been confirmed.

            // --------------SEND EMAIL FOR REGISTRATION CONFIRMATION-------------
            const str = `${userAuthInfo.uid}~~${userAuthInfo.email}`;
            const token = this.helper.encrypt(str);
            const emailData = {
                name: userAuthInfo.displayName,
                confirm_link: `${config.ui_host}/auth/verifyRegisteredEmail?token=${token}`
            }

            await this.emailService.send(lang, [userAuthInfo.email], EMAIL_TEMPLATES.EMAIL_VERIFICATION, emailData);
            // ------------------------------------------------------------------

            return Promise.resolve({status:true});
        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }



    /**
     * Validates email registration
     * @param req 
     * @param res 
     */
    public async verifyRegisteredEmail(token: string): Promise<any> {
        try {
            const decryptedText: string = this.helper.decrypt(token);
            const userInfo = decryptedText.split('~~');
            const uid: string = userInfo[0];
            const email: string = userInfo[1];

            const userAuthInfo:any = await this.admin.auth().getUser(uid);
            
            if(userAuthInfo.email !== email) return Promise.reject(this.errorService.build("E1034", null));     // Invalid User ID and Email combination.
            
            if(userAuthInfo.emailVerified) return Promise.reject(this.errorService.build("E1035", null));       // This email has already been confirmed.

            // Update emailVerified status to true.
            await this.admin.auth().updateUser(uid, {emailVerified:true});
            const secret :string = this.helper.encrypt(uid);    // this secret is used to verify the user during password creation process
            return Promise.resolve({status:true, email:userAuthInfo.email, secret:  secret});

        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }


    /**
     * Sends email to verify given email.  The target emails must be registered
     * @param req 
     * @param res 
     */
     public async sendEmailOTPLink(email: string, lang:string = config.defaultLanguage): Promise<any> {
        try {

            // Get user profile to make sure the user is already registered, before sending email otp link
            const userAuthInfo:any = await this.admin.auth().getUserByEmail(email);

            // https://firebase.google.com/docs/auth/admin/email-action-links
            // https://firebase.google.com/docs/auth/web/email-link-auth
            const emailOTPLink =  await this.admin.auth().generateSignInWithEmailLink(email, {url: `${config.ui_host}/auth/verifyEmailOTP`});

            // --------------SEND EMAIL FOR EMAIL OTP LOGIN-------------
            const emailData = {
                name: userAuthInfo.displayName,
                confirm_link: emailOTPLink
            }

            const emailConfirmation = await this.emailService.send(lang, [userAuthInfo.email], EMAIL_TEMPLATES.EMAIL_OTP_LOGIN, emailData);
            // ------------------------------------------------------------------

            return Promise.resolve({status:emailConfirmation});
        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }

    /**
     * Verifies email otp link
     * @param req 
     * @param res 
     */
    public async verifyEmailOTPLink(email: string, queryParams: any, res: Response, lang: string = config.defaultLanguage): Promise<any> {

        try {
            const urlParams = `?mode=${queryParams.mode}&lang=${queryParams.lang}&oobCode=${queryParams.oobCode}&apiKey=${queryParams.apiKey}`
            const preCheck = firebase.auth().isSignInWithEmailLink(urlParams);

            if (!preCheck) {
                return Promise.reject(this.errorService.build("E1037", null));      // Invalid email link login attempt.
            }

            const userProfile = await firebase.auth().signInWithEmailLink(email, urlParams)

            // ------------------ TOKEN HOUSEKEEPING ------------------
            // This is to satisfy tokenhousekeeping
            const _req: any = {
                useragent: {
                    email_otp: true
                }
            }

            const token: any = await userProfile.user?.getIdToken();
            let userInfo = await this.admin.auth().verifyIdToken(token);
            userInfo.id = userInfo.uid;
            userInfo.token = token;

            userInfo = await this.tokenHouseKeeping(_req, res, userInfo);
            // ------------------------------------------------------

            
            // Get allowed roles based on user's role
            let roles: Array<any> = [];
            if (userInfo.role.code) {
                roles = this.helper.getRoles(parseFloat(userInfo.role.code));
            }

            userInfo.providerId = "email_otp";

            return Promise.resolve({ userInfo: userInfo, roles: roles });

        } catch (error) {
            const fireError = this.helper.getFirebaseError(error);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(error, null));
            }
        }

    }

    
    /**
     * Sends email to initiate password reset process
     * @param req 
     * @param res 
     */
     public async sendForgotPasswordLink(email: string, lang:string = config.defaultLanguage): Promise<any> {
        try {

            // Get user profile to make sure the user is already registered, before sending email otp link
            const userAuthInfo: any = await this.admin.auth().getUserByEmail(email);

            let forgotPasswordLink = await this.admin.auth().generatePasswordResetLink(email);
            forgotPasswordLink = `${config.ui_host}/auth/forgotPassword?` + forgotPasswordLink.split("?")[1];

            // --------------SEND EMAIL FOR FORGOT PASSWORD-------------
            const emailData = {
                name: userAuthInfo.displayName,
                confirm_link: forgotPasswordLink
            }

            const emailConfirmation = await this.emailService.send(lang, [userAuthInfo.email], EMAIL_TEMPLATES.FORGOT_PASSWORD, emailData);
            // ------------------------------------------------------------------

            return Promise.resolve({status:emailConfirmation});
        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }



    /**
     * Verifies forgot password link
     * @param req 
     * @param res 
     */
    public async verifyForgotPasswordLink(email: string, queryParams: any, res: Response, lang: string = config.defaultLanguage): Promise<any> {

        try {
            // const urlParams = `?mode=${queryParams.mode}&lang=${queryParams.lang}&oobCode=${queryParams.oobCode}&apiKey=${queryParams.apiKey}`

            // const preCheck = firebase.auth().isSignInWithEmailLink(urlParams);

            // if (!preCheck) {
            //     return Promise.reject(this.errorService.build("E1037", null));      // Invalid email link login attempt.
            // }

            const userEmail = await firebase.auth().verifyPasswordResetCode(queryParams.oobCode);

            return Promise.resolve({ userEmail: userEmail })

        } catch (error) {
            const fireError = this.helper.getFirebaseError(error);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(error, null));
            }
        }

    }

    /**
     * Creates new password.  Usually called during forgot password process.
     * @param req
     * @param res
     */
    public async resetPassword(req: Request, res: Response) {

        try{
            const hashedNewPassword = await this.helper.hash(req.body.newPassword);
            await firebase.auth().confirmPasswordReset(req.body.oobCode, hashedNewPassword);
    
            // Login user with new credentials
            req.headers.authorization = this.helper.encodeBase64(`${req.body.email}:${req.body.newPassword}`);
            const userInfo = await this.login(req, res);
    
            return Promise.resolve(userInfo);
    
        } catch (error) {
            const fireError = this.helper.getFirebaseError(error);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(error, null));
            }
        }

    }

    /**
     * Login using credentials generated by federated login on client
     * @param req
     * @param res
     */
    public async federatedLogin(req: Request, res: Response) {

        try {

            // Build Firebase credential with the Google ID token.
            // type = firebase.auth.OAuthCredential
            let credential: any;
            const userAgent:string = req.body.credential.providerId.toLowerCase();

            switch (userAgent) {
                case "google.com":
                    credential = firebase.auth.GoogleAuthProvider.credential(req.body.credential.oauthIdToken);
                    break;
                case "facebook.com":
                    credential = firebase.auth.FacebookAuthProvider.credential(req.body.credential.oauthAccessToken);
                    break;
            }

            let userInfo = Object.assign({}, req.body.user);
            if (!userInfo.role) userInfo.role = { "code": ROLES.USER, "name": ROLES[ROLES.USER] };

            // Sign in with credential from the Google user.  
            // This command creates user in Authentication collection, but not in 'users' collection
            const federatedUserInfo = await firebase.auth().signInWithCredential(credential);
            userInfo.id = federatedUserInfo.user?.uid;
            userInfo.providerId = credential.providerId;

            // Check whether user already exists
            const firstTimeUser = await this.userService.readOne(userInfo.id)

            // If user does not exist, create a recrod in 'Users' collection.  
            if (firstTimeUser.length == 0) {

                try {

                    let sampleRec:any = {
 "displayName": "Horacio",
 "email": "Craig.McLaughlin84@gmail.com",
 "password": "ztFeAYzDZnRDT6O",
 "phoneNumber": "+16460175934",
 "emailVerified": true,
 "disabled": false,
 "photoURL": "http://myphotosdfsf.com",
 "role": {
  "code": 0,
  "name": "SUPERADMIN"
 },
 "dob": "2020-09-14T12:07:00.123Z",
 "ssn": 384156528,
 "address": {
  "city": {
   "colony": "Calistashire",
   "foundedon": "2020-12-24T12:57:25.297Z"
  },
  "country": "Armenia"
 }
};
                    delete sampleRec.phoneNumber;

                    let userAuthData: any = {
                        displayName: userInfo.displayName,
                        photoURL: userInfo.photoURL,
                        email: userInfo.email,
                        role: userInfo.role
                    };

                    if (userInfo.phoneNumber) userAuthData.phoneNumber = userInfo.phoneNumber;

                    // Create new record in users collection with given id. 
                    // Skip creating Authentication as it is already created by firebase.auth().signInWithCredential
                    const _userAuthData = JSON.stringify(userAuthData);
                    await this.userService.create(Object.assign({}, sampleRec, JSON.parse( _userAuthData  ) ), userInfo.id, true);

                    // Update user information in Authentication
                    userAuthData.id = userInfo.id;
                    await this.userService.update({}, userAuthData);

                } catch (error) {
                    const fireError = this.helper.getFirebaseError(error);
                    if (fireError instanceof FirebaseError) {
                        return Promise.reject(this.errorService.build(fireError, null));
                    }
                    else {
                        return Promise.reject(this.errorService.build(error, null));
                    }
                }

                // Refresh token again so that newly added roles are included in the token
                const _user = firebase.auth().currentUser
                userInfo.token = await _user?.getIdToken(true); // get new token
            }
            else{
                // TODO:  For some reason, if user account is created with google email and password 
                // and later signed in with google federated login, the displayName and Photo URL are replaced with blank
                // Not sure whether the issue is with firebase emulater locally.
                // If this happens in production, user the information available in 'req.body.user' and put the displayName and photoURL back in firebase authentication collection.
            }

            // ------------------ TOKEN HOUSEKEEPING ------------------
            // This is to satisfy tokenhousekeeping
            const _req: any = { useragent: {} }
            _req.useragent[userAgent] = true;

            userInfo = await this.tokenHouseKeeping(_req, res, userInfo);
            // ------------------------------------------------------

            // Get allowed roles based on user's role
            let roles: Array<any> = [];
            if (userInfo.role.code) {
                roles = this.helper.getRoles(parseFloat(userInfo.role.code));
            }

            // Get user menu data
            const userMainMenu = await this.getMenuByRole("mainmenu",userInfo.role,config.defaultLanguage);

            return Promise.resolve({ userInfo: userInfo, roles: roles, mainMenu:userMainMenu });

        } catch (error) {
            const fireError = this.helper.getFirebaseError(error);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else {
                return Promise.reject(this.errorService.build(error, null));
            }
        }

    }



    /**
     * Expects Firebase idToken from Authentication header and tknuid from cookies.
     * Delets the token from active tokens collection of the given user to prevent using this token to access data.
     * 
     * @param req - express request object
     * @param res - express response object
     */

    public async logout(req: Request, res: Response): Promise<any> {
        try {
            // Get the idToken provided by client
            const idToken = await this.helper.getToken(req);            

            const userProfile = await this.admin.auth().verifyIdToken(idToken); // build user profile from token

            // Remove current token and old tokens (if any) from tokens collection to prevent unauthorized usage.
            const token_uid = this.helper.getCookie('tknuid', req);
            if(userProfile && !!token_uid){
                await this.admin.firestore().collection('users').doc(userProfile.id).collection('tokens').doc(token_uid).delete();
            }

            await firebase.auth().signOut();

            return Promise.resolve(true);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    // Creates new password.  Usually called when user email is confirmed after registration
    public async createPassword(req: Request, res: Response): Promise<any> {
        try {

            // Find the user by email
            const uid = this.helper.decrypt(req.body.secret);
            const email = req.body.email
            const hashedNewPassword = await this.helper.hash(req.body.newPassword);
            const userProfile: Admin.auth.UserRecord = await this.admin.auth().getUserByEmail(email);

            // Check whether the email is verified
            if(userProfile.email !== email || !userProfile.emailVerified || userProfile.uid !== uid){
                return Promise.reject(this.errorService.build("E1039", null));        // Invalid attempt to create password
            }

            if (userProfile) {
                // Set new password
                await this.admin.auth().updateUser(uid, {password:hashedNewPassword});

                // Login user with new credentials
                req.headers.authorization = this.helper.encodeBase64(`${email}:${req.body.newPassword}`);
                const userInfo = await this.login(req,res);

                return Promise.resolve(userInfo);
            }
            else{
                return Promise.reject(this.errorService.build("E1038", null));
            }
        }
        catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if(fireError instanceof FirebaseError){
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else{
                return Promise.reject(this.errorService.build(e,null));
            }            
        }
    }


    public async changePassword(req: Request, res: Response): Promise<any> {
        try {
            // Extract login and password from headers
            const login = req.body.login;
            const hashedPassword = await this.helper.hash(req.body.currentPassword);    // Hash the password as the stored password is hashed.
            const hashedNewPassword = await this.helper.hash(req.body.newPassword);

            // Check whether the current credentials are valid
            const userCredentials = await firebase.auth().signInWithEmailAndPassword(login.toLowerCase(), hashedPassword);
            if (userCredentials.user) {
                
                // Update password.
                const user = firebase.auth().currentUser;
                await user?.updatePassword(hashedNewPassword);
                const token = await user?.getIdToken(true);

                return Promise.resolve({token:token});
            }
            else{
                return Promise.reject(this.errorService.build("E1019", null));
            }
        }
        catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if(fireError instanceof FirebaseError){
                return Promise.reject(this.errorService.build(fireError, null));
            }
            else{
                return Promise.reject(this.errorService.build(e,null));
            }            
        }
    }

    // Encode a string with base64
    public async encode(req: Request): Promise<any> {
        try {
            return Promise.resolve(this.helper.encodeBase64(req.params.value));
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, req.params.value));
        }
    }

    // Hash a string
    public async hash(value: any): Promise<any> {
        try {
           const hsh:string = await this.helper.hash(value);
            return Promise.resolve(hsh);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, value));
        }
    }

    public async decode(req: Request): Promise<any> {
        try {
            return Promise.resolve(this.helper.decodeBase64(req.params.value));
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, req.params.value));
        }
    }

    /**
     * Generates token.  
     * 
     * It is noticed that the issuedAt (iat) time is 2-3 seconds lesser than actual time
     * thus causing the token life less than n seconds.
     * 
     * @param payload 
     * @param secretOrPublicKey 
     * @param options 
     */
    public async getToken(payload: any, secretOrPublicKey: string, options: any = {}): Promise<any> {
        options = Object.assign({expiresIn:config.tokenLife}, options)
        try {
            const token = jwt.sign({ data: payload }, secretOrPublicKey, options);
            return Promise.resolve(token);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    // Verifies token
    public async verifyToken(token: string): Promise<any> {
        try {
            const decoded:any = jwt.verify(token, config.secret);
            return Promise.resolve(decoded);
        }
        catch (e) {
            let errCode = (e instanceof TokenExpiredError )? "E1001":"E1018";

            return Promise.reject(this.errorService.build(errCode, e));
        }
    }

    // Renews token
    public async renewToken(req: Request, res: Response): Promise<any> {
        try {
            const newToken = await firebase.auth().currentUser?.getIdToken(true);

            if(newToken === undefined) return Promise.resolve("");

            const userInfo = await this.tokenHouseKeeping(req, res, {id:req.userProfile.id, token:newToken});

            return Promise.resolve(userInfo);
        }
        catch (e) {
            if (e instanceof TokenExpiredError) {
                return Promise.reject(this.errorService.build("E1001", e));
            }
            else {
                return Promise.reject(this.errorService.build(e, null));
            }
        }
    }

 
    private async tokenHouseKeeping(req: Request, res: Response, userInfo: any,) {

        // Find out the device type to check whether a token is already issued.
        const deviceType = this.helper.getUserAgent(req);

        // ************* DELETE TOKENS THAT ARE OLDER THAN ONE HOUR, JUST IN CASE IF THERE ARE ANY LEFTOVERS
        let tokenExpirationThreshold: number = this.helper.getUTC() - 3600000;   // get the timestamp one hour less than now
        const batchjob = this.admin.firestore().batch();

        const expiredTokens: firestore.QuerySnapshot = await this.admin.firestore().collection('users').doc(userInfo.id).collection('tokens').where("issuedAt", "<", tokenExpirationThreshold).get();

        // Delete all existing tokens issed for the given type of device.  A new one will be issues upon successful login.
        if (expiredTokens.size > 0) expiredTokens.forEach(rec => batchjob.delete(rec.ref));

        // ************* CONTROL THE NUMBER OF LOGINS
        const allowSimultaneousLogin = false;   // Read this value from app config
        if (!allowSimultaneousLogin) {

            // THIS LOGIC ALLOWS ONE LOGIN PER DEVICE (web,android,ios)
            // Check whether the user logged in from same type of device already.
            if (Object.keys(deviceType).length > 0) {
                const deviceIdentifier: string = Object.keys(deviceType)[0];    // type of the device
                // Get all tokens generated for this type of device.  There should not be more than one.
                const existingTokens: firestore.QuerySnapshot = await this.admin.firestore().collection('users').doc(userInfo.id).collection('tokens').where(deviceIdentifier, "==", true).get();

                // Delete all existing tokens issed for the given type of device.  A new one will be issues upon successful login.
                if (existingTokens.size > 0) existingTokens.forEach(rec => batchjob.delete(rec.ref));
            }
        }

        await batchjob.commit();

        // Store idtoken in subcollection for future verification
        let tokenRec: any = { idToken: userInfo.token, issuedAt: this.helper.getUTC() }
        // Store the device type as part of cookie so that we can limit the number of logins from the same type of device.
        tokenRec = Object.assign({}, tokenRec, deviceType);

        const idTokenRef: firestore.DocumentData = await this.admin.firestore().collection('users').doc(userInfo.id).collection('tokens').add(tokenRec);

        // Set token uid as cookie to quickly retrieve the record for verification in validateToken middleware.
        this.helper.setCookie(res, 'tknuid', idTokenRef.id);
        this.helper.setHeader(res, 'tknuid', idTokenRef.id);
        userInfo.tknuid = idTokenRef.id;

        return Promise.resolve(userInfo);
    }   

    // Returns firebase app config
    // This is to help initialize firebase app on client.
    public getFirebaseAppConfig(){
        return Container.get("firebaseAppConfig");
    }

/*
    public signInWithPhone(data: any) {

        // Call reCAPTCHA to verify the response token provided by the client

        rp({
            uri: "https://www.google.com/recaptcha/api/siteverify",
            method: "POST",
            formData: {
                secret: "6Ldo_NYaAAAAAAfgfpk0kk9ItlQ0-UaLtSb5WLGU",
                response: data.recaptcha_token
            },
            json: true
        })
            .then((result: any) => {
                if (result.success) {
                    return true; 
                }
                else {
                    return Promise.reject(false)
                }

            })
            .catch((error: any) => {
                // Send error message back to client
                return Promise.reject(this.errorService.build("E1027", error));
            })
    }
*/

    // This method is called when user successfully logged in using his/her phone number on the client
    // The intent is to store the token ID under respective users's tokens collection for token management
    public async onSignInWithPhoneNumber(req: Request, res: Response): Promise<any> {
        try {
            let token = await this.helper.getToken(req);

            let userInfo: any = {}

            // Token verification fails if the token is generated by firebase and processed by local emulator
            try {
                userInfo = await this.admin.auth().verifyIdToken(token);
            } catch (error) {
                return Promise.reject(this.errorService.build("E1030", error));
            }

            const iat: number = parseInt(userInfo.iat);  // token issued time in seconds
            const currentTime: number = new Date().getTime() / 1000;   // convert current time to seconds
            let diffInSeconds = (currentTime - iat);

            // console.log('diffInSeconds', diffInSeconds);

            // This is to satisfy tokenhousekeeping
            const _req: any = {
                useragent: {
                    phone: true
                }
            }
            // If this token is generated in last 5 seconds and the authentication method is phone
            // Consider it as genuine request and add the token to user's tokens subcollection
            if (
                req.body.additionalUserInfo.providerId.toLowerCase() === "phone" &&
                req.body.operationType.toLowerCase() === "signin" &&
                diffInSeconds <= 100
            ) {

                userInfo.id = userInfo.uid;
                userInfo.token = token;
                userInfo = await this.tokenHouseKeeping(_req, res, userInfo);
                userInfo.providerId = req.body.additionalUserInfo.providerId.toLowerCase();

                // Get allowed roles based on user's role
                let roles: Array<any> = [];
                if (userInfo.role.code) {
                    roles = this.helper.getRoles(parseFloat(userInfo.role.code));
                }

                return Promise.resolve({ userInfo: userInfo, roles: roles });
            }
            else {
                return Promise.reject(this.errorService.build("E1029", null));
            }

        } catch (error) {
            return Promise.reject(this.errorService.build("E1029", error));
        }
    }

    // Provides role specific main menu
    public async getMenuByRole(menuname:string, role:IRole, lang:string = config.defaultLanguage): Promise<any>{
        try {

            // Get the menu from cache if available
            const key = `${menuname}:${lang}:${role.name}`.toLowerCase();
            if(this.cacheService.has(key)) return this.cacheService.get(key);

            // Get the menu from database, if not available in cache.
            const menuData:any = await this.cacheService.getAppConf(menuname, lang);
            
            // If the role is SUPERADMIN, return entire menu, if not filter out 'no access' menu items
            const userMenu = (role.code === ROLES.SUPERADMIN) ? menuData.data : this.helper.getRoleMenuItems(menuData.data, role.name);

            // Store in cache for later use
            this.cacheService.set(key, userMenu);

            return userMenu;

        } catch (error) {
            return [];
        }
    }

    // Provides role specific route access check
    public async getRouteAccess(route: string, role: string): Promise<any> {

        try {

            let allowed: boolean
            // Get the menu from cache if available
            const key = `rbac:${route}:${role.toLowerCase()}`;
            if (this.cacheService.has(key)) {
                allowed = this.cacheService.get(key);
            }
            else {
                // Get the rbac from database, if not available in cache.
                // NOTE: It is always going to be en-US for RBAC
                const rbacData: any = await this.cacheService.getAppConf('rbac', "en-US");

                // Extract the rbac items that the user has access to.
                const rbac = jp.query(rbacData, `$.data[?(@.${route})][?(@.${role.toLowerCase()}===true)]`);

                // Store in cache for later use
                allowed = (rbac.length > 0);
                this.cacheService.set(key, allowed);
            }

            if (!allowed) console.log('RBAC EXCEPTION', 'Route:', route, 'Role:', role);

            return allowed;

        } catch (error) {
            return false;
        }
    }

}