import 'reflect-metadata';
import {Container, Service} from 'typedi';
// import config from "../config"
import { ICRUDServiceInterface, IWhereQuery} from "../interfaces/IAppInterfaces";
// import {ICarts, INewCarts, IUpdateCarts} from '../interfaces/ICartsInterface';
import CartsRepository from '../repositories/cartsRepository'
import ErrorService from "./errorService";
// const { Parser } = require('json2csv');
import { ICarts } from '../interfaces';
import middleware from "../middleware";

// import firebase from "firebase/app";
// import "firebase/auth";



@Service("carts_service")
export default class CartsService implements ICRUDServiceInterface {
    
    constructor(
        private errorService:ErrorService = Container.get(ErrorService)
    ){
        this.errorService = Container.get(ErrorService);
    }

    /**
      * Handles Carts preload operations
    */
    public async preload(userProfile:any, whereQuery:IWhereQuery): Promise<any> {

        try {
            // Retrieve Carts records for preload
            let cartsInfo:ICarts[] = await this.readMany(whereQuery);     // Get the records of people who are lower in hierarchy.

            const result = {
                cartsData: cartsInfo
            };

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    /**
	 * Helps to validate record.  This method is used as schema keyword validate function.
	 * @param id
	 */
	public async idExists(id:string): Promise<any>{
		let result = await this.readOne(id);
		// if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
		return Promise.resolve((result.length != 0));
	}


    /**
    * Handles Carts single read operations
    */
    public async readOne(id:string): Promise<any> {
        try {
            let result:any;

            if(!id) return Promise.resolve([]);

            result = await CartsRepository.read(id);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Carts where query operations
    */
     public async readMany(where:IWhereQuery): Promise<any> {
        try {
            let result:any;

            const whereQuery:IWhereQuery = where;
            result = await CartsRepository.readWhere(whereQuery);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Carts Create operations
    * @param data
    */
    public async create(data: any): Promise<any> {
        try {

            const result = await CartsRepository.create(data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    /**
    * Handles Carts Update operations
    * @param id
    * @param data
    */
    public async update(id: string, data: any): Promise<any> {
        try {


            const result = await CartsRepository.update(id, data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Carts Delete operations
    * @param id
    */
    public async delete(id: string): Promise<any> {
        try {
            const result = await CartsRepository.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Carts Download operations
    * @param id
    */
    public async download(query: any): Promise<any> {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data

            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Carts Upload operations
    * @param id
    */
    public async upload(req: any): Promise<any> {

        try {
            return middleware.validateRequest("carts", {
                method:"UPLOAD",
                body:req.body,
                params:{}
            },{}, async (err:any) => {

                if(err){
                    return Promise.reject(this.errorService.build(err,null));
                }
                else{
                    await CartsRepository.bulkInsert(req.body.bulkData)
                    return Promise.resolve(true);
                }

            })
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }

    }





}