import 'reflect-metadata';
import { Container, Service } from 'typedi';
import ErrorService from "./errorService";
import Mustache from 'mustache';
import nodemailer from "nodemailer";
// import util from 'util';
const functions = require('firebase-functions');
const fs = require('fs');

@Service()
export default class EmailService {
    constructor(
        private errorService: ErrorService = Container.get(ErrorService),
        // private helper: Helper = Container.get(Helper),
    ) {
    }

    /**
     * Sends email
     * @param to -- array of TO email addresses
     * @param subject -- SUBJECT text
     * @param templateID -- Id of the template to be used
     * @param emailData -- Email data
     * @param cc -- array of CC email addresses - OPTIONAL
     * @param bcc -- array of BCC email addresses - OPTIONAL
     */
    public async send(lang: string, to: Array<string>, templateID: string, emailData: any, cc: Array<string> = [], bcc: Array<string> = []): Promise<any> {
        try {
            // Could not put this variable in constructor as EmailTemplates will not get loaded by the time this service is called
            const emailTemplates: any = Container.get("EmailTemplates");

            const templateInfo = emailTemplates[lang][templateID]; // Load given language specific template
            const templateFile = `${__dirname}/../emailTemplates/${lang}/${templateInfo.templateRef}`;

            // https://stackoverflow.com/questions/46867517/how-to-read-file-with-async-await-properly
            // promisify regular method
            // const readFile:any = util.promisify(fs.readFile);

            // TODO: Read templates from database.
            // Read template file content
            await fs.readFile(templateFile, 'utf8', async (err: any, templateString: any) => {
                if (err) {
                    return Promise.reject(this.errorService.build(err, null));
                }
                else {
                    // Render template
                    const emailBody:string = Mustache.render(templateString, emailData);

                    // Send email
                    const emailConfig = functions.config().email_config;
                    const emailTransport = nodemailer.createTransport({
                        host:emailConfig.smtpserver,
                        port:emailConfig.smtpport,
                        secure: (emailConfig.secure.toLowerCase() === 'true'),
                        // dkim:"",         // Email hosting provider like Rackspace keeps the private key and signs all out going emails
                        auth: {
                            user: emailConfig.email,
                            pass: emailConfig.password
                        }
                    });

                    const mailOptions = {
                        from:`${emailConfig.email}`,
                        to:to.join(','),
                        subject: templateInfo.subject,
                        // text: emailBody,
                        html:emailBody
                    }

                    // Send email
                    const emailResponse = await emailTransport.sendMail(mailOptions);
                    
                    return Promise.resolve(emailResponse);
                }
            })

        } catch (error) {
            return Promise.reject(this.errorService.build("E1031", null));
        }
    }

}