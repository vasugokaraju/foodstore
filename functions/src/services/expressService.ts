import {Express} from 'express'
import express = require('express');        // import * as express from 'express';

const expressEngine = ():Express => {
    const app:Express = express();
  
    return app;
};
  
export default expressEngine;