import 'reflect-metadata';
import { Container, Service } from 'typedi';
import { IAppCode, IAppError, IAppErrorCode } from "../interfaces/IAppInterfaces";
import Helper from "../helpers/helper";
import { DATATYPES } from "../loaders/enums";
import { Ajv, ValidationError } from 'ajv';
import FirebaseError from '../classes/firebaseError';
import CacheService from './cacheService';

// This service enhances error handling

@Service()
export default class ErrorService {
    constructor(
        private helper: Helper = Container.get(Helper),
        private cacheService: CacheService = Container.get(CacheService)
    ) {
        // TODO: Need to find out why "helper" class constructure parameter is not getting initialized with Helper class
        // The following is workaround for the above issue.
        this.helper = Container.get(Helper);        
     }

    // Standardize various error objects into appError
    // Primary focus is on finding error location and  adding it to errorStack for logging.
    // Rest of the error handling is done in customResponses middleware.
    public build(errorCode: string, moreInfo: any): IAppError;   // Generally used where the actual exception happens
    public build(appError: IAppError, moreInfo: any): IAppError; //  Generally used in Catch block to add more to original error
    public build(error: Error, moreInfo: any): IAppError; //  Generally used in catch all unhandled exceptions method
    public build(error: FirebaseError, moreInfo: any): IAppError; //  Generally used in repositories
    public build(error: ValidationError, moreInfo: any): IAppError; //  Generally used in catch all unhandled exceptions method to handle AJV Schema related exceptions
    public build(error: string | Error | FirebaseError | IAppError | ValidationError, moreInfo: any): IAppError {
        let appError: IAppError = { errorCode: "", errorData: "", errorStack: [] };

        // Get the location of the caller.  This is most likely from Catch block.
        let errorLocation: any = new Error().stack;  // This gives stack including the caller function/method
        errorLocation = errorLocation.split(/\n/)[2].replace(/\\/g, "/");   // Extract caller function/method details where actual error happened

        if (typeof error == "string") {   // When Pre-defined error code is passed as error
            appError.errorCode = error;  // Pre-defined error code
            appError.errorData = this.moreInfo2ErrorStack(moreInfo);   // Specific information about error.  Generally provided by the app or database

            appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

            appError.errorStack.push(errorLocation);
            appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));   // Add moreInfo to the error stack to provide better context of the error
        }
        else if (typeof error == "object") { // when  Error | IAppError is passed as error

            let standardError: any = <Error>error;

            // Handle 'Celebrate' request parameter validation errors
            if (standardError.joi) {
                appError.errorCode = "E1200"; // Invalid Payload

                appError.errorData = standardError.joi.message;

                appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

                // Copy JOI stack data to appError errorStack as part of standardization
                appError.errorStack.push(...standardError.joi.stack.split(/\n/));

                // Get the location of the caller.  This is most likely from Catch block.
                appError.errorStack.push(errorLocation);

                // Place moreInfo also in the errorStack to provide better context to developer
                appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
            }
            // If appError type is passed as error
            else if (error.hasOwnProperty("errorCode") && error.hasOwnProperty("errorStack")) {
                appError = <IAppError>error;

                appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

                // Append moreInfo to existing appError
                appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
            }

            // Firebase Errors
            else if (error instanceof FirebaseError) {
                appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

                // Get the location of the caller.  This is most likely from Catch block.
                appError.errorStack.push(errorLocation);

                appError.errorCode = error.code;
                // appError.errorData = [error.message];
                appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
            }

            // ********** AJV schema validation related errors
            else if (standardError instanceof ValidationError) {
                appError.errorCode = "E1202";

                // schemaName is needed and it is available in moreInfo
                appError.errorData = this.ajvErrorSummary(moreInfo.schemaName, standardError.errors);
                moreInfo = undefined;
            }
            else if (standardError.hasOwnProperty("message") && standardError.message.indexOf("no schema with key or ref") != -1) {
                appError.errorCode = "E1204";
                appError.errorData = standardError.message;
            }
            else if (standardError.hasOwnProperty("missingSchema")) {
                appError.errorCode = "E1205";
                appError.errorData = standardError;
            }
            // ***********************************************

            // api-query-params (aqp) raises standard exception for invalid JSON exceptions
            else if (standardError.message.indexOf("Invalid JSON string") != -1) {
                appError.errorCode = "E1201";

                appError.errorData = standardError.message;  // Set first line as data since standard exception contains crucial info in first line.

                appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

                // Get the location of the caller.  This is most likely from Catch block.
                appError.errorStack.push(errorLocation);

                appError.errorStack.push(...standardError.stack.split(/\n/));  // Re-arrange for easy read in the log.

                // Place moreInfo also in the errorStack to provide better context to developer
                appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
            }
            else {   // For unhandled exceptions
                // Copy error stack data to appError errorStack as part of standardization
                appError.errorCode = "E0000";    // Unhandled exception

                appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

                if (standardError.stack) appError.errorStack.push(...standardError.stack.split(/\n/));  // Re-arrange for easy read in the log.
                else appError.errorStack.push(...this.moreInfo2ErrorStack(standardError));

                // Standard Error provides error location
                //appError.errorStack.push(errorLocation);

                // Place moreInfo also in the errorStack to provide better context to developer
                appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
            }
        }
        else {   // If the error parameter is of type undefined, this situation needs to be handled.
            appError.errorCode = "E0000";  // Pre-defined error code

            appError.errorStack.push(...this.contextSeparator(errorLocation));     // Add separator for better readability

            appError.errorStack.push(errorLocation);

            // Collect moreInfo information
            appError.errorStack.push(...this.moreInfo2ErrorStack(moreInfo));
        }

        return appError;
    }

    public async resolve(error: Error | IAppError) {
        let errorMessage: string = '';
        let errorTitle;
        let errorCode;
        let errorData: any = undefined;  // Specific information about the error, provided by app or database

        let statusCode;
        let errorInfo: IAppCode = { code:"", title: "", message: "", httpCode: 0, action: 0 };
        // let errorCodes: any = {};

        // // In case the ErrorCodes are not loaded.
        // try {
            
        //     console.log('cache', this.cacheService.getAppCode("E0000"));

        //     errorCodes = Container.get("ErrorCodes");
        // } catch (e) {
        //     errorCodes = {}
        // }

        // Handle the error objects built by the above build() method.
        if (error && error.hasOwnProperty("errorCode") && error.hasOwnProperty("errorStack")) {
            let appError: IAppError = <IAppError>error; // Convert error object to of type IAppError
            errorCode = appError.errorCode;
            errorInfo = await this.cacheService.getAppCode(errorCode);  // Get pre-defined error details based on errorCode;
            
            //if(enhance) helper.enchanceError(errorInfo); // Make the error message more user-friendly

            // Set the error details to send out in response.
            if (errorInfo) {
                errorMessage = errorInfo.message;
                errorTitle = errorInfo.title;
                statusCode = errorInfo.httpCode;
                errorData = appError.errorData ? appError.errorData : null; // System provided specific error information should be passed on to consumer
            }
            else {
                errorCode = "E0001";
                errorInfo = <IAppCode>{ "code":"", "title": "Error Code Exception", "message": "Error code is not available in master list OR error codes are not loaded.", "httpCode": 500, "action": 1 };
                errorMessage = errorInfo.message;
                errorTitle = errorInfo.title;
                statusCode = errorInfo.httpCode;
                errorData = { "missingErrorCode": appError.errorCode };

                // If error data is provided by build() function, pass it to the user.
                if((<IAppError>error).errorData != undefined){
                    errorData.otherInfo = (<IAppError>error).errorData;
                }                
            }
        }
        else if (error) {       // Handle other errors. Most likely unhandled exceptions
            let standardError: any = <Error>error;

            errorCode = "E0000"; // Unknown exception
            // errorInfo = <IAppErrorCode>errorCodes[errorCode];
            errorInfo = await this.cacheService.getAppCode(errorCode);  // Get pre-defined error details based on errorCode;
            errorMessage = errorInfo.message;
            errorTitle = errorInfo.title;
            statusCode = errorInfo.httpCode;
            errorData = this.moreInfo2ErrorStack(standardError.stack);
        }

        // It is rare but possible
        if (error == undefined) {
            let appError: IAppError = { errorCode: "E0000", errorData: "", errorStack: [] }; // Convert error object to of type IAppError
            errorCode = appError.errorCode;
            // errorInfo = <IAppErrorCode>errorCodes[errorCode];  // Get pre-defined error details based on errorCode;
            errorInfo = await this.cacheService.getAppCode(errorCode);  // Get pre-defined error details based on errorCode;

            // Set the error details to send out in response.
            errorMessage = errorInfo.message;
            errorTitle = errorInfo.title;
            statusCode = errorInfo.httpCode;

            // Try to get the location from where this function is called with undefined as error parameter
            let errorLocation: any = new Error().stack;  // This gives stack including the caller function/method
            errorLocation = errorLocation.split(/\n/)[2];   // Extract caller function/method details where actual error happened
            appError.errorStack.push(errorLocation);
            error = appError;
        }

        if (errorData && this.helper.getDataType(errorData) == DATATYPES.array && errorData.length == 0) {
            errorData = null;
        }
        else if (errorData && this.helper.getDataType(errorData) == DATATYPES.object && Object.keys(errorData).length == 0) {
            errorData = null;
        }

        errorData = (errorData == null) ? undefined : errorData;

        let response: any = { status: 'fail', code: errorCode, title: errorTitle, message: errorMessage, data: errorData };

        let log = this.helper.object2JSON(Object.assign({}, error, { response: response }));

        return { statusCode: statusCode, response: response, log: log, logAction: errorInfo.action };
    }

    // Processes payloads and put them in array
    private moreInfo2ErrorStack(payload: any): any {
        let rtn: any = [];

        if (!payload) {
            rtn = [];
        }
        else if (this.helper.getDataType(payload) === DATATYPES.object && payload.hasOwnProperty("callee")) {  // function "arguments" object
            Object.keys(payload).forEach((argPos: any) => {

                if (payload[argPos].hasOwnProperty('headers') && payload[argPos].hasOwnProperty('body') && payload[argPos].hasOwnProperty('params')) {
                    rtn.push("HTTP Request Information:");
                    rtn.push({
                        http_url: payload[argPos].originalUrl,
                        http_method: payload[argPos].method,
                        http_header: payload[argPos].headers,
                        http_body: payload[argPos].body,
                        http_query: payload[argPos].query,
                        http_params: payload[argPos].params
                    });
                }
                else {
                    rtn.push("Method Argument " + argPos + ":");
                    rtn.push(payload[argPos]);
                }

            });
        }
        else if (this.helper.getDataType(payload) == DATATYPES.array) {
            rtn.push(...payload);
        }
        // If http req object is sent as payload which is very common, extract only useful data for logging
        else if (payload.hasOwnProperty('headers') && payload.hasOwnProperty('body') && payload.hasOwnProperty('params')) {
            rtn.push("HTTP Request Information:");
            rtn.push({
                http_url: payload.originalUrl,
                http_method: payload.method,
                http_header: payload.headers,
                http_body: payload.body,
                http_query: payload.query,
                http_params: payload.params
            });
        }
        else {
            rtn.push(payload);
        }

        return rtn;
    }

    private contextSeparator(errorLocation: string) {

        let contextSeparator: string = "";

        // Guess the layer and add context separation line
        if (errorLocation.indexOf("/routes") != -1) {
            contextSeparator = "**********ROUTE CONTEXT**********"
        }
        else if (errorLocation.indexOf("/services") != -1) {
            contextSeparator = "**********SERVICE CONTEXT**********"
        }
        else if (errorLocation.indexOf("/repositories") != -1) {
            contextSeparator = "**********REPOSITORY CONTEXT**********"
        }
        else if (errorLocation.indexOf("/middleware") != -1) {
            contextSeparator = "**********MIDDLEWARE CONTEXT**********"
        }

        return ["", "", contextSeparator];
    }

    // Consolidated all errors and generates error message object
    private ajvErrorSummary(schemaName: string, errors: any) {
        let summary: any = []
        let path: string;

        let ajv: Ajv = Container.get("AJV")

        let errorCodes: Array<IAppErrorCode> = Container.get("ErrorCodes") || {};

        errors.forEach((error: any) => {
            if (error.keyword != 'if') {
                // path = error.schemaPath.replace(/#\/definitions\/|\/properties/g, '').replace(/\//g, '.');
                path = error.dataPath.replace(/\//g, ".").substr(1);  // Convert dataPath to json key string
                let err: any = { path: "", code: "", message: "", title: "" };

                // Handle error codes here.  Usually the same error code since it is about data type validation
                // Custom keyword functions (ex: idExists) may provide specific error code at error.params.code
                err["path"] = path;
                err["code"] = error.params.errorCode || error.params.code || "E1200"; // Please check the payload including body and query parameters.
                err["title"] = errorCodes[err["code"]].message

                // error.data is available when custom keyword function like "idExists" is triggered. It contains specific information about the invalid value
                err["message"] = (error.data && Array.isArray(error.data)) ? error.message + " [" + error.data.join() + "]" : error.message.replace(/should/, 'Should');

                // Provide enum list in the message to make it easy for the developer
                let schemaPath = error.schemaPath.replace(/#\/definitions\/|\/properties/g, '').replace(/\//g, '.');
                if (schemaPath.indexOf('enum') != -1) {
                    let schema: any = ajv.getSchema(schemaName + error.schemaPath);
                    err["message"] += " [" + schema.schema + "].";
                }

                summary.push(err);
            }
        });

        return summary;
    }

}