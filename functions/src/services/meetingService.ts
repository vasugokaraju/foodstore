import 'reflect-metadata';
import {Container, Service} from 'typedi';
// import config from "../config"
import { ICRUDServiceInterface, IWhereQuery} from "../interfaces/IAppInterfaces";
// import {IMeeting, INewMeeting, IUpdateMeeting} from '../interfaces/IMeetingInterface';
import MeetingRepository from '../repositories/meetingRepository'
import ErrorService from "./errorService";
// const { Parser } = require('json2csv');
import { IMeeting } from '../interfaces';
import middleware from "../middleware";

// import firebase from "firebase/app";
// import "firebase/auth";

import { MEETING_INVITATION} from "../interfaces/IAppInterfaces";
import { RtcTokenBuilder, RtcRole } from "agora-access-token";
import config from "../config";
import { v4 as uuid } from 'uuid'

// Rtc Examples
const appID = config.agora_app_id;
const appCertificate = config.agora_app_certificate;
const role = RtcRole.PUBLISHER;



@Service("meeting_service")
export default class MeetingService implements ICRUDServiceInterface {
    
    constructor(
        private errorService:ErrorService = Container.get(ErrorService)
    ){
        this.errorService = Container.get(ErrorService);
    }

    /**
      * Handles Meeting preload operations
    */
    public async preload(userProfile:any, whereQuery:IWhereQuery): Promise<any> {

        try {
            // Retrieve Meeting records for preload
            let meetingInfo:IMeeting[] = await this.readMany(whereQuery);     // Get the records of people who are lower in hierarchy.

            const result = {
                meetingData: meetingInfo
            };

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e, null));
        }
    }

    /**
	 * Helps to validate record.  This method is used as schema keyword validate function.
	 * @param id
	 */
	public async idExists(id:string): Promise<any>{
		let result = await this.readOne(id);
		// if not found, make resolve "false" instead of reject.  This will help the caller function to execute the rest of the logic as expected.
		return Promise.resolve((result.length != 0));
	}


    /**
    * Handles Meeting single read operations
    */
    public async readOne(id:string): Promise<any> {
        try {
            let result:any;

            if(!id) return Promise.resolve([]);

            result = await MeetingRepository.read(id);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Meeting where query operations
    */
     public async readMany(where:IWhereQuery): Promise<any> {
        try {
            let result:any;

            const whereQuery:IWhereQuery = where;
            result = await MeetingRepository.readWhere(whereQuery);

            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Meeting Create operations
    * @param data
    */
    public async create(data: any): Promise<any> {
        try {

            const result = await MeetingRepository.create(data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    /**
    * Handles Meeting Update operations
    * @param id
    * @param data
    */
    public async update(id: string, data: any): Promise<any> {
        try {

            // If meeting invitation is accepted, create agora meeting token and include in the update data.
            if(data.clStts == MEETING_INVITATION.ACCEPTED || data.clStts == MEETING_INVITATION.REJECTED){
                const dt = new Date();
                data.strtTm = dt.toISOString();
                data.endTm = new Date( dt.setHours(dt.getHours() + 1) ).toISOString();
                data.chnl = uuid();
                data.tkn = await this.generateToken(data.chnl);

                // Update receiver's record also with channel and token 
                // so that cannection can be made with agora video service from their end
                await MeetingRepository.update(data.rcvrPh, data);
            }

            const result = await MeetingRepository.update(id, data);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Meeting Delete operations
    * @param id
    */
    public async delete(id: string): Promise<any> {
        try {
            const result = await MeetingRepository.delete(id);
            return Promise.resolve(result);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Meeting Download operations
    * @param id
    */
    public async download(query: any): Promise<any> {
        try {
            // const qry:IFindQuery = {};
            // const data = await this.read(qry);  // Get download data

            // const fields = query.fields.split(","); // Specifiy the fields to extract from the above data
            // const opts = { fields };
            // const parser = new Parser(opts);
            // const csv = parser.parse(data); // Parse data array to csv
            // return Promise.resolve(csv);
            return Promise.resolve([]);
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    /**
    * Handles Meeting Upload operations
    * @param id
    */
    public async upload(req: any): Promise<any> {

        try {
            return middleware.validateRequest("meeting", {
                method:"UPLOAD",
                body:req.body,
                params:{}
            },{}, async (err:any) => {

                if(err){
                    return Promise.reject(this.errorService.build(err,null));
                }
                else{
                    await MeetingRepository.bulkInsert(req.body.bulkData)
                    return Promise.resolve(true);
                }

            })
        }
        catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }

    }





    public async generateToken(channelName:string) {
        // Build token with uid
        // let dt:Date = new Date();   //4294967295
        const privilegeExpiredTs = Math.floor(Date.now() / 1000) + 600; //+ (600 * 1000);   // 10 minutes after current time
        
        const uid = Date.now();
        // console.log('xxxxxxxxxxxxxx', appID, appCertificate, channelName, uid, role, privilegeExpiredTs);
        
        const tokenA = RtcTokenBuilder.buildTokenWithUid(appID, appCertificate, channelName, uid, role, privilegeExpiredTs);

        return tokenA;
    }

}