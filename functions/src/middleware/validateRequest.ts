/**
 * Attach custom responses to req
 * @param {*} req Express req Object
 * @param {*} res  Express res Object
 * @param {*} next  Express next Function
 */
 
import { Container } from "typedi";
import Helper from "../helpers/helper";
import ErrorService from "../services/errorService";
import ValidationService from "../services/validationService";

const validateRequest = async (routeName:string, req: any, res: any, next: any) => {
    let helper: Helper = Container.get(Helper);
    let errorService: ErrorService = Container.get(ErrorService);
    let validationService: ValidationService = Container.get(ValidationService);

    try {

        // Process and consolidate the req.query and req.params fields to a manageable object.
        req.findQuery = await helper.buildQueryParams(req);

        // let route = helper.getRouteName(req);
        await validationService.validate(routeName + "Schema", req);

        return next();
    } catch (e) {
        return next(errorService.build(e,null));
    }
};

export default validateRequest;