import { Container } from "typedi";
import { ROLES } from "../loaders/enums";
import ErrorService from "../services/errorService";
import EntitlementService from "../services/entitlementService";
// import UsersService from "../services/usersService";
import Helper from '../helpers/helper'

// This file plays major role in limiting access to data.
// The incoming query is manipulated here based on many factors including user role, role hierarchy
const usersEntitlement = async (req: any, res: any, next: any) => {
    let errorService = Container.get(ErrorService);
    let entitlementService = Container.get(EntitlementService);
    // let usersService: UsersService = Container.get(UsersService);
    let helper = Container.get(Helper);

    try {

        // ----------------------- COMMON ENTITLEMENT VALIDATIONS -----------------------

        // Collect User provided query/params parameters
        // req.query.findQuery = await entitlementService.buildQueryParams(req);   // Returns req.query.findQuery[]

        
        
        // SUPERADMIN role should be exempted from the following validations.
        if (req.userProfile.role.code === ROLES.SUPERADMIN) {
            helper.enforceUserWhereCriteria(req);
            return next();
        }

        // ----------------------- ROLESPECIFIC ENTITLEMENT VALIDATIONS -----------------------
        // Check whether User's hierarchy allows to perform this operation.
        // await usersService.checkRoleHierarchy(req);

        // Perform other entitlement checks to deny access to data that the user is not entitled to.
        // These methods are expected to reject the request with proper error code
        // await entitlementService.someEntitlementCheck(req);
        
        // Influence query to filter out data that the user is not entitled to.
        await entitlementService.enforceUsersEntitlements(req);

        return next();

    } catch (e) {
        if (e.stack && e.stack.indexOf("api-query-params") != -1) {
            return next(errorService.build("E1200", e));    // Invalid Payload
        } else {
            return next(errorService.build(e, null));
        }
    }
};

export default usersEntitlement;