import {Container} from "typedi";
import { ROLES } from "../loaders/enums";
import AuthService from "../services/authService";
import ErrorService from "../services/errorService";

//This is to check route access privileges
const accessControlCheck = async (req: any, res: any, next: any) => {
    let errorService : ErrorService = Container.get(ErrorService);
    let authService: AuthService = Container.get(AuthService);

    try {
        
        // For admin user, no route access check.
        if(ROLES.SUPERADMIN == parseFloat(req.userProfile.role.code)) return next();

        const allowed = await authService.getRouteAccess(req.baseUrl.split("/")[2], req.userProfile.role.name)
        if(!allowed)console.log("Illegal attempt to access restricted routes. User:", req.userProfile, "Route:", req.baseUrl );
        
        return (allowed) ? next(): next(errorService.build("E1004",null));

    } catch (e) {
        // If rbac matching record is not found.
        return next(errorService.build("E1004",e));
    }
};

export default accessControlCheck;