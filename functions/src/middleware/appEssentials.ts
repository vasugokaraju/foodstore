/**
 * Attach custom responses to req
 * @param {*} req Express req Object
 * @param {*} res  Express res Object
 * @param {*} next  Express next Function
 */
 
import {Container} from "typedi";
import { IAppCode, IAppError } from "../interfaces/IAppInterfaces";
// import {HTTP_STATUS,DATATYPES} from "../loaders/enums";
// import Helper from "../helpers/helper";
import Logger from "../loaders/logger";
import ErrorService from "../services/errorService";
import CacheService from "../services/cacheService";
// import AuditService from "../services/auditService";

const appEssentials = async (req: any, res: any, next: any) => {
    // let helper: Helper = Container.get(Helper);
    let errorService : ErrorService = Container.get(ErrorService);
    let cacheService: CacheService = Container.get(CacheService);
    // let auditService : AuditService = Container.get(AuditService);

    try {
        // appSuccess signature is added to Response interface in @types/express/index.d.ts
        res.appSuccess = async function (successCode:string, data: any, customMessage?:string) {
            try {
                // let successCodes:any = Container.get("SuccessCodes");
                // let successInfo:IAppSuccessCode = <IAppSuccessCode>successCodes[successCode];
                let successInfo: IAppCode = await cacheService.getAppCode(successCode, "en-US");    // Provides app code details for the given code.

                let msg = customMessage ? customMessage : successInfo.message;

                res.status(successInfo.httpCode).json({ "status": 'success', title: successInfo.title, "message": msg, "data": data });

            } catch (error) {
                // Requested success code is not available
                res.appFail(errorService.build(error, req));
            }
            
			// Make an attempt to add audit log. Do not wait for response.
			try {
				if (req.method.toLowerCase() != "get") {
					// let logRec = helper.getAuditLogRecord(req, data);
					// auditService.log(logRec);
				}
			} catch (e) { }
        }

        
        // appFail signature is added to Response interface in @types/express/index.d.ts
        res.appFail = async function (error: Error | IAppError) {
            let errorResolve = await errorService.resolve(error);
            res.status(errorResolve.statusCode).json(errorResolve.response);

            if (errorResolve.logAction > 0) Logger.error(JSON.stringify(errorResolve.log, null, " "));    // Dump all the info about the error to the log
        }

        return next();
    } catch (e) {
        return next(errorService.build(e,req));
    }
};

export default appEssentials;