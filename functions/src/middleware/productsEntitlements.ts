import { Container } from "typedi";
import { ROLES } from "../loaders/enums";
import ErrorService from "../services/errorService";
import EntitlementService from "../services/entitlementService";

// This file plays major role in limiting access to data.
// The incoming query is manipulated here based on many factors including user role, role hierarchy
const productsEntitlement = async (req: any, res: any, next: any) => {
    let errorService = Container.get(ErrorService);
    let entitlementService = Container.get(EntitlementService);

    try {

        // ----------------------- COMMON ENTITLEMENT VALIDATIONS -----------------------

        // Collect User provided query/params parameters
        // req.query.findQuery = await entitlementService.buildQueryParams(req);   // Returns req.query.findQuery[]

        
        
        // SUPERADMIN role should be exempted from the following validations.
        if (req.userProfile.role.code === ROLES.SUPERADMIN) {
            return next();
        }

        // ----------------------- ROLESPECIFIC ENTITLEMENT VALIDATIONS -----------------------

        // Perform other entitlement checks to deny access to data that the user is not entitled to.
        // These methods are expected to reject the request with proper error code
        // await entitlementService.someEntitlementCheck(req);
        
        // Influence query to filter out data that the user is not entitled to.
        await entitlementService.enforceProductsEntitlements(req);

        return next();

    } catch (e) {
        if (e.stack && e.stack.indexOf("api-query-params") != -1) {
            return next(errorService.build("E1200", e));    // Invalid Payload
        } else {
            return next(errorService.build(e, null));
        }
    }
};

export default productsEntitlement;