import appEssentials from './appEssentials';
import validateRequest from './validateRequest';
import accessControlCheck from './accessControlCheck';

import filesUpload from './filesUpload';
import validateToken from './validateToken'
import usersEntitlements from './usersEntitlements'
import productsEntitlements from './productsEntitlements'
import cartsEntitlements from './cartsEntitlements'
import meetingEntitlements from './meetingEntitlements'
import appCodsEntitlements from './appCodsEntitlements'
import appConfEntitlements from './appConfEntitlements'


import {  summaryReportEntitlements,  financeReportEntitlements,  } from './reportsEntitlements'


export default {
    appEssentials,
    validateRequest,
    validateToken,
    accessControlCheck,
    usersEntitlements,
    productsEntitlements,
    cartsEntitlements,
    meetingEntitlements,
    appCodsEntitlements,
    appConfEntitlements,
    filesUpload,

    summaryReportEntitlements,
    financeReportEntitlements,

};