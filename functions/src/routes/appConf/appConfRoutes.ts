import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import AppConfService from "../../services/appConfService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerAppConfPreload = Router();
const routerAppConfReadOne = Router();
const routerAppConfReadMany = Router();
const routerAppConfCreate = Router();
const routerAppConfUpdate = Router();
const routerAppConfDelete = Router();
const routerAppConfDownload = Router();
const routerAppConfUpload = Router();

const appConfService = Container.get(AppConfService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerAppConfPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appConf", req,res,next)},
  middleware.appConfEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await appConfService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerAppConfReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appConf", req,res,next)}, 
  middleware.appConfEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await appConfService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await appConfService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerAppConfReadMany.post('/', 
  middleware.validateToken,
  middleware.appConfEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await appConfService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerAppConfCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appConf", req,res,next)}, 
  middleware.appConfEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await appConfService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerAppConfUpdate.put('/:id', middleware.validateToken, (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appConf", req,res,next)}, async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const data = req.body;
    const result = await appConfService.update(id, data);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerAppConfDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appConf", req,res,next)}, 
  middleware.appConfEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const result = await appConfService.delete(id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Downloads module data
*/
routerAppConfDownload.post('/', middleware.validateToken, middleware.appConfEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await appConfService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerAppConfUpload.post('/', middleware.validateToken, middleware.appConfEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await appConfService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});



export const appConfPreload: IRoutes = { name: 'appConfPreload', router: routerAppConfPreload };
export const appConfReadOne: IRoutes = { name: 'appConfReadOne', router: routerAppConfReadOne };
export const appConfReadMany: IRoutes = { name: "appConfReadMany", router: routerAppConfReadMany };
export const appConfCreate: IRoutes = { name: "appConfCreate", router: routerAppConfCreate };
export const appConfUpdate: IRoutes = { name: "appConfUpdate", router: routerAppConfUpdate };
export const appConfDelete: IRoutes = { name: "appConfDelete", router: routerAppConfDelete };
export const appConfDownload: IRoutes = { name: "appConfDownload", router: routerAppConfDownload };
export const appConfUpload: IRoutes = { name: "appConfUpload", router: routerAppConfUpload };