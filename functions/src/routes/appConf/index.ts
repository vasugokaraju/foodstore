import { 
    appConfPreload, 
    appConfCreate, 
    appConfReadOne, 
    appConfReadMany, 
    appConfUpdate, 
    appConfDelete, 
    appConfDownload, 
    appConfUpload,
    } from './appConfRoutes';
import { IRoutes } from '../../interfaces';

export const appConf: IRoutes[] = [
    appConfPreload, 
    appConfCreate, 
    appConfReadOne, 
    appConfReadMany, 
    appConfUpdate, 
    appConfDelete, 
    appConfDownload, 
    appConfUpload,
    ];