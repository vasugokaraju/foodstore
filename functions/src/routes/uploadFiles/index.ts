import { uploadFile, uploadFiles } from './uploadFilesRoutes';
import { IRoutes } from '../../interfaces';

export const upload: IRoutes[] = [uploadFile, uploadFiles];