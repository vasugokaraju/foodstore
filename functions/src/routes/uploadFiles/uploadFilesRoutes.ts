import { Request, Response, Router } from 'express';
import { Container } from 'typedi';
import { IRoutes } from '../../interfaces';
import middleware from '../../middleware';
import ErrorService from '../../services/errorService';
import StorageService from '../../services/storageService';

const routerUploadFile = Router();
const routerUploadFiles = Router();

let errorService = Container.get(ErrorService);
let storageService = Container.get(StorageService);


routerUploadFile.post('/',
  middleware.filesUpload, 
  async (req: Request, res: Response) => {
    try {
      const result = await storageService.save(req);
      res.appSuccess("S1009", result);	//Record has been created.
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }
  })


// Route to register admin app users
routerUploadFiles.post('/',
async (req: Request, res: Response) => {
  try {
    const result = await storageService.save(req);
    res.appSuccess("S1009", result);	//Record has been created.
  }
  catch (e) {
    res.appFail(errorService.build(e, req));
  }
})


export const uploadFile: IRoutes = { name: "uploadFile", router: routerUploadFile };
export const uploadFiles: IRoutes = { name: "uploadFiles", router: routerUploadFiles };
