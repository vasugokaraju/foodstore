import { 
    cartsPreload, 
    cartsCreate, 
    cartsReadOne, 
    cartsReadMany, 
    cartsUpdate, 
    cartsDelete, 
    cartsDownload, 
    cartsUpload,
    } from './cartsRoutes';
import { IRoutes } from '../../interfaces';

export const carts: IRoutes[] = [
    cartsPreload, 
    cartsCreate, 
    cartsReadOne, 
    cartsReadMany, 
    cartsUpdate, 
    cartsDelete, 
    cartsDownload, 
    cartsUpload,
    ];