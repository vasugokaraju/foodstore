import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import CartsService from "../../services/cartsService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerCartsPreload = Router();
const routerCartsReadOne = Router();
const routerCartsReadMany = Router();
const routerCartsCreate = Router();
const routerCartsUpdate = Router();
const routerCartsDelete = Router();
const routerCartsDownload = Router();
const routerCartsUpload = Router();

const cartsService = Container.get(CartsService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerCartsPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("carts", req,res,next)},
  middleware.cartsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await cartsService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerCartsReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("carts", req,res,next)}, 
  middleware.cartsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await cartsService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await cartsService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerCartsReadMany.post('/', 
  middleware.validateToken,
  middleware.cartsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await cartsService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerCartsCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("carts", req,res,next)}, 
  middleware.cartsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await cartsService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerCartsUpdate.put('/:id', middleware.validateToken, (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("carts", req,res,next)}, async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const data = req.body;
    const result = await cartsService.update(id, data);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerCartsDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("carts", req,res,next)}, 
  middleware.cartsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const result = await cartsService.delete(id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Downloads module data
*/
routerCartsDownload.post('/', middleware.validateToken, middleware.cartsEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await cartsService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerCartsUpload.post('/', middleware.validateToken, middleware.cartsEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await cartsService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});



export const cartsPreload: IRoutes = { name: 'cartsPreload', router: routerCartsPreload };
export const cartsReadOne: IRoutes = { name: 'cartsReadOne', router: routerCartsReadOne };
export const cartsReadMany: IRoutes = { name: "cartsReadMany", router: routerCartsReadMany };
export const cartsCreate: IRoutes = { name: "cartsCreate", router: routerCartsCreate };
export const cartsUpdate: IRoutes = { name: "cartsUpdate", router: routerCartsUpdate };
export const cartsDelete: IRoutes = { name: "cartsDelete", router: routerCartsDelete };
export const cartsDownload: IRoutes = { name: "cartsDownload", router: routerCartsDownload };
export const cartsUpload: IRoutes = { name: "cartsUpload", router: routerCartsUpload };