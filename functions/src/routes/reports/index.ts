import { 
    summaryReport,
    financeReport,
    } from './reportsRoutes';

import { IRoutes } from '../../interfaces';

export const reports: IRoutes[] = [
    summaryReport,
    financeReport,
    ];