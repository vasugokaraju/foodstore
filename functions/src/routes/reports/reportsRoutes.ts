import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import ReportsService from "../../services/reportsService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerSummaryReport = Router();
const routerFinanceReport = Router();

const reportsService = Container.get(ReportsService);
let errorService = Container.get(ErrorService);

/**
  * Preload summaryReport data that may contain multiple data sets related to the page.
**/
routerSummaryReport.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("reports", req,res,next)},
  middleware.summaryReportEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await reportsService.summaryReport(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
  * Preload financeReport data that may contain multiple data sets related to the page.
**/
routerFinanceReport.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("reports", req,res,next)},
  middleware.financeReportEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await reportsService.financeReport(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

export const summaryReport: IRoutes = { name: 'summaryReport', router: routerSummaryReport };
export const financeReport: IRoutes = { name: 'financeReport', router: routerFinanceReport };