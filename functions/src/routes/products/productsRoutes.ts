import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import ProductsService from "../../services/productsService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerProductsPreload = Router();
const routerProductsReadOne = Router();
const routerProductsReadMany = Router();
const routerProductsCreate = Router();
const routerProductsUpdate = Router();
const routerProductsDelete = Router();
const routerProductsDownload = Router();
const routerProductsUpload = Router();
const routerGetProductsIdNameReadMany = Router();

const productsService = Container.get(ProductsService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerProductsPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("products", req,res,next)},
  middleware.productsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await productsService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerProductsReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("products", req,res,next)}, 
  middleware.productsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await productsService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await productsService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerProductsReadMany.post('/', 
  middleware.validateToken,
  middleware.productsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await productsService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerProductsCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("products", req,res,next)}, 
  middleware.productsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await productsService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerProductsUpdate.put('/:id', middleware.validateToken, (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("products", req,res,next)}, async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const data = req.body;
    const result = await productsService.update(id, data);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerProductsDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("products", req,res,next)}, 
  middleware.productsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const result = await productsService.delete(id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Downloads module data
*/
routerProductsDownload.post('/', middleware.validateToken, middleware.productsEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await productsService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerProductsUpload.post('/', middleware.validateToken, middleware.productsEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await productsService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});


/**
* Returns Products data in the form of code-name for the purpose of Autocomplete controls on UI.
*/
routerGetProductsIdNameReadMany.post('/', 
    middleware.validateToken,
    middleware.productsEntitlements,
    middleware.accessControlCheck,
    async (req: Request, res: Response) => {

    try{
        const result = await productsService.getProductsIdNameReadMany(req);
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S0000",result, customMessage);	                // Record(s) found.
    }
    catch (e){
        res.appFail(errorService.build(e, req));
    }
});

export const productsPreload: IRoutes = { name: 'productsPreload', router: routerProductsPreload };
export const productsReadOne: IRoutes = { name: 'productsReadOne', router: routerProductsReadOne };
export const productsReadMany: IRoutes = { name: "productsReadMany", router: routerProductsReadMany };
export const productsCreate: IRoutes = { name: "productsCreate", router: routerProductsCreate };
export const productsUpdate: IRoutes = { name: "productsUpdate", router: routerProductsUpdate };
export const productsDelete: IRoutes = { name: "productsDelete", router: routerProductsDelete };
export const productsDownload: IRoutes = { name: "productsDownload", router: routerProductsDownload };
export const productsUpload: IRoutes = { name: "productsUpload", router: routerProductsUpload };
export const productsIdNameReadMany: IRoutes = { name: "productsIdNameReadMany", router: routerGetProductsIdNameReadMany };