import { 
    productsPreload, 
    productsCreate, 
    productsReadOne, 
    productsReadMany, 
    productsUpdate, 
    productsDelete, 
    productsDownload, 
    productsUpload,
    productsIdNameReadMany,
    } from './productsRoutes';
import { IRoutes } from '../../interfaces';

export const products: IRoutes[] = [
    productsPreload, 
    productsCreate, 
    productsReadOne, 
    productsReadMany, 
    productsUpdate, 
    productsDelete, 
    productsDownload, 
    productsUpload,
    productsIdNameReadMany,
    ];