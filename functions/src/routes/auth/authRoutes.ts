import { IRoutes } from '../../interfaces';
import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import middleware from '../../middleware';
import ErrorService from "../../services/errorService";
import AuthService from "../../services/authService";
import UsersService from "../../services/usersService";

const routerRegister = Router();
const routerRegisterID3 = Router();   // for Admin App registration
const routerVerifyID3 = Router();     // for Admin App user verification
const routerAuthLogin = Router();
const routerAuthLogout = Router();
const routerAuthChangePassword = Router();
const routerAuthCreatePassword = Router();
const routerAuthRenewToken = Router();
const routerAuthTest_Get = Router();
const routerAuthEncode = Router();
const routerAuthDecode = Router();
const routerAuthHash = Router();
const routerGFAC = Router();        // Get Firebase App Configuration

const routerOnSignInwithPhoneNumber = Router();        // Sign in with phone number and reCaptcha verifier
const routerIsPhoneNumberRegistered = Router();

const routerSendEmailVerification = Router();
const routerVerifyRegisteredEmail = Router();

const routerSendEmailOTPLink = Router();
const routerVerifyEmailOTPLink = Router();

const routerSendForgotPasswordLink = Router();
const routerVerifyForgotPasswordLink = Router();

const routerResetPassword = Router();

const routerFederatedLogin = Router();

let errorService = Container.get(ErrorService);
let authService = Container.get(AuthService);
let usersService = Container.get(UsersService);

routerRegister.post('/',
  (req: Request, res: Response, next: NextFunction) => { middleware.validateRequest("users", req, res, next) },
  async (req: Request, res: Response) => {
    try {
      const data = req.body;
      // TODO:  Set the role as USER for all public registrations
      const result = await usersService.create(data);
      res.appSuccess("S1401", result, "1 record has been created.");	//Record has been created.
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }
  })

// Route to register admin app users
routerRegisterID3.post('/',
  (req: Request, res: Response, next: NextFunction) => { middleware.validateRequest("users", req, res, next) },
  async (req: Request, res: Response) => {
    try {
      const data = req.body;
      data.disabled = true;         // Admin has to manually verify and enable the user
      data.emailVerified = false;
      // Registers app admin
      // FID, IMEI and Gmail are called as "ID3".
      // https://www.notion.so/Authentication-Page-bdcd5473080c45bd853c928b493c032d
      const result = await usersService.create(data);
      res.appSuccess("S1401", result, "1 record has been created.");	//Record has been created.
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }
  })

// Route to register admin app users
routerVerifyID3.post('/',
  (req: Request, res: Response, next: NextFunction) => { middleware.validateRequest("users", req, res, next) },
  async (req: Request, res: Response) => {
    try {
      const data = req.body;

      // TODO: Validate ID3 and return the status

      const result:any = {
        status:true,        // For testing only. Replace this with actual value
        data:data
      }
      res.appSuccess("S0000", result);	// Query has been successful.
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }
  })


// Validate user credentials using Basic-Auth and generate JWT token based on user's Consumer credentials and return token
routerAuthLogin.post('/', async (req: Request, res: Response) => {

  try {
    const userInfo = await authService.login(req, res);
    res.appSuccess("S1000", userInfo);    // Credentials are valid
  }
  catch (e) {
    res.appFail(errorService.build(e, null));   // Unknown error during string encode attempt.  Please try again.
  }

})

routerAuthLogout.post('/', middleware.validateRequest, async (req: Request, res: Response) => {
  try {
    const status = await authService.logout(req, res);
    res.appSuccess("S1008", { loggedOut: status });    // Logged out
  }
  catch (e) {
    res.appFail(errorService.build(e, null));   // Unknown error during string encode attempt.  Please try again.
  }
})

// // A route for testing
// route.post('/test', middleware.validateToken, async (req: Request, res: Response) => {

//     setTimeout(() => {
//         res.appSuccess("S0000", Object.assign({ status: "success"},req.body));
//     }, (parseInt(req.body.delay) * 1000) || 0);

// })

// Validate user credentials using Basic-Auth and generate JWT token based on user's Consumer credentials and return token
routerAuthTest_Get.get('/', async (req: Request, res: Response) => {
  res.appSuccess("S0000", { status: "success", timestamp: Date.now() });
})


routerAuthCreatePassword.post('/',
  async (req: Request, res: Response) => {
    try {
      const data: any = await authService.createPassword(req, res);
      res.appSuccess("S1017", Object.assign({ status: "success" }, data));    // Password has been changed.
    }
    catch (e) {
      res.appFail(errorService.build(e, null));   // Unknown error during string encode attempt.  Please try again.
    }
  })

routerAuthChangePassword.post('/',
  middleware.validateToken,
  (req: Request, res: Response, next: NextFunction) => { middleware.validateRequest("auth", req, res, next) },
  async (req: Request, res: Response) => {
    try {
      const data: any = await authService.changePassword(req, res);
      res.appSuccess("S1003", Object.assign({ status: "success" }, data));    // Password has been changed.
    }
    catch (e) {
      res.appFail(errorService.build(e, null));   // Unknown error during string encode attempt.  Please try again.
    }
  })

routerAuthRenewToken.post('/',
  middleware.validateToken,
  (req: Request, res: Response, next: NextFunction) => { middleware.validateRequest("auth", req, res, next) },
  async (req: Request, res: Response) => {

    try {
      const response = await authService.renewToken(req, res);     // Generate token with user info

      setTimeout(() => {
        res.appSuccess("S1004", response);    // Token has been renewed.
      }, 0);
    }
    catch (e) {
      res.appFail(errorService.build(e, null));   // Unknown error during string encode attempt.  Please try again.
    }

  })

routerAuthEncode.post('/:value', async (req: Request, res: Response) => {

  try {
    res.appSuccess("S1005", await authService.encode(req));    //String has been encoded with base64.
  }
  catch (e) {
    res.appFail(errorService.build(e, req.params.value));   //Unknown error during string encode attempt.  Please try again.
  }

})

routerAuthDecode.post('/:value', async (req: Request, res: Response) => {

  try {
    res.appSuccess("S1006", await authService.decode(req));        // String has been decoded with base64.
  }
  catch (e) {
    res.appFail(errorService.build(e, req.params.value));   //Unknown error during string decode attempt.  Please try again.
  }

})

routerAuthHash.post('/:value', async (req: Request, res: Response) => {

  try {
    res.appSuccess("S1007", await authService.hash(req.params.value));        // Hash string with n salt rounds
  }
  catch (e) {
    res.appFail(errorService.build(e, req.params.value));   //Unknown error during string hash attempt.  Please try again.
  }

})

// Returns firebase app configuration
routerGFAC.post('/', async (req: Request, res: Response) => {

  try {
    res.appSuccess("S0000", authService.getFirebaseAppConfig());
  }
  catch (e) {
    res.appFail(errorService.build(e, req.params.value));   //Unknown error during string hash attempt.  Please try again.
  }

})

// This route is called when user successfully logged in using his/her phone number on the client
// The intent is to store the token ID under respective users's tokens collection for token management
routerOnSignInwithPhoneNumber.post('/', async (req: Request, res: Response) => {

  try {
    const data = await authService.onSignInWithPhoneNumber(req, res);
    res.appSuccess("S0000", data);
  }
  catch (e) {
    res.appFail(errorService.build(e, req.params.value));   //Unknown error during string hash attempt.  Please try again.
  }

})

routerIsPhoneNumberRegistered.get('/:phoneNumber',
  async (req: Request, res: Response) => {

    try {
      const result = await usersService.isPhoneNumberRegistered(req.params.phoneNumber);
      res.appSuccess("S0000", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

routerVerifyRegisteredEmail.get('/',
  async (req: Request, res: Response) => {

    try {
      const token: any = req.query["token"];
      const result = await authService.verifyRegisteredEmail(token);
      res.appSuccess("S1016", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

// Sends verification email.  This is used to send email again as user demands
routerSendEmailVerification.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.sendEmailVerificationEmail(req.body.email, req.body.lang || "en-US");
      res.appSuccess("S1014", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

// Sends verification email.  This is used to send email again as user demands
routerSendEmailOTPLink.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.sendEmailOTPLink(req.body.email);
      res.appSuccess("S1015", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

// Sends verification email.  This is used to send email again as user demands
routerVerifyEmailOTPLink.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.verifyEmailOTPLink(req.body.email, req.body, res);
      res.appSuccess("S1000", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });


// Sends forgot password verification email link
routerSendForgotPasswordLink.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.sendForgotPasswordLink(req.body.email);
      res.appSuccess("S1018", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });


// Verifies forgot password link
routerVerifyForgotPasswordLink.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.verifyForgotPasswordLink(req.body.email, req.body, res);
      res.appSuccess("S1016", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

// Reset password.  Last step in forgot password process
routerResetPassword.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.resetPassword(req, res);
      res.appSuccess("S1020", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });

// Reset password.  Last step in forgot password process
routerFederatedLogin.post('/',
  async (req: Request, res: Response) => {

    try {
      const result = await authService.federatedLogin(req, res);
      res.appSuccess("S1021", result);
    }
    catch (e) {
      res.appFail(errorService.build(e, req));
    }

  });
  
   
export const register: IRoutes = { name: 'register', router: routerRegister };
export const registerID3: IRoutes = { name: 'registerID3', router: routerRegisterID3 };
export const verifyID3: IRoutes = { name: 'verifyID3', router: routerVerifyID3 };
export const login: IRoutes = { name: 'login', router: routerAuthLogin };
export const logout: IRoutes = { name: 'logout', router: routerAuthLogout };
export const createPassword: IRoutes = { name: "createPassword", router: routerAuthCreatePassword };
export const changePassword: IRoutes = { name: "changePassword", router: routerAuthChangePassword };
export const renewToken: IRoutes = { name: "renewToken", router: routerAuthRenewToken };
export const test: IRoutes = { name: "test", router: routerAuthTest_Get };
export const encode: IRoutes = { name: "encode", router: routerAuthEncode };
export const decode: IRoutes = { name: "decode", router: routerAuthDecode };
export const hash: IRoutes = { name: "hash", router: routerAuthHash };
export const gfac: IRoutes = { name: "gfac", router: routerGFAC };
export const onSignInWithPhone: IRoutes = { name: "onSignInWithPhone", router: routerOnSignInwithPhoneNumber };
export const isPhoneNumberRegistered: IRoutes = { name: "isPhoneNumberRegistered", router: routerIsPhoneNumberRegistered };
export const verifyRegisteredEmail: IRoutes = { name: "verifyRegisteredEmail", router: routerVerifyRegisteredEmail };
export const sendEmailVerification: IRoutes = { name: "sendEmailVerification", router: routerSendEmailVerification };
export const sendEmailOTPLink: IRoutes = { name: "sendEmailOTPLink", router: routerSendEmailOTPLink };
export const verifyEmailOTPLink: IRoutes = { name: "verifyEmailOTPLink", router: routerVerifyEmailOTPLink };
export const sendForgotPasswordLink: IRoutes = { name: "sendForgotPasswordLink", router: routerSendForgotPasswordLink };
export const verifyForgotPasswordLink: IRoutes = { name: "verifyForgotPasswordLink", router: routerVerifyForgotPasswordLink };
export const resetPassword: IRoutes = { name: "resetPassword", router: routerResetPassword };
export const federatedLogin: IRoutes = { name: "federatedLogin", router: routerFederatedLogin };