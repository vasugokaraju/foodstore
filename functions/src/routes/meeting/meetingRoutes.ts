import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import MeetingService from "../../services/meetingService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerMeetingPreload = Router();
const routerMeetingReadOne = Router();
const routerMeetingReadMany = Router();
const routerMeetingCreate = Router();
const routerMeetingUpdate = Router();
const routerMeetingDelete = Router();
const routerMeetingDownload = Router();
const routerMeetingUpload = Router();

const meetingService = Container.get(MeetingService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerMeetingPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("meeting", req,res,next)},
  middleware.meetingEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await meetingService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerMeetingReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("meeting", req,res,next)}, 
  middleware.meetingEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await meetingService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await meetingService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerMeetingReadMany.post('/', 
  middleware.validateToken,
  middleware.meetingEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await meetingService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerMeetingCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("meeting", req,res,next)}, 
  middleware.meetingEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await meetingService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerMeetingUpdate.put('/:id', middleware.validateToken, (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("meeting", req,res,next)}, async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const data = req.body;
    const result = await meetingService.update(id, data);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerMeetingDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("meeting", req,res,next)}, 
  middleware.meetingEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const result = await meetingService.delete(id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Downloads module data
*/
routerMeetingDownload.post('/', middleware.validateToken, middleware.meetingEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await meetingService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerMeetingUpload.post('/', middleware.validateToken, middleware.meetingEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await meetingService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});



export const meetingPreload: IRoutes = { name: 'meetingPreload', router: routerMeetingPreload };
export const meetingReadOne: IRoutes = { name: 'meetingReadOne', router: routerMeetingReadOne };
export const meetingReadMany: IRoutes = { name: "meetingReadMany", router: routerMeetingReadMany };
export const meetingCreate: IRoutes = { name: "meetingCreate", router: routerMeetingCreate };
export const meetingUpdate: IRoutes = { name: "meetingUpdate", router: routerMeetingUpdate };
export const meetingDelete: IRoutes = { name: "meetingDelete", router: routerMeetingDelete };
export const meetingDownload: IRoutes = { name: "meetingDownload", router: routerMeetingDownload };
export const meetingUpload: IRoutes = { name: "meetingUpload", router: routerMeetingUpload };