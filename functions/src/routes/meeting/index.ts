import { 
    meetingPreload, 
    meetingCreate, 
    meetingReadOne, 
    meetingReadMany, 
    meetingUpdate, 
    meetingDelete, 
    meetingDownload, 
    meetingUpload,
    } from './meetingRoutes';
import { IRoutes } from '../../interfaces';

export const meeting: IRoutes[] = [
    meetingPreload, 
    meetingCreate, 
    meetingReadOne, 
    meetingReadMany, 
    meetingUpdate, 
    meetingDelete, 
    meetingDownload, 
    meetingUpload,
    ];