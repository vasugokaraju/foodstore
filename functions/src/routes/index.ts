import { IRoutes } from '../interfaces';
import { auth } from "./auth";
import { users } from "./users";
import { products } from "./products";
import { carts } from "./carts";
import { meeting } from "./meeting";
import { appCods } from "./appCods";
import { appConf } from "./appConf";
import { reports } from "./reports"
import { upload } from "./uploadFiles"

const baseArray: IRoutes[] = [];

export const routes: IRoutes[] = baseArray.concat(users,products,carts,meeting,appCods,appConf,auth,reports, upload);

/*
export default () => {
    const app = Router();

    usersRoute(app);         // Load users route
    productsRoute(app);         // Load products route
    cartsRoute(app);         // Load carts route
    meetingRoute(app);         // Load meeting route
    appCodsRoute(app);         // Load appCods route
    appConfRoute(app);         // Load appConf route
    authRoute(app);       // Load authentication route

	return app;
}
*/