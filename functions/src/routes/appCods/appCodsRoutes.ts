import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import AppCodsService from "../../services/appCodsService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerAppCodsPreload = Router();
const routerAppCodsReadOne = Router();
const routerAppCodsReadMany = Router();
const routerAppCodsCreate = Router();
const routerAppCodsUpdate = Router();
const routerAppCodsDelete = Router();
const routerAppCodsDownload = Router();
const routerAppCodsUpload = Router();

const appCodsService = Container.get(AppCodsService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerAppCodsPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appCods", req,res,next)},
  middleware.appCodsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await appCodsService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerAppCodsReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appCods", req,res,next)}, 
  middleware.appCodsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await appCodsService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await appCodsService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerAppCodsReadMany.post('/', 
  middleware.validateToken,
  middleware.appCodsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await appCodsService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerAppCodsCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appCods", req,res,next)}, 
  middleware.appCodsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await appCodsService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerAppCodsUpdate.put('/:id', middleware.validateToken, (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appCods", req,res,next)}, async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const data = req.body;
    const result = await appCodsService.update(id, data);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerAppCodsDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("appCods", req,res,next)}, 
  middleware.appCodsEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const id = req.params.id;
    const result = await appCodsService.delete(id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Downloads module data
*/
routerAppCodsDownload.post('/', middleware.validateToken, middleware.appCodsEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await appCodsService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerAppCodsUpload.post('/', middleware.validateToken, middleware.appCodsEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await appCodsService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});



export const appCodsPreload: IRoutes = { name: 'appCodsPreload', router: routerAppCodsPreload };
export const appCodsReadOne: IRoutes = { name: 'appCodsReadOne', router: routerAppCodsReadOne };
export const appCodsReadMany: IRoutes = { name: "appCodsReadMany", router: routerAppCodsReadMany };
export const appCodsCreate: IRoutes = { name: "appCodsCreate", router: routerAppCodsCreate };
export const appCodsUpdate: IRoutes = { name: "appCodsUpdate", router: routerAppCodsUpdate };
export const appCodsDelete: IRoutes = { name: "appCodsDelete", router: routerAppCodsDelete };
export const appCodsDownload: IRoutes = { name: "appCodsDownload", router: routerAppCodsDownload };
export const appCodsUpload: IRoutes = { name: "appCodsUpload", router: routerAppCodsUpload };