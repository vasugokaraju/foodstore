import { 
    appCodsPreload, 
    appCodsCreate, 
    appCodsReadOne, 
    appCodsReadMany, 
    appCodsUpdate, 
    appCodsDelete, 
    appCodsDownload, 
    appCodsUpload,
    } from './appCodsRoutes';
import { IRoutes } from '../../interfaces';

export const appCods: IRoutes[] = [
    appCodsPreload, 
    appCodsCreate, 
    appCodsReadOne, 
    appCodsReadMany, 
    appCodsUpdate, 
    appCodsDelete, 
    appCodsDownload, 
    appCodsUpload,
    ];