import {Container} from 'typedi';
import { Router, Request, Response, NextFunction } from 'express';
import { IRoutes } from '../../interfaces';
import UsersService from "../../services/usersService";
import ErrorService from "../../services/errorService";
import middleware from "../../middleware";

const routerUsersPreload = Router();
const routerUsersReadOne = Router();
const routerUsersReadMany = Router();
const routerUsersCreate = Router();
const routerUsersUpdate = Router();
const routerUsersDelete = Router();
const routerUsersDownload = Router();
const routerUsersUpload = Router();
const routerGetUsersIdDisplayNameReadMany = Router();

const usersService = Container.get(UsersService);
let errorService = Container.get(ErrorService);

/**
  * Preload data that may contain multiple data sets related to the page.
**/
routerUsersPreload.post('/:id?', 
  middleware.validateToken,
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("users", req,res,next)},
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    const result = await usersService.preload(req.userProfile, req.body.whereQuery);

    if (result) {
        res.appSuccess("S0000",result);
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

routerUsersReadOne.get('/:id',
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("users", req,res,next)}, 
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try {
    // const result = await usersService.read(req.params.id? req.params : req.query);  // id || query parameters
    const result = await usersService.readOne(req.params.id);

    if (result.length != 0) {
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
    } else {
        res.appFail(errorService.build("E1405",req));           // No matching record found in database.
    }     
  }
  catch (e) {
      res.appFail(errorService.build(e, req));   // Unknown error during string encode attempt.  Please try again.
  }

});

/**
 * Finds records using where clause
 */
routerUsersReadMany.post('/', 
  middleware.validateToken,
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await usersService.readMany(req.body.whereQuery);
    let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
    res.appSuccess("S1400",result, customMessage);	                // Record(s) found.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }
});

routerUsersCreate.post('/', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("users", req,res,next)}, 
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const data = req.body;
    const result = await usersService.create(data);
    res.appSuccess("S1401",result,"1 record has been created.");	//Record has been created.
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerUsersUpdate.put('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("users", req,res,next)}, 
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    // the validateToken middleware verifies the token and stores current user profile in req.userProfile
    // the second parameter contains new profile (full or partial)
    const result = await usersService.update(req.userProfile, req.body);
    res.appSuccess("S1402", result, "1 record has been updated.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

routerUsersDelete.delete('/:id', 
  middleware.validateToken, 
  (req:Request, res:Response, next:NextFunction) => {middleware.validateRequest("users", req,res,next)}, 
  middleware.usersEntitlements,
  middleware.accessControlCheck,
  async (req: Request, res: Response) => {

  try{
    const result = await usersService.delete(req.params.id);
    res.appSuccess("S1403", result.value,"1 record has been deleted.");
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});


/**
  * Downloads module data
*/
routerUsersDownload.post('/', middleware.validateToken, middleware.usersEntitlements, middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await usersService.download(req.body);
    res.appSuccess("S1403", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});

/**
  * Uploads module data
*/
routerUsersUpload.post('/', middleware.validateToken, middleware.usersEntitlements,  middleware.accessControlCheck, async (req: Request, res: Response) => {

  try{
    const result = await usersService.upload(req);
    res.appSuccess("S1404", result);
  }
  catch (e){
    res.appFail(errorService.build(e, req));
  }

});


/**
* Returns Users data in the form of code-name for the purpose of Autocomplete controls on UI.
*/
routerGetUsersIdDisplayNameReadMany.post('/', 
    middleware.validateToken,
    middleware.usersEntitlements,
    middleware.accessControlCheck,
    async (req: Request, res: Response) => {

    try{
        const result = await usersService.getUsersIdDisplayNameReadMany(req);
        let customMessage = (result.length==1)? "1 record found.": result.length + " records found.";
        res.appSuccess("S0000",result, customMessage);	                // Record(s) found.
    }
    catch (e){
        res.appFail(errorService.build(e, req));
    }
});

export const usersPreload: IRoutes = { name: 'usersPreload', router: routerUsersPreload };
export const usersReadOne: IRoutes = { name: 'usersReadOne', router: routerUsersReadOne };
export const usersReadMany: IRoutes = { name: "usersReadMany", router: routerUsersReadMany };
export const usersCreate: IRoutes = { name: "usersCreate", router: routerUsersCreate };
export const usersUpdate: IRoutes = { name: "usersUpdate", router: routerUsersUpdate };
export const usersDelete: IRoutes = { name: "usersDelete", router: routerUsersDelete };
export const usersDownload: IRoutes = { name: "usersDownload", router: routerUsersDownload };
export const usersUpload: IRoutes = { name: "usersUpload", router: routerUsersUpload };
export const usersIdDisplayNameReadMany: IRoutes = { name: "usersIdDisplayNameReadMany", router: routerGetUsersIdDisplayNameReadMany };