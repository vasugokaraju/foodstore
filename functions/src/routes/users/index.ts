import { 
    usersPreload, 
    usersCreate, 
    usersReadOne, 
    usersReadMany, 
    usersUpdate, 
    usersDelete, 
    usersDownload, 
    usersUpload,
    usersIdDisplayNameReadMany,
    } from './usersRoutes';
import { IRoutes } from '../../interfaces';

export const users: IRoutes[] = [
    usersPreload, 
    usersCreate, 
    usersReadOne, 
    usersReadMany, 
    usersUpdate, 
    usersDelete, 
    usersDownload, 
    usersUpload,
    usersIdDisplayNameReadMany,
    ];