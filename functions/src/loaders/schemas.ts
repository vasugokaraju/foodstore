import { Container } from 'typedi';
// import Logger from './logger';
import ErrorService from "../services/errorService";
import fs from 'fs';
import Ajv from 'ajv'

let ajv = new Ajv({allErrors: true, async: "es7", jsonPointers: true, $data:true});

require('../modules/ajv-bson')(ajv);

export default async (): Promise<any> => {
    return new Promise( (resolve, reject) => {
        let errorService : ErrorService = Container.get(ErrorService);

        try {

            // STEP1: Get the list of schema file names from /schema folder
            let schemaFolder = __dirname + "/../schemas";
            let schemaFiles:Array<string> = fs.readdirSync(schemaFolder).filter(file => !file.endsWith(".js.map"));
            // let schemas:Array<any> = [];

            // Load all schemas
            // Although addSchema does not compile schemas, explicit compilation is not required - the schema will be compiled when it is used first time.
            schemaFiles.forEach(file => {
                let schema = require("../schemas/" + file).default;    // get each schema
                let schemaName = file.split(".")[0];
                // Because of Firebase way of loading each route as a function, this code is causing errors. trycatch is a workaround
                try {
                    ajv.addSchema(schema, schemaName);  // Add each schema. They will be compiled on first use.
                } catch (error) {
                    
                }
            });

            Container.set("AJV",ajv);

            return resolve(true);
        }catch (e) {
            Container.set("StaticCodeSnippets",{});
            return reject(errorService.build(e,null));
        }

    })
}