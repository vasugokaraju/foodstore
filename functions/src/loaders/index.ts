import {Container} from "typedi";
// import expressLoader from './express';
// import mongodbClient from './mongodbClient';
// import mongodbCollections from './mongodbCollections';

import Logger from './logger';
import jsonDataLoader from "./jsonDataLoader";
import ErrorService from "../services/errorService";
import schemas from "./schemas";
import config from '../config'


// import initialSetup from "./initialSetup";

export default async ({ expressApp }:any) => {
    let errorService : ErrorService = Container.get(ErrorService);

    try {
        
        // Load config
        Container.set("config", config);

        // Load app error codes
        await jsonDataLoader();
        // Logger.info('Error Codes are loaded.');



        // await initialSetup(mongoConnection);
        // Logger.warn('Initial setup is completed. Please comment "await initialSetup(mongoConnection)" in loaders/index.ts');

        await schemas();
        // Logger.info('Data validation schemas are loaded.');
        
        // await expressLoader({ app: expressApp });
        // Logger.info('Express loaded');


    }catch (e) {
        let errorData = errorService.build(e,null);
        let errorResolve = await errorService.resolve(errorData);
        Logger.error(JSON.stringify(errorResolve.log,null," "));

        // Terminate load process as one of the modules had failed to load.
        process.exit();
    }
}