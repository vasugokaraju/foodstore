// HTTP statuses to use in responses.
export enum HTTP_STATUS {
    OK = 200,
    Created = 201,
    Accepted = 202,
    Non_Authoritative_Information = 203,
    No_Content = 204,
    Bad_Request = 400,
    Unauthorized = 401,
    Forbidden = 403,
    Not_Found = 404,
    Method_Not_Allowed = 405,
    Duplicate_Key = 409,
    Invalid_Input = 422,
    Internal_Server_Error = 500,
    Not_Implemented = 501,
    Service_Unavailable = 503,
}

// Roles
export enum ROLES{ 
	"SUPERADMIN"=0,
	"MANAGER"=1.1,
	"SUPERVISOR"=2.2,
	"USER"=3.3
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/typeof
export enum DATATYPES {
    string= "string",
    number = "number",
    boolean = "boolean",
    undefined = "undefined",
    date = "date",
    object = "object",
    bigint = "bigint",
    symbol = "symbol",
    function = "function",
    array = "array"
}

export enum EMAIL_TEMPLATES {
    WELCOME = "welcome",
    EMAIL_VERIFICATION = "email-verification",
    EMAIL_OTP_LOGIN = "email-opt-login",
    FORGOT_PASSWORD = "forgot_password"
}