let winston = require('winston');
import admin = require('firebase-admin');
import config from '../config';

// Imports the Google Cloud client library for Winston
import { LoggingWinston } from '@google-cloud/logging-winston';

let serviceAccountConfig:any = Object.assign({},admin.credential.applicationDefault());
const loggingWinston = new LoggingWinston({projectId:serviceAccountConfig.projectId});

const transports = [];
if(process.env.NODE_ENV !== 'development') {
    // Print logs to google cloud logging
    transports.push(loggingWinston);
} else {
    // Prints logs to console window.
    transports.push(
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.cli(),
                winston.format.splat(),
            )
        })
    );

}

const LoggerInstance = winston.createLogger({
    level: config.logs.level,
    levels: winston.config.npm.levels,
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.errors({ stack: true }),
        winston.format.splat(),
        winston.format.printf( (info:any) => "\n\n" + info.message + "\n____________________________________________________________________________________________")
    ),
    transports
});

export default LoggerInstance;