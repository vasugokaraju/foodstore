import { Container } from 'typedi';
// import Logger from './logger';
import ErrorService from "../services/errorService";
const fs = require('fs');

export default async (): Promise<any> => {
    return new Promise( (resolve, reject) => {
        let errorService : ErrorService = Container.get(ErrorService);

        const dataFiles:Array<any> = [
            {
                type:"ErrorCodes",
                file:"errorCodesData.json"
            },
            {
                type:"SuccessCodes",
                file:"successCodesData.json"
            },
            {
                type:"EmailTemplates",
                file:"emailTemplatesData.json"
            }
        ]

        try {

            dataFiles.forEach( item => {
                const dataFile = __dirname + "/" + item.file;
                const jsonData = fs.readFileSync(dataFile).toString();
                const parsedData = JSON.parse(jsonData);
                Container.set(item.type,parsedData);
            });

            return resolve(true);
        }catch (e) {
            Container.set("ErrorCodes",{});
            Container.set("SuccessCodes",{});
            Container.set("EmailTemplates",{});

            return reject(errorService.build(e,null));
        }

    })
}