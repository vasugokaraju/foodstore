/**
 * In the absence of official FirebaseError class
 * https://github.com/firebase/firebase-admin-node/issues/403
 */
export default class FirebaseError extends Error{
    code:string;
    message:string;

    constructor(code:string, message:string){
        super();
        this.code = code;
        this.message = message;
    }
}