import {IAppError} from "../../interfaces/IAppInterfaces";

declare global {

    namespace Express {

        export interface Request {
            findQuery: any      // A variable that holds incoming query for CRUD operations. Middleware sets value to this variable and Routes use it.
            userProfile:any     // A variable that holds logged-in user profile. Middleware sets value to this variable and Routes use it.
        }

        export interface Response {
            // Custom response function name for success responses. Check /src/api/middleware/appResponses.ts for implementation
            appSuccess: (successCode:string, data: any, customMessage?:string) => {};
            // Custom response function name for fail responses. Check /src/api/middleware/appResponses.ts for implementation
            appFail: (error: Error | IAppError) => {};
        }
    }
}