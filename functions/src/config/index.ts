import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config({path: __dirname + "/../.env"});

if (!envFound) {
    // This error should crash whole process

    throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
    appName: <string>process.env.APP_NAME,

    protocol: <string>process.env.API_PROTOCOL,
    host: <string>process.env.API_HOST,
    port: <string>process.env.API_PORT,
    allowedOrigins: <string><unknown>process.env.ALLOWED_ORIGINS,

    ui_host: <string>process.env.UI_HOST,

    databaseURL: <string>process.env.MONGODB_URI,   // Main DB

    

    auditIndex: <string>process.env.AUDIT_INDEX,
    auditType: <string>process.env.AUDIT_TYPE,



    saltRounds: <number><unknown>process.env.SALT_ROUNDS,
    secret: <string>process.env.TOKEN_SECRET,

    cryptoSecret: <string>process.env.CRYPTO_SECRET,

    pageLimit: 5, // Default pagination page limit (rows)
    tokenLife:<string>process.env.TOKEN_LIFE, // Default token life

    authenticationFields: ["displayName", "email","emailVerified","phoneNumber","password","photoURL","disabled"],
    customClaimsFields: ["role"],

    storage_bucket: <string>process.env.STORAGE_BUCKET,
    storage_root: <string>process.env.STORAGE_ROOT,

    agora_app_id:<string>process.env.AGORA_APP_ID,
    agora_app_certificate:<string>process.env.AGORA_APP_CERTIFICATE,

    defaultLanguage: "en-US",

    api: {
        prefix: '/api',    // this makes the url look like https://0.0.0.0:443/createapi
    },

    /**
     * Used by winston logger
     */
    logs: {
        level: process.env.LOG_LEVEL || 'silly',
    },

}