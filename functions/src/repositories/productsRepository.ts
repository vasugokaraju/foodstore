import {Container} from 'typedi';
import {IProductsRepository, IProducts, IReadProducts} from "../interfaces/IProductsInterface";
import {IWhereQuery} from "../interfaces/IAppInterfaces";
import {INewProducts, IUpdateProducts } from '../interfaces/IProductsInterface';
import Helper from '../helpers/helper';
import FirebaseError from '../classes/firebaseError';
import ErrorService from "../services/errorService";
import admin = require('firebase-admin');

// const products = admin.firestore().collection('products');

class productsRepository implements IProductsRepository {
	constructor(
        private errorService:ErrorService = Container.get(ErrorService),
        private products:FirebaseFirestore.CollectionReference = admin.firestore().collection('products'),
        private helper: Helper = Container.get(Helper),
    ){
        this.errorService = Container.get(ErrorService);
    }

    // Retrieve one record.
    async read(id: string): Promise<any> {

        try {

            if (id != undefined && id.trim().length > 0) {    // If valid id is provided
                
                // Get the record
                const doc: admin.firestore.DocumentData = await this.products.doc(id).get();

                if (doc.exists) {                   // If record exists
                    const data = doc.data();        // Extract data
                    data.id = id;             // add id back to returning dataset for better handling of the data

                    const transformed:Array<IProducts> = this.helper.timestampToString([data], []);
                    return Promise.resolve(transformed);
                }
                else {
                    return Promise.resolve([]);     // If no record exists
                }
            }
            else {
                return Promise.resolve([]);         // If the id parameter is not present
            }

        } catch (e) {
            // Handle any exception
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, {id:id}));
            }
            else {
                return Promise.reject(this.errorService.build(e, {id:id}));
            }
        }
    }


    // Find records based on provided WHERE condition
    // https://cloud.google.com/appengine/docs/standard/go111/datastore/query-restrictions
    // Cannot have inequality filters on multiple properties
    // Properties used in inequality filters must be sorted first
    async readWhere(query:IWhereQuery): Promise<any> {
        
        try {
            
            // Get where query
            const whereQuery: any = await this.helper.getWhereQuery(admin.firestore().collection('products'), query);

            const snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentReference> = await whereQuery.get();

            // Extract actual data from snapshot
            let data:IReadProducts[] = snapshot.docs.map((dataItem: FirebaseFirestore.QueryDocumentSnapshot) => {
                return <IReadProducts>Object.assign({}, dataItem.data(), { id: dataItem.id });
            });

            // Convert date objects to ISO string
            data = this.helper.timestampToString<IReadProducts>(data, []);

            return Promise.resolve(data);
        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if(fireError instanceof FirebaseError){
                return Promise.reject(this.errorService.build(fireError, query));
            }
            else{
                return Promise.reject(this.errorService.build(e,query));
            }
        }
    }



    /**
     * Create new Products.
     * @param rec new Products data
     * @param lang 
     * @returns 
     */
    async create(rec: INewProducts, lang:string = "en-US"): Promise<any> {
        try {

            const data: admin.firestore.DocumentData = await this.products.add(rec);

            return Promise.resolve( Object.assign( {}, rec, {id:data.id} ) );

        } catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    // Update record
    async update(id:string, updateRec: IUpdateProducts) {
        try {

            const updatedData: admin.firestore.DocumentData = await this.products.doc(id).set(updateRec,{merge:true});
            return Promise.resolve(updatedData);

        } catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    // Delete record
    async delete(id:string) {    
        try {
            const deletedData: admin.firestore.DocumentData = await this.products.doc(id).delete();
            return Promise.resolve(deletedData);
        } catch (e) {
            return Promise.reject(this.errorService.build(e,id));
        }
    }
    



    // Bulk Insert
    async bulkInsert(data: Array<any>) {

        try {

            const db = admin.firestore();
            await this.helper.bulkInsert(db, admin.firestore().collection('products'), data);
            return Promise.resolve(true);

        } catch (error) {
            return Promise.reject(this.errorService.build(error,null));
        }

    }    
}

export default new productsRepository();