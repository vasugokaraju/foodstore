import {Container} from 'typedi';
import {ICartsRepository, ICarts, IReadCarts} from "../interfaces/ICartsInterface";
import {IWhereQuery} from "../interfaces/IAppInterfaces";
import {INewCarts, IUpdateCarts } from '../interfaces/ICartsInterface';
import Helper from '../helpers/helper';
import FirebaseError from '../classes/firebaseError';
import ErrorService from "../services/errorService";
import admin = require('firebase-admin');

// const carts = admin.firestore().collection('carts');

class cartsRepository implements ICartsRepository {
	constructor(
        private errorService:ErrorService = Container.get(ErrorService),
        private carts:FirebaseFirestore.CollectionReference = admin.firestore().collection('carts'),
        private helper: Helper = Container.get(Helper),
    ){
        this.errorService = Container.get(ErrorService);
    }

    // Retrieve one record.
    async read(id: string): Promise<any> {

        try {

            if (id != undefined && id.trim().length > 0) {    // If valid id is provided
                
                // Get the record
                const doc: admin.firestore.DocumentData = await this.carts.doc(id).get();

                if (doc.exists) {                   // If record exists
                    const data = doc.data();        // Extract data
                    data.id = id;             // add id back to returning dataset for better handling of the data

                    const transformed:Array<ICarts> = this.helper.timestampToString([data], []);
                    return Promise.resolve(transformed);
                }
                else {
                    return Promise.resolve([]);     // If no record exists
                }
            }
            else {
                return Promise.resolve([]);         // If the id parameter is not present
            }

        } catch (e) {
            // Handle any exception
            const fireError = this.helper.getFirebaseError(e);
            if (fireError instanceof FirebaseError) {
                return Promise.reject(this.errorService.build(fireError, {id:id}));
            }
            else {
                return Promise.reject(this.errorService.build(e, {id:id}));
            }
        }
    }


    // Find records based on provided WHERE condition
    // https://cloud.google.com/appengine/docs/standard/go111/datastore/query-restrictions
    // Cannot have inequality filters on multiple properties
    // Properties used in inequality filters must be sorted first
    async readWhere(query:IWhereQuery): Promise<any> {
        
        try {
            
            // Get where query
            const whereQuery: any = await this.helper.getWhereQuery(admin.firestore().collection('carts'), query);

            const snapshot: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentReference> = await whereQuery.get();

            // Extract actual data from snapshot
            let data:IReadCarts[] = snapshot.docs.map((dataItem: FirebaseFirestore.QueryDocumentSnapshot) => {
                return <IReadCarts>Object.assign({}, dataItem.data(), { id: dataItem.id });
            });

            // Convert date objects to ISO string
            data = this.helper.timestampToString<IReadCarts>(data, []);

            return Promise.resolve(data);
        } catch (e) {
            const fireError = this.helper.getFirebaseError(e);
            if(fireError instanceof FirebaseError){
                return Promise.reject(this.errorService.build(fireError, query));
            }
            else{
                return Promise.reject(this.errorService.build(e,query));
            }
        }
    }



    /**
     * Create new Carts.
     * @param rec new Carts data
     * @param lang 
     * @returns 
     */
    async create(rec: INewCarts, lang:string = "en-US"): Promise<any> {
        try {

            const where: IWhereQuery = {
                where: [
                    ["customerId","==",rec.customerId],
                    ["productId","==",rec.productId],
                ],
                limit: 1
            };
            const duplicate = await this.readWhere(where);

            if (duplicate.length > 0) {
                return Promise.reject(this.errorService.build("E1400", null));
            }
            else {
                const data: admin.firestore.DocumentData = await this.carts.add(rec);
                return Promise.resolve( Object.assign( {}, rec, {id:data.id} ) );
            }

        } catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }


    // Update record
    async update(id:string, updateRec: IUpdateCarts) {
        try {

            const where: IWhereQuery = {
                where: [
                    ["customerId","==",updateRec.customerId],
                    ["productId","==",updateRec.productId],
                ],
                limit: 1
            };
            const duplicate = await this.readWhere(where);

            if (duplicate.length > 0) {
                return Promise.reject(this.errorService.build("E1400", null));
            }
            else {
            const updatedData: admin.firestore.DocumentData = await this.carts.doc(id).update(updateRec);
            return Promise.resolve(updatedData);
            }

        } catch (e) {
            return Promise.reject(this.errorService.build(e,null));
        }
    }

    // Delete record
    async delete(id:string) {    
        try {
            const deletedData: admin.firestore.DocumentData = await this.carts.doc(id).delete();
            return Promise.resolve(deletedData);
        } catch (e) {
            return Promise.reject(this.errorService.build(e,id));
        }
    }
    



    // Bulk Insert
    async bulkInsert(data: Array<any>) {

        try {

            const db = admin.firestore();
            await this.helper.bulkInsert(db, admin.firestore().collection('carts'), data);
            return Promise.resolve(true);

        } catch (error) {
            return Promise.reject(this.errorService.build(error,null));
        }

    }    
}

export default new cartsRepository();