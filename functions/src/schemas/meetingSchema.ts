// Schema to validate user input for CRUD operations on Meeting module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/meetingSchema#",
    "title": "Meeting Data Validation Schema",
    "description": "This schema is to validate user input for Meeting module CRUD operations.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
		"mtngId": {
			"bsonType": "string",
			"minLength": 0,
			"maxLength": 50,
			"description": "Meeting ID",
			"label": "Meeting ID"
		},
		"clrPh": {
			"bsonType": "string",
			"minLength": 0,
			"maxLength": 12,
			"description": "Caller phone number",
			"label": "Caller Phone"
		},
		"rcvrPh": {
			"bsonType": "string",
			"minLength": 4,
			"maxLength": 12,
			"description": "Receiver Phone number",
			"label": "Receiver Phone"
		},
		"chnl": {
			"bsonType": "string",
			"minLength": 0,
			"maxLength": 100,
			"description": "Conference channel id",
			"label": "Channel ID"
		},
		"tkn": {
			"bsonType": "string",
			"minLength": 0,
			"maxLength": 250,
			"description": "Conference token",
			"label": "Token"
		},
		"clStts": {
			"bsonType": "number",
			"enum": [
				0,
				1,
				2,
				3,
				4,
				5
			],
			"description": "Call status. 0=New Call, 1=Call Accepted, 2=Call Rejected, 3=Running",
			"label": "Call Status"
		},
		"strtTm": {
			"bsonType": "date",
			"description": "Meeting start time",
			"label": "Meeting Start Time"
		},
		"endTm": {
			"bsonType": "date",
			"description": "Meeting end time",
			"label": "Meeting End Time"
		},
		"id": {
			"bsonType": "string",
			"description": "Record unique identifier.",
			"label": "Meeting Id"
		},

        // Query related fields
        "sort": {
            "bsonType": "string",
            "minLength": 0,
            "maxLength": 100,
            "description": "List of sort fields",
            "isNotEmpty": true
        },
        "skip": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to skip"
        },
        "limit": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to pull"
        },
        "fields": {
            "bsonType": "object",
            "description": "List of fields to project"
        },
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            }
        }        
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/meetingCreate" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": 
{
          "mtngId": {
                    "$ref": "#/definitions/mtngId"
          },
          "clrPh": {
                    "$ref": "#/definitions/clrPh"
          },
          "rcvrPh": {
                    "$ref": "#/definitions/rcvrPh"
          },
          "chnl": {
                    "$ref": "#/definitions/chnl"
          },
          "tkn": {
                    "$ref": "#/definitions/tkn"
          },
          "clStts": {
                    "$ref": "#/definitions/clStts"
          },
          "strtTm": {
                    "$ref": "#/definitions/strtTm"
          },
          "endTm": {
                    "$ref": "#/definitions/endTm"
          }
},
                        "required":["clrPh","rcvrPh"]
                    }
                }
            }
        },

        // To validate Meeting Preload request
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/meetingPreload" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery",
                                "properties": {
                                    "where": {"$ref": "#/definitions/whereQuery/where"},
                                    "orderBy": {"$ref": "#/definitions/whereQuery/orderBy"},
                                    "limit": {"$ref": "#/definitions/whereQuery/limit"},
                                    "paginationRec": {"$ref": "#/definitions/whereQuery/paginationRec"},
                                    "direction": {"$ref": "#/definitions/whereQuery/direction"}
                                },
                                "required": ["orderBy"]
                            }
                        },
                        "required": ["whereQuery"]
                    }
                }
            }
        },


        // FOR GET OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "GET" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type":"object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":[]
                    },
                    "query": {
                        "type":"object",
                        "properties": {
"sort":{ "$ref": "#/definitions/sort" },
"skip":{ "$ref": "#/definitions/skip" },
"limit":{ "$ref": "#/definitions/limit" },
"fields":{ "$ref": "#/definitions/fields" },
                        },
                        "required":[]
                    }

                }
            }
        },

        // FOR PUT OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "PUT" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":["id"]
                    },
                    "body": {
                        "type": "object",
                        "properties": 
{
          "mtngId": {
                    "$ref": "#/definitions/mtngId"
          },
          "clrPh": {
                    "$ref": "#/definitions/clrPh"
          },
          "rcvrPh": {
                    "$ref": "#/definitions/rcvrPh"
          },
          "chnl": {
                    "$ref": "#/definitions/chnl"
          },
          "tkn": {
                    "$ref": "#/definitions/tkn"
          },
          "clStts": {
                    "$ref": "#/definitions/clStts"
          },
          "strtTm": {
                    "$ref": "#/definitions/strtTm"
          },
          "endTm": {
                    "$ref": "#/definitions/endTm"
          }
},
                        "required": []
                    }
                }
            }
        },

        // FOR DELETE OPERATIONS
        
        {
            "if": {
                "properties": {
                    "method": { "const": "DELETE" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    }
                }
            }
        },


        // FOR UPLOAD OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "UPLOAD" }
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "bulkData": {
                                "bsonType": "array",
                                "items": [
                                    {
                                        "type":"object",
                                        "properties":
{
          "mtngId": {
                    "$ref": "#/definitions/mtngId"
          },
          "clrPh": {
                    "$ref": "#/definitions/clrPh"
          },
          "rcvrPh": {
                    "$ref": "#/definitions/rcvrPh"
          },
          "chnl": {
                    "$ref": "#/definitions/chnl"
          },
          "tkn": {
                    "$ref": "#/definitions/tkn"
          },
          "clStts": {
                    "$ref": "#/definitions/clStts"
          },
          "strtTm": {
                    "$ref": "#/definitions/strtTm"
          },
          "endTm": {
                    "$ref": "#/definitions/endTm"
          }
},
                                            "required":["clrPh","rcvrPh"]
                                    }
                                ]
                            }
                        },
                        "required": ["bulkData"],
                        "additionalProperties": false
                    }
                }
            }
        },

        
    ]
}