// Schema to validate user input for CRUD operations on AppConf module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/appConfSchema#",
    "title": "AppConf Data Validation Schema",
    "description": "This schema is to validate user input for AppConf module CRUD operations.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
        "lng": {
            "bsonType": "string",
            "minLength": 5,
            "maxLength": 5,
            "description": "Language",
            "label": "Language"
        },
        "lbl": {
            "bsonType": "string",
            "minLength": 3,
            "maxLength": 50,
            "description": "Label that represent the data",
            "label": "Label"
        },
        "data": {
            "bsonType": "object",
            "description": "Data",
            "label": "Data",
            "properties": {}
        },
        "id": {
            "bsonType": "string",
            "description": "Record unique identifier.",
            "label": "AppConf Id"
        },

        // Query related fields
        "sort": {
            "bsonType": "string",
            "minLength": 0,
            "maxLength": 100,
            "description": "List of sort fields",
            "isNotEmpty": true
        },
        "skip": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to skip"
        },
        "limit": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to pull"
        },
        "fields": {
            "bsonType": "object",
            "description": "List of fields to project"
        },
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            }
        }
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/appConfCreate" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "lng": {
                                "$ref": "#/definitions/lng"
                            },
                            "lbl": {
                                "$ref": "#/definitions/lbl"
                            },
                            "data": {
                                "anyOf":[
                                    {
                                        "$ref": "#/definitions/data",
                                        "description":"Value could be an object",
                                        "properties": {},
                                        "required": []
                                    },
                                    {
                                        "type":"array",
                                        "description":"Value could be an array",
                                        "items":{
                                            "$ref": "#/definitions/data",
                                        }
                                    }                                    
                                ]
                            }
                        },
                        "required": ["lbl", "lng", "data"]
                    }
                }
            }
        },

        // To validate AppConf Preload request
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/appConfPreload" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery",
                                "properties": {
                                    "where": { "$ref": "#/definitions/whereQuery/where" },
                                    "orderBy": { "$ref": "#/definitions/whereQuery/orderBy" },
                                    "limit": { "$ref": "#/definitions/whereQuery/limit" },
                                    "paginationRec": { "$ref": "#/definitions/whereQuery/paginationRec" },
                                    "direction": { "$ref": "#/definitions/whereQuery/direction" }
                                },
                                "required": ["orderBy"]
                            }
                        },
                        "required": ["whereQuery"]
                    }
                }
            }
        },


        // FOR GET OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "GET" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
                            "id": { "$ref": "#/definitions/id" },
                        },
                        "required": []
                    },
                    "query": {
                        "type": "object",
                        "properties": {
                            "sort": { "$ref": "#/definitions/sort" },
                            "skip": { "$ref": "#/definitions/skip" },
                            "limit": { "$ref": "#/definitions/limit" },
                            "fields": { "$ref": "#/definitions/fields" },
                        },
                        "required": []
                    }

                }
            }
        },

        // FOR PUT OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "PUT" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
                            "id": { "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    },
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "lng": {
                                "$ref": "#/definitions/lng"
                            },
                            "lbl": {
                                "$ref": "#/definitions/lbl"
                            },
                            "data": {
                                "anyOf":[
                                    {
                                        "$ref": "#/definitions/data",
                                        "description":"Value could be an object",
                                        "properties": {},
                                        "required": []
                                    },
                                    {
                                        "type":"array",
                                        "description":"Value could be an array",
                                        "items":{
                                            "$ref": "#/definitions/data",
                                        }
                                    }                                    
                                ]
                            }
                        },
                        "required": []
                    }
                }
            }
        },

        // FOR DELETE OPERATIONS

        {
            "if": {
                "properties": {
                    "method": { "const": "DELETE" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
                            "id": { "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    }
                }
            }
        },


        // FOR UPLOAD OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "UPLOAD" }
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "bulkData": {
                                "bsonType": "array",
                                "items": [
                                    {
                                        "type": "object",
                                        "properties":
                                        {
                                            "lng": {
                                                "$ref": "#/definitions/lng"
                                            },
                                            "lbl": {
                                                "$ref": "#/definitions/lbl"
                                            },
                                            "data": {
                                                "anyOf":[
                                                    {
                                                        "$ref": "#/definitions/data",
                                                        "description":"Value could be an object",
                                                        "properties": {},
                                                        "required": []
                                                    },
                                                    {
                                                        "type":"array",
                                                        "description":"Value could be an array",
                                                        "items":{
                                                            "$ref": "#/definitions/data",
                                                        }
                                                    }                                    
                                                ]
                                            }
                                        },
                                        "required": ["lbl", "lng", "data"]
                                    }
                                ]
                            }
                        },
                        "required": ["bulkData"],
                        "additionalProperties": false
                    }
                }
            }
        },

    ]
}