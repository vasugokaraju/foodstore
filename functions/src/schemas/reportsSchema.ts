// Schema to validate user input for CRUD operations on Users module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/reportsSchema#",
    "title": "Repoets Query Validation Schema",
    "description": "This schema is to validate reports where query.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            },
            "required":["orderBy"]
        }        
    },

    "allOf": [

    
        // FOR QUERY OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/summaryReport" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery"
                            }
                        },
                        "required": ["whereQuery"],
                        "additionalProperties": false
                    }
                }
                
            }
        },

    
        // FOR QUERY OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/financeReport" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery"
                            }
                        },
                        "required": ["whereQuery"],
                        "additionalProperties": false
                    }
                }
                
            }
        },

    ]
}