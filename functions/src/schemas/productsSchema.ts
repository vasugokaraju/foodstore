// Schema to validate user input for CRUD operations on Products module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/productsSchema#",
    "title": "Products Data Validation Schema",
    "description": "This schema is to validate user input for Products module CRUD operations.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
		"name": {
			"bsonType": "string",
			"minLength": 5,
			"maxLength": 50,
			"description": "Name of the product",
			"label": "Product Name"
		},
		"desc": {
			"bsonType": "string",
			"minLength": 5,
			"maxLength": 200,
			"description": "Description of the product",
			"label": "Product Description"
		},
		"price": {
			"bsonType": "decimal",
			"description": "Product price",
			"label": "Unit Price"
		},
		"id": {
			"bsonType": "string",
			"description": "Record unique identifier.",
			"label": "Products Id"
		},

        // Query related fields
        "sort": {
            "bsonType": "string",
            "minLength": 0,
            "maxLength": 100,
            "description": "List of sort fields",
            "isNotEmpty": true
        },
        "skip": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to skip"
        },
        "limit": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to pull"
        },
        "fields": {
            "bsonType": "object",
            "description": "List of fields to project"
        },
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            }
        }        
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/productsCreate" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": 
{
          "name": {
                    "$ref": "#/definitions/name"
          },
          "desc": {
                    "$ref": "#/definitions/desc"
          },
          "price": {
                    "$ref": "#/definitions/price"
          }
},
                        "required":["name"]
                    }
                }
            }
        },

        // To validate Products Preload request
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/productsPreload" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery",
                                "properties": {
                                    "where": {"$ref": "#/definitions/whereQuery/where"},
                                    "orderBy": {"$ref": "#/definitions/whereQuery/orderBy"},
                                    "limit": {"$ref": "#/definitions/whereQuery/limit"},
                                    "paginationRec": {"$ref": "#/definitions/whereQuery/paginationRec"},
                                    "direction": {"$ref": "#/definitions/whereQuery/direction"}
                                },
                                "required": ["orderBy"]
                            }
                        },
                        "required": ["whereQuery"]
                    }
                }
            }
        },


        // FOR GET OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "GET" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type":"object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":[]
                    },
                    "query": {
                        "type":"object",
                        "properties": {
"sort":{ "$ref": "#/definitions/sort" },
"skip":{ "$ref": "#/definitions/skip" },
"limit":{ "$ref": "#/definitions/limit" },
"fields":{ "$ref": "#/definitions/fields" },
                        },
                        "required":[]
                    }

                }
            }
        },

        // FOR PUT OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "PUT" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":["id"]
                    },
                    "body": {
                        "type": "object",
                        "properties": 
{
          "name": {
                    "$ref": "#/definitions/name"
          },
          "desc": {
                    "$ref": "#/definitions/desc"
          },
          "price": {
                    "$ref": "#/definitions/price"
          }
},
                        "required": []
                    }
                }
            }
        },

        // FOR DELETE OPERATIONS
        
        {
            "if": {
                "properties": {
                    "method": { "const": "DELETE" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    }
                }
            }
        },


        // FOR UPLOAD OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "UPLOAD" }
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "bulkData": {
                                "bsonType": "array",
                                "items": [
                                    {
                                        "type":"object",
                                        "properties":
{
          "name": {
                    "$ref": "#/definitions/name"
          },
          "desc": {
                    "$ref": "#/definitions/desc"
          },
          "price": {
                    "$ref": "#/definitions/price"
          }
},
                                            "required":["name"]
                                    }
                                ]
                            }
                        },
                        "required": ["bulkData"],
                        "additionalProperties": false
                    }
                }
            }
        },

        
    ]
}