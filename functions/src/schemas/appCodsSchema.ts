// Schema to validate user input for CRUD operations on AppCods module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/appCodsSchema#",
    "title": "AppCods Data Validation Schema",
    "description": "This schema is to validate user input for AppCods module CRUD operations.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
		"lng": {
			"bsonType": "string",
			"minLength": 5,
			"maxLength": 5,
			"description": "Language",
			"label": "Language"
		},
		"code": {
			"bsonType": "string",
			"minLength": 5,
			"maxLength": 50,
			"description": "application code. Ex: E0000, S0000",
			"label": "App Code"
		},
		"titl": {
			"bsonType": "string",
			"minLength": 5,
			"maxLength": 100,
			"description": "App code short description",
			"label": "Title"
		},
		"msg": {
			"bsonType": "string",
			"minLength": 10,
			"maxLength": 100,
			"description": "App code long description",
			"label": "Message"
		},
		"httpCode": {
			"bsonType": "int",
			"description": "HTTP code associated with the app code",
			"label": "HTTP Code"
		},
		"actn": {
			"bsonType": "int",
			"description": "Log action code.",
			"label": "Log Action"
		},
		"id": {
			"bsonType": "string",
			"description": "Record unique identifier.",
			"label": "AppCods Id"
		},

        // Query related fields
        "sort": {
            "bsonType": "string",
            "minLength": 0,
            "maxLength": 100,
            "description": "List of sort fields",
            "isNotEmpty": true
        },
        "skip": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to skip"
        },
        "limit": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to pull"
        },
        "fields": {
            "bsonType": "object",
            "description": "List of fields to project"
        },
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            }
        }        
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/appCodsCreate" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": 
{
          "lng": {
                    "$ref": "#/definitions/lng"
          },
          "code": {
                    "$ref": "#/definitions/code"
          },
          "titl": {
                    "$ref": "#/definitions/titl"
          },
          "msg": {
                    "$ref": "#/definitions/msg"
          },
          "httpCode": {
                    "$ref": "#/definitions/httpCode"
          },
          "actn": {
                    "$ref": "#/definitions/actn"
          }
},
                        "required":["lng","code","titl","msg","httpCode","actn"]
                    }
                }
            }
        },

        // To validate AppCods Preload request
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/appCodsPreload" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery",
                                "properties": {
                                    "where": {"$ref": "#/definitions/whereQuery/where"},
                                    "orderBy": {"$ref": "#/definitions/whereQuery/orderBy"},
                                    "limit": {"$ref": "#/definitions/whereQuery/limit"},
                                    "paginationRec": {"$ref": "#/definitions/whereQuery/paginationRec"},
                                    "direction": {"$ref": "#/definitions/whereQuery/direction"}
                                },
                                "required": ["orderBy"]
                            }
                        },
                        "required": ["whereQuery"]
                    }
                }
            }
        },


        // FOR GET OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "GET" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type":"object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":[]
                    },
                    "query": {
                        "type":"object",
                        "properties": {
"sort":{ "$ref": "#/definitions/sort" },
"skip":{ "$ref": "#/definitions/skip" },
"limit":{ "$ref": "#/definitions/limit" },
"fields":{ "$ref": "#/definitions/fields" },
                        },
                        "required":[]
                    }

                }
            }
        },

        // FOR PUT OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "PUT" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":["id"]
                    },
                    "body": {
                        "type": "object",
                        "properties": 
{
          "lng": {
                    "$ref": "#/definitions/lng"
          },
          "code": {
                    "$ref": "#/definitions/code"
          },
          "titl": {
                    "$ref": "#/definitions/titl"
          },
          "msg": {
                    "$ref": "#/definitions/msg"
          },
          "httpCode": {
                    "$ref": "#/definitions/httpCode"
          },
          "actn": {
                    "$ref": "#/definitions/actn"
          }
},
                        "required": []
                    }
                }
            }
        },

        // FOR DELETE OPERATIONS
        
        {
            "if": {
                "properties": {
                    "method": { "const": "DELETE" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    }
                }
            }
        },


        // FOR UPLOAD OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "UPLOAD" }
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "bulkData": {
                                "bsonType": "array",
                                "items": [
                                    {
                                        "type":"object",
                                        "properties":
{
          "lng": {
                    "$ref": "#/definitions/lng"
          },
          "code": {
                    "$ref": "#/definitions/code"
          },
          "titl": {
                    "$ref": "#/definitions/titl"
          },
          "msg": {
                    "$ref": "#/definitions/msg"
          },
          "httpCode": {
                    "$ref": "#/definitions/httpCode"
          },
          "actn": {
                    "$ref": "#/definitions/actn"
          }
},
                                            "required":["lng","code","titl","msg","httpCode","actn"]
                                    }
                                ]
                            }
                        },
                        "required": ["bulkData"],
                        "additionalProperties": false
                    }
                }
            }
        },

        
    ]
}