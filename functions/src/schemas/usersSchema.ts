// Schema to validate user input for CRUD operations on Users module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/usersSchema#",
    "title": "Users Data Validation Schema",
    "description": "This schema is to validate user input for Users module CRUD operations.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
		"displayName": {
			"bsonType": "string",
			"minLength": 3,
			"maxLength": 50,
			"description": "User name",
			"label": "User Name"
		},
		"email": {
			"bsonType": "string",
			"minLength": 3,
			"maxLength": 50,
			"description": "User login id.",
			"label": "Email"
		},
		"password": {
			"bsonType": "string",
			"minLength": 3,
			"maxLength": 50,
			"description": "Password",
			"label": "Password"
		},
		"phoneNumber": {
			"bsonType": "string",
			"minLength": 10,
			"maxLength": 12,
			"description": "Phone Number",
			"label": "Phone Number"
		},
		"emailVerified": {
			"bsonType": "boolean",
			"description": "Indicates whether the email is verified.",
			"label": "Email Verified"
		},
		"disabled": {
			"bsonType": "boolean",
			"description": "Indicates whether the user is active.",
			"label": "Disabled"
		},
		"photoURL": {
			"bsonType": "string",
			"minLength": 0,
			"maxLength": 100,
			"description": "User Phogo URL",
			"label": "Photo URL"
		},
		"role": {
			"bsonType": "object",
			"description": "User role.",
			"label": "Role",
			"properties": {
				"code": {
					"bsonType": "decimal",
					"label": "Role Code",
					"description": "User role code"
				},
				"name": {
					"bsonType": "string",
					"minLength": 3,
					"maxLength": 100,
					"label": "Role Name",
					"description": "User role name"
				}
			}
		},
		"dob": {
			"bsonType": "date",
			"description": "Date of birth.",
			"label": "Date of Birth"
		},
		"ssn": {
			"bsonType": "int",
			"description": "Social Security Number",
			"label": "SSN"
		},
		"address": {
			"bsonType": "object",
			"description": "User address.",
			"label": "Address",
			"properties": {
				"city": {
					"bsonType": "object",
					"label": "City",
					"properties": {
						"colony": {
							"bsonType": "string",
							"description": "colony.",
							"label": "Colony"
						},
						"foundedon": {
							"bsonType": "date",
							"description": "foundedon.",
							"label": "FoundedOn"
						}
					}
				},
				"country": {
					"bsonType": "string",
					"label": "Country",
					"minLength": 2,
					"maxLength": 100
				}
			}
		},
		"id": {
			"bsonType": "string",
			"description": "Record unique identifier.",
			"label": "Users Id"
		},

        // Query related fields
        "sort": {
            "bsonType": "string",
            "minLength": 0,
            "maxLength": 100,
            "description": "List of sort fields",
            "isNotEmpty": true
        },
        "skip": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to skip"
        },
        "limit": {
            "bsonType": "integer",
            "minimum": 0,
            "maximum": 100000,
            "description": "Number of records to pull"
        },
        "fields": {
            "bsonType": "object",
            "description": "List of fields to project"
        },
        "whereQuery": {
            "bsonType": "object",
            "description": "Where query object with query, sort and filter values",
            "label": "where query",
            "properties": {
                "where": {
                    "bsonType": "array",
                    "label": "where",
                    "minLength": 0,
                    "maxLength": 100,
                    "items": [
                        { "bsonType": "string" },
                        {
                            "bsonType": "array",
                            "items": {
                                "bsonType": "string"
                            }
                        }
                    ]
                },
                "orderBy": {
                    "bsonType": "array",
                    "label": "order by",
                    "minLength": 0,
                    "maxLength": 100
                },
                "limit": {
                    "bsonType": "integer",
                    "minimum": 0,
                    "maximum": 100000,
                    "description": "Number of records to pull"
                },
                "direction": {
                    "bsonType": "integer",
                    "minimum": -1,
                    "maximum": 1,
                    "description": "Number of records to pull"
                },
                "paginationRec": {
                    "bsonType": "object",
                    "description": "Pagination record for firestore use."
                }
            }
        }        
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/usersCreate" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": 
{
          "displayName": {
                    "$ref": "#/definitions/displayName"
          },
          "email": {
                    "$ref": "#/definitions/email"
          },
          "password": {
                    "$ref": "#/definitions/password"
          },
          "phoneNumber": {
                    "$ref": "#/definitions/phoneNumber"
          },
          "emailVerified": {
                    "$ref": "#/definitions/emailVerified"
          },
          "disabled": {
                    "$ref": "#/definitions/disabled"
          },
          "photoURL": {
                    "$ref": "#/definitions/photoURL"
          },
          "role": {
                    "$ref": "#/definitions/role",
                    "properties": {
                              "code": {
                                        "$ref": "#/definitions/role/code"
                              },
                              "name": {
                                        "$ref": "#/definitions/role/name"
                              }
                    },
                    "required": []
          },
          "dob": {
                    "$ref": "#/definitions/dob"
          },
          "ssn": {
                    "$ref": "#/definitions/ssn"
          },
          "address": {
                    "$ref": "#/definitions/address",
                    "properties": {
                              "city": {
                                        "$ref": "#/definitions/address/city",
                                        "properties": {
                                                  "colony": {
                                                            "$ref": "#/definitions/address/city/colony"
                                                  },
                                                  "foundedon": {
                                                            "$ref": "#/definitions/address/city/foundedon"
                                                  }
                                        },
                                        "required": [
                                                  "colony"
                                        ]
                              },
                              "country": {
                                        "$ref": "#/definitions/address/country"
                              }
                    },
                    "required": [
                              "city",
                              "country"
                    ]
          }
},
                        "required":["displayName","email","phoneNumber","role","dob","address"]
                    }
                }
            }
        },

        // To validate Users Preload request
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/usersPreload" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "whereQuery": {
                                "$ref": "#/definitions/whereQuery",
                                "properties": {
                                    "where": {"$ref": "#/definitions/whereQuery/where"},
                                    "orderBy": {"$ref": "#/definitions/whereQuery/orderBy"},
                                    "limit": {"$ref": "#/definitions/whereQuery/limit"},
                                    "paginationRec": {"$ref": "#/definitions/whereQuery/paginationRec"},
                                    "direction": {"$ref": "#/definitions/whereQuery/direction"}
                                },
                                "required": ["orderBy"]
                            }
                        },
                        "required": ["whereQuery"]
                    }
                }
            }
        },

        // User registration schema
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "baseUrl": { "const": "/api/register" },
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties": 
{
          "displayName": {
                    "$ref": "#/definitions/displayName"
          },
          "email": {
                    "$ref": "#/definitions/email"
          },
          "password": {
                    "$ref": "#/definitions/password"
          },
          "phoneNumber": {
                    "$ref": "#/definitions/phoneNumber"
          },
          "emailVerified": {
                    "$ref": "#/definitions/emailVerified"
          },
          "disabled": {
                    "$ref": "#/definitions/disabled"
          },
          "photoURL": {
                    "$ref": "#/definitions/photoURL"
          },
          "role": {
                    "$ref": "#/definitions/role",
                    "properties": {
                              "code": {
                                        "$ref": "#/definitions/role/code"
                              },
                              "name": {
                                        "$ref": "#/definitions/role/name"
                              }
                    },
                    "required": []
          },
          "dob": {
                    "$ref": "#/definitions/dob"
          },
          "ssn": {
                    "$ref": "#/definitions/ssn"
          },
          "address": {
                    "$ref": "#/definitions/address",
                    "properties": {
                              "city": {
                                        "$ref": "#/definitions/address/city",
                                        "properties": {
                                                  "colony": {
                                                            "$ref": "#/definitions/address/city/colony"
                                                  },
                                                  "foundedon": {
                                                            "$ref": "#/definitions/address/city/foundedon"
                                                  }
                                        },
                                        "required": [
                                                  "colony"
                                        ]
                              },
                              "country": {
                                        "$ref": "#/definitions/address/country"
                              }
                    },
                    "required": [
                              "city",
                              "country"
                    ]
          }
},
                        "required":["displayName","email","phoneNumber","role","dob","address"]
                    }
                }
            }
        },        

        // FOR GET OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "GET" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type":"object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":[]
                    },
                    "query": {
                        "type":"object",
                        "properties": {
"sort":{ "$ref": "#/definitions/sort" },
"skip":{ "$ref": "#/definitions/skip" },
"limit":{ "$ref": "#/definitions/limit" },
"fields":{ "$ref": "#/definitions/fields" },
                        },
                        "required":[]
                    }

                }
            }
        },

        // FOR PUT OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "PUT" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required":["id"]
                    },
                    "body": {
                        "type": "object",
                        "properties": 
{
          "displayName": {
                    "$ref": "#/definitions/displayName"
          },
          "email": {
                    "$ref": "#/definitions/email"
          },
          "password": {
                    "$ref": "#/definitions/password"
          },
          "phoneNumber": {
                    "$ref": "#/definitions/phoneNumber"
          },
          "emailVerified": {
                    "$ref": "#/definitions/emailVerified"
          },
          "disabled": {
                    "$ref": "#/definitions/disabled"
          },
          "photoURL": {
                    "$ref": "#/definitions/photoURL"
          },
          "role": {
                    "$ref": "#/definitions/role",
                    "properties": {
                              "code": {
                                        "$ref": "#/definitions/role/code"
                              },
                              "name": {
                                        "$ref": "#/definitions/role/name"
                              }
                    },
                    "required": []
          },
          "dob": {
                    "$ref": "#/definitions/dob"
          },
          "ssn": {
                    "$ref": "#/definitions/ssn"
          },
          "address": {
                    "$ref": "#/definitions/address",
                    "properties": {
                              "city": {
                                        "$ref": "#/definitions/address/city",
                                        "properties": {
                                                  "colony": {
                                                            "$ref": "#/definitions/address/city/colony"
                                                  },
                                                  "foundedon": {
                                                            "$ref": "#/definitions/address/city/foundedon"
                                                  }
                                        },
                                        "required": [
                                                  "colony"
                                        ]
                              },
                              "country": {
                                        "$ref": "#/definitions/address/country"
                              }
                    },
                    "required": [
                              "city",
                              "country"
                    ]
          }
},
                        "required": []
                    }
                }
            }
        },

        // FOR DELETE OPERATIONS
        
        {
            "if": {
                "properties": {
                    "method": { "const": "DELETE" }
                }
            },
            "then": {
                "properties": {
                    "params": {
                        "type": "object",
                        "properties": {
"id":{ "$ref": "#/definitions/id" },
                        },
                        "required": ["id"]
                    }
                }
            }
        },


        // FOR UPLOAD OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "UPLOAD" }
                }
            },
            "then": {
                "properties": {
                    "body": {
                        "type": "object",
                        "properties":
                        {
                            "bulkData": {
                                "bsonType": "array",
                                "items": [
                                    {
                                        "type":"object",
                                        "properties":
{
          "displayName": {
                    "$ref": "#/definitions/displayName"
          },
          "email": {
                    "$ref": "#/definitions/email"
          },
          "password": {
                    "$ref": "#/definitions/password"
          },
          "phoneNumber": {
                    "$ref": "#/definitions/phoneNumber"
          },
          "emailVerified": {
                    "$ref": "#/definitions/emailVerified"
          },
          "disabled": {
                    "$ref": "#/definitions/disabled"
          },
          "photoURL": {
                    "$ref": "#/definitions/photoURL"
          },
          "role": {
                    "$ref": "#/definitions/role",
                    "properties": {
                              "code": {
                                        "$ref": "#/definitions/role/code"
                              },
                              "name": {
                                        "$ref": "#/definitions/role/name"
                              }
                    },
                    "required": []
          },
          "dob": {
                    "$ref": "#/definitions/dob"
          },
          "ssn": {
                    "$ref": "#/definitions/ssn"
          },
          "address": {
                    "$ref": "#/definitions/address",
                    "properties": {
                              "city": {
                                        "$ref": "#/definitions/address/city",
                                        "properties": {
                                                  "colony": {
                                                            "$ref": "#/definitions/address/city/colony"
                                                  },
                                                  "foundedon": {
                                                            "$ref": "#/definitions/address/city/foundedon"
                                                  }
                                        },
                                        "required": [
                                                  "colony"
                                        ]
                              },
                              "country": {
                                        "$ref": "#/definitions/address/country"
                              }
                    },
                    "required": [
                              "city",
                              "country"
                    ]
          }
},
                                            "required":["displayName","email","phoneNumber","role","dob","address"]
                                    }
                                ]
                            }
                        },
                        "required": ["bulkData"],
                        "additionalProperties": false
                    }
                }
            }
        },

        
    ]
}