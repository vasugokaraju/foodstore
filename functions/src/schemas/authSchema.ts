// Schema to validate user input for CRUD operations on auth module;

export default {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/schemas/authSchema.ts#",
    "title": "Authentication Data Validation Schema",
    "description": "This schema is to validate authenticated route calls.",
    "type": "object",
    "$async": true,
    "required": [],

    "definitions": {
		"authorization": {
			"bsonType": "string",
            "description": "authorization header that holds token value"
		},
		"loggedin": {
			"bsonType": "string",
            "description": "Flag to indicate whether user is logged-in on the client.",
            "enum":["true","false"]
		},
    },

    "allOf": [
        // FOR POST OPERATIONS
        {
            "if": {
                "properties": {
                    "method": { "const": "POST" },
                    "originalUrl": { "const": "/api/renewToken/" }
                }
            },
            "then": {
                "properties": {
                    "headers": {
                        "type": "object",
                        "properties": {
							"authorization": { "$ref": "#/definitions/authorization"},
							"loggedin": { "$ref": "#/definitions/loggedin"}
                        },
                        "required":["authorization","loggedin"]
                    }
                }
            }
        }
    ]
}