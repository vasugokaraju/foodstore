// This interface forces data access layer classes to implement the following methods
export interface ICartsRepository {
    create: (newRec:INewCarts) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateCarts, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface ICarts {
	customerId:string;
	customerName:string;
	productId:string;
	productName:string;
}

// Interface for new Carts data object.
export interface INewCarts {
	customerId:string;
	customerName:string;
	productId:string;
	productName:string;
}

// Interface to update Carts data.
export interface IUpdateCarts {
	customerId:string;
	customerName?:string;
	productId:string;
	productName?:string;
}

// Interface for read Carts data
export interface IReadCarts extends ICarts {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.