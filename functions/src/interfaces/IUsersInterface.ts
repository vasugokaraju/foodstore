// This interface forces data access layer classes to implement the following methods
export interface IUsersRepository {
    create: (newRec:INewUsers) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateUsers, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface IUsers {
	displayName:string;
	email:string;
	password:string;
	phoneNumber:string;
	emailVerified:boolean;
	disabled:boolean;
	photoURL:string;
	role:object;
	dob:Date;
	ssn:number;
	address:object;
}

// Interface for new Users data object.
export interface INewUsers {
	displayName:string;
	email:string;
	password:string;
	phoneNumber:string;
	emailVerified:boolean;
	disabled:boolean;
	photoURL:string;
	role:object;
	dob:Date;
	ssn:number;
	address:object;
}

// Interface to update Users data.
export interface IUpdateUsers {
	displayName:string;
	email:string;
	password:string;
	phoneNumber:string;
	emailVerified?:boolean;
	disabled?:boolean;
	photoURL?:string;
	role:object;
	dob:Date;
	ssn?:number;
	address?:object;
}

// Interface for read Users data
export interface IReadUsers extends IUsers {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.