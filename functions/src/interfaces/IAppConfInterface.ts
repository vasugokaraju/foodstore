// This interface forces data access layer classes to implement the following methods
export interface IAppConfRepository {
    create: (newRec:INewAppConf) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateAppConf, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface IAppConf {
	lng:string;
	lbl:string;
	data:object;
}

// Interface for new AppConf data object.
export interface INewAppConf {
	lng:string;
	lbl:string;
	data:object;
}

// Interface to update AppConf data.
export interface IUpdateAppConf {
	lng:string;
	lbl:string;
	data:object;
}

// Interface for read AppConf data
export interface IReadAppConf extends IAppConf {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.