// This interface forces data access layer classes to implement the following methods
export interface IAppCodsRepository {
    create: (newRec:INewAppCods) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateAppCods, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface IAppCods {
	lng:string;
	code:string;
	titl:string;
	msg:string;
	httpCode:number;
	actn:number;
}

// Interface for new AppCods data object.
export interface INewAppCods {
	lng:string;
	code:string;
	titl:string;
	msg:string;
	httpCode:number;
	actn:number;
}

// Interface to update AppCods data.
export interface IUpdateAppCods {
	lng:string;
	code:string;
	titl:string;
	msg:string;
	httpCode:number;
	actn:number;
}

// Interface for read AppCods data
export interface IReadAppCods extends IAppCods {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.