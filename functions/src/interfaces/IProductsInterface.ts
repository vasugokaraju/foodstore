// This interface forces data access layer classes to implement the following methods
export interface IProductsRepository {
    create: (newRec:INewProducts) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateProducts, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface IProducts {
	name:string;
	desc:string;
	price:number;
}

// Interface for new Products data object.
export interface INewProducts {
	name:string;
	desc:string;
	price:number;
}

// Interface to update Products data.
export interface IUpdateProducts {
	name:string;
	desc?:string;
	price?:number;
}

// Interface for read Products data
export interface IReadProducts extends IProducts {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.