// This interface forces data access layer classes to implement the following methods
export interface IMeetingRepository {
    create: (newRec:INewMeeting) => Promise<any>;
    read: (id:string) => Promise<any>;
    update: (updateQuery:string, updateData: IUpdateMeeting, options?:any) => Promise<any>;
    delete: (deleteQuery:string) => Promise<any>;
}

// Interface for complete record
export interface IMeeting {
	mtngId:string;
	clrPh:string;
	rcvrPh:string;
	chnl:string;
	tkn:string;
	clStts:number;
	strtTm:Date;
	endTm:Date;
}

// Interface for new Meeting data object.
export interface INewMeeting {
	mtngId:string;
	clrPh:string;
	rcvrPh:string;
	chnl:string;
	tkn:string;
	clStts:number;
	strtTm:Date;
	endTm:Date;
}

// Interface to update Meeting data.
export interface IUpdateMeeting {
	mtngId?:string;
	clrPh:string;
	rcvrPh:string;
	chnl?:string;
	tkn?:string;
	clStts?:number;
	strtTm?:Date;
	endTm?:Date;
}

// Interface for read Meeting data
export interface IReadMeeting extends IMeeting {
    id:string;
}

// Use IFindQuery2 from IAppInterfaces for Update and Delete operations.