// App specific error type
export interface IAppError {
    errorCode:string;       // Holds pre-defined error code
    errorData:any;           // Holds more info about error. Generally the exception data is provided by the database, app
    errorStack:Array<any>;  // Holds the error location and payload
}

// Pre-defined errorInfo record
export interface IAppErrorCode {
    title:string;           // Error title
    message:string;       // Error message
    httpCode:number;      // HTTP response code
    action:number;         // 0=no logging, 1=log, 2=log and email to admin, 3=log and email to jira
}

// App specific success type
export interface IAppSuccess {
    successCode:string;       // Holds pre-defined success code
    successData:any;           // Holds more info about success. Generally the exception data is provided by the database, app
}

// Pre-defined successInfo record
export interface IAppSuccessCode {
    title:string;           // Success title
    message:string;       // Success message
    httpCode:number;      // HTTP response code
    action:number;         // 0=no logging, 1=log, 2=log and email to admin, 3=log and email to jira
}

// App Code type
export interface IAppCode {
    code:string;           // App code
    title:string;           // title
    message:string;
    httpCode:number;      // HTTP response code
    action:number;         // 0=no logging, 1=log, 2=log and email to admin, 3=log and email to jira
}

// Interface for audit log record
export interface IAuditLog {
	"Action": string;
	"ModifiedTime": Date;
	"Comments": string;
	"SourceId": string;
	"SourceName": string;
	"SerializedEntity": string;
	"ModifiedBy": string;
}

export interface INewElasticSearch {
	index:string,
	id:string,
	type:string,
	body:any
}

// Interface for find query
export interface IFindQuery {
    id?:string;
    query?:object;
    project?:object;
    skip?:number;
    limit?:number;    
    sort?:object;
}

// Interface for Update and Delete query
export interface IFindQuery2 {
    _id:string;
}

// Interface to be used by every module specific service.
export interface ICRUDServiceInterface{
    idExists:(_id:string) => Promise<boolean>;      // Implement this method to check whether the record exist.
    readOne: (id:string) => Promise<any>;              // The idExists method uses the 'read' method, thus must be implemented
}

// Interface to pass Where query parameters
export interface IWhereQuery{
    where:Array<any>;
    orderBy?:Array<any>;
    limit?:number;
    paginationRec?:any;
    direction?:number;
}

// Interface for User Role
export interface IRole{
    code:number;
    name:string;
}

// Interface for Code-Name record
export interface ICodeName{
    code:string;
    name:string;
}

// Languages
export enum LANGUAGES{
    Telugu = 'te-IN',
    Kannada =  'kn-IN',
    Tamil =  'ta-IN',
    Malayalam =  'ml-IN',
    Hindi =  'hi-IN',
    English =  'en-US',
}

// Meeting call status
export enum MEETING_INVITATION {
    INVITE = 0,
    ACCEPTED = 1,
    REJECTED = 2,
    PROGRESS = 3,
    ENDED = 4,
    LEFT = 5
}