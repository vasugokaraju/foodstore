// Sets the environment before the app is launched.  Seems to be better option than cluttering package.json file.
const fs = require('fs');
const fse = require('fs-extra');

const environment = (process.env.NODE_ENV || process.argv[2] || "development").trim().toLowerCase();
const source = "./src"
const dest = "./lib"

console.log('Bootstrapping for ', environment.toUpperCase())

try {
    fs.mkdirSync(dest + "/loaders/",{recursive:true});

    // Copy firebase configuration bash script file
    fs.copyFileSync( "./firebaseConfig." + environment, "./firebaseConfigSet.sh");
    // Copy environment file
    fs.copyFileSync( source + "/.env." + environment, dest + "/.env");
    
    // Copy data files
    fs.copyFileSync(source + "/loaders/successCodesData.json", dest + "/loaders/successCodesData.json");
    fs.copyFileSync(source + "/loaders/errorCodesData.json", dest + "/loaders/errorCodesData.json");
    fs.copyFileSync(source + "/loaders/emailTemplatesData.json", dest + "/loaders/emailTemplatesData.json");

    try {
        fse.copySync(source + "/emailTemplates", dest + "/emailTemplates");
        console.log(`Email templates are copied successfully to ${dest + "/emailTemplates/"}.`);
    } catch (error) {
        console.log('Failed to copy email templates.', err);
    }

    switch (environment) {
        case "development":
            // In DEVELOPMENT mode, we use "node -r ts-node/register ./src/app.ts" to dynamically compile .ts code.
            // This mode of debugging does not generate /build folder
            // Only .env file needs to be replaced with .env.development file content.
            fs.copyFileSync( source + "/.env." + environment, source + "/.env");

            break;
        case "staging":
        case "production":
        default:
            // Copy self signed certificate files.
            // fs.copyFileSync(source + "/server.crt", dest + "/server.crt");
            // fs.copyFileSync(source + "/server.key", dest + "/server.key");

            // Set google cloud service account credentials file path
            // https://stackoverflow.com/questions/62209760/external-network-resource-requested-log-error-from-firebase-emulator
            // The following key can be generated from Firebase Project Console > Project Settings > Service Accounts > Generate new private key
            process.env.GOOGLE_APPLICATION_CREDENTIALS = "/home/node/my-firebase-project-18725-firebase-adminsdk-briqk-028e83229e.json"
    }

    console.log('Bootstrap tasks are completed.\n')
}catch (e) {
    console.log('Bootstrap Error:', e)
}