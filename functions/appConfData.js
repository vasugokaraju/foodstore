// This script builds appConfData.json and appCodesData.json file to use in bulk upload of respective pages.

const fs = require('fs');

let output = [];

const errorCodes = {
    "auth/invalid-email":{"title":"Authentication Exception", "message":"Invalid email.", "httpCode":401, "action":1},
    "auth/invalid-phone-number":{"title":"Authentication Exception", "message":"Invalid phone number.", "httpCode":401, "action":1},
    "auth/invalid-photo-url":{"title":"Authentication Exception", "message":"Invalid photo url.", "httpCode":401, "action":1},
    "auth/user-disabled":{"title":"Authentication Exception", "message":"User account has been disabled.", "httpCode":401, "action":1},
    "auth/user-not-found":{"title":"Authentication Exception", "message":"User account not found. Please register.", "httpCode":401, "action":1},
    "auth/wrong-password":{"title":"Authentication Exception", "message":"Wrong password.", "httpCode":401, "action":1},
    "auth/id-token-expired":{"title":"Token Exception", "message":"Token expired.", "httpCode":401, "action":1},
    "auth/id-token-revoked":{"title":"Token Exception", "message":"The Firebase ID token has been revoked.", "httpCode":401, "action":1},
    "auth/phone-number-already-exists":{"title":"Registration Exception", "message":"Phone number already exists", "httpCode":401, "action":1},
    "auth/email-already-exists":{"title":"Registration Exception", "message":"Email already exists", "httpCode":401, "action":1},
    "auth/invalid-action-code":{"title":"Email OTP Exception", "message":"This OTP is malformed, expired, or has already been used.", "httpCode":401, "action":1},

    "E0000":{"title":"Unknown Exception", "message":"An unknown exception took place and the administrator has been informed.", "httpCode":500, "action":1},
    "E0001":{"title":"Error Code Exception", "message":"Error code is not available in master list OR error codes are not loaded.", "httpCode":500, "action":1},
    "E0002":{"title":"MongoDB Connection Error", "message":"Failed to connect to MongoDB", "httpCode":500, "action":1},
    "E0003":{"title":"Redis Connection Error", "message":"Failed to connect to Redis", "httpCode":500, "action":1},
    "E0004":{"title":"ElasticSearch Connection Error", "message":"Failed to connect to ElasticSearch", "httpCode":500, "action":1},
    "E0005":{"title":"CORS Exception", "message":"Unauthorized request origin.", "httpCode":500, "action":1},

    "E1000":{"title":"Authentication Exception", "message":"Please login.", "httpCode":401, "action":1},
    "E1001":{"title":"Authorization Exception", "message":"Token expired. Please login to renew.", "httpCode":401, "action":1},
    "E1002":{"title":"Authorization Exception", "message":"Invalid token.  Check Authentication header.  (Ex: Authentication: Bearer <token>).", "httpCode":401, "action":1},
    "E1003":{"title":"Entitlement Exception", "message":"Access is restricted to this resource.", "httpCode":401, "action":1},
    "E1004":{"title":"RBAC Exception", "message":"Unauthorized attempt to access the route.", "httpCode":401, "action":1},
    "E1005":{"title":"Login Exception", "message":"Invalid login credentials.", "httpCode":401, "action":1},
    "E1006":{"title":"Login Exception", "message":"Unknown error during login attempt. Failed to login.", "httpCode":500, "action":1},
    "E1007":{"title":"Logout Exception", "message":"Unknown error during logout attempt. Failed to logout.", "httpCode":500, "action":1},
    "E1008":{"title":"Change Password Exception", "message":"Unknown error during password change attempt. Old password remains.", "httpCode":500, "action":1},
    "E1009":{"title":"Token Renewal Exception", "message":"Unknown error during token renewal attempt.  Please try again.", "httpCode":500, "action":1},
    "E1010":{"title":"Kong Consumer Exception", "message":"Unknown error during Kong consumer creation attempt.", "httpCode":500, "action":1},
    "E1011":{"title":"Kong Consumer Exception", "message":"Unknown error during Kong consumer deletion attempt.", "httpCode":500, "action":1},
    "E1012":{"title":"User Profile Exception", "message":"Invalid user profile or profile not found.", "httpCode":422, "action":1},
    "E1013":{"title":"Authorization Exception", "message":"Missing Basic Auth headers.  Invalid attempt to access APIs directly.", "httpCode":422, "action":1},
    "E1014":{"title":"Authorization Exception", "message":"Missing API Gateway headers.  Invalid attempt to access APIs directly.", "httpCode":422, "action":1},
    "E1015":{"title":"Kong BasicAuth Exception", "message":"Duplicate Kong BasicAuth credential username.", "httpCode":409, "action":1},
    "E1016":{"title":"Role Hierarchy Exception", "message":"You cannot perform this action on the higher or equal roles.", "httpCode":409, "action":1},
    "E1017":{"title":"Role Hierarchy Exception", "message":"Role value must be lower in hierarchy than your role.", "httpCode":409, "action":1},
    "E1018":{"title":"Authorization Exception", "message":"Malformed Token. Please login.", "httpCode":401, "action":1},
    "E1019":{"title":"Change of Password", "message":"Current login credentials are invalid.", "httpCode":401, "action":1},
    "E1020":{"title":"Attachment Delete Exception", "message":"Failed to delete the attachment file.", "httpCode":401, "action":1},
    "E1021":{"title":"Attachment Save Exception", "message":"Failed to upload the attachment file.", "httpCode":401, "action":1},
    "E1022":{"title":"Attachment Read Exception", "message":"Failed to create dwonload link for the attachment file.", "httpCode":401, "action":1},
    "E1023":{"title":"Attachment Read Exception", "message":"Failed to create dwonload buffer for the attachment file.", "httpCode":401, "action":1},
    "E1024":{"title":"Entitlement Exception", "message":"You cannot create a user with higher role than yours.", "httpCode":401, "action":1},
    "E1025":{"title":"Entitlement Exception", "message":"You cannot delete your own record.", "httpCode":401, "action":1},
    "E1026":{"title":"Entitlement Exception", "message":"You cannot change a role to higher or equal to your own role.", "httpCode":401, "action":1},
    "E1027":{"title":"Security Exception", "message":"reCAPTCHA verification failed. Possible suspicious activity detected.", "httpCode":401, "action":1},
    "E1028":{"title":"Phone Authentication Exception", "message":"Failed to send OTP through SMS.", "httpCode":401, "action":1},
    "E1029":{"title":"Phone Authentication Exception", "message":"Too late for token housekeeping.", "httpCode":401, "action":1},
    "E1030":{"title":"Phone Authentication Exception", "message":"Failed to validate token.  Firebase ID token has incorrect algorithm.", "httpCode":401, "action":1},
    "E1031":{"title":"Email Template Exception", "message":"Requested email template is not available.", "httpCode":401, "action":1},
    "E1032":{"title":"Encryption Exception", "message":"Failed to encrypt.", "httpCode":401, "action":1},
    "E1033":{"title":"Decryption Exception", "message":"Failed to decrypt.", "httpCode":401, "action":1},
    "E1034":{"title":"Decryption Exception", "message":"Invalid User ID and Email combination.", "httpCode":401, "action":1},
    "E1035":{"title":"Email Confirmation Exception", "message":"This email has already been verified.  Please login.", "httpCode":401, "action":1},
    "E1036":{"title":"Authentication Exception", "message":"This email is not verified yet.", "httpCode":401, "action":1},
    "E1037":{"title":"Email Authentication Exception", "message":"Invalid email link login attempt.", "httpCode":401, "action":1},
    "E1038":{"title":"Create Password", "message":"Failed to create password.", "httpCode":401, "action":1},
    "E1039":{"title":"Password Creation Exception", "message":"Invalid attempt to create password.", "httpCode":401, "action":1},
    "E1040":{"title":"Google Authentication Exception", "message":"Failed to validate token.  Firebase ID token has incorrect algorithm.", "httpCode":401, "action":1},
    
    "E1104":{"title":"Token Exception", "message":"providerId is not found in cookies.", "httpCode":401, "action":1},
    "E1105":{"title":"Token Exception", "message":"Token UID is not found in cookies.", "httpCode":401, "action":1},
    "E1999":{"title":"Unhandled Firebase Exception", "message":"Unhandled Firebase exception.", "httpCode":500, "action":1},

    "E1200":{"title":"Invalid Payload", "message":"Please check the payload including body and query parameters.", "httpCode":422, "action":0},
    "E1201":{"title":"Invalid JSON", "message":"Please check the JSON for syntax errors.", "httpCode":422, "action":1},
    "E1202":{"title":"Schema Exception", "message":"Please check the payload.", "httpCode":422, "action":1},
    "E1203":{"title":"Schema Exception", "message":"Please check the sample data in the payload.", "httpCode":422, "action":1},
    "E1204":{"title":"Schema Exception", "message":"No schema available with this name.", "httpCode":422, "action":1},
    "E1205":{"title":"Schema Exception", "message":"Missing schema reference.", "httpCode":422, "action":1},
    "E1206":{"title":"Datatype Exception", "message":"Invalid objectId.", "httpCode":422, "action":1},
    
    "E1400":{"title":"Duplicate Record Exception", "message":"Please check the data", "httpCode":409, "action":0},
    "E1401":{"title":"Create Record Exception", "message":"Failed to create record in database.", "httpCode":409, "action":1},
    "E1402":{"title":"Update Record Exception", "message":"Failed to update record in database.", "httpCode":409, "action":1},
    "E1403":{"title":"Delete Record Exception", "message":"Failed to delete record from database.", "httpCode":409, "action":1},
    "E1404":{"title":"Read Record Exception", "message":"Failed to read record from database.", "httpCode":404, "action":1},
    "E1405":{"title":"Record Not Found Exception", "message":"No matching record found in database.", "httpCode":404, "action":1},

    "E1406":{"title":"Redis Create Record Exception", "message":"Failed to create record in Redis.", "httpCode":500, "action":1},
    "E1407":{"title":"Redis Update Record Exception", "message":"Failed to update record in Redis.", "httpCode":500, "action":1},
    "E1408":{"title":"Redis Delete Record Exception", "message":"Failed to delete record from Redis.", "httpCode":500, "action":1},
    "E1409":{"title":"Redis Read Record Exception", "message":"Failed to read record from Redis.", "httpCode":500, "action":1},
    "E1410":{"title":"Redis Record Not Found Exception", "message":"No matching record found in Redis.", "httpCode":500, "action":1},

    "E1411":{"title":"ElasticSearch Create Record Exception", "message":"Failed to create record in ElasticSearch.", "httpCode":500, "action":1},
    "E1412":{"title":"ElasticSearch Update Record Exception", "message":"Failed to update record in ElasticSearch.", "httpCode":500, "action":1},
    "E1413":{"title":"ElasticSearch Delete Record Exception", "message":"Failed to delete record from ElasticSearch.", "httpCode":500, "action":1},
    "E1414":{"title":"ElasticSearch Read Record Exception", "message":"Failed to read record from ElasticSearch.", "httpCode":500, "action":1},
    "E1415":{"title":"ElasticSearch Record Not Found Exception", "message":"No matching record found in ElasticSearch.", "httpCode":500, "action":1},

    "E1600":{"title":"Template File Exception", "message":"Template not found.", "httpCode":500, "action":1}
}

const successCodes = {
    "S0000":{"title":"Success", "message":"Query has been successful.", "httpCode":200, "action":0},
    "S1000":{"title":"Login Success", "message":"Credentials are valid", "httpCode":200, "action":0},
    "S1001":{"title":"Authentication", "message":"Token is valid", "httpCode":200, "action":0},
    "S1002":{"title":"", "message":"Token is valid", "httpCode":200, "action":0},
    "S1003":{"title":"Password Update", "message":"Password has been changed.", "httpCode":200, "action":0},
    "S1004":{"title":"New Token", "message":"Token has been renewed.", "httpCode":200, "action":0},
    "S1005":{"title":"Encoded", "message":"String has been encoded with base64.", "httpCode":200, "action":0},
    "S1006":{"title":"Decoded", "message":"String has been decoded with base64.", "httpCode":200, "action":0},
    "S1007":{"title":"Hash", "message":"String has been hashed.", "httpCode":200, "action":0},
    "S1008":{"title":"Logged Out", "message":"Logged out", "httpCode":200, "action":0},
    "S1009":{"title":"File Upload", "message":"File has been uploaded.", "httpCode":200, "action":0},
    "S1010":{"title":"File Download", "message":"File has been downloaded.", "httpCode":200, "action":0},
    "S1011":{"title":"File Deleted", "message":"File has been deleted.", "httpCode":200, "action":0},
    "S1012":{"title":"File Download Link", "message":"File download link has been generated.", "httpCode":200, "action":0},
    "S1013":{"title":"File Download Buffer", "message":"File download buffer has been generated.", "httpCode":200, "action":0},
    "S1014":{"title":"Email Sent", "message":"Verification email has been sent.", "httpCode":200, "action":0},
    "S1015":{"title":"Email OTP Sent", "message":"An email OTP has been sent. Please check your email.", "httpCode":200, "action":0},
    "S1016":{"title":"Email Verified", "message":"Your email has been verified.  Please create password.", "httpCode":200, "action":0},
    "S1017":{"title":"Password Created", "message":"Password has been created.", "httpCode":200, "action":0},
    "S1018":{"title":"Password Reset Email Sent", "message":"An email has been sent.  Please follow the instructions to reset your password.", "httpCode":200, "action":0},
    "S1019":{"title":"Password Reset Completed", "message":"Your password has been reset.", "httpCode":200, "action":0},
    "S1020":{"title":"Password Reset Successful", "message":"Your password has been reset.", "httpCode":200, "action":0},
    "S1021":{"title":"Google Login", "message":"Google user data is retrieved.", "httpCode":200, "action":0},
    
    "S1400":{"title":"Read Data", "message":"1 record found.", "httpCode":200, "action":0},
    "S1401":{"title":"New Record", "message":"Record has been created.", "httpCode":201, "action":0},
    "S1402":{"title":"Update Record", "message":"Update operation is successful.", "httpCode":200, "action":0},
    "S1403":{"title":"Delete Record", "message":"Delete operation is successful.", "httpCode":200, "action":0},
    "S1404":{"title":"Bulkupload", "message":"Bulkupload is successful.", "httpCode":200, "action":0},
    "S1405":{"title":"Record Exists", "message":"Record already exists. No changes are made.", "httpCode":200, "action":0},
    "S1406":{"title":"Download", "message":"Download is successful.", "httpCode":200, "action":0}
}

const mainMenu = {
    "te-IN": [
        {
            label: 'స్వాగతం',
            routerLink: '/home',
            id: 'HOME'

        },
        {
            label: 'పునరుద్దరణ',
            routerLink: '/restoration',
            id: 'RESTORATION'

        },
        {
            label: 'నిర్వహణ',
            id: 'ADMINISTRATION',
            routerLink: '/temple',
        },

        {
            label: 'విముక్తి',
            id: 'REDEMPTION',
            routerLink: '/redemption'
        },

        {
            label: 'డౌన్లోడ్',
            icon: 'pi pi-fw pi-download',
            id: 'DOWNLOAD',
            routerLink: '/download'
        },

        {
            label: 'ఇతర భాషలు',
            icon: 'pi pi-fw pi-flag',
            id: 'LANGUAGE',
            items: [
                {
                    label: 'తెలుగు',
                    icon: 'pi pi-fw pi-download',
                    id: 'te-IN'
                }, {
                    label: 'English',
                    icon: 'pi pi-fw pi-download',
                    id: 'en-US'
                },
            ]
        },

        {
            label: 'ప్రవేశము / నంమోదు',
            icon: 'pi pi-fw pi-user-plus',
            routerLink: '/login'
        }
    ],
    "en-US": [
        {
            label: 'Welcome',
            routerLink: '/home',
            id: 'HOME'

        },
        {
            label: 'Restoration',
            routerLink: '/restoration',
            id: 'RESTORATION'

        },
        {
            label: 'Administration',
            id: 'ADMINISTRATION',
            routerLink: '/temple',
        },

        {
            label: 'Redemption',
            id: 'REDEMPTION',
            routerLink: '/redemption',
        },

        {
            label: 'Downloads',
            icon: 'pi pi-fw pi-download',
            id: 'DOWNLOAD',
            routerLink: '/download'
        },

        {
            label: 'Other Languages',
            icon: 'pi pi-fw pi-flag',
            id: 'LANGUAGE',
            items: [
                {
                    label: 'తెలుగు',
                    icon: 'pi pi-fw pi-download',
                    id: 'te-IN'
                }, {
                    label: 'English',
                    icon: 'pi pi-fw pi-download',
                    id: 'en-US'
                },
            ]
        },

        {
            label: 'Log In / Sign Up',
            icon: 'pi pi-fw pi-user-plus',
            routerLink: '/login'
        }
    ]

};

const burgerMenu = {
    "te-IN": [
        {
            label: 'పరిచయం',
            routerLink: '/managudi/introduction'
        },
        {
            label: 'వ్యక్తులు',
            routerLink: '/managudi/people'
        },
        {
            label: 'ఆశయాలు',
            routerLink: '/managudi/aspirations'
        },
        {
            label: 'ప్రణాళిక & ప్రయత్నం',
            routerLink: '/managudi/plan_attempt'
        },
        {
            label: 'వ్యవస్థ',
            routerLink: '/managudi/system'
        },
        {
            label: 'శ్రేయోభిలాషులు',
            routerLink: '/managudi/supporters'
        }
    ],
    "en-US": [
        {
            label: 'Introduction',
            routerLink: '/managudi/introduction'
        },
        {
            label: 'People',
            routerLink: '/managudi/people'
        },
        {
            label: 'Aspirations',
            routerLink: '/managudi/aspirations'
        },
        {
            label: 'Plan & Attempt',
            routerLink: '/managudi/plan_attempt'
        },
        {
            label: 'System',
            routerLink: '/managudi/system'
        },
        {
            label: 'Supporters',
            routerLink: '/managudi/supporters'
        }
    ]
}

const templeAdminMenu = {
    "te-IN": [
        {
            label: "దేవాలయం",
            items: [
                {
                    label: 'స్వాగతం',
                    routerLink: '/home'
                },
                {
                    label: 'చరిత్ర',
                    routerLink: '/history'
                }
            ]
        },                
        {
            label: "దేవాలయ సేవలు",
            items: [
                {
                    label: 'పూజలు',
                    routerLink: '/services'
                },
                {
                    label: 'అపర సంస్కారాలు',
                    routerLink: '/religiousServices'
                },
                {
                    label: 'పండుగలు, పర్వదినాలు',
                    routerLink: '/events'
                },
                {
                    label: 'పంచాంగం',
                    routerLink: '/panchangam'
                },
            ]
        },
        {
            label: "నిర్వాహకులు",
            items: [
                {
                    label: 'అర్చకస్వాములు',
                    routerLink: '/priests'
                },                                        
                {
                    label: 'సిబ్బంది',
                    routerLink: '/staff'
                },
                {
                    label: 'స్వయంసేవకులు',
                    routerLink: '/volunteers'
                },
                {
                    label: 'దాతలు',
                    routerLink: '/donors'
                },
                {
                    label: 'సభ్యులు',
                    routerLink: '/members'
                },
            ]
        },                
        {
            label: "దైనందిక నిర్వహణ",
            items: [

                {
                    label: 'నిర్వహణ',
                    routerLink: '/maintenance'
                },
                {
                    label: 'భద్రత',
                    routerLink: '/security'
                },
                {
                    label: 'కార్యాలయం',
                    routerLink: '/office'
                },
                {
                    label: 'లావాదేవీలు',
                    routerLink: '/transactions'
                },
            ]
        },
        {
            label: "ఆర్ధిక వ్యవస్థ",
            items: [

                {
                    label: 'ఆర్ధిక ప్రణాళిక',
                    routerLink: '/budget'
                },
                {
                    label: 'దాతలు',
                    routerLink: '/donors'
                },
                {
                    label: 'హుండి',
                    routerLink: '/hundi'
                }
            ]
        },
        {
            label: "ఆస్తులు",
            items: [
                {
                    label: 'భూములు',
                    routerLink: '/lands'
                },
                {
                    label: 'వస్తువుల జాబితా',
                    routerLink: '/inventory'
                }
            ]
        }
    ],

    "en-US": [
        {
            label: "Temple",
            items: [
                {
                    label: 'Home',
                    routerLink: '/home'
                },
                {
                    label: 'History',
                    routerLink: '/history'
                }
            ]
        },
        {
            label: "Services",
            items: [
                {
                    label: 'Priest Services',
                    routerLink: '/services'
                },
                {
                    label: 'Apara Samskaras',
                    routerLink: '/religiousServices'
                },
                {
                    label: 'Events',
                    routerLink: '/events'
                },
                {
                    label: 'Panchangam',
                    routerLink: '/panchangam'
                },
            ]
        },
        {
            label: "Team",
            items: [
                {
                    label: 'Priests',
                    routerLink: '/priests'
                },                                        
                {
                    label: 'Staff',
                    routerLink: '/staff'
                },
                {
                    label: 'Volunteers',
                    routerLink: '/volunteers'
                },
                {
                    label: 'Donors',
                    routerLink: '/donors'
                },
                {
                    label: 'Members',
                    routerLink: '/members'
                },
            ]
        },
        {
            label: "Maintenance",
            items: [

                {
                    label: 'Cleaning',
                    routerLink: '/maintenance'
                },
                {
                    label: 'Security',
                    routerLink: '/security'
                },
                {
                    label: 'Office',
                    routerLink: '/office'
                },
                {
                    label: 'Transactions',
                    routerLink: '/transactions'
                },
            ]
        },
        {
            label: "Finances",
            items: [

                {
                    label: 'Budget',
                    routerLink: '/budget'
                },
                {
                    label: 'Donors',
                    routerLink: '/donors'
                },
                {
                    label: 'Hundi',
                    routerLink: '/hundi'
                }
            ]
        },
        {
            label: "Property",
            items: [
                {
                    label: 'Lands',
                    routerLink: '/lands'
                },
                {
                    label: 'Inventory',
                    routerLink: '/inventory'
                }
            ]
        }

    ]
}

const redemptionMenu = {
    "te-IN": [
        {
            label: "అంశాలు",
            items: [
                {
                    label: 'చట్టం & న్యాయం',
                    routerLink: 'redemption/legal'
                }
            ]
        }
    ],
    "en-US": [
        {
            label: "Topics",
            items: [
                {
                    label: 'Legal',
                    routerLink: 'redemption/legal'
                }                        

            ]
        }
    ]
}

const downloadMenu = {
    "te-IN": [
        {
            label: "అంశాలు",
            items: [
                {
                    label: 'సంస్థ ముద్రలు',
                    routerLink: 'download/branding'
                },
                {
                    label: 'విముక్తి',
                    routerLink: 'download/redemption'
                },
                {
                    label: 'చట్టం & న్యాయం',
                    routerLink: 'download/legal'
                }                        

            ]
        }
    ],
    "en-US": [
        {
            label: "Topics",
            items: [
                {
                    label: 'Branding',
                    routerLink: 'download/branding'
                },
                {
                    label: 'Redemption',
                    routerLink: 'download/redemption'
                },
                {
                    label: 'Legal',
                    routerLink: 'download/legal'
                }                        

            ]
        }
    ]
}

const homePageLabels = {
    "te-IN":{
      "newTmpl":"నూతన దేవాలయాలు",
      "moreDtls":"మరిన్ని వివరాలు",
      "tours":"యాత్రలు",
      "prds":"విక్రయాలు",
      "testm":"ప్రముఖుల వ్యాఖ్యలు"
    },
    "en-US":{
        "newTmpl":"New Temples",
        "moreDtls":"More Details",
        "tours":"Tours",
        "prds":"Products",
        "testm":"Testimonials"
    }      
  }

const dataVariables = [mainMenu, burgerMenu, templeAdminMenu, redemptionMenu, downloadMenu, homePageLabels]
const labels = ["mainMenu", "burgerMenu", "templeAdminMenu", "redemptionMenu", "downloadMenu", "homePageLabels"]

// errorCodes
Object.keys(errorCodes).forEach( item => {
    const data = errorCodes[item];
    output.push(
        {
            docId:(item.replace(/\//g,'_') + ":en-US").toLowerCase(),
            lng:'en-US',
            code:item.toLowerCase(),
            titl:data.title, 
            msg:data.message, 
            httpCode:data.httpCode, 
            actn: data.action
        }
    )
})

// successCodes
Object.keys(successCodes).forEach( item => {
    const data = successCodes[item];
    output.push(
        {
            docId:(item.replace(/\//g,'_') + ":en-US").toLowerCase(),
            lng:'en-US',
            code:item.toLowerCase(),
            titl:data.title, 
            msg:data.message, 
            httpCode:data.httpCode, 
            actn: data.action
        }
    )
})

fs.writeFile('appCodesData.json',JSON.stringify(output, null, 2), (err) => {
    if(err) console.log(err)
    else console.log("File saved.");
})

output = [];

dataVariables.forEach( (data, index) => {
    let label = labels[index].toLowerCase();

    Object.keys(data).forEach( (item) => { 
        // console.log('labels',labels[index]);
        const _data = data[item];

        output.push(
            {
                docId: (`${label}:` + item.replace(/\//g,'_')).toLowerCase(),
                lng:item,
                lbl:label,
                data: _data
            }
        )
    })
})

fs.writeFile('appConfData.json',JSON.stringify(output, null, 2), (err) => {
    if(err) console.log(err)
    else console.log("File saved.");
})