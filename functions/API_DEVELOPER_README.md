<style>
td{
    vertical-align:top;
}

.bigRowGap li{
    margin: 20px 0 0 0;
}
</style>

# API DEVELOPER REFERENCE

## INITIAL SETUP

Once you download the zip file and extract its content, you will see a folder structure similar to the shown in the FOLDER STRUCTURE section.  Then follow the steps below to setup the newly created application.

<ol class="bigRowGap">
    <li>Run the <code>other/dbscripts/db_initialization.js</code> script using your favorite database client tool.  This file contains statements to drop existing collections and we have double commented them to be safe.  We strongly suggest that you drop all the existing collections and start fresh everytime.  Never run these scripts on production databases.
    <li>Switch to the root folder where the <code>package.json</code> file is located.
    <li>Run <code>npm install</code> command to install all packages. It may take a minute or two</li>
    <li>Make sure no app is running at <i>http://http:5000</i></li>
    <li>Now you can run the app in debug mode in the following ways<br/><br/>
        <table>
            <tr>
                <td style="width:150px;">VSCode IDE</td>
                <td>Switch to Run veiw using Activity Bar on the left and run "DEBUG" launch script.</td>
            </tr>
            <tr>
                <td>Linux command line</td>
                <td><code>npm run dev</code></td>
            </tr>
            <tr>
                <td>Windows command line</td>
                <td><code>npm run win_dev</code></td>
            </tr>
        </table>    
    </li>
    <li>Congratulations!  Now you have working codebase with fundamental building blocks in place.  Read the rest of the help sections to gain indepth understanding of how things are implemented.</li>
</ol>
<br/><br/>

## FIRST STEPS

Assuming that you have completed the INITIAL SETUP and your app is up and running, let us look at how the app functions.  For the sake of simplicity we assume that the developer is using VSCode IDE and REST Client plugin.

When an app is equipped with authentication, you need a backdoor for the very first user to get in.  The backdoor in this case is located in <code>other/dbscripts/db_initialization.js</code> file.  The <code>db.users.insert(...)</code> script creates the first user with predefined login and password.

To login as default user, open <code>test/rest-client/auth_API_Tests.rest</code> file.  Click on _Send Request_ link in the LOGIN section.  The Authorization header value 'YWRtaW46cGFzc3dvcmQ' is base64-encoded string of 'admin:password'.  The credentials are expected as <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization">base64 encoded</a> string.  

Now you should see a token in the response. Copy the token string without quotes. This should be used to access every protected API.  Let us create a new users.

Open <code>test/rest-client/users_API_Tests.rest</code> file.  There you should find a variable named <code>@token</code>.  Paste the token as value of this variable.  Do not put quotes around the token string. 

Now move to 'CREATE users' section.  Here you can see http headers as below.

```
Authorization:Bearer {{token}}
loggedin:true
```

The <code></code> variable is used to inject the token into the http headers.
The <code>loggedin</code> header indicates whether the user is still logged-in on the client, 
while the <code>token</code> provides user information along with token expiration time.  

In the world of stateless REST APIs, the server has no means to check user login status.  It simply accepts requests as long as the token is not expired, irrespective of whether the user is logged in/logged out.

So the <code>loggedin</code> header helps the developer to prevent the user 
Token expiration is verified only if <code>loggedin</code> is true.  This approach eliminates the burden of <a href="https://stackoverflow.com/questions/21978658/invalidating-json-web-tokens">Invalidating JSON Web Tokens</a> on server-side when the user logs out on the client-side.


Now click on _Send Request_ link.

## FOLDER STRUCTURE
The following table describes the folders and their purpose.
<table>
<tbody>
<tr>
    <th style="width:100px">Folder Name</th>
    <th>Description</th>
</tr>
<tr>
    <td><code>@types</code></td>
    <td>Contains custom/local TypeScript type definitions.</td>
</tr>
<tr>
    <td><code>config</code></td>
    <td>Contains app's config.</td>
</tr>
<tr>
    <td><code>helpers</code></td>
    <td>Contains generic helper file which provides common routines to use throughout the app.</td>
</tr>
<tr>
    <td><code>interfaces</code></td>
    <td>Contains all interface definition files.</td>
</tr>
<tr>
    <td><code>loaders</code></td>
    <td>Contains loader files that make essentials modules and data ready.</td>
</tr>
<tr>
    <td><code>media</code></td>
    <td>Contains media files used in DEVELOPER_README.md file.  This folder can be removed in production environment.</td>
</tr>
<tr>
    <td><code>middleware</code></td>
    <td>Contains all middleware files to support Express route validations.</td>
</tr>
<tr>
    <td><code>modules</code></td>
    <td>Contains custom modules.</td>
</tr>
<tr>
    <td><code>repositories</code></td>
    <td>Contains files that interact with database.  All database/cache calls must be done through these files.  Only Services are expected to interact with Repositories.</td>
</tr>
<tr>
    <td><code>routes</code></td>
    <td>Contains route files for each module.</td>
</tr>
<tr>
    <td><code>schemas</code></td>
    <td>Contains schema file for each module.  All CRUD data validations are performed using these schemas.</td>
</tr>
<tr>
    <td><code>services</code></td>
    <td>Contains all service files.  Only Routes and other Services are expected to use services.</td>
</tr>

</tbody>
</table>
<br/><br/>

## FILES
The following table describes critical files and their purpose.
<table>
<tbody>
<tr>
    <th style="width:100px">File Name</th>
    <th>Description</th>
</tr>
<tr>
    <td><code>package.json</code></td>
    <td>Contains information about packages and scripts.</td>
</tr>
<tr>
    <td><code>tsconfig.json</code></td>
    <td>Contains TypeScript specific configuration used by the TS compiler.</td>
</tr>
<tr>
    <td><code>DEVELOPER_README.md</code></td>
    <td>Contains application implementation details for developers.</td>
</tr>
<tr>
    <td><code>bootstrap.js</code></td>
    <td>Performs bootstrap tasks before app launch. This file can be customized as needed.</td>
</tr>
<tr>
    <td><code>.gitignore</code></td>
    <td>Contains the names of files and folders that are to be ignored when checkin.</td>
</tr>
<tr>
    <td><code>src/app.ts</code></td>
    <td>This is the starting point of the application.</td>
</tr>
<tr>
    <td><code>src/.env</code></td>
    <td>Contains app specific environment files.  The <code>dotenv</code> module loads environment variables from a <code>.env</code> file into process.env.</td>
</tr>
<tr>
    <td><code>src/.env.development</code>, <code>src/.env.staging</code>, <code>src/.env.production</code></td>
    <td>These environment specific configuration files are used to replace the content of the <code>.env</code> file based on the <code>process.env.NODE_ENV</code> value and it is done by <code>bootstrap.js</code></td>
</tr>
<tr>
    <td><code>src/server.crt</code>, <code>src/server.crt</code></td>
    <td>These are self-signed certificate files to launch the app as secure server in development environment.
    <br/><br/>If you run into <code>CERT_HAS_EXPIRED</code> exception, you may avoid it with the following setting.
    <br/><br/>
    <code>process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';</code>
    <br/><br/>
    <strong>Note:</strong> These files and settings should never be used in production environment.</td>
</tr>
<tr>
    <td><code>src/@types/express/index.d.ts</code></td>
    <td>It is common practice to add custom fields/keys to Request object in nodejs applications.  When it comes to TypeScript, you are forced to define the custom fields/keys in this file before using them in the app.  This file contains the custom fields/keys created for this app.</td>
</tr>

</tbody>
</table>
<br/><br/>


## DEBUG LAUNCH SEQUENCE
To debug using VSCode IDE, simply run the <code>DEBUG</code> launch script.

To debug from windows command line, run <code>npm run dev_win</code>  
To debug from linux command line, run <code>npm run dev</code>
<br/><br/>


## REQUEST FLOW
The following diagram depicts the path of the Request and the eventual Response.

<image src='./src/media/RequestFlow.png'>

### CONTROLLER/ROUTE
Controller/Route layer receives the request and middleware functions are called to perform various actions like Token verification, RBAC, Hierarchy verification, Schema validation and Data Entitlements.

### SERVICE
Services are used for all crucial tasks related to modules.  Incoming data can be modified here before passing it to Repositories.

### REPOSITORY
Repository layer communicates with database/cache and responds back with result or exception.
<br/><br/>

## DATA VALIDATIONS
Incoming data is validated by a predefined schema using <a href="https://www.npmjs.com/package/ajv">AJV</a> plugin.  Apart from data validity checks like datatype, size, enum etc, data integrity checks can also be performed using async keyword functions.  Check "idExists" custom keyword for reference.

<i>It is observed that if schema contains a field name "description", ajv compilation fails.</i>
<br/><br/>

## ERROR HANDLING
Application is supported by centralized error handling system which provides context based error information.  Error handling calles are handled by <code>/services/errorService.ts</code>.
<br/><br/><br/>

## AUTHENTICATION
While there are many authentication methods available, the following popular ones are supported.

### LOCAL_JWT (<a href="https://jwt.io/introduction/">JWT</a>)

#### Implementation
The following table describes the files related to local_jwt authentication.
<table>
<tbody>
<tr>
    <th style="width:100px">File Name</th>
    <th>Description</th>
</tr>
<tr>
    <td><code>routes/authRoute.ts</code></td>
    <td>This route/controller file contains the routes like /login, /changePassword etc. This file issues token after successful login.</td>
</tr>
<tr>
    <td><code>services/authService.ts</code></td>
    <td>This service file contains functions like login, changePassword etc.  to accomodate authentication needs.  The above route file uses this service to perform respective tasks.</td>
</tr>
<tr>
    <td><code>routes/&ltmodel&gt;Route.ts</code></td>
    <td>Every route in the model specific route file contains <code>middleware.validateToken</code> middleware function call to validate incoming token.</td>
</tr>
<tr>
    <td><code>middleware/validateToken.ts</code></td>
    <td>When the above mentioned model specific route files inoke the <code>middleware.validateToken</code> function, this middleware function validates token.</td>
</tr>
<tr>
    <td><code>schemas/usersSchema.ts</code></td>
    <td>The 'users.password' field in the schema file contains "hashIt" custom keyword to hash password during Create and Update transactions.</td>
</tr>
<tr>
    <td><code>other/dbscripts/db_initialization.js</code></td>
    <td>This file contains script to create the very first user.  This is needed to login with proper credentials.</td>
</tr>
<tr>
    <td><code>test/rest-client/&lt;model&gt;_API_Tests.rest</code></td>
    <td>.rest files contain <code>Authorization</code>, <code>loggedin</code> headers that are needed to access the API.  VSCode IDE users with 'REST Client' plugin can make use of these .rest files.</td>
</tr>
</tbody>
</table>
<br/><br/>

#### Execution
Here we discuss about how authentication works.  For the sake of simplicity we assume that the developer is using VSCode IDE.


<br/>
The <code>loggedin</code> header indicates whether the user is still logged-in on the client, 
while the <code>token</code> provides user information along with token expiration time.  Token expiration is verified only if <code>loggedin</code> is true.  This approach eliminates the burden of <a href="https://stackoverflow.com/questions/21978658/invalidating-json-web-tokens">Invalidating JSON Web Tokens</a> on server-side when the user logs out on the client-side.

<br/><br/>
### KONG_JWT
If <code>kong_jwt</code> option is chosen, the API creates Consumer in Kong and enables Basic-Auth and JWT plugins on Consumer.  The Kong API Gateway takes the responsibility of validating user credentials using Basic-Auth plugin and also provides <code>key</code> and <code>secret</code> values to generate JWT token.

<br/><br/>
## AUTHORIZATION

<br/><br/>
## Question and Answers