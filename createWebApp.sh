#!/usr/bin/env bash

# THIS FILE CREATES NEW WEB PROJECT IN CURRENT FIREBASE PROJECT AND CREATES .runtimeconfig.json FILE

# PARAMETERS
# @app_display_name = Display Name of the service


# VARIABLES
# $project_id = Current project that the user is already logged into.  Service will be created under this project.
# $web_app_id = Newly created web app id


# USAGE
# ./createWebApp.sh "My Web App 1"


# firebase apps:sdkconfig WEB 1:792224779387:web:59f34a3359615877b326de


# Assign command line parameters to variables
app_display_name=$1

project_id=$(firebase use)  # get current firebase project id.

# Function to create web app in current firebase project
createWebapp(){
    echo "Creating web app '$app_display_name' in '$project_id'"
    # firebase apps:create WEB $app_display_name
    web_app_id="npx $(firebase apps:create WEB $app_display_name)"
    echo "New web app id: $web_app_id"
}

listFirebaseProjects(){
    firebase projects:list
}

listWebApps(){
    firebase apps:list WEB
}


clear
echo "Create '$app_display_name' web app in '$project_id' project?" [N/y]
read project_confirmation

if [ "$project_confirmation" != "${project_confirmation#[Yy]}" ] ;then

    # Create google service account
    if createWebapp
    then

        # Call function to list existing apps under this project.
        listWebApps

    else 
        echo
        echo "Web App '$app_display_name' already exists in '$project_id' project.  Please change display name and try again."
        echo

        listFirebaseProjects
    fi

else
    echo
    echo "Please switch to desired project using 'firebase use \"your-project-id\"' and try to create the web app again."
    echo
    echo

    listFirebaseProjects
fi