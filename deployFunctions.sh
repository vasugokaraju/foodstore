clear
echo Deploying Firebase Functions...
echo $1

# THIS SCRIPT DEPLOYS FIREBASE FUNCTIONS USING FIREBASE CLI

# USAGE
# ./deployFunctions.sh func1,func2
# ./deployFunctions.sh decode,encode,hash,login,logout,register,test,usersCreate,usersDelete,usersRead,usersUpdate


input=$1
functions=${input//[,]/,functions:}
functions="functions:$functions"
# Deploy functions
firebase deploy --only $functions


# Remove 'Allow unauthenticated' setting for given function
IFS=', ' read -r -a array <<< "$1"
echo
for function in "${array[@]}"
do
echo
# echo "Removing 'Allow unauthenticated' for $function"
# gcloud functions remove-iam-policy-binding --member=allUsers --role=roles/cloudfunctions.invoker $function
done

# Get identity token to test the functions
echo
echo "GCloud Identity token to test functions:"
echo
gcloud auth print-identity-token


# THIS SCRIPT DEPLOYS FIREBASE FUNCTIONS USING GCLOUD CLI

authenticatedFunctions=( hash )
unauthenticatedFunctions=( encode decode )

# for function in "${authenticatedFunctions[@]}"
# do
# # https://cloud.google.com/functions/docs/deploying/filesystem
# echo "gcloud functions deploy $function \
# --entry-point $function \
# --trigger-http --security-level=secure-always \
# --runtime nodejs12 \
# --trigger-http"
# done


# for function in "${unauthenticatedFunctions[@]}"
# do
# # https://cloud.google.com/functions/docs/deploying/filesystem
# gcloud functions deploy $function \
# --entry-point $function \
# --trigger-http --security-level=secure-always \
# --runtime nodejs12 \
# --trigger-http \
# --allow-unauthenticated
# done

echo
echo
echo "DONE"