clear

echo "*********************** HELP ***********************"
echo
echo ---------------------GOOGLE CLOUD
echo \# Login to google cloud account
echo gcloud auth login
echo
echo \# Login to firebase account
echo firebase login
echo
echo \# List all google cloud projects
echo gcloud projects list
echo
echo \# Switch to given google cloud project
echo gcloud config set project \<project_id\>
echo
echo \# List all service accounts
echo gcloud iam service-accounts list
echo
echo \# Create a service account \(run from firebase project root folder\)
echo ./createServiceAccount.sh \"my-test-service-1\" \"My Test Service Account 1\" \"This is a test service account.\"
echo
echo \# Delete a service account
echo gcloud iam service-accounts delete \<service account email\>
echo
echo \# Switch to given service account
echo export GOOGLE_APPLICATION_CREDENTIALS="path_to_service_account_key_file.json"
echo
echo \# List Policies of $gcloud_project_id project
echo gcloud projects get-iam-policy $gcloud_project_id
echo
echo \# Add new role
echo gcloud projects add-iam-policy-binding $gcloud_project_id --member="serviceAccount:$service_account_id@$gcloud_project_id.iam.gserviceaccount.com" --role="roles/logging.admin"
echo
echo \# Delete given role
echo gcloud projects remove-iam-policy-binding $gcloud_project_id --member="serviceAccount:$service_account_id@$gcloud_project_id.iam.gserviceaccount.com" --role="roles/role.name"
echo
echo \# acquire new user credentials to use for Application Default Credentials
echo gcloud auth application-default login
echo
echo \# List all functions
echo gcloud functions list
echo
echo \# Get the log of given function.  Useful when deployment fails
echo gcloud functions logs read
echo
echo ----------------------FIREBASE
echo \# List all firebase projects under the current user
echo firebase projects:list
echo
echo \# Switch to a given firebase project
echo firebase use \<project id\>
echo
echo \# List all Apps under active firebase project
echo firebase apps:list
echo
echo \# List all hosting sites associated with firebase account
echo firebase hosting:sites:list
echo
echo \# Get runtime configuration for Firebase Web App \(run this command from \/functions folder\)
echo cd functions && ./getRuntimeConfig.sh \<web app id\>
echo
echo \# Get list of firestore indexes
echo firebase firestore:indexes
echo
echo
echo ----------------------NODE
echo \# List globally install package names and versions
echo npm ls --depth=0 -global
echo
echo "****************************************************"
echo
echo

echo Collecting information...

firebase_project_id="$(firebase use)"
google_account=$(gcloud config get-value account)       # https://cloud.google.com/sdk/gcloud/reference/config
gcloud_project_id=$(gcloud config get-value project)

if test -f "$GOOGLE_APPLICATION_CREDENTIALS"; then
    echo ''
else
    echo
    echo GOOGLE_APPLICATION_CREDENTIALS variable is not set.
    echo "Use the following sample command to set your service key file path to GOOGLE_APPLICATION_CREDENTIALS"
    echo "or load the ~/.bashrc file"
    echo
    echo export GOOGLE_APPLICATION_CREDENTIALS="/home/node/myfirebase-service-account-key.json"
    echo
    echo
    echo If the service is not created, use the following sample command to create new service.
    echo ./createServiceAccount.sh \"my-test-service-1\" \"My Test Service Account 1\" \"This is a test service account.\"
    echo
    echo
    exit
fi

service_account_client_email=$(grep 'client_email' $GOOGLE_APPLICATION_CREDENTIALS | cut -d ":" -f2)
service_account_client_email=${service_account_client_email//[\", ]/}
service_account_id=$(echo $service_account_client_email | cut -d ":" -f2 | cut -d "@" -f1)
service_account_id=(${service_account_id//[\"]/})

active_access_token=$(gcloud auth application-default print-access-token)

service_roles="$(gcloud projects get-iam-policy $gcloud_project_id  \
--flatten="bindings[].members" \
--format="table(bindings.role)" \
--filter="-bindings.members:deleted bindings.members:$service_account_id" | grep "roles/")"

service_roles=$(echo $service_roles | tr ' ' ';')

runtime_config_file=$(find ~+ -type f -name .runtimeconfig.json | head -1)

web_app_id="$(firebase apps:sdkconfig WEB)"
if [[ "$web_app_id" == *"Error:"* ]]; then
    web_app_id="No Web Apps Found"
else
    web_app_id=${web_app_id##*appId\":}
    web_app_id=$(echo $web_app_id | cut -d "," -f1)
    web_app_id=(${web_app_id//[\"]/})
fi

echo
echo "*************** ACTIVE CONFIGURATION ***************"
echo
echo "Logged in Google Account: $google_account"
echo
echo ---------------------GOOGLE CLOUD
echo "GCloud Project ID        : $gcloud_project_id"
echo
echo "Service Account ID       : $service_account_id ($service_account_client_email)"
echo
echo "Service Account Key File : $GOOGLE_APPLICATION_CREDENTIALS"
echo
echo "Service Account Roles    : $service_roles"
#echo "Access token for current Application Default Credentials: $active_access_token"
echo
echo ----------------------FIREBASE
echo "Firebase Project ID      : $firebase_project_id"
echo
echo "Firebase Web App ID      : $web_app_id"
echo
echo "Runtime Configuration    : $runtime_config_file"
echo 
echo "****************************************************"
echo
echo